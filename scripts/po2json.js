const fs = require('fs')
const util = require('util')
const uglify = require("uglify-js")
const rmdir = require('rmdir')
const replaceFile = require('replace-in-file')
const transformer = require('react-intl-po/lib/filterPOAndWriteTranslateSync')

// Function
const generateLocaleFiles = (locales) => {
    locales.forEach(locale => {
        const file = util.format('translations/locale/%s.json', locale)
        const intlFile = util.format('translations/react-intl/%s.js', locale)

        // Check Files
        if (!fs.existsSync(file)) {
            console.warn('\x1b[33mWarning: File %s for locale %s not exists\x1b[0m', file, locale)
            return
        }

        if (!fs.existsSync(intlFile)) {
            console.warn('\x1b[33mWarning: File %s for locale %s not exists\x1b[0m', file, locale)
            return
        }

        const filesOptions = { encoding: 'utf8' }

        // Get and construct React-intl store messages
        const messages = fs.readFileSync(file, filesOptions).toString()
        const windowLocale = util.format(
            "window.__LOCALE={\"locale\": \"%s\", \"messages\": %s}",
            locale,
            messages.replace(/'/g, "\\'")
        )
        const localeBuildFile = util.format('translations/build/%s.js', locale)
        fs.writeFile(localeBuildFile, windowLocale, filesOptions, err => {
            if (err) throw err;

            const uglified = uglify.minify([intlFile, localeBuildFile])
            fs.writeFile(localeBuildFile, uglified.code, err => {
                if (err) throw err;

                console.log('Finish: %s locale', locale)
            })
        })
    })
}

const clearFilesAndTransform = (filePath) => {
    const options = {
        files: filePath,

        //Replacement to make (string or regex)
        from: /\n#, fuzzy/g,
        to: ''
    }

    replaceFile.sync(options)

    transformer(filePath, {
        messagesPattern: 'translations/messages/**/*.json',
        output: 'translations/locale'
    })
}

// Get Params
let locales = []

if ( process.argv[2] && typeof process.argv[2] === 'string' ) {
    locales = process.argv[2].split(',')
} else {
    throw new Error("First parameter must be a string with locales. Ex: es,fr")
}

// Clear Build dir
rmdir('translations/build', (err, dirs, files) => {
    // Create Build Folder
    fs.mkdirSync('translations/build')

    // Transform po files to JSON
    clearFilesAndTransform('translations/po/*.po')

    // Generate finally locale files
    generateLocaleFiles(locales)
})
