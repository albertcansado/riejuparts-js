import { schema as Schema } from 'normalizr'

// Causes
const causeSchema = new Schema.Entity('causes')

// Warranty
const linesSchema = new Schema.Entity('lines')
const attachmentsSchema = new Schema.Entity('attachments')
const warrantySchema = new Schema.Entity('warranties', {
    lines: [linesSchema],
    attachments: [attachmentsSchema]
})

const historySchema = new Schema.Entity('histories')

const historyAPISchema = new Schema.Object({
    histories: new Schema.Array(historySchema)
})

const Schemas = {
    WARRANTY: warrantySchema,
    LINE: linesSchema,
    CAUSE: causeSchema,
    CAUSES: [causeSchema],
    HISTORIES: [historySchema],
    HISTORYAPI: historyAPISchema,
    NOPARSE: 'NOPARSE'
}

export default Schemas
