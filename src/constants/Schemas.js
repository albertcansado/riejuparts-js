import { schema as Schema } from 'normalizr'

const categorySchema = new Schema.Entity('categories', {}, { idAttribute: 'alias' })
const cardsSchema = new Schema.Entity('cards')

const enginesSchema = new Schema.Entity('engines', {
    cards: [cardsSchema]
})

const versionSchema = new Schema.Entity('versions', {
    cards: [cardsSchema],
    engines: [enginesSchema]
})
const modelSchema = new Schema.Entity('models', {
    versions: [versionSchema],
    cards: [cardsSchema]
})

const orderLineSchema = new Schema.Entity('lines')
const orderExtraSchema = new Schema.Entity('extras')
const orderSchema = new Schema.Entity('orders', {
    'orders_lines': [orderLineSchema],
    'orders_extras': [orderExtraSchema]
})


// Products
const productsSchema = new Schema.Entity('products')

const Schemas = {
    CATEGORY: categorySchema,
    CATEGORIES: [categorySchema],
    VERSION: versionSchema,
    VERSIONS: [versionSchema],
    MODEL: modelSchema,
    MODELS: [modelSchema],
    CARD: cardsSchema,
    CARDS: [cardsSchema],
    ORDER: orderSchema,
    ORDERS: [orderSchema],
    ORDERLINE: orderLineSchema,
    ORDEREXTRA: orderExtraSchema,
    PRODUCTS: [productsSchema],
    NOPARSE: 'NOPARSE'
}

export default Schemas
