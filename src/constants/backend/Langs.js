const langs = [
    {
        locale: 'es_ES',
        label: 'Español'
    },
    {
        locale: 'fr_FR',
        label: 'Français'
    },
    {
        locale: 'en_US',
        label: 'English'
    },
    {
        locale: 'it_IT',
        label: 'Italiano'
    },
    /*{
        locale: 'de_DE',
        label: 'Deutsch'
    }*/
]

export default langs
