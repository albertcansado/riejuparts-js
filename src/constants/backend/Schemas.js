import { schema as Schema } from 'normalizr'

// Parts
const partSchema = new Schema.Entity('parts')

// Cards
const cardModSchema = new Schema.Entity('modifications', {
    part: partSchema
})

const cardPartSchema = new Schema.Entity('cards_parts', {
    part: partSchema
})

const cardSchema = new Schema.Entity('cards', {
    cards_parts: [cardPartSchema]
})

// Versions
const versionSchema = new Schema.Entity('versions', {
    cards_mods: [cardModSchema]
})

// Engines
const engineSchema = new Schema.Entity('engines')

// Categories
const categorySchema = new Schema.Entity('categories')

// Entity
const entitySchema = new Schema.Entity('items', {
    cards: [cardSchema],
    versions: [versionSchema],
    engines: [engineSchema],
    category: categorySchema
})

// const lineSchema = new Schema.Entity('cardLine')

const Schemas = {
    ENTITY: entitySchema,
    CARD: cardSchema,
    VERSION: versionSchema,
    NOPARSE: 'NOPARSE'
}

export default Schemas