import React from 'react'
import { render } from 'react-dom'

import './scss/base.scss'

import Root from './container/Root'
import RootCart from './container/RootCart'
import RootCartBtn from './container/RootCartBtn'
import configureStore from './store/configureStore'
import RootProducts from './container/RootProducts'

const store = configureStore()

// Catalog
const rootCatalog = document.getElementById('root')
if (rootCatalog) {
    render(
        <Root store={store} />,
        rootCatalog
    )
}

// Cart Button
const rootBarBtns = document.getElementById('cart-btn-root')
if (rootBarBtns) {
    render(
        <RootCartBtn store={store} />,
        rootBarBtns
    )
}

// Cart Container
const rootCart = document.getElementById('cart-root')
if (rootCart) {
    render(
        <RootCart store={store} />,
        rootCart
    )
}

// Products/Equipacion
const rootProducts = document.getElementById('root-products')
if (rootProducts) {
    render(
        <RootProducts store={store} />,
        rootProducts
    )
}
