/**
 * Este archivo no se usa en la aplicacion.
 * Solo sirve para que babel-intl-po extraiga los mensajes
 */
import { defineMessages } from 'react-intl'

defineMessages({
    "app.clear": {
        id: 'app.clear',
        defaultMessage: "Clear"
    },
    "app.yes": {
        id: "app.yes",
        defaultMessage: "Yes"
    },
    "app.no": {
        id: "app.no",
        defaultMessage: "No"
    },

    "app.display.title": {
        id: 'app.display.title',
        defaultMessage: "Get a display"
    },
    "app.display.text": {
        id: 'app.display.text',
        defaultMessage: "Para descargar el display personalizado introduce el precio de venta del modelo, además puedes introducir un breve comentario que aparecera en el pie del display (por ejemplo: {example})"
    },
    "app.display.example": {
        id: 'app.display.example',
        defaultMessage: "Matriculación Incluída"
    },
    "app.display.price": {
        id: 'app.display.price',
        defaultMessage: "Introduce el precio del modelo (ej. 1500.20)"
    },
    "app.display.textarea": {
        id: 'app.display.textarea',
        defaultMessage: "Puedes añadir un texto breve"
    },
    "app.display.alert.ok": {
        id: 'app.display.alert.ok',
        defaultMessage: "Obtener"
    },

    "app.alert.confirm": {
        id: 'app.alert.confirm',
        defaultMessage: 'Yes, please!'
    },
    "app.alert.cancel": {
        id: 'app.alert.cancel',
        defaultMessage: 'Cancel'
    },

    "warranty.newAlert.title": {
        id: 'warranty.newAlert.title',
        defaultMessage: "warranty.newAlert.title"
    },
    "warranty.newAlert.placeholder": {
        id: 'warranty.newAlert.placeholder',
        defaultMessage: "warranty.newAlert.placeholder"
    },
    "warranty.newAlert.required": {
        id: 'warranty.newAlert.required',
        defaultMessage: "warranty.newAlert.required"
    },
    "warranty.newAlert.notavailableTitle": {
        id: "warranty.newAlert.notavailableTitle",
        defaultMessage: "warranty.newAlert.notavailableTitle"
    },
    "warranty.newAlert.availableTitle": {
        id: "warranty.newAlert.availableTitle",
        defaultMessage: "warranty.newAlert.availableTitle"
    },
    "warranty.newAlert.availableConfirm": {
        id: "warranty.newAlert.availableConfirm",
        defaultMessage: "warranty.newAlert.availableConfirm"
    },

    // Backend
    "validate_order.title": {
        id: 'validate_order.title',
        defaultMessage: 'validate_order.title'
    },

    "validate_order.manage.title": {
        id: 'validate_order.manage.title',
        defaultMessage: 'validate_order.manage.title'
    },
    "validate_order.manage.text": {
        id: 'validate_order.manage.text',
        defaultMessage: 'validate_order.manage.text'
    },

    "validate_order.factory.title": {
        id: 'validate_order.factory.title',
        defaultMessage: 'validate_order.factory.title'
    },
    "validate_order.factory.text": {
        id: 'validate_order.factory.text',
        defaultMessage: 'validate_order.factory.text'
    },

    "change_warrantyend.title": {
        id: "change_warrantyend.title",
        defaultMessage: "change_warrantyend.title"
    },
    "change_warrantyend.text": {
        id: "change_warrantyend.text",
        defaultMessage: "change_warrantyend.text"
    },
    "change_warrantyend.error": {
        id: "change_warrantyend.error",
        defaultMessage: "change_warrantyend.error"
    },
})

// Icon Select (Form/SelectIcon.jsx)
defineMessages({
    selectIconNoResults: {
        id: 'selectIcon.noResults',
        defaultMessage: "Not has results"
    },
    selectIconPlaceholder: {
        id: 'selectIcon.placeholder',
        defaultMessage: "Select and icon card"
    },
    selectIconLoading: {
        id: 'selectIcon.loading',
        defaultMessage: "Loading..."
    }
})
