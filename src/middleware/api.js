import { normalize } from 'normalizr'
import fetch from 'isomorphic-fetch'
import { isString, isUndefined, isObject } from 'lodash'

export const API_ROOT = window.config.URL

export const ASSETS_ROOT = window.config.ASSETSURL

const defaultHeaders = (process.env.NODE_ENV === 'development')
    ? {
        'X-Requested-With': 'XMLHttpRequest',
        'x-dev': 'albert'
    }
    : {
        'X-Requested-With': 'XMLHttpRequest',
    }

// Default Schema
const NOPARSE = 'NOPARSE'

// API FUNCTION
export const fetchRequest = (url = '', options = {}) => {
    options = Object.assign({
        method: 'GET'
    }, options)

    const { method } = options
    let { headers, body } = options

    headers = Object.assign({}, defaultHeaders, headers)
    if (isObject(body) && body.constructor.name === 'Object') {
        body = JSON.stringify(body)
        headers['Accept'] = 'application/json'
        headers['Content-Type'] = 'application/json'
    }

    const fullUrl = (url.indexOf(API_ROOT) === -1) ? API_ROOT + url : url

    return fetch(fullUrl, { headers, method, body, credentials: 'same-origin' })
        .then(response =>
            response.json().then(json => ({ json, response }))
        )
}

export const callApi = (url = '', schema = NOPARSE, options = {}) => {
    const { method } = options
    let { headers, body } = options

    headers = Object.assign({}, defaultHeaders, headers)

    if (isObject(body) && body.constructor.name === 'Object') {
        body = JSON.stringify(body)
        headers['Accept'] = 'application/json'
        headers['Content-Type'] = 'application/json'
    }

    const fullUrl = (url.indexOf(API_ROOT) === -1) ? API_ROOT + url : url

    return fetch(fullUrl, { headers, method, body, credentials: 'same-origin' })
        .then(response =>
            response.json().then(json => ({ json, response }))
        ).then(({ json, response }) => {
            if (!response.ok || !json.success) {
                return Promise.reject(json)
            }

            return (schema === NOPARSE) ? json.data : normalize(json.data, schema)
        })
}

// Action key that carries API call info interpreted by this Redux middleware.
export const CALL_API = Symbol('Call API')

// Redux middleware to use API
export default store => next => action => {
    const callAPI = action[CALL_API]
    if (isUndefined(callAPI)) {
        return next(action)
    }

    const { url, schema, types } = callAPI

    // Check URL
    if (!isString(url)) {
        throw new Error('Specify a string endpoint URL.')
    }

    // Check Schema
    /*if (!schema) {
        throw new Error('Specify one of the exported Schemas.')
    }*/

    // Check types
    if (!Array.isArray(types) || types.length !== 3) {
        throw new Error('Expected an array of three action types.')
    }

    if (!types.every(type => isString(type) || isObject(type))) {
        throw new Error('Expected action types to be strings.')
    }

    // Check if method is undefined
    if (!callAPI.hasOwnProperty('method') || !callAPI.method) {
        callAPI.method = 'GET'
    }

    // Attach data to action
    const actionWith = (data, extra) => {
        let finalAction = Object.assign({}, action, data)
        delete finalAction[CALL_API]

        if (isString(extra)) {
            finalAction['type'] = extra
        } else {
            finalAction = Object.assign({}, extra, finalAction)
        }

        return finalAction
    }

    // Extract types
    const [ requestType, successType, failureType ] = types

    // FETCH_XXX (request)
    next(actionWith({}, requestType))

    return callApi(url, schema || NOPARSE, callAPI).then(
        response => next(actionWith({
            payload: response
        }, successType)),
        error => next(actionWith({
            error: true,
            payload: error.error || 'Something bad happened'
        }, failureType))
    )
}
