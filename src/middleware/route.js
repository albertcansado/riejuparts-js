export const ROUTE_PATH = {
    prefix: 'catalog'
}

export const build = ({ category, version, card }) => {
    let pathElements = []

    if (ROUTE_PATH.prefix) {
        pathElements.push(ROUTE_PATH.prefix)
    }

    if (category) {
        pathElements.push(category)
    }

    if (version) {
        pathElements.push(version)
    }

    if (card) {
        pathElements.push(card)
    }

    return pathElements.join('/')
}
