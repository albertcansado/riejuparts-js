import 'scss/backend.scss'
import { on } from 'delegated-events'
import swal from 'sweetalert2'
import { u } from 'umbrellajs'
import Modal from './lib/modal'
import Dropdown from './lib/dropdown'
import flatpickr from 'flatpickr'
import idx from 'idx'
require('lib/webfontload')

import { fetchRequest } from './middleware/api'

const setAttributes = (el, options) => {
    Object.keys(options).forEach(attr => el.setAttribute(attr, options[attr]) )

    return el
}

const getCalendarInstance = (el) => {
    return flatpickr(el, {
        locale: window.flatpickr ? window.flatpickr.l10ns[window.__LOCALE.locale] : 'en',
        onReady: function (dateObj, dateStr, instance) {
            if (!instance.calendarContainer.querySelector('.flatpickr-clear')) {
                const div = document.createElement('div')
                div.className = 'flatpickr-clear'
                div.innerText = idx(window.__LOCALE, _ => _.messages['app.clear'])
                div.onclick = ev => {
                    instance.clear()
                    instance.close()
                }

                instance.calendarContainer.append(div)
            }
        }
    })
}

const postForm = (url, options) => {
    if (!url) {
        return
    }

    const form = setAttributes(
        document.createElement('form'),
        {
            method: 'POST',
            action: url
        }
    )
    form.style.visibility = 'hidden'

    if (options.hasOwnProperty('data')) {
        Object.keys(options.data).forEach(d => {
            form.appendChild(
                setAttributes(
                    document.createElement('input'),
                    {
                        type: 'hidden',
                        name: d,
                        value: options.data[d]
                    }
                )
            )
        })
    }

    // forms cannot be submitted outside of body
    document.body.appendChild(form);
    form.submit();
}

new Dropdown('[data-dropdown]')

const getStyle = (element, property) => {
    const el = window.getComputedStyle(element)
    return el.getPropertyValue(property)
}

const main = document.querySelector('.global-main')
const bartop = document.querySelector('.bartop')
const content = document.querySelector('.content')

const calculateHeight = () => {
    if (!main || !content || !bartop) {
        return;
    }

    const mainClient = main.getBoundingClientRect()
    const bartopHeight = bartop.getBoundingClientRect().height

    content.style.height = (mainClient.height - bartopHeight - Number.parseInt(getStyle(bartop, 'margin-top'), 10) - Number.parseInt(getStyle(content, 'margin-bottom'), 10)) + 'px'
}

window.onresize = ev => calculateHeight()

calculateHeight()

// Alerts
const alertCallbacks = {
    deleteOrder: () => {
        window.localStorage.removeItem('order_id')
    }
}

const triggerAlert = ev => {
    ev.preventDefault()

    const target = ev.currentTarget

    const callback = target.getAttribute('data-alert-fn')
    const swType = target.getAttribute('data-alert-type') || 'warning'
    const swTitle = target.getAttribute('data-alert-title') || ''
    const swText = target.getAttribute('data-alert') || ''
    const swConfirm = target.getAttribute('data-alert-confirm') || idx(window.__LOCALE, _ => _.messages["app.alert.confirm"])
    const swCancel = target.getAttribute('data-alert-cancel') || idx(window.__LOCALE, _ => _.messages["app.alert.cancel"])
    const swAsync = Number.parseInt(target.getAttribute('data-alert-async'), 10) || false

    const stopProp = target.getAttribute('data-stop') || false

    if (stopProp) {
        ev.stopPropagation()
    }

    swal({
        title: swTitle,
        text: swText,
        type: swType,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonColor: "#30b19b",
        confirmButtonText: swConfirm,
        cancelButtonText: swCancel
    }).then(
        () => {
            if (alertCallbacks.hasOwnProperty(callback)) {
                alertCallbacks[callback]()
            }

            if (target.hasAttribute('data-alert-form')) {
                document[target.getAttribute('data-alert-form')].submit()
            } else if (target.hasAttribute('href')) {
                if (swAsync) {
                    fetchRequest(target.getAttribute('href'))
                } else {
                    window.location.href = target.getAttribute('href')
                }
            }
        }
    ).catch(swal.noop)
}

on('click', '[data-alert]', triggerAlert)

const checkSelectValue = (target) => {
    u(target)[target.value === '' ? 'addClass' : 'removeClass']('is-empty')
}

on('change', 'select.input__field', ev => checkSelectValue(ev.currentTarget))

// Models
on('click', '.js-models-create', ev => {
    ev.preventDefault()

    const target = ev.currentTarget
    swal({
        title: 'Añadir un modelo',
        html: '<input type="text" class="input__field" id="mc-name" placeholder="Introduce el nombre del modelo" autocomplete="off" /><input type="number" class="input__field u-mgt--15 u-mgb--15" id="mc-year" placeholder="Introduce el año del modelo" autocomplete="off" />',
        preConfirm: (val) => {
            const name = document.querySelector('#mc-name').value
            const year = document.querySelector('#mc-year').value

            return new Promise((resolve, reject) => {
                if (name !== '' && year !== '') {
                    resolve([name, year])
                } else {
                    reject('Rellene todos los campos!')
                }
            })
        },
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonColor: "#30b19b",
        confirmButtonText: 'Crear',
        cancelButtonText: idx(window.__LOCALE, _ => _.messages["app.alert.cancel"]),
    }).then(result => {
        postForm(target.getAttribute('href'), {
            data: {
                name: result[0],
                year: result[1]
            }
        })
    }).catch(swal.noop)
})


// Modal
const modal = new Modal()
on('click', '[data-modal]', (ev) => {
    if (!ev.target.hasAttribute('data-modal') && u(ev.target).closest('.js-nomodal').length) {
        return
    }

    ev.preventDefault()
    //ev.stopPropagation()

    const url = ev.currentTarget.getAttribute('data-url') || ev.currentTarget.getAttribute('href')
    if (url) {
        modal.openAsync(url)
    }
})

// Calendar
//
getCalendarInstance('[data-datetime]')

on('click', '.js-check-all', ev => {
    const checked = ev.currentTarget.checked
    u('[data-check]').each(node => {
        node.checked = checked
    })
})

on('click', '.js-check-leaf', ev => {
    const children = ev.currentTarget.getAttribute('data-children')
    if (children) {
        u(children + ' [type="checkbox"]').each(node => node.checked = ev.currentTarget.checked)
    }
})


// Order
// Validate
on('click', '.js-accept-order', ev => {
    const target = ev.currentTarget

    if (!target.hasAttribute('data-validate')) {
        ev.preventDefault();

        let html = []
        html.push('<p class="txt-left txt--medium"><strong>');
        html.push(idx(window.__LOCALE, _ => _.messages["validate_order.manage.title"]))
        html.push('</strong>:<br />')
        html.push(idx(window.__LOCALE, _ => _.messages["validate_order.manage.text"]))
        html.push('.</p><p class="txt-left txt--medium u-mgt--10"><strong>')
        html.push(idx(window.__LOCALE, _ => _.messages["validate_order.factory.title"]))
        html.push('</strong>:<br />')
        html.push(idx(window.__LOCALE, _ => _.messages["validate_order.factory.text"]))
        html.push('</p>')

        swal({
            title: idx(window.__LOCALE, _ => _.messages["validate_order.title"]) || '',
            html: html.join(''),
            type: 'info',
            showCancelButton: true,
            showCloseButton: true,
            confirmButtonColor: "#30b19b",
            focusConfirm: false,
            cancelButtonColor: "#3273dc",
            confirmButtonText: idx(window.__LOCALE, _ => _.messages["validate_order.manage.title"]),
            cancelButtonText: idx(window.__LOCALE, _ => _.messages["validate_order.factory.title"])
        }).then(
            () => {
                return Promise.resolve('importer');
            }, dismiss => {
                if (dismiss === 'cancel') {
                    return Promise.resolve('factory');
                }

                return Promise.reject()
            }
        ).then(result => {
            target.appendChild(
                setAttributes(
                    document.createElement('input'),
                    {
                        name: 'action',
                        type: 'hidden',
                        'class': 'is-hidden',
                        value: result
                    }
                )
            )
            target.setAttribute('data-validate', '1')
            target.submit()
        })
        .catch(swal.noop)
    }
})

on('click', '.js-ch-frame-end', ev => {
    ev.preventDefault()
    const target = ev.currentTarget
    let calendarInstance = null

    swal({
        title: idx(window.__LOCALE, _ => _.messages["change_warrantyend.title"]) || '',
        html:
            '<p class="txt--medium">' + idx(window.__LOCALE, _ => _.messages["change_warrantyend.text"]) + ':</p>' +
            '<input type="text" id="ch-frame-end" class="input__field u-mgt--5 u-mgb--5"/>',
        showCancelButton: true,
        showCloseButton: true,
        confirmButtonColor: "#30b19b",
        focusConfirm: false,
        confirmButtonText: idx(window.__LOCALE, _ => _.messages["warranty.header.send"]),
        cancelButtonText: idx(window.__LOCALE, _ => _.messages["app.alert.cancel"]),
        preConfirm: () => {
            return new Promise((resolve, reject) => {
                if (!calendarInstance.value) {
                    return reject(idx(window.__LOCALE, _ => _.messages["change_warrantyend.error"]))
                } else {
                    return resolve(calendarInstance.value)
                }
            })
        }
    }).then(result => {
        postForm(target.getAttribute('href'), {
            data: {
                date: result
            }
        })
    }).catch(swal.noop)

    calendarInstance = document.querySelector('#ch-frame-end')
    getCalendarInstance(calendarInstance)
})
