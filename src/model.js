import React from 'react'
import { render } from 'react-dom'

import RootModel from './container/Backend/RootModel'
import configureStore from './store/editorStore'

const store = configureStore()

const rootModel = document.getElementById('root-models')
if (rootModel) {
    render(
        <RootModel store={store} />,
        rootModel
    )
}
