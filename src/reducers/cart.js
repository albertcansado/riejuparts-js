import { reduce as _reduce } from 'lodash'
import * as actions from '../constants/ActionCarts'
import swal from 'sweetalert2'

const initState = {
    id: null,
    count: 0,
    totalLines: 0,
    totalExtras: 0,
    isOpen: false
}

export default function cart(state = initState, action) {
    switch(action.type) {
        case actions.TOGGLE_CART:
            return {
                ...state,
                isOpen: !state.isOpen
            }

        case actions.CART_SUCCESS:
            if (action.payload.result) {
                window.localStorage.setItem('order_id', action.payload.result)
            }

            return {
                ...state,
                id: action.payload.result,
                count: _reduce(action.payload.entities.lines, (result, line) => {
                    return result + line.qty
                }, 0)
            }

        case actions.CART_CHANGE:
            return {
                ...state,
                id: action.payload.id,
                count: 0,
                totalLines: 0,
                totalExtras: 0
            }

        // ADD PART
        case actions.CART_ADD_LINE_REQUEST:
            return {
                ...state,
                count: state.count + action.payload.qty
            }

        // UPDATE
        case actions.CART_UPDATE_LINE_REQUEST:
            return {
                ...state,
                count: state.count + (action.payload.nextQty - action.payload.prevQty)
            }

        // DELETE
        case actions.CART_DELETE_LINE_REQUEST:
            return {
                ...state,
                count: state.count - action.payload.qty
            }

        case actions.CART_ADD_LINE_ERROR:
        case actions.CART_UPDATE_LINE_ERROR:
            return {
                ...state,
                count: state.count - action.meta.qty
            }

        case actions.CART_DELETE_LINE_ERROR:
            return {
                ...state,
                count: state.count + action.meta.data.qty
            }

        case actions.CART_ERROR:
            swal({
                title: 'Error on load Cart',
                'text': action.payload.message,
                'type': 'error'
            })

            if (action.meta) {
                window.localStorage.setItem('order_id', action.meta)
            } else {
                window.localStorage.removeItem('order_id')

                return {
                    ...state,
                    id: null
                }
            }
        default: // eslint-disable-line no-fallthrough
            return state
    }
}

export const getCartCount = ({ cart }) => {
    return cart.count
}

export const isCartOpen = ({ cart }) => {
    return cart.isOpen
}
