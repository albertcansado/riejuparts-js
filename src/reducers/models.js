// import { merge } from 'lodash'

import * as actions from '../constants/ActionModels'

const initState = {}

export default function models(state = initState, action) {
    switch(action.type) {
        case actions.RECEIVE_MODELS:
            return action.payload
        default:
            return state
    }
}
