import { merge, map as _map, filter as _filter, omit as _omit } from 'lodash'

import * as modelsActions from '../constants/ActionModels'
import * as cartsActions from '../constants/ActionCarts'
import * as productsActions from '../constants/ActionProducts'

// import { isCategoriesFetched } from '../reducers/helpers'

const arrayReplace = (list = [], key = '', withValue = null) => {
    const index = list.indexOf(key)
    if (!~index) {
        return list
    }

    return [
        ...list.slice(0, index),
        withValue,
        ...list.slice(index + 1)
    ]
}

const initState = {
    categories: {},
    models: {},
    versions: {},
    cards: {},
    lines: {},
    orders: {},
    products: {}
}

export default function entities(state = initState, action) {
    switch(action.type) {
        case modelsActions.CATEGORIES_SUCCESS:
        case modelsActions.MODELS_SUCCESS:
        case modelsActions.VERSIONS_SUCCESS:
        case cartsActions.CART_SUCCESS:
        case productsActions.PRODUCTS_SUCCESS:
        case cartsActions.CART_CSV_SUCCESS:
            if (action.type === modelsActions.MODELS_SUCCESS/* && action.payload.entities.models*/) {
                action.payload.entities['categories'] = {
                    [action.meta]: {
                        models: action.payload.result,
                        isFetched: true
                    }
                }
            } else if (action.type === modelsActions.VERSIONS_SUCCESS) {
                action.payload.entities.versions[action.payload.result].isFetched = true
            }

            return merge({}, state, action.payload.entities)

        //
        // VERSIONS
        case modelsActions.VERSIONS_FETCH:
            return {
                ...state,
                'versions': {
                    ...state.versions,
                    [action.payload]: {
                        ...state.versions[action.payload],
                        isFetched: false
                    }
                }
            }

        //
        // MODELS
        case modelsActions.MODELS_FETCH:
            return {
                ...state,
                'categories': {
                    ...state.categories,
                    [action.payload]: {
                        ...state.categories[action.payload],
                        isFetched: false
                    }
                }
            }

        //
        // CARDS
        case modelsActions.CARDS_FETCH:
            return {
                ...state,
                'cards': {
                    ...state.cards,
                    [action.payload]: {
                        ...state.cards[action.payload],
                        isFetched: false
                    }
                }
            }
        case modelsActions.CARDS_SUCCESS:
            return {
                ...state,
                'cards': {
                    ...state.cards,
                    [action.meta]: {
                        ...state.cards[action.meta],
                        items: action.payload,
                        isFetched: true
                    }
                }
            }

        //
        // CART
        case cartsActions.CART_UPDATE_SUCCESS:
            if (!action.payload.hasOwnProperty('id')) {
                return state
            }

            var order = state.orders[action.payload.id]
            return {
                ...state,
                orders: {
                    ...state.orders,
                    [action.payload.id]: Object.assign({}, order, action.payload)
                }
            }

        case cartsActions.CART_CHANGE:
            return {
                ...state,
                orders: {
                    ...state.orders,
                    [action.payload.id]: action.payload
                }
            }

        // Add Line
        case cartsActions.CART_ADD_LINE_REQUEST:
            var order = state.orders[action.payload.order_id] // eslint-disable-line no-redeclare
            return {
                ...state,
                lines: {
                    ...state.lines,
                    [action.payload.id]: action.payload
                },
                orders: {
                    ...state.orders,
                    [action.payload.order_id]: {
                        ...order,
                        lines: order.lines + 1,
                        orders_lines: [
                            ...order['orders_lines'],
                            action.payload.id
                        ]
                    }
                }
            }

        case cartsActions.CART_ADD_LINE_SUCCESS:
            var line = state.lines[action.meta] // eslint-disable-line no-redeclare
            return {
                ...state,
                lines: {
                    ...state.lines,
                    [action.meta]: Object.assign({}, line, {
                        internal_id: action.payload.id,
                        position: action.payload.position
                    })
                }
            }

        case cartsActions.CART_ADD_LINE_ERROR:
            var order = state.orders[action.meta.order_id] // eslint-disable-line no-redeclare
            return {
                ...state,
                orders: {
                    ...state.lines,
                    [action.meta.order_id]: {
                        ...order,
                        lines: order.lines - 1,
                        orders_lines: order.orders_lines.filter(it => it !== action.meta.id)
                    }
                }
            }

        // Update Line
        case cartsActions.CART_UPDATE_LINE_REQUEST:
            var line = state.lines[action.payload.id] // eslint-disable-line no-redeclare
            return {
                ...state,
                lines: {
                    ...state.lines,
                    [action.payload.id]: {
                        ...line,
                        qty: action.payload.nextQty
                    }
                }
            }
        case cartsActions.CART_UPDATE_LINE_ERROR:
            var line = state.lines[action.meta.id] // eslint-disable-line no-redeclare
            return {
                ...state,
                lines: {
                    ...state.lines,
                    [action.meta.id]: {
                        ...line,
                        qty: action.meta.qty
                    }
                }
            }

        // Delete
        case cartsActions.CART_DELETE_LINE_REQUEST:
            var order = state.orders[action.payload.order_id] // eslint-disable-line no-redeclare
            return {
                ...state,
                orders: {
                    ...state.orders,
                    [order.id]: {
                        ...order,
                        lines: order.lines - 1,
                        orders_lines: order.orders_lines.filter(it => it !== action.payload.id)
                    }
                }
            }

        case cartsActions.CART_DELETE_LINE_ERROR:
            var order = state.orders[action.meta.order_id] // eslint-disable-line no-redeclare
            return {
                ...state,
                orders: {
                    ...state.orders,
                    [order.id]: Object.assign({}, order, action.meta.data)
                }
            }

        //
        // EXTRA
        case cartsActions.CART_ADD_EXTRA_REQUEST:
        case cartsActions.CART_DELETE_EXTRA_ERROR:
            var item = (action.type === cartsActions.CART_ADD_EXTRA_REQUEST) ? action.payload : action.meta
            var order = state.orders[item.order_id] // eslint-disable-line no-redeclare
            return {
                ...state,
                extras: {
                    ...state.extras,
                    [item.id]: item
                },
                orders: {
                    ...state.orders,
                    [item.order_id]: {
                        ...order,
                        orders_extras: [
                            ...order['orders_extras'],
                            item.id
                        ]
                    }
                }
            }
        case cartsActions.CART_ADD_EXTRA_SUCCESS:
            var item = state.extras[action.meta] // eslint-disable-line no-redeclare
            var order = state.orders[action.payload.order_id] // eslint-disable-line no-redeclare
            return {
                ...state,
                extras: {
                    ...state.extras,
                    [action.payload.id]: {
                        ...item,
                        ...action.payload
                    },
                    [action.meta]: null
                },
                orders: {
                    ...state.orders,
                    [action.payload.order_id]: {
                        ...order,
                        orders_extras: arrayReplace(order.orders_extras, action.meta, action.payload.id)
                    }
                }
            }
        case cartsActions.CART_ADD_EXTRA_ERROR:
        case cartsActions.CART_DELETE_EXTRA_REQUEST:
            var item = (action.type === cartsActions.CART_DELETE_EXTRA_REQUEST) ? action.payload : action.meta // eslint-disable-line no-redeclare
            var order = state.orders[item.order_id] // eslint-disable-line no-redeclare

            return {
                ...state,
                extras: _omit(state.extras, item.id),
                orders: {
                    ...state.orders,
                    [order.id]: {
                        ...order,
                        orders_extras: order.orders_extras.filter(it => it !== item.id)
                    }
                }
            }
        default:
            return state
    }
}

// Categories
export const getCategoryEntity = ({ entities }, name = '') => {
    return entities.categories[name] || {}
}

export const getCategories = ({ entities }) => {
    return entities.categories
}

// Cards
export const isCardFetched = ({ entities }, alias) => {
    const c = _filter(entities.cards, { alias })

    return c.isFetched || false
}

export const getCardEntity = (state, id) => {
    if (!state.entities.cards.hasOwnProperty(id)) {
        return {}
    }

    return state.entities.cards[id]
}


// Engines
export const getEngineEntity = (state, id) => {
    if (!state.entities.engines.hasOwnProperty(id)) {
        return {}
    }

    const e = state.entities.engines[id]

    return {
        ...e,
        cards: e.hasOwnProperty('cards') ? e.cards.map(cardId => getCardEntity(state, cardId)) : [],
    }
}


// Versions
export const isVersionFetched = ({ entities }, id) => {
    return entities.versions[id].isFetched || false
}

export const getVersionEntity = (state, id, fetchChildren = true) => {
    if (!state.entities.versions.hasOwnProperty(id)) {
        return {}
    }

    const v = state.entities.versions[id]

    if (!fetchChildren) {
        return v
    }

    return {
        ...v,
        cards: v.hasOwnProperty('cards') ? v.cards.map(cardId => getCardEntity(state, cardId)) : [],
        engines: v.hasOwnProperty('engines') ? v.engines.map(engineId => getEngineEntity(state, engineId)) : []
    }
}


// Models
export const isModelsFetched = (state, name) => {
    return getCategoryEntity(state, name).isFetched || false
}

export const getModelEntity = (state, id) => {
    if (!state.entities.models.hasOwnProperty(id)) {
        return {}
    }

    const model = state.entities.models[id]

    return {
        ...model,
        versions: model.versions.map(verId => {
            return getVersionEntity(state, verId)
        })
    }
}

export const getModels = (state, name) => {
    if (!state.entities.categories.hasOwnProperty(name) || !state.entities.categories[name].hasOwnProperty('models')) {
        return []
    }

    return state.entities.categories[name].models.map(item => {
        return getModelEntity(state, item)
    })

    /*if (!entities.models.hasOwnProperty(name)) {
        return {}
    }

    return _map(entities.models[name], (item => {
        return {
            ...item,
            versions: item['versions'].map(el => {
                return entities.versions[el]
            })
        }
    }))*/
}

// Carts
export const getCartCount = (state) => {
    const { entities, cart } = state

    if (!entities.orders.hasOwnProperty(cart)) {
        return 0;
    }

    return entities.orders[cart].lines
}

export const getCartTotals = ({ entities, cart }) => {
    if (!entities.orders.hasOwnProperty(cart.id)) {
        return {
            lines: 0,
            extras: 0
        }
    }

    const cartEntity = entities.orders[cart.id]
    return {
        lines: cartEntity.orders_lines.reduce(
            (total, it) => {
                return total + (entities.lines[it].part.price * entities.lines[it].qty)
            },
            0
        ),
        extras: cartEntity.orders_extras.reduce(
            (total, it) => total + entities.extras[it].pvp,
            0
        )
    }
}

export const getCart = ({ entities, cart }, includeChildren = true) => {
    if (!entities.orders.hasOwnProperty(cart.id)) {
        return {}
    }

    /*return {
        ...entities.orders[cart.id],
        orders_extras: entities.orders[cart.id].orders_extras.map(l => entities.extras[l])
    }*/

    return entities.orders[cart.id]
}

export const getCartLine = ({ entities }, id) => {
    if (!entities.lines.hasOwnProperty(id)) {
        return {}
    }

    return entities.lines[id]
}

export const getCartExtraLines = ({ entities }, ids) => {
    return ids.map(i => entities.extras[i])
}

/**
 * Devuelve todos los sku de las piezas que hay dentro del pedido
 * @param  object entities
 * @param  object cart
 * @return array
 */
export const getCartParts = ({ entities, cart }) => {
    if (!entities.orders.hasOwnProperty(cart.id)) {
        return {}
    }

    let parts = {}
    entities.orders[cart.id].orders_lines.forEach(line_id => {
        if (!entities.lines.hasOwnProperty(line_id)) {
            return null
        }

        const line = entities.lines[line_id]
        const sku = line['part']['sku']
        parts[sku] = {
            id: line_id,
            qty: line['qty'],
            order_id: cart.id,
            internal_id: line.internal_id || null
        }
    })

    return parts
}


// Products
//
export const getProducts = ({ entities }) => {
    if (!Object.keys(entities.products).length) {
        return []
    }

    return _map(entities.products, it => it)
}
