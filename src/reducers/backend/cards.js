import { merge } from 'lodash'

import * as cardsActions from '../../constants/backend/ActionsCards'
import { ENTITY_SUCCESS } from '../../constants/backend/ActionsEntities'

export default function cards(state = {}, action) {
    switch(action.type) {
        case ENTITY_SUCCESS:
        case cardsActions.CARD_CREATE_SUCCESS:
            return merge(state, action.payload.entities.cards)

        // FETCH
        case cardsActions.CARD_FETCH:
            const card = state[action.meta]
            return {
                ...state,
                [action.meta]: {
                    ...card,
                    isFetching: true
                }
            }

        case cardsActions.CARD_SUCCESS:
            return merge(
                state,
                action.payload.entities.cards,
                {
                    [action.meta]: {
                        isFetched: true,
                        isFetching: false
                    }
                }
            )

        // UPDATE
        case cardsActions.CARD_UPDATE_REQUEST:
            var card = state[action.payload.card_id] // eslint-disable-line no-unused-vars
            return {
                ...state,
                [action.payload.card_id]: Object.assign({}, card, action.payload.data)
            }

        case cardsActions.CARD_UPDATE_ERROR:
            var card_id = action.meta.id
            return {
                ...state,
                [card_id]: Object.assign({}, state[card_id], action.meta.data)
            }

        case cardsActions.CARD_UPLOAD_SUCCESS:
            var card = action.payload // eslint-disable-line no-redeclare
            return {
                ...state,
                [card.id]: Object.assign({}, state[card.id], card)
            }

        // DELETE
        case cardsActions.CARD_DELETE_REQUEST:
            return {
                ...state,
                [action.payload.id]: {
                    ...state[action.payload.id],
                    isDeleted: true
                }
            }

        case cardsActions.CARD_DELETE_ERROR:
            return {
                ...state,
                [action.meta.id]: {
                    ...state[action.meta.id],
                    isDeleted: false
                }
            }

        // ADD CARD PART
        case cardsActions.CARD_LINE_ADD_REQUEST:
            var card = state.cards[action.payload.card_id] // eslint-disable-line no-redeclare

            return {
                ...state,
                [card.id]: {
                    ...card,
                    cards_parts: [
                        ...card.cards_parts,
                        action.payload.id
                    ]
                }
            }

        case cardsActions.CARD_LINE_ADD_ERROR:
            return {
                ...state,
                [action.meta.card_id]: state[action.meta.card_id].cards.filter(cp => cp !== action.meta.line_id)
            }

        default:
            return state
    }
}
