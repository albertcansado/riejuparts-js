import { merge } from 'lodash'

import { CARD_SUCCESS, CARD_LINE_ADD_REQUEST } from '../../constants/backend/ActionsCards'

export default function parts(state = {}, action) {
    switch (action.type) {
        case CARD_SUCCESS:
            return merge(state, action.payload.entities.parts)

        case CARD_LINE_ADD_REQUEST:
            return {
                ...state,
                [action.payload.part.id]: {
                    ...action.payload.part
                }
            }

        default:
            return state
    }
}
