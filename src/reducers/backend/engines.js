import { merge } from 'lodash'

import { ENTITY_SUCCESS } from '../../constants/backend/ActionsEntities'

export default function engines(state = {}, action) {
    switch(action.type) {
        case ENTITY_SUCCESS:
            return merge(state, action.payload.entities.engines)

        default:
            return state
    }
}
