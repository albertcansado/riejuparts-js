import { omit as _omit, merge, forEach as _forEach } from 'lodash'

import * as entitiesActions from '../../constants/backend/ActionsEntities'
import * as versionsActions from '../../constants/backend/ActionsVersions'
import * as cardsActions from '../../constants/backend/ActionsCards'

const initState = {
    items: {},
    cards: {},
    cards_parts: {},
    parts: {},
    engines: {},
    versions: {},
    modifications: {}
}

export default function entities(state = initState, action) {
    switch(action.type) {
        case entitiesActions.ENTITY_SUCCESS:
        case cardsActions.CARD_SUCCESS:
        case cardsActions.CARD_CREATE_SUCCESS:
        case versionsActions.VERSION_FETCH_SUCCESS:
        case versionsActions.VERSION_CREATE_SUCCESS:
            if (action.payload.entities.hasOwnProperty('cards')) {
                _forEach(action.payload.entities.cards, card => {
                    card.isFetched = true
                    card.isFetching = false
                })
            }

            const nextState = merge({}, state, action.payload.entities)
            if (action.hasOwnProperty('meta')) {
                switch (action.type) {
                    case entitiesActions.ENTITY_SUCCESS:
                        if (nextState.items.hasOwnProperty(action.meta)) {
                            nextState.items[action.meta] = Object.assign({}, nextState.items[action.meta], {
                                isFetched: true,
                            })
                        }
                        break;

                    case cardsActions.CARD_SUCCESS:
                        if (nextState.cards.hasOwnProperty(action.meta)) {
                            nextState.cards[action.meta] = Object.assign({}, nextState.cards[action.meta], {
                                isFetched: true,
                                isFetching: false
                            })
                        }
                        break;

                    case cardsActions.CARD_CREATE_SUCCESS:
                        const item = nextState.items.hasOwnProperty(action.meta) ? nextState.items[action.meta] : null
                        if (item) {
                            nextState.items[action.meta] = {
                                ...item,
                                cards: [
                                    ...item.cards,
                                    action.payload.result
                                ]
                            }
                        }
                        break;

                    case versionsActions.VERSION_CREATE_SUCCESS:
                        var item = nextState.items.hasOwnProperty(action.meta) ? nextState.items[action.meta] : null
                        if (item) {
                            nextState.items[action.meta] = {
                                ...item,
                                versions: [
                                    ...item.versions,
                                    action.payload.result
                                ]
                            }
                        }
                        break;

                    default:
                        break;
                }
            }

            if (action.type === versionsActions.VERSION_FETCH_SUCCESS) {
                nextState.versions[action.payload.result] = Object.assign({}, nextState.versions[action.payload.result], {
                    isFetched: true,
                    isFetching: false
                })
            }

            return nextState

        case entitiesActions.ENTITY_UPDATE_REQUEST:
        case entitiesActions.ENTITY_UPDATE_CARDORDER_REQUEST:
            var item = state.items[action.payload.id] // eslint-disable-line no-redeclare
            return {
                ...state,
                items: {
                    ...state.items,
                    [action.payload.id]: Object.assign({}, item, action.payload.data)
                }
            }

        case entitiesActions.ENTITY_UPDATE_ERROR:
        case entitiesActions.ENTITY_UPDATE_CARDORDER_ERROR:
            return {
                ...state,
                items: {
                    ...state.items,
                    [action.meta.id]: Object.assign({}, state.items[action.meta.id], action.meta.data)
                }
            }

        // CARDS
        case cardsActions.CARD_FETCH:
            const card = state.cards[action.payload]
            return {
                ...state,
                cards: {
                    ...state.cards,
                    [action.payload]: {
                        ...card,
                        isFetching: true
                    }
                }
            }

        case cardsActions.CARD_UPDATE_REQUEST:
            var card = state.cards[action.payload.card_id] // eslint-disable-line no-unused-vars
            return {
                ...state,
                cards: {
                    ...state.cards,
                    [action.payload.card_id]: Object.assign({}, card, action.payload.data)
                }
            }

        case cardsActions.CARD_UPDATE_ERROR:
            var card_id = action.meta.id
            return {
                ...state,
                cards: {
                    ...state.cards,
                    [card_id]: Object.assign({}, state.cards[card_id], action.meta.data)
                }
            }

        case cardsActions.CARD_UPLOAD_SUCCESS:
            var card = action.payload // eslint-disable-line no-redeclare
            return {
                ...state,
                cards: {
                    ...state.cards,
                    [card.id]: Object.assign({}, state.cards[card.id], card)
                }
            }

        case cardsActions.CARD_DELETE_REQUEST:
            var item = state.items[action.payload.foreign_key] // eslint-disable-line no-redeclare
            return {
                ...state,
                items: {
                    ...state.items,
                    [item.id]: {
                        ...item,
                        cards: item.cards.filter(line => line !== action.payload.id)
                    }
                },
                cards: {
                    ...state.cards,
                    [action.payload.id]: {
                        ...state.cards[action.payload.id],
                        isDeleted: true
                    }
                }
            }
        case cardsActions.CARD_DELETE_ERROR:
            return {
                ...state,
                items: {
                    ...state.items,
                    [action.meta.item.id]: Object.assign({}, state.items[action.meta.item.id], action.meta.item)
                },
                cards: {
                    ...state.cards,
                    [action.meta.id]: {
                        ...state.cards[action.meta.id],
                        isDeleted: false
                    }
                }
            }

        case cardsActions.CARD_LINE_ADD_REQUEST:
            var card = state.cards[action.payload.card_id] // eslint-disable-line no-redeclare
            return {
                ...state,
                cards: {
                    ...state.cards,
                    [card.id]: {
                        ...card,
                        cards_parts: [
                            ...card.cards_parts,
                            action.payload.id
                        ]
                    }
                },
                cards_parts: {
                    ...state.cards_parts,
                    [action.payload.id]: {
                        ...action.payload.line,
                        id: action.payload.id,
                        part: action.payload.part.id
                    }
                },
                parts: {
                    ...state.parts,
                    [action.payload.part.id]: action.payload.part
                }
            }

        case cardsActions.CARD_LINE_ADD_SUCCESS:
            var line_id = action.meta
            return {
                ...state,
                cards_parts: {
                    ...state.cards_parts,
                    [line_id]: {
                        ...state.cards_parts[line_id],
                        internal_id: action.payload.id
                    }
                }
            }

        case cardsActions.CARD_LINE_ADD_ERROR:
            var line = state.cards_parts[action.meta]
            return {
                ...state,
                cards_parts: _omit(state.cards_parts, action.meta),
                cards: {
                    ...state.cards,
                    [line.card_id]: {
                        ...state.cards[line.card_id],
                        cards_parts: state.cards[line.card_id].cards.filter(cp => cp !== action.meta)
                    }
                }
            }

        case cardsActions.CARD_LINE_UPDATE_REQUEST:
            var line = state.cards_parts[action.payload.id] // eslint-disable-line no-redeclare
            var part = action.payload.data.part
            return {
                ...state,
                cards_parts: {
                    ...state.cards_parts,
                    [line.id]: Object.assign({}, line, action.payload.data, {
                        part: action.payload.data.part.id
                    })
                },
                parts: {
                    ...state.parts,
                    [part.id]: part
                }
            }

        case cardsActions.CARD_LINE_UPDATE_ERROR:
            var line_id = action.meta.id // eslint-disable-line no-redeclare
            return {
                ...state,
                cards_parts: {
                    ...state.cards_parts,
                    [line_id]: Object.assign({}, state.cards_parts[line_id], action.meta.data)
                }
            }

        case cardsActions.CARD_LINE_DELETE_FETCH:
        case cardsActions.CARD_LINE_DELETE_ERROR:
            const deletedValue = action.type === cardsActions.CARD_LINE_DELETE_FETCH

            // TODO show notification when error

            return {
                ...state,
                cards_parts: {
                    ...state.cards_parts,
                    [action.payload]: {
                        ...state.cards_parts[action.payload],
                        isDeleted: deletedValue
                    }
                }
            }

        // ATTACH ENGINE
        case entitiesActions.ATTACH_ENGINE_REQUEST:
            var item = state.items[action.payload.id] // eslint-disable-line no-redeclare
            return {
                ...state,
                items: {
                    ...state.items,
                    [action.payload.id]: {
                        ...item,
                        engines: [
                            ...item.engines,
                            action.payload.draft_id
                        ]
                    }
                },
                engines: {
                    ...state.engines,
                    [action.payload.draft_id]: {
                        id: action.payload.draft_id,
                        ...action.payload.data
                    }
                }
            }

        case entitiesActions.ATTACH_ENGINE_SUCCESS:
            var engine = action.payload.engines[0] || false
            if (!engine) {
                return state
            }

            return {
                ...state,
                engines: {
                    ...state.engines,
                    [action.meta]: {
                        ...state.engines[action.meta],
                        internal_id: engine.id,
                        name: engine.name
                    }
                }
            }

        case entitiesActions.ATTACH_ENGINE_ERROR:
        case entitiesActions.UNATTACH_ENGINE_ERROR:
            var item = state.items[action.meta.entity_id] // eslint-disable-line no-redeclare

            return {
                ...state,
                items: {
                    ...state.items,
                    [item.id]: Object.assign({}, item, action.meta.data)
                }
            }

        // UNATTACH
        case entitiesActions.UNATTACH_ENGINE_REQUEST:
            return {
                ...state,
                items: {
                    ...state.items,
                    [action.payload.entity_id]: {
                        ...state.items[action.payload.entity_id],
                        engines: state.items[action.payload.entity_id].engines.filter(id => id !== action.payload.id)
                    }
                }
            }

        /*case entitiesActions.UNATTACH_ENGINE_SUCCESS:
            return state*/

        // VERSIONS
        case versionsActions.VERSION_FETCH_REQUEST:
            return {
                ...state,
                versions: {
                    ...state.versions,
                    [action.payload]: {
                        ...state.versions[action.payload],
                        isFetched: false,
                        isFetching: true
                    }
                }
            }

        // Update
        case versionsActions.VERSION_UPDATE_REQUEST:
            var version = state.versions[action.payload.id] // eslint-disable-line no-unused-vars
            return {
                ...state,
                versions: {
                    ...state.versions,
                    [action.payload.id]: Object.assign({}, version, action.payload.data)
                }
            }

        case versionsActions.VERSION_UPDATE_ERROR:
            const version_id = action.meta.id
            return {
                ...state,
                versions: {
                    ...state.versions,
                    [version_id]: Object.assign({}, state.versions[version_id], action.meta.data)
                }
            }

        // Upload
        case versionsActions.VERSION_UPLOAD_SUCCESS:
            var version = action.payload // eslint-disable-line no-redeclare
            return {
                ...state,
                versions: {
                    ...state.versions,
                    [version.id]: Object.assign({}, state.versions[version.id], version)
                }
            }

        // Remove
        case versionsActions.VERSION_REMOVE_REQUEST:
            return {
                ...state,
                versions: {
                    ...state.versions,
                    [action.payload.id]: {
                        ...state.versions[action.payload.id],
                        isDeleted: true
                    }
                },
                items: {
                    ...state.items,
                    [action.payload.model_id]: {
                        ...state.items[action.payload.model_id],
                        versions: state.items[action.payload.model_id].versions.filter(id => id !== action.payload.id)
                    }
                }
            }

        case versionsActions.VERSION_REMOVE_ERROR:
            var item_id = action.payload.model_id
            return {
                ...state,
                versions: {
                    ...state.versions,
                    [action.payload.id]: {
                        ...state.versions[action.payload.id],
                        isDeleted: false
                    }
                },
                items: {
                    ...state.items,
                    [item_id]: Object.assign({}, state.items[item_id], action.meta)
                }
            }

        // MODIFICATIONS
        //
        case versionsActions.MODIFICATION_ADD_REQUEST:
            var version = state.versions[action.payload.version_id] // eslint-disable-line no-unused-vars, no-redeclare

            return {
                ...state,
                parts: {
                    ...state.parts,
                    [action.payload.part.id]: action.payload.part
                },
                versions: {
                    ...state.versions,
                    [action.payload.version_id]: {
                        ...version,
                        cards_mods: [
                            ...version.cards_mods,
                            action.payload.id
                        ]
                    }
                },
                modifications: {
                    ...state.modifications,
                    [action.payload.id]: {
                        id: action.payload.id,
                        ...action.payload.data
                    }
                }
            }

        case versionsActions.MODIFICATION_ADD_SUCCESS:
            var mod = action.payload.cards_mods[0]

            return {
                ...state,
                modifications: {
                    ...state.modifications,
                    [action.meta]: {
                        ...state.modifications[action.meta],
                        internal_id: mod.id,
                        foreign_key: mod.foreign_key,
                        model: mod.model
                    }
                }
            }

        case versionsActions.MODIFICATION_ADD_ERROR:
            var version = state.versions[action.meta.version_id] // eslint-disable-line no-redeclare

            return {
                ...state,
                versions: {
                    ...state.versions,
                    [action.meta.version_id]: {
                        ...version,
                        ...action.meta.data
                    }
                }
            }

        // Update
        case versionsActions.MODIFICATION_UPDATE_REQUEST:
            return {
                ...state,
                parts: {
                    ...state.parts,
                    [action.payload.part.id]: action.payload.part
                },
                modifications: {
                    ...state.modifications,
                    [action.payload.id]: Object.assign({},
                        state.modifications[action.payload.id],
                        action.payload.data
                    )
                }
            }

        case versionsActions.MODIFICATION_UPDATE_ERROR:
            return {
                ...state,
                modifications: {
                    ...state.modifications,
                    [action.meta.id]: Object.assign({},
                        state.modifications[action.meta.id],
                        action.meta.data
                    )
                }
            }

        // Remove
        case versionsActions.MODIFICATION_REMOVE_REQUEST:
            var version = state.versions[action.payload.version_id] // eslint-disable-line no-redeclare

            return {
                ...state,
                versions: {
                    ...state.versions,
                    [action.payload.version_id]: {
                        ...version,
                        cards_mods: version.cards_mods.filter(id => id !== action.payload.mod_id)
                    }
                }
            }

        case versionsActions.MODIFICATION_REMOVE_ERROR:
            var version = state.versions[action.meta.version_id] // eslint-disable-line no-redeclare

            return {
                ...state,
                versions: {
                    ...state.versions,
                    [action.meta.version_id]: {
                        ...version,
                        ...action.meta.data
                    }
                }
            }

        default:
            return state
    }
}

export const getEntity = ({ entities }, id) => {
    if (!entities.items.hasOwnProperty(id)) {
        return {}
    }

    return entities.items[id]
}

export const isEntityFetched = (state, id) => getEntity(state, id).isFetched || false

// Cards
//
export const getCard = ({ entities }, id) => {
    if (!entities.cards.hasOwnProperty(id)) {
        return {}
    }

    return entities.cards[id]
}

export const getCards = (state, items) => items.map(id => getCard(state, id))

export const isCardFetching = (state, id) => {
    const card = getCard(state, id)
    if (!card.hasOwnProperty('isFetching')) {
        return true
    }

    return card.isFetching
}

export const isCardLinesFetched = (state, id) => {
    const card = getCard(state, id)
    if (!card.hasOwnProperty('isFetched')) {
        return false
    }

    return card.isFetched
}


// Card Lines
//
const getLine = ({ entities }, id) => {
    if (!entities.cards_parts.hasOwnProperty(id)) {
        return {}
    }

    return entities.cards_parts[id]
}

export const getCardLines = (state, id) => {
    const card = getCard(state, id)

    if (!card.hasOwnProperty('cards_parts')) {
        return []
    }

    return card.cards_parts
        .map(line => getLine(state, line))
        .filter(line => !line.hasOwnProperty('isDeleted') || !line.isDeleted)
}

// Part
//
export const getPart = ({ entities }, id) => {
    if (!entities.parts.hasOwnProperty(id)) {
        return {}
    }

    return entities.parts[id]
}


// Versions
//
export const getVersion = ({ entities }, id) => {
    if (!entities.versions.hasOwnProperty(id)) {
        return {}
    }

    return entities.versions[id]
}

export const isVersionFetching = (state, id) => {
    const version = getVersion(state, id)

    if (!version.hasOwnProperty('isFetching')) {
        return true
    }

    return version.isFetching
}

export const isVersionFetched = (state, id) => {
    const version = getVersion(state, id)

    if (!version.hasOwnProperty('isFetched')) {
        return false
    }

    return version.isFetched
}

export const getVersions = (state, items) => items.map(id => getVersion(state, id))


// Engines
//
export const getEngine = ({ entities }, id) => {
    if (!entities.engines.hasOwnProperty(id)) {
        return {}
    }

    return entities.engines[id]
}

export const getEngines = (state, ids) => ids.map(id => getEngine(state, id))


// Modifications
//
const getModification = ({ entities }, id) => {
    if (!entities.modifications.hasOwnProperty(id)) {
        return {}
    }

    return entities.modifications[id]
}

const getEntityCardLine = (state, id) => {
    const line = getLine(state, id)

    return {
        ...line,
        card: getCard(state, line.card_id),
        part: getPart(state, line.part)
    }
}

const getTargetEntity = (state, options) => {
    switch (options.action) {
        case 1:
            return getCard(state, options.id)
        case 2:
        case 3:
        default:
            return getEntityCardLine(state, options.id)
    }
}

export const getModifications = (state, version_id, attachEntities = false) => {
    const version = getVersion(state, version_id)

    if (!version.hasOwnProperty('cards_mods')) {
        return []
    }

    return version.cards_mods.map(id => {
        const mod = getModification(state, id)

        if (!attachEntities) {
            return mod
        }

        return {
            ...mod,
            part: mod.hasOwnProperty('part') ? getPart(state, mod.part) : {},
            target: getTargetEntity(state, { id: mod.target_id, action: mod.action })
        }
    })
}


// Extra
export const getEntityCards = (state, id, attachLines = false) => {
    const entity = getEntity(state, id)

    if (!attachLines) {
        return entity.cards.map(card => getCard(state, card))
    }

    return entity.cards.map(card => {
        const c = getCard(state, card)

        return {
            ...c,
            cards_parts: c.cards_parts.map(line => {
                const l = getLine(state, line)

                return {
                    ...l,
                    part: getPart(state, l.part)
                }
            })
        }
    })
}
