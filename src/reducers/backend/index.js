import { combineReducers } from 'redux'
import { intlReducer as intl } from 'react-intl-redux'

import helpers from '../helpers'
import entities from './entities'
import modal from '../modal'

/*import items from './items'
import cards from './cards'
import categories from './categories'
import engines from './engines'
import cards_parts from './cards_parts'
import parts from './parts'*/

const rootReducer = combineReducers({
    helpers,
    modal,
    /*entities: combineReducers({
        items,
        cards,
        cards_parts,
        parts,
        categories,
        engines
    }),*/
    entities,
    intl,
})

export default rootReducer

/*export const getEntity = ({ entities }, id) => {
    if (!entities.items.hasOwnProperty(id)) {
        return {}
    }

    return entities.items[id]
}

export const isEntityFetched = (state, id) => getEntity(state, id).isFetched || false

// Cards
//
export const getCard = ({ entities }, id) => {
    if (!entities.cards.hasOwnProperty(id)) {
        return {}
    }

    return entities.cards[id]
}

export const getCards = (state, items) => items.map(id => getCard(state, id))

export const isCardFetching = (state, id) => {
    const card = getCard(state, id)
    if (!card.hasOwnProperty('isFetching')) {
        return true
    }

    return card.isFetching
}

export const isCardLinesFetched = (state, id) => {
    const card = getCard(state, id)
    if (!card.hasOwnProperty('isFetched')) {
        return false
    }

    return card.isFetched
}

// Card Lines
//
const getLine = ({ entities }, id) => {
    if (!entities.cards_parts.hasOwnProperty(id)) {
        return {}
    }

    return entities.cards_parts[id]
}

export const getCardLines = (state, id) => {
    const card = getCard(state, id)

    if (!card.hasOwnProperty('cards_parts')) {
        return []
    }

    return card.cards_parts
        .map(line => getLine(state, line))
        .filter(line => !line.hasOwnProperty('isDeleted') || !line.isDeleted)
}

// Part
//
export const getPart = ({ entities }, id) => {
    if (!entities.parts.hasOwnProperty(id)) {
        return {}
    }

    return entities.parts[id]
}*/
