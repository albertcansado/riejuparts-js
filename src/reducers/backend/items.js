import { merge } from 'lodash'

import * as entitiesActions from '../../constants/backend/ActionsEntities'
import { CARD_CREATE_SUCCESS, CARD_DELETE_REQUEST, CARD_DELETE_ERROR } from '../../constants/backend/ActionsCards'

export default function items(state = {}, action) {
    switch (action.type) {
        case entitiesActions.ENTITY_SUCCESS:
            return merge(
                state,
                action.payload.entities.items,
                {
                    [action.meta]: {
                        isFetched: true
                    }
                }
            )

        // ADD CARD
        case CARD_CREATE_SUCCESS:
            var item = state[action.meta]

            return {
                ...state,
                [action.meta]: {
                    ...item,
                    cards: [
                        ...item.cards,
                        action.payload.result
                    ]
                }
            }

        // DELETE CARD
        case CARD_DELETE_REQUEST:
            var item = state[action.payload.foreign_key] // eslint-disable-line no-redeclare

            return {
                ...state,
                [item.id]: {
                    ...item,
                    cards: item.cards.filter(line => line !== action.payload.id)
                }
            }

        case CARD_DELETE_ERROR:
            return {
                ...state,
                [action.meta.item.id]: Object.assign({}, state[action.meta.item.id], action.meta.item)
            }

        default:
            return state
    }
}
