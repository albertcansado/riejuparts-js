import { merge, omit as _omit } from 'lodash'

import { CARD_SUCCESS, CARD_LINE_ADD_REQUEST, CARD_LINE_ADD_SUCCESS , CARD_LINE_ADD_ERROR, CARD_LINE_UPDATE_REQUEST, CARD_LINE_UPDATE_ERROR, CARD_LINE_DELETE_FETCH, CARD_LINE_DELETE_ERROR } from '../../constants/backend/ActionsCards'

export default function cards_parts(state = {}, action) {
    switch (action.type) {
        case CARD_SUCCESS:
            return merge(state, action.payload.entities.cards_parts)

        case CARD_LINE_ADD_REQUEST:
            return {
                ...state,
                [action.payload.id]: {
                    ...action.payload.line,
                    id: action.payload.id,
                    part: action.payload.part.id
                }
            }

        case CARD_LINE_ADD_SUCCESS:
            return {
                ...state,
                [action.meta]: {
                    ...state[action.meta],
                    internal_id: action.payload.id
                }
            }

        case CARD_LINE_ADD_ERROR:
            return _omit(state, action.meta.line_id)

        case CARD_LINE_UPDATE_REQUEST:
            var line = state[action.payload.id] // eslint-disable-line no-redeclare
            return {
                ...state,
                [line.id]: Object.assign({}, line, action.payload.data, {
                    part: action.payload.data.part.id
                })
            }

        case CARD_LINE_UPDATE_ERROR:
            return {
                ...state,
                [action.meta.id]: Object.assign({}, state[action.meta.id], action.meta.data)
            }


        case CARD_LINE_DELETE_FETCH:
        case CARD_LINE_DELETE_ERROR:
            const deletedValue = action.type === CARD_LINE_DELETE_FETCH

            // TODO show notification when error

            return {
                ...state,
                [action.payload]: {
                    ...state[action.payload],
                    isDeleted: deletedValue
                }
            }

        default:
            return state
    }
}
