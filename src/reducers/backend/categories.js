import { merge } from 'lodash'

import { ENTITY_SUCCESS } from '../../constants/backend/ActionsEntities'

export default function categories(state = {}, action) {
    switch(action.type) {
        case ENTITY_SUCCESS:
            return merge(state, action.payload.entities.categories)

        default:
            return state
    }
}
