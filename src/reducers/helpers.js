import { LOCATION_CHANGE } from 'react-router-redux'

import * as initHelpers from '../constants/ActionHelpers'
import langs from '../constants/backend/Langs'

const INIT_STATES = {
    START: 'START',
    FINISH: 'FINISH'
}

const initState = {
    init: INIT_STATES.START,
    initCart: INIT_STATES.START,
    initProducts: INIT_STATES.START,
    isLoading: true,
    langs
}

export default function helpers(state = initState, action) {
    switch(action.type) {
        case initHelpers.INIT_SUCCESS:
            return {
                ...state,
                init: INIT_STATES.FINISH,
                isLoading: false
            }
        case initHelpers.INIT_CART_SUCCESS:
            return {
                ...state,
                initCart: INIT_STATES.FINISH
            }
        case initHelpers.INIT_PRODUCTS_SUCCESS:
            return {
                ...state,
                initProducts: INIT_STATES.FINISH
            }
        case initHelpers.SET_LOADING:
        case LOCATION_CHANGE:
            return {
                ...state,
                isLoading: (action.type === LOCATION_CHANGE) ? true : action.payload
            }
        default:
            return state
    }
}

export const isInitFinish = ({ helpers }) => helpers.init === INIT_STATES.FINISH

export const isInitCartFinish = ({ helpers }) => helpers.initCart === INIT_STATES.FINISH

export const isInitProductFinish = ({ helpers }) => helpers.initProducts === INIT_STATES.FINISH

export const getLoading = ({ helpers }) => helpers.isLoading

export const getLangs = ({ helpers }) => helpers.langs
