import { combineReducers } from 'redux'
import { intlReducer as intl } from 'react-intl-redux'
import { routerReducer as routing } from 'react-router-redux'

import helpers from './helpers'
// import models from './models'
import cart from './cart'
import entities from './entities'
import modal from './modal'

const rootReducer = combineReducers({
    cart,
    helpers,
    // models,
    entities,
    modal,
    intl,
    routing
})

export default rootReducer
