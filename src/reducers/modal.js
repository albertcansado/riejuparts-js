// import { merge } from 'lodash'

import * as actions from '../constants/ActionsModal'

const initState = {
    isOpen: false,
    component: null,
    props: {},
    modalProps: {}
}

export default function modal(state = initState, action) {
    switch(action.type) {
        case actions.OPEN_MODAL:
            return {
                ...state,
                ...action.payload,
                isOpen: true
            }
        case actions.CLOSE_MODAL:
            return initState
        default:
            return state
    }
}

export const isOpen = ({ modal }) => modal.isOpen

export const getProps = ({ modal }) => modal.props

export const getComponent = ({ modal }) => modal.component

export const getModalProps = ({ modal }) => modal.modalProps
