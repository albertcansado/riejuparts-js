import { merge } from 'lodash'

import { CAUSES_SUCCESS } from '../../constants/warranty/ActionsWarranties'

export default function causes(state = {}, action) {
    switch(action.type) {
        case CAUSES_SUCCESS:
            return merge(state, action.payload.entities.causes)
        default:
            return state
    }
}
