import { merge } from 'lodash'

import * as warrantiesActions from '../../constants/warranty/ActionsWarranties'

export default function lines(state = {}, action) {
    switch(action.type) {
        // Create
        case warrantiesActions.WARRANTY_LINE_CREATE_REQUEST:
            return {
                ...state,
                [action.payload.id]: action.payload
            }
        case warrantiesActions.WARRANTY_LINE_CREATE_SUCCESS:
            return {
                ...state,
                [action.meta]: {
                    ...state[action.meta],
                    internal_id: action.payload.id
                }
            }
        /*case warrantiesActions.WARRANTY_LINE_CREATE_ERROR:
            return state*/

        case warrantiesActions.WARRANTY_LINE_UPDATE_REQUEST:
            return {
                ...state,
                [action.payload.id]: Object.assign({}, state[action.payload.id], action.payload.data)
            }

        case warrantiesActions.WARRANTY_LINE_UPDATE_ERROR:
            return {
                ...state,
                [action.meta.id]: Object.assign({}, state[action.meta.id], action.meta.data)
            }

        case warrantiesActions.WARRANTY_LINE_DELETE_REQUEST:
            return {
                ...state,
                [action.payload.id]: {
                    ...state[action.payload.id],
                    isDeleted: true
                }
            }

        case warrantiesActions.WARRANTY_LINE_DELETE_ERROR:
            return {
                ...state,
                [action.meta.id]: {
                    ...state[action.meta.id],
                    isDeleted: false
                }
            }

        // Init Fetch
        case warrantiesActions.WARRANTY_SUCCESS:
        case warrantiesActions.CSV_UPLOAD_SUCCESS:
            return merge(state, action.payload.entities.lines)

        default:
            return state
    }
}
