import { merge } from 'lodash'

import * as warrantiesActions from '../../constants/warranty/ActionsWarranties'

export default function warranties(state = {}, action) {
    switch(action.type) {
        case warrantiesActions.WARRANTY_SUCCESS:
            return merge({}, state, action.payload.entities.warranties)
        case warrantiesActions.WARRANTY_UPDATE_REQUEST:
            var warranty = state[action.payload.id]
            return {
                ...state,
                [warranty.id]: Object.assign({}, warranty, action.payload.data)
            }
        case warrantiesActions.WARRANTY_UPDATE_ERROR:
            return {
                ...state,
                [action.meta.id]: Object.assign({}, state[action.meta.id], action.meta.data)
            }

        // Lines
        //
        case warrantiesActions.WARRANTY_LINE_CREATE_REQUEST:
            var warranty = state[action.payload.warranty_id] // eslint-disable-line no-redeclare
            return {
                ...state,
                [warranty.id]: {
                    ...warranty,
                    lines: [
                        ...warranty.lines,
                        action.payload.id
                    ]
                }
            }

        // Delete
        case warrantiesActions.WARRANTY_LINE_DELETE_REQUEST:
            var warranty = state[action.payload.warranty_id] // eslint-disable-line no-redeclare
            return {
                ...state,
                [warranty.id]: {
                    ...state[warranty.id],
                    lines: warranty.lines.filter(id => id !== action.payload.id)
                }
            }

        case warrantiesActions.WARRANTY_LINE_CREATE_ERROR:
        case warrantiesActions.WARRANTY_LINE_DELETE_ERROR:
            return {
                ...state,
                [action.meta.warranty_id]: Object.assign({}, state[action.meta.warranty_id], action.meta.data)
            }

        // Attachments
        case warrantiesActions.ATTACHMENT_UPLOAD_REQUEST:
            var warranty = state[action.payload.warranty_id] // eslint-disable-line no-redeclare
            return {
                ...state,
                [warranty.id]: {
                    ...warranty,
                    attachments: [
                        ...warranty.attachments,
                        action.payload.id
                    ]
                }
            }

        case warrantiesActions.ATTACHMENT_DELETE_REQUEST:
            var warranty = state[action.payload.warranty_id] // eslint-disable-line no-redeclare
            return {
                ...state,
                [warranty.id]: {
                    ...state[warranty.id],
                    attachments: warranty.attachments.filter(id => id !== action.payload.id)
                }
            }

        case warrantiesActions.ATTACHMENT_UPLOAD_ERROR:
        case warrantiesActions.ATTACHMENT_DELETE_ERROR:
            return {
                ...state,
                [action.meta.warranty_id]: Object.assign({}, state[action.meta.warranty_id], action.meta.data)
            }

        default:
            return state
    }
}
