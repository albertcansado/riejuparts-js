
import { HISTORY_SUCCESS } from '../../constants/warranty/ActionsHistories'

const initState = {
    historySync: false
}

export default function helpers(state = initState, action) {
    switch (action.type) {
        case HISTORY_SUCCESS:
            return {
                ...state,
                historySync: true
            }
        default:
            return state
    }
}
