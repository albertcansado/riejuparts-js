import { combineReducers } from 'redux'
import { intlReducer as intl } from 'react-intl-redux'
import idx from 'idx'

import modal from '../modal'
import warranties from './warranties'
import causes from './causes'
import lines from './lines'
import attachments from './attachments'
import histories from './histories'
import helpers from './helpers'

const rootReducer = combineReducers({
    entities: combineReducers({
        warranties,
        causes,
        lines,
        attachments,
        histories
    }),
    helpers,
    modal,
    intl
})

export default rootReducer

const getEntity = (state = {}, name = '', id = null) => {
    return idx(state.entities, _ => _[name][id])
}


/** Warranty **/
export const getWarranty = (state = {}, id = null) => {
    const entity = getEntity(state, 'warranties', id)
    return entity ? entity : {}
}

// Indica si la garantia esta completa
export const isCompleted = (state = {}, id = null) => {
    const warranty = getWarranty(state, id)

    if (!warranty.hasOwnProperty('id')) {
        return false
    }

    // Check transport or preKm0 is active (only on pregarantia)
    const preValidation = !warranty.type ? warranty.transport || warranty.pre_km0 : true

    // Check Km only if is not prewarranty
    const kmValidate = !warranty.type ? true : warranty.km

    return warranty.lines.length > 0 && kmValidate && preValidation && !!warranty.comment_dealer
}

// Devuelve todas las causas en un Array
export const getCauses = (state = {}) => Object.keys(state.entities.causes).map(c => state.entities.causes[c])

// Devuelve todas las linias de la garantia
export const getLines = (state = {}, id = null) => {
    const warranty = getEntity(state, 'warranties', id)

    if (!warranty) {
        return []
    }

    return warranty.lines.map(l => {
        const line = getEntity(state, 'lines', l)
        return {
            ...line,
            cause: getEntity(state, 'causes', line.cause)
        }
    })
}

// Devuelve las lineas con el formato:
// { sku: {id: null, qty: [\d]+} }
export const getLinesParts = (state = {}, id = null) => {
    const warranty = getEntity(state, 'warranties', id)

    if (!warranty) {
        return []
    }

    const list = {}
    warranty.lines.forEach(lineId => {
        const line = getEntity(state, 'lines', lineId)

        list[line.part.sku] = {
            id: line.id,
            qty: line.qty
        }
    })

    return list
}


/** Attachments **/
export const getAttachments = ({ entities }, ids) => !ids ? [] : ids.map(id => entities.attachments[id])


/** Histories **/
export const getHistories = ({ entities }) => entities.histories


/** Helpers **/
export const isHistoryFetched = ({ helpers }) => helpers.historySync
