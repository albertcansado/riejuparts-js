import { merge } from 'lodash'

import * as historyActions from '../../constants/warranty/ActionsHistories'

export default function histories(state = {}, action) {
    switch (action.type) {
        case historyActions.HISTORIES_SUCCESS:
            return merge(state, action.payload.entities.histories)
        default:
            return state
    }
}
