import { merge } from 'lodash'

import { WARRANTY_SUCCESS, ATTACHMENT_UPLOAD_REQUEST, ATTACHMENT_UPLOAD_SUCCESS, ATTACHMENT_DELETE_REQUEST, ATTACHMENT_DELETE_ERROR } from '../../constants/warranty/ActionsWarranties'

export default function attachments(state = {}, action) {
    switch(action.type) {
        // Init Fetch
        case WARRANTY_SUCCESS:
            return merge(state, action.payload.entities.attachments)

        case ATTACHMENT_UPLOAD_REQUEST:
            return {
                ...state,
                [action.payload.id]: action.payload
            }

        case ATTACHMENT_UPLOAD_SUCCESS:
            return {
                ...state,
                [action.meta]: {
                    ...state[action.meta],
                    internal_id: action.payload.id,
                    isUploading: false
                }
            }

        case ATTACHMENT_DELETE_REQUEST:
            return {
                ...state,
                [action.payload.id]: {
                    ...state[action.payload.id],
                    isDeleted: true
                }
            }

        case ATTACHMENT_DELETE_ERROR:
            return {
                ...state,
                [action.meta.id]: {
                    ...state[action.meta.id],
                    isDeleted: false
                }
            }

        default:
            return state
    }
}
