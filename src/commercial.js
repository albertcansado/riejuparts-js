import intlWrapper from 'lib/intlWrapper'

import CommercialContainer from 'container/Backend/CommercialContainer'

const rootDOM = document.querySelector('#commercial-root')
if (rootDOM) {
    intlWrapper({
        element: rootDOM,
        component: CommercialContainer,
        props: {
            fetchUrl: '/d/commercial/{id}.json',
            saveUrl: '/d/commercial'
        }
    })
}
