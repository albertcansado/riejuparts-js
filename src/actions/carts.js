import { forEach as _forEach, pick as _pick } from 'lodash'

import { CALL_API } from '../middleware/api'
import Schemas from '../constants/Schemas'
import * as cartsActions from '../constants/ActionCarts'
import { setInitCartFinished } from './helpers'
import { getCart } from '../reducers/entities'

// TOGGLE CART
export const toggleCart = () => {
    return {
        type: cartsActions.TOGGLE_CART
    }
}


// INIT DATA
const fetchData = (orderId = null) => {
    const order_id = orderId ? orderId : window.localStorage.getItem('order_id')
    let id = ''
    if (order_id) {
        id = '/' + order_id
    }

    return {
        [CALL_API]: {
            types: [cartsActions.CART_FETCH, cartsActions.CART_SUCCESS, cartsActions.CART_ERROR],
            url: `/carts${id}.json`,
            schema: Schemas.ORDER
        }
    }
}

export const fetchCartInit = () => {
    return (dispatch, getState) => {
        dispatch(fetchData())
        .then(() =>
            dispatch(setInitCartFinished())
        )
    }
}

export const fetchCart = (id) => {
    return (dispatch, getState) => {
        dispatch(fetchData(id))
    }
}


// CREATE CART
const fetchCreateCart = (id) => {
    return {
        [CALL_API]: {
            types: [
                cartsActions.CART_CREATE_REQUEST,
                cartsActions.CART_SUCCESS,
                {
                    type: cartsActions.CART_ERROR,
                    meta: id
                }
            ],
            url: '/carts/create.json',
            method: 'POST',
            schema: Schemas.ORDER
        }
    }
}

export const createCart = () => {
    return (dispatch, getState) => {
        const state = getState()
        dispatch(
            fetchCreateCart(state.cart.id)
        )
    }
}

// Change Cart
//
export const changeCart = (order) => {
    return {
        type: cartsActions.CART_CHANGE,
        payload: order
    }
}

// UPDATE CART
export const updateCart = (id = null, data = {}) => {
    return {
        [CALL_API]: {
            types: [cartsActions.CART_UPDATE_REQUEST, cartsActions.CART_UPDATE_SUCCESS, cartsActions.CART_UPDATE_ERROR],
            url: `/carts/edit/${id}.json`,
            method: 'POST',
            body: data,
            schema: Schemas.NOPARSE
        }
    }
}

const transformItemToLine = (item, data) => {
    let part = {}
    if (item.hasOwnProperty('sku')) {
        // Part
        part = item
    } else {
        // CardPart
        part = item.part
        part.name = item.name
    }

    return Object.assign({}, data, {
        total: data.qty * part.price,
        part_id: part.id,
        name: part.name,
        part
    })
}

// ADD PRODUCT
const fetchAddToCart = (cart, item, nextQty) => {
    const line = transformItemToLine(
        item,
        {
            id: new Date().getTime().toString(),
            order_id: cart.id,
            qty: nextQty
        }
    )

    return {
        [CALL_API]: {
            types: [
                {
                    type: cartsActions.CART_ADD_LINE_REQUEST,
                    payload: line
                },
                {
                    type: cartsActions.CART_ADD_LINE_SUCCESS,
                    meta: line.id
                },
                {
                    type: cartsActions.CART_ADD_LINE_ERROR,
                    meta: {
                        id: line.id,
                        order_id: cart.id,
                        qty: nextQty,
                    }
                }
            ],
            url: `/carts/${cart.id}/line.json`,
            method: 'PUT',
            body: {
                part: line.part.id,
                qty: nextQty
            },
            schema: Schemas.NOPARSE
        }
    }
}

export const addToCart = (line = {}, qty = 1) => {
    return (dispatch, getState) => {
        const state = getState()
        return dispatch(
            fetchAddToCart(state.cart, line, qty)
        )
    }
}

// DELETE PRODUCT
const fetchDeleteLine = (line, order) => {
    const backendId = line.internal_id || line.id
    return {
        [CALL_API]: {
            types: [
                {
                    type: cartsActions.CART_DELETE_LINE_REQUEST,
                    payload: {
                        id: line.id,
                        order_id: line.order_id,
                        qty: line.qty
                    }
                },
                cartsActions.CART_DELETE_LINE_SUCCESS,
                {
                    type: cartsActions.CART_DELETE_LINE_ERROR,
                    meta: {
                        id: line.id,
                        order_id: line.order_id,
                        data: _pick(order, ['orders_lines', 'lines', 'qty'])
                    }
                }
            ],
            url: `/carts/${line.order_id}/line/${backendId}.json`,
            method: 'DELETE',
            schema: Schemas.NOPARSE
        }
    }
}

export const deleteCartLine = (item) => {
    return (dispatch, getState) => {
        return dispatch(fetchDeleteLine(
            item,
            getCart(getState())
        ))
    }
}

// UPDATE PRODUCT
const fetchUpdateCartLine = (line, nextQty) => {
    const backendId = line.internal_id || line.id
    return {
        [CALL_API]: {
            types: [
                {
                    type: cartsActions.CART_UPDATE_LINE_REQUEST,
                    payload: {
                        id: line.id,
                        prevQty: line.qty,
                        nextQty
                    }
                },
                cartsActions.CART_UPDATE_LINE_SUCCESS,
                {
                    type: cartsActions.CART_UPDATE_LINE_ERROR,
                    meta: {
                        id: line.id,
                        qty: line.qty
                    }
                }
            ],
            url: `/carts/${line.order_id}/line/${backendId}.json`,
            method: 'POST',
            body: {
                qty: nextQty
            },
            schema: Schemas.NOPARSE
        }
    }
}

export const updateCartLine = (line = {}, nextQty = 1) => {
    return (dispatch, getState) => {
        if (nextQty === 0) {
            return dispatch(
                deleteCartLine(line)
            )
        } else {
            return dispatch(
                fetchUpdateCartLine(
                    line,
                    nextQty
                )
            )
        }
    }
}


// CSV
export const sendCSV = (cart, data) => {
    return {
        [CALL_API]: {
            types: [
                cartsActions.CART_CSV_REQUEST,
                cartsActions.CART_CSV_SUCCESS,
                cartsActions.CART_CSV_ERROR
            ],
            url: `/carts/${cart.id}/lines.json`,
            method: 'PUT',
            body: {
                lines: data
            },
            schema: Schemas.ORDER
        }
    }
}


// ADD EXTRA
const fetchAddExtraToCart = (cart, title, price) => {
    const draftId = new Date().getTime()
    return {
        [CALL_API]: {
            types: [
                {
                    type: cartsActions.CART_ADD_EXTRA_REQUEST,
                    payload: {
                        id: draftId,
                        order_id: cart.id,
                        pvp: price,
                        title,
                        draftId
                    }
                },
                {
                    type: cartsActions.CART_ADD_EXTRA_SUCCESS,
                    meta: draftId
                },
                {
                    type: cartsActions.CART_ADD_EXTRA_ERROR,
                    meta: draftId
                }
            ],
            url: `/carts/${cart.id}/extra.json`,
            method: 'PUT',
            body: {
                title,
                price
            },
            schema: Schemas.NOPARSE
        }
    }
}

export const addExtraToCart = (title = '', price = 0) => {
    return (dispatch, getState) => {
        dispatch (
            fetchAddExtraToCart(getState().cart, title, price)
        )
    }
}

export const deleteExtra = (item = {}) => {
    return {
        [CALL_API]: {
            types: [
                {
                    type: cartsActions.CART_DELETE_EXTRA_REQUEST,
                    payload: item
                },
                cartsActions.CART_DELETE_EXTRA_SUCCESS,
                {
                    type: cartsActions.CART_DELETE_EXTRA_ERROR,
                    meta: item
                }
            ],
            url: `/carts/${item.order_id}/extra/${item.id}.json`,
            method: 'DELETE',
            schema: Schemas.NOPARSE
        }
    }
}
