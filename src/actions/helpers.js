import * as helperActions from '../constants/ActionHelpers'

export const setInitFinished = () => {
    return {
        type: helperActions.INIT_SUCCESS
    }
}

export const setInitCartFinished = () => {
    return {
        type: helperActions.INIT_CART_SUCCESS
    }
}

export const setInitProductsFinished = () => {
    return {
        type: helperActions.INIT_PRODUCTS_SUCCESS
    }
}

export const setLoading = (value = false) => {
    return {
        type: helperActions.SET_LOADING,
        payload: value
    }
}
