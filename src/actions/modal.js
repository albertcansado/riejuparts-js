import * as actions from '../constants/ActionsModal'

export const openModal = (options = {}) => {
    return {
        type: actions.OPEN_MODAL,
        payload: options
    }
}

export const closeModal = () => {
    return {
        type: actions.CLOSE_MODAL
    }
}
