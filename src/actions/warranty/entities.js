import { pick as _pick } from 'lodash'

import { CALL_API } from '../../middleware/api'
import Schemas from '../../constants/warranty/Schemas'

import * as warrantyActions from '../../constants/warranty/ActionsWarranties'

export const fetchWarranty = (id = null) => {
    return {
        [CALL_API]: {
            types: [warrantyActions.WARRANTY_FETCH, warrantyActions.WARRANTY_SUCCESS, warrantyActions.WARRANTY_ERROR],
            url: `/warranties/editor/${id}.json`,
            schema: Schemas.WARRANTY
        }
    }
}

export const fetchCauses = () => {
    return {
        [CALL_API]: {
            types: [warrantyActions.CAUSES_FETCH, warrantyActions.CAUSES_SUCCESS, warrantyActions.CAUSES_ERROR],
            url: `/warranties/causes.json`,
            schema: Schemas.CAUSES
        }
    }
}

export const fetchWarrantyInit = (id = null) => {
    return (dispatch, _) => {
        return Promise.all([
            dispatch(fetchWarranty(id)),
            dispatch(fetchCauses())
        ])
    }
}

// Update
//
export const updateWarranty = (warranty, data = {}) => {
    return {
        [CALL_API]: {
            types: [
                {
                    type: warrantyActions.WARRANTY_UPDATE_REQUEST,
                    payload: {
                        id: warranty.id,
                        data
                    }
                },
                warrantyActions.WARRANTY_UPDATE_SUCCESS,
                {
                    type: warrantyActions.WARRANTY_UPDATE_ERROR,
                    meta: {
                        id: warranty.id,
                        data: _pick(warranty, Object.keys(data))
                    }
                }
            ],
            url: `/warranties/${warranty.id}.json`,
            method: 'POST',
            body: data,
            schema: Schemas.NOPARSE
        }
    }
}


// LINES
//
//

// Create
export const createLine = (warranty, data) => {
    const draftId = new Date().getTime()
    return {
        [CALL_API]: {
            types: [
                {
                    type: warrantyActions.WARRANTY_LINE_CREATE_REQUEST,
                    payload: {
                        id: draftId,
                        part: data.part,
                        qty: data.qty,
                        cause: data.cause,
                        warranty_id: warranty.id
                    }
                },
                {
                    type: warrantyActions.WARRANTY_LINE_CREATE_SUCCESS,
                    meta: draftId
                },
                {
                    type: warrantyActions.WARRANTY_LINE_CREATE_ERROR,
                    meta: {
                        id: draftId,
                        warranty_id: warranty.id,
                        data: _pick(warranty, ['lines'])
                    }
                }
            ],
            url: `/warranties/${warranty.id}/lines.json`,
            method: 'PUT',
            body: {
                part_id: data.part.id,
                cause_id: data.cause,
                qty: data.qty
            },
            schema: Schemas.NOPARSE
        }
    }
}

export const updateLine = (_, { line, data }) => {
    const backendId = line.hasOwnProperty('internal_id') ? line.internal_id : line.id

    // Transform data
    const requestData = data
    if (requestData.hasOwnProperty('cause')) {
        requestData['cause_id'] = requestData.cause
        delete requestData.cause
    }

    return {
        [CALL_API]: {
            types: [
                {
                    type: warrantyActions.WARRANTY_LINE_UPDATE_REQUEST,
                    payload: {
                        id: line.id,
                        data
                    }
                },
                warrantyActions.WARRANTY_LINE_UPDATE_SUCCESS,
                {
                    type: warrantyActions.WARRANTY_LINE_UPDATE_ERROR,
                    meta: {
                        id: line.id,
                        data: _pick(line, Object.keys(data))
                    }
                }
            ],
            url: `/warranties/${line.warranty_id}/lines/${backendId}.json`,
            method: 'POST',
            body: requestData,
            schema: Schemas.NOPARSE
        }
    }
}

export const deleteLine = (warranty, line) => {
    const backendId = line.hasOwnProperty('internal_id') ? line.internal_id : line.id
    return {
        [CALL_API]: {
            types: [
                {
                    type: warrantyActions.WARRANTY_LINE_DELETE_REQUEST,
                    payload: {
                        id: line.id,
                        warranty_id: warranty.id
                    }
                },
                warrantyActions.WARRANTY_LINE_DELETE_SUCCESS,
                {
                    type: warrantyActions.WARRANTY_LINE_DELETE_ERROR,
                    meta: {
                        id: line.id,
                        warranty_id: warranty.id,
                        data: _pick(warranty, ['lines'])
                    }
                }
            ],
            url: `/warranties/${line.warranty_id}/lines/${backendId}.json`,
            method: 'DELETE',
            schema: Schemas.NOPARSE
        }
    }
}


// CSV
//
//
export const createCsvLines = (warranty, lines) => {
    return {
        [CALL_API]: {
            types: [
                warrantyActions.CSV_UPLOAD_REQUEST,
                warrantyActions.CSV_UPLOAD_SUCCESS,
                warrantyActions.CSV_UPLOAD_ERROR
            ],
            url: `/warranties/${warranty.id}/lines.json`,
            method: 'PUT',
            body: {
                lines
            },
            schema: Schemas.WARRANTY
        }
    }
}


// ATTACHMENTS
//
//
const uploadAttachment = (warranty, file) => {
    const data = new FormData()
    data.append('attachment', file)

    return {
        [CALL_API]: {
            types: [
                {
                    type: warrantyActions.ATTACHMENT_UPLOAD_REQUEST,
                    payload: Object.assign(
                        {},
                        _pick(file, ['name', 'size', 'type', 'id', 'isUploading']),
                        {
                            warranty_id: warranty.id
                        }
                    )
                },
                {
                    type: warrantyActions.ATTACHMENT_UPLOAD_SUCCESS,
                    meta: file.id
                },
                {
                    type: warrantyActions.ATTACHMENT_UPLOAD_ERROR,
                    meta: {
                        id: file.id,
                        warranty_id: warranty.id,
                        data: _pick(warranty, ['attachments'])
                    }
                }
            ],
            url: `/warranties/${warranty.id}/uploader.json`,
            method: 'POST',
            body: data,
            schema: Schemas.NOPARSE
        }
    }
}

export const uploadAttachments = (warranty, files) => {
    if (Array.isArray(files)) {
        return (dispatch, _) => {
            return Promise.all(files.map(
                file => dispatch(uploadAttachment(warranty, file))
            ))
        }
    }

    return uploadAttachment(warranty, files)
}

export const deleteAttachment = (warranty, file) => {
    const backendId = file.hasOwnProperty('internal_id') ? file.internal_id : file.id
    return {
        [CALL_API]: {
            types: [
                {
                    type: warrantyActions.ATTACHMENT_DELETE_REQUEST,
                    payload: {
                        id: file.id,
                        warranty_id: warranty.id
                    }
                },
                warrantyActions.ATTACHMENT_DELETE_SUCCESS,
                {
                    type: warrantyActions.ATTACHMENT_DELETE_ERROR,
                    meta: {
                        id: file.id,
                        warranty_id: warranty.id,
                        data: _pick(warranty, ['attachments'])
                    }
                }
            ],
            url: `/warranties/${warranty.id}/uploader/${backendId}.json`,
            method: 'DELETE',
            schema: Schemas.NOPARSE
        }
    }
}
