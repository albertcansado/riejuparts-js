import { CALL_API } from '../../middleware/api'
import Schemas from '../../constants/warranty/Schemas'

import * as historiesActions from '../../constants/warranty/ActionsHistories'
import { isHistoryFetched } from '../../reducers/warranty/'

const shouldFetchHistories = (state) => {
    return !isHistoryFetched(state)
}

const fetchModels = (number) => {
    return {
        [CALL_API]: {
            types: [
                historiesActions.HISTORIES_FETCH,
                historiesActions.HISTORIES_SUCCESS,
                historiesActions.HISTORIES_ERROR
            ],
            url: `/warranties/history.json?n=${number}`,
            schema: Schemas.HISTORYAPI
        }
    }
}

export const fetchHistoriesIfNeeded = (number) => {
    return (dispatch, getState) => {
        if (shouldFetchHistories(getState())) {
            return dispatch(fetchModels(number))
        } else {
            return Promise.resolve()
        }
    }
}
