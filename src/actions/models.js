import { CALL_API } from '../middleware/api'
import Schemas from '../constants/Schemas'
import * as modelsActions from '../constants/ActionModels'

import { setLoading, setInitFinished } from './helpers'
import { isModelsFetched, isVersionFetched, isCardFetched } from '../reducers/entities'
// import { isInitFinish } from '../reducers/helpers'

// Categories
/*const shouldFetchCategories = (state) => {
    return !state.helpers.categories.fetch
}*/

const fetchCategories = () => {
    return {
        [CALL_API]: {
            types: [modelsActions.CATEGORIES_FETCH, modelsActions.CATEGORIES_SUCCESS, modelsActions.CATEGORIES_ERROR],
            url: '/catalog.json',
            schema: Schemas.CATEGORIES
        }
    }
}

/*export const fetchCatergoriesIfNeeded = () => {
    return (dispatch, getState) => {
        if (shouldFetchCategories(getState())) {
            return dispatch(fetchCategories())
        }
    }
}*/


// Models - Example all MRT models
//
const shouldFetchModels = (state, name) => {
    return !isModelsFetched(state, name)
}

const fetchModels = (name) => {
    return {
        [CALL_API]: {
            types: [
                {
                    type: modelsActions.MODELS_FETCH,
                    payload: name
                },
                {
                    type: modelsActions.MODELS_SUCCESS,
                    meta: name
                },
                modelsActions.MODELS_ERROR
            ],
            url: `/catalog/${name}.json`,
            schema: Schemas.MODELS
        }
    }
}

export const fetchModelsIfNeeded = (categoryName) => {
    return (dispatch, getState) => {
        if (shouldFetchModels(getState(), categoryName)) {
            return Promise.all([
                dispatch(setLoading(true)),
                dispatch(fetchModels(categoryName))
                .then(() => {
                    dispatch(setLoading(false))
                })
            ])
        } else {
            return dispatch(setLoading(false))
        }
    }
}


// Version - Example MRT 50 PRO - Red Jump
//
const shouldFetchVersions = (state, verId) => {
    return !isVersionFetched(state, verId)
}

const fetchVersion = (cat, verId) => {
    return {
        [CALL_API]: {
            types: [
                {
                    type: modelsActions.VERSIONS_FETCH,
                    payload: verId
                },
                modelsActions.VERSIONS_SUCCESS,
                modelsActions.VERSIONS_ERROR
            ],
            url: `/catalog/${cat}/${verId}.json`,
            schema: Schemas.VERSION,
            meta: [verId]
        }
    }
}

export const fetchVersionsIfNeeded = (catName, verId) => {
    return (dispatch, getState) => {
        if (shouldFetchVersions(getState(), verId)) {
            return Promise.all([
                dispatch(setLoading(true)),
                dispatch(fetchVersion(catName, verId))
                .then(() => {
                    return dispatch(setLoading(false))
                })
            ])
        } else {
            return dispatch(setLoading(false))
        }
    }
}


// Cards - Example MRT 50 PRO - Red Jump (Chasis)
//
const shouldFetchCard = (state, cardAlias) => {
    return !isCardFetched(state, cardAlias)
}

const fetchCard = (verId, cardAlias, cardId) => {
    return {
        [CALL_API]: {
            types: [{
                    type: modelsActions.CARDS_FETCH,
                    payload: cardId
                }, {
                    type: modelsActions.CARDS_SUCCESS,
                    meta: cardId
                }, {
                    type: modelsActions.CARDS_ERROR,
                    meta: cardId
                }],
            url: `/cards/parts.json?i=${verId}&c=${cardId}`,
            schema: Schemas.NOPARSE
        }
    }
}

export const fetchCardIfNeeded = (verId, cardAlias, cardId) => {
    return (dispatch, getState) => {
        if (shouldFetchCard(getState(), cardId)) {
            return dispatch(fetchCard(verId, cardAlias, cardId))
        }

        return Promise.resolve()
    }
}


// Init Fetch
//
export const fetchDataOnInit = (request) => {
    return (dispatch, getState) => {
        // const state = getState()
        let urls = [dispatch(fetchCategories())]

        if (request.category) {
            urls.push(dispatch(fetchModels(request.category)))
        }

        if (request.version) {
            urls.push(dispatch(fetchVersion(request.category, request.version)))
        }

        /*if (request.card) {
            urls.push(dispatch(fetchVersion(request.category, request.version)))
            dispatch (
                fetchCardIfNeeded(request.category, request.version, request.card)
            )*/

        return Promise.all(urls)
            .then(() => {
                return dispatch(setInitFinished())
            })
    }
}
