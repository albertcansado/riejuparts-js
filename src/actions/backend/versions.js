import { pick as _pick } from 'lodash'

import { CALL_API } from '../../middleware/api'
import Schemas from '../../constants/backend/Schemas'
import * as versionsActions from '../../constants/backend/ActionsVersions'

import { isVersionFetched, getVersion, getEntity } from '../../reducers/backend/entities'

// Fetch
const shouldFetchVersion = (state, id) => {
    return !isVersionFetched(state, id)
}

const fetchVersion = (id) => {
    return {
        [CALL_API]: {
            types: [
                {
                    type: versionsActions.VERSION_FETCH_REQUEST,
                    payload: id
                },
                versionsActions.VERSION_FETCH_SUCCESS,
                versionsActions.VERSION_FETCH_ERROR,
            ],
            url: `/d/versions/${id}.json`,
            method: 'GET',
            schema: Schemas.VERSION
        }
    }
}

export const fetchVersionPartsIfNeeded = (id) => {
    return (dispatch, getState) => {
        if (shouldFetchVersion(getState(), id)) {
            return dispatch(
                fetchVersion(id)
            )
        }
    }
}

// Create
export const createVersion = (data) => {
    return {
        [CALL_API]: {
            types: [
                versionsActions.VERSION_CREATE_REQUEST,
                {
                    type: versionsActions.VERSION_CREATE_SUCCESS,
                    meta: data.model_id
                },
                versionsActions.VERSION_CREATE_ERROR,
            ],
            url: `/d/versions.json`,
            method: 'PUT',
            body: data,
            schema: Schemas.VERSION
        }
    }
}

// Update
export const updateVersion = (version, data) => {
    return {
        [CALL_API]: {
            types: [
                {
                    type: versionsActions.VERSION_UPDATE_REQUEST,
                    payload: {
                        id: version.id,
                        data
                    }
                },
                versionsActions.VERSION_UPDATE_SUCCESS,
                {
                    type: versionsActions.VERSION_UPDATE_ERROR,
                    meta: {
                        id: version.id,
                        data: _pick(version, Object.keys(data))
                    }
                }
            ],
            url: `/d/versions/${version.id}.json`,
            method: 'POST',
            body: data,
            schema: Schemas.NOPARSE
        }
    }
}

// Enable / Disable Version
export const enableDisableVersion = (version) => {
    const nextActive = !version.active
    return {
        [CALL_API]: {
            types: [
                {
                    type: versionsActions.VERSION_UPDATE_REQUEST,
                    payload: {
                        id: version.id,
                        data: {
                            active: nextActive
                        }
                    }
                },
                versionsActions.VERSION_UPDATE_SUCCESS,
                {
                    type: versionsActions.VERSION_UPDATE_ERROR,
                    meta: {
                        id: version.id,
                        data: {
                            active: !nextActive
                        }
                    }
                }
            ],
            url: `/d/versions/${version.id}/enable.json`,
            method: 'POST',
            body: {
                enable: nextActive
            },
            schema: Schemas.NOPARSE
        }
    }
}

// Delete a version
export const removeVersion = (version) => {
    return (dispatch, getState) => {
        const model = getEntity(getState(), version.model_id)

        dispatch({
            [CALL_API]: {
                types: [
                    {
                        type: versionsActions.VERSION_REMOVE_REQUEST,
                        payload: {
                            id: version.id,
                            model_id: version.model_id
                        }
                    },
                    versionsActions.VERSION_REMOVE_SUCCESS,
                    {
                        type: versionsActions.VERSION_REMOVE_ERROR,
                        meta: _pick(model, ['versions'])
                    }
                ],
                url: `/d/versions/${version.id}.json`,
                method: 'DELETE'
            }
        })
    }
}


// Upload Image to Version
//
export const uploadVersionImage = (version, file) => {
    const data = new FormData()
    data.append('image', file)

    return {
        [CALL_API]: {
            types: [
                versionsActions.VERSION_UPLOAD_REQUEST,
                versionsActions.VERSION_UPLOAD_SUCCESS,
                {
                    type: versionsActions.VERSION_UPLOAD_ERROR,
                    meta: {
                        id: version.id,
                        image: version.image
                    }
                }
            ],
            url: `/d/versions/${version.id}/uploader.json`,
            method: 'POST',
            body: data,
            schema: Schemas.NOPARSE
        }
    }
}


// Modifications
//
export const addModification = (version_id, data) => {
    return (dispatch, getState) => {
        const version = getVersion(getState(), version_id)
        const draftId = new Date().getTime()

        // Extract part
        const part = data.part
        data.part = part.id

        return dispatch({
            [CALL_API]: {
                types: [
                    {
                        type: versionsActions.MODIFICATION_ADD_REQUEST,
                        payload: {
                            id: draftId,
                            version_id,
                            data,
                            part
                        }
                    },
                    {
                        type: versionsActions.MODIFICATION_ADD_SUCCESS,
                        meta: draftId
                    },
                    {
                        type: versionsActions.MODIFICATION_ADD_ERROR,
                        meta: {
                            id: draftId,
                            version_id,
                            data: _pick(version, ['cards_mods'])
                        }
                    }
                ],
                url: `/d/versions/${version_id}/modifications.json`,
                method: 'PUT',
                body: {
                    ...data,
                    part_id: data.part
                },
                schema: Schemas.NOPARSE
            }
        })
    }
}


export const editModification = (modification, versionId, data) => {
    const backendId = modification.internal_id || modification.id

    // Extract part
    const part = data.part
    data.part = part.id

    return {
        [CALL_API]: {
            types: [
                {
                    type: versionsActions.MODIFICATION_UPDATE_REQUEST,
                    payload: {
                        id: modification.id,
                        data,
                        part
                    }
                },
                versionsActions.MODIFICATION_UPDATE_SUCCESS,
                {
                    type: versionsActions.MODIFICATION_UPDATE_ERROR,
                    meta: {
                        id: modification.id,
                        data: _pick(modification, Object.keys(data))
                    }
                }
            ],
            url: `/d/versions/${versionId}/modifications/${backendId}.json`,
            method: 'POST',
            body: {
                ...data,
                part_id: data.part
            },
            schema: Schemas.NOPARSE
        }
    }
}


export const deleteModification = (version_id, modification) => {
    return (dispatch, getState) => {
        const version = getVersion(getState(), version_id)

        const backendId = modification.internal_id || modification.id
        return dispatch({
            [CALL_API]: {
                types: [
                    {
                        type: versionsActions.MODIFICATION_REMOVE_REQUEST,
                        payload: {
                            version_id,
                            mod_id: modification.id
                        }
                    },
                    versionsActions.MODIFICATION_REMOVE_SUCCESS,
                    {
                        type: versionsActions.MODIFICATION_REMOVE_ERROR,
                        meta: {
                            version_id,
                            mod_id: modification.id,
                            data: _pick(version, ['cards_mods'])
                        }
                    }
                ],
                url: `/d/versions/${version_id}/modifications/${backendId}.json`,
                method: 'DELETE',
                schema: Schemas.NOPARSE
            }
        })
    }
}
