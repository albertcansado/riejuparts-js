import { pick as _pick } from 'lodash'

import { CALL_API } from '../../middleware/api'
import Schemas from '../../constants/backend/Schemas'
import * as cardsActions from '../../constants/backend/ActionsCards'

import { isCardLinesFetched, getEntity } from '../../reducers/backend/entities'

// Cards
//
const shouldFetchCardLines = (state, id) => {
    return !isCardLinesFetched(state, id)
}

const fetchCard = (id) => {
    return {
        [CALL_API]: {
            types: [
                {
                    type: cardsActions.CARD_FETCH,
                    payload: id
                },
                {
                    type: cardsActions.CARD_SUCCESS,
                    meta: id
                },
                cardsActions.CARD_ERROR
            ],
            url: `/d/cards/lines/${id}.json`,
            schema: Schemas.CARD
        }
    }
}


// Create
export const createCard = (data) => {
    return {
        [CALL_API]: {
            types: [
                cardsActions.CARD_CREATE_REQUEST,
                {
                    type: cardsActions.CARD_CREATE_SUCCESS,
                    meta: data.foreign_key
                },
                cardsActions.CARD_CREATE_ERROR,
            ],
            url: `/d/cards.json`,
            method: 'POST',
            body: data,
            schema: Schemas.CARD
        }
    }
}


// Update
export const updateCard = (card, data) => {
    return {
        [CALL_API]: {
            types: [
                {
                    type: cardsActions.CARD_UPDATE_REQUEST,
                    payload: {
                        card_id: card.id,
                        data
                    }
                },
                cardsActions.CARD_UPDATE_SUCCESS,
                {
                    type: cardsActions.CARD_UPDATE_ERROR,
                    meta: {
                        id: card.id,
                        data: _pick(card, Object.keys(data))
                    }
                }
            ],
            url: `/d/cards/${card.id}.json`,
            method: 'POST',
            body: data,
            schema: Schemas.NOPARSE
        }
    }
}


// Enable / Disable Card
export const enableDisableCard = (card) => {
    const nextActive = !card.active
    return {
        [CALL_API]: {
            types: [
                {
                    type: cardsActions.CARD_UPDATE_REQUEST,
                    payload: {
                        card_id: card.id,
                        data: {
                            active: nextActive
                        }
                    }
                },
                cardsActions.CARD_UPDATE_SUCCESS,
                {
                    type: cardsActions.CARD_UPDATE_ERROR,
                    meta: {
                        id: card.id,
                        data: {
                            active: !nextActive
                        }
                    }
                }
            ],
            url: `/d/cards/enable/${card.id}.json`,
            method: 'POST',
            body: {
                enable: nextActive
            },
            schema: Schemas.NOPARSE
        }
    }
}


// Upload Image to Card
//
export const uploadCardImage = (card, file) => {
    const data = new FormData()
    data.append('image', file)

    return {
        [CALL_API]: {
            types: [
                cardsActions.CARD_UPLOAD_REQUEST,
                cardsActions.CARD_UPLOAD_SUCCESS,
                {
                    type: cardsActions.CARD_UPLOAD_ERROR,
                    meta: {
                        id: card.id,
                        image: card.image
                    }
                }
            ],
            url: `/d/cards/uploader/${card.id}.json`,
            method: 'POST',
            body: data,
            schema: Schemas.NOPARSE
        }
    }
}


// Delete a Card
const fetchDelete = (card, entity) => {
    return {
        [CALL_API]: {
            types: [
                {
                    type: cardsActions.CARD_DELETE_REQUEST,
                    payload: card
                },
                cardsActions.CARD_DELETE_SUCCESS,
                {
                    type: cardsActions.CARD_DELETE_ERROR,
                    meta: {
                        id: card.id,
                        item: _pick(entity, ['id', 'cards'])
                    }
                }
            ],
            url: `/d/cards/${card.id}.json`,
            method: 'DELETE',
            schema: Schemas.NOPARSE
        }
    }
}

export const deleteCard = (card) => {
    return (dispatch, getState) => {
        return dispatch(fetchDelete(
            card,
            getEntity(getState(), card.foreign_key)
        ))
    }
}


// LINES
//
export const fetchCardLinesIfNeeded = (id) => {
    return (dispatch, getState) => {
        if (shouldFetchCardLines(getState(), id)) {
            return dispatch(fetchCard(id))
        }
    }
}

export const addCardLine = (card_id, data) => {
    const draftId = new Date().getTime()
    return {
        [CALL_API]: {
            types: [
                {
                    type: cardsActions.CARD_LINE_ADD_REQUEST,
                    payload: {
                        id: draftId,
                        part: data.part,
                        line: {
                            number: data.number,
                            info: data.info,
                            qty: data.qty
                        },
                        card_id
                    }
                },
                {
                    type: cardsActions.CARD_LINE_ADD_SUCCESS,
                    meta: draftId
                },
                {
                    type: cardsActions.CARD_LINE_ADD_ERROR,
                    meta: {
                        line_id: draftId,
                        card_id
                    }
                }
            ],
            url: '/d/cards/lines.json',
            method: 'POST',
            body: {
                card_id,
                part_id: data.part.id,
                info: data.info,
                number: data.number,
                qty: data.qty
            },
            schema: Schemas.NOPARSE
        }
    }
}

export const updateCardLine = (card_id, line, data) => {
    const backendId = line.internal_id || line.id
    return {
        [CALL_API]: {
            types: [
                {
                    type: cardsActions.CARD_LINE_UPDATE_REQUEST,
                    payload: {
                        id: line.id,
                        data
                    }
                },
                cardsActions.CARD_LINE_UPDATE_SUCCESS,
                {
                    type: cardsActions.CARD_LINE_UPDATE_ERROR,
                    meta: {
                        id: line.id,
                        data: _pick(line, ['internal_id', 'number', 'qty', 'part', 'info'])
                    }
                }
            ],
            url: `/d/cards/lines/${backendId}.json`,
            method: 'POST',
            body: {
                card_id,
                part_id: data.part.id,
                number: data.number,
                qty: data.qty,
                info: data.info
            },
            schema: Schemas.NOPARSE
        }
    }
}


// Delete Card Line
export const deleteCardLine = (line) => {
    const backendId = line.internal_id || line.id
    return {
        [CALL_API]: {
            types: [
                {
                    type: cardsActions.CARD_LINE_DELETE_FETCH,
                    payload: line.id
                },
                cardsActions.CARD_LINE_DELETE_SUCCESS,
                {
                    type: cardsActions.CARD_LINE_DELETE_ERROR,
                    meta: line.id
                }
            ],
            url: `/d/cards/lines/${backendId}.json`,
            method: 'DELETE',
            schema: Schemas.NOPARSE
        }
    }
}
