import { pick as _pick } from 'lodash'

import { CALL_API } from '../../middleware/api'
import Schemas from '../../constants/backend/Schemas'
import * as entitiesActions from '../../constants/backend/ActionsEntities'

import { isEntityFetched } from '../../reducers/backend/entities'
import { setLoading } from '../helpers'

// Global functions
//
const shouldFetchEntity = (state, id) => {
    return !isEntityFetched(state, id)
}

const fetchUpdate = ({ entity, data, url }) => {
    return {
        [CALL_API]: {
            types: [
                {
                    type: entitiesActions.ENTITY_UPDATE_REQUEST,
                    payload: {
                        id: entity.id,
                        data
                    }
                },
                entitiesActions.ENTITY_UPDATE_SUCCESS,
                {
                    type: entitiesActions.ENTITY_UPDATE_ERROR,
                    meta: {
                        id: entity.id,
                        data: _pick(entity, Object.keys(data))
                    }
                }
            ],
            url: url,
            method: 'POST',
            body: data,
            schema: Schemas.NOPARSE
        }
    }
}

export const fetchUpdateEntityCardOrders = (entity, cards) => {
    return {
        [CALL_API]: {
            types: [
                {
                    type: entitiesActions.ENTITY_UPDATE_CARDORDER_REQUEST,
                    payload: {
                        id: entity.id,
                        data: {
                            cards
                        }
                    }
                },
                entitiesActions.ENTITY_UPDATE_CARDORDER_SUCCESS,
                {
                    type: entitiesActions.ENTITY_UPDATE_CARDORDER_ERROR,
                    meta: {
                        id: entity.id,
                        data: _pick(entity, ['cards'])
                    }
                }
            ],
            url: `/d/cards/reorder.json`,
            method: 'POST',
            body: {
                cards,
                "foreign_key": entity.id
            },
            schema: Schemas.NOPARSE
        }
    }
}


// Engines
//
const fetchEngine = (id) => {
    return {
        [CALL_API]: {
            types: [
                {
                    type: entitiesActions.ENTITY_FETCH,
                    payload: id
                },
                {
                    type: entitiesActions.ENTITY_SUCCESS,
                    meta: id
                },
                entitiesActions.ENTITY_ERROR
            ],
            url: `/d/engines/${id}.json`,
            schema: Schemas.ENTITY
        }
    }
}

export const fetchEngineIfNeeded = (id) => {
    return (dispatch, getState) => {
        if (shouldFetchEntity(getState(), id)) {
            return Promise.all([
                dispatch(setLoading(true)),
                dispatch(fetchEngine(id))
                .then(() => {
                    dispatch(setLoading(false))
                })
            ])
        } else {
            return dispatch(setLoading(false))
        }
    }
}

export const updateEngine = (entity, data) => {
    return fetchUpdate({
        entity,
        data,
        url: `/d/engines/${entity.id}.json`
    })
}


// Models
//
const fetchModel = (id) => {
    return {
        [CALL_API]: {
            types: [
                {
                    type: entitiesActions.ENTITY_FETCH,
                    payload: id
                },
                {
                    type: entitiesActions.ENTITY_SUCCESS,
                    meta: id
                },
                entitiesActions.ENTITY_ERROR
            ],
            url: `/d/models/${id}.json`,
            schema: Schemas.ENTITY
        }
    }
}

export const fetchModelIfNeeded = (id) => {
    return (dispatch, getState) => {
        if (shouldFetchEntity(getState(), id)) {
            return Promise.all([
                dispatch(setLoading(true)),
                dispatch(fetchModel(id))
                .then(() => {
                    dispatch(setLoading(false))
                })
            ])
        } else {
            return dispatch(setLoading(false))
        }
    }
}

// Update
export const updateModel = (entity, data) => {
    return fetchUpdate({
        entity,
        data,
        url: `/d/models/${entity.id}.json`
    })
}

// Attach
export const attachEngine = (model, data) => {
    const draft_id = new Date().getTime()
    return {
        [CALL_API]: {
            types: [
                {
                    type: entitiesActions.ATTACH_ENGINE_REQUEST,
                    payload: {
                        id: model.id,
                        draft_id,
                        data
                    }
                },
                {
                    type: entitiesActions.ATTACH_ENGINE_SUCCESS,
                    meta: draft_id
                },
                {
                    type: entitiesActions.ATTACH_ENGINE_ERROR,
                    meta: {
                        entity_id: model.id,
                        draft_id,
                        data: _pick(model, 'engines')
                    }
                }
            ],
            url: `/d/models/attach/${model.id}.json`,
            method: 'POST',
            body: data,
            schema: Schemas.NOPARSE
        }
    }
}

// Unattach
export const unattachEngine = (model, engine) => {
    return {
        [CALL_API]: {
            types: [
                {
                    type: entitiesActions.UNATTACH_ENGINE_REQUEST,
                    payload: {
                        id: engine.id,
                        entity_id: model.id,
                        engine_id: engine.engine_id
                    }
                },
                entitiesActions.UNATTACH_ENGINE_SUCCESS,
                {
                    type: entitiesActions.UNATTACH_ENGINE_ERROR,
                    meta: {
                        entity_id: model.id,
                        data: _pick(model, 'engines')
                    }
                }
            ],
            url: `/d/models/unattach/${model.id}.json`,
            method: 'POST',
            body: {
                id: engine.id,
                engine_id: engine.engine_id
            },
            schema: Schemas.NOPARSE
        }
    }
}
