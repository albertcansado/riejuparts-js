import { CALL_API } from '../middleware/api'
import Schemas from '../constants/Schemas'
import * as productsActions from '../constants/ActionProducts'

import { setInitProductsFinished } from './helpers'
import { isInitProductFinish } from '../reducers/helpers'


// Products
//
const shouldFetchProducts = (state) => {
    return !isInitProductFinish(state)
}

const fetchProducts = () => {
    return {
        [CALL_API]: {
            types: [
                productsActions.PRODUCTS_FETCH,
                productsActions.PRODUCTS_SUCCESS,
                productsActions.PRODUCTS_ERROR
            ],
            url: `/products.json`,
            schema: Schemas.PRODUCTS
        }
    }
}

export const fetchProductIfNeeded = () => {
    return (dispatch, getState) => {
        if (shouldFetchProducts(getState())) {
            return dispatch(fetchProducts())
                .then(() => {
                    dispatch(setInitProductsFinished())
                })
        }

        return Promise.resolve()
    }
}
