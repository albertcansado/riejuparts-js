import React from 'react'
import { render } from 'react-dom'

import Root from './container/RootWarranty'
import configureStore from './store/configureWarrantyStore'

const store = configureStore()

const rootWarranty = document.getElementById('root-warranty')
if (rootWarranty) {
    render(
        <Root store={store} />,
        rootWarranty
    )
}
