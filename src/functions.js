import { u } from 'umbrellajs'
import swal from 'sweetalert2'
import { on } from 'delegated-events'
import flatpickr from 'flatpickr'
import idx from 'idx'
import Cookies from 'js-cookie'

require('lib/webfontload')

import { fetchRequest } from './middleware/api'

import Dropdown from './lib/dropdown'
import Tabs from './lib/tabs'
import Collapse from './lib/collapse'
import Nav from './lib/nav'
import Modal from './lib/modal'

// Init Components
new Dropdown('[data-dropdown]')
new Tabs('[data-tab]')
const collapseItems = new Collapse('[data-collapse]')
const modal = new Modal()

// Aside
const wrapper = document.querySelector('.wrapper')
on('click', '.js-open-aside', ev => {
    const target = ev.currentTarget
    target.classList[(target.classList.contains('is-open')) ? 'remove' : 'add']('is-open')

    if (wrapper.classList.contains('menu-is-open')) {
        wrapper.classList.remove('menu-is-open')
    } else {
        wrapper.classList.add('menu-is-open')
    }
})


// Menu
//
const asideEl = document.querySelector('.global__aside')
const navEl = document.querySelector('.global__nav')
let maxChildrenHeight = 350
if (asideEl && navEl) {
    maxChildrenHeight = asideEl.offsetHeight - navEl.offsetHeight - 20
}

new Nav('.global__nav', {
    childrenMaxHeight: maxChildrenHeight
})


// Change Cart
//
on('click', '.js-changecart', ev => {
    window.localStorage.removeItem('order_id')
    /*if (ev.currentTarget.hasAttribute('data-id')) {
        window.changeOrder(ev.currentTarget.getAttribute('data-id'))
    }*/
})

const alertCallbacks = {
    deleteOrder: () => {
        window.localStorage.removeItem('order_id')
    }
}

const triggerAlert = ev => {
    ev.preventDefault()

    const target = ev.currentTarget

    const callback = target.getAttribute('data-alert-fn')
    const swType = target.getAttribute('data-alert-type') || 'warning'
    const swTitle = target.getAttribute('data-alert-title') || ''
    const swText = target.getAttribute('data-alert') || ''
    const swConfirm = target.getAttribute('data-alert-confirm') || idx(window.__LOCALE, _ => _.messages["app.alert.confirm"])
    const swCancel = target.getAttribute('data-alert-cancel') || idx(window.__LOCALE, _ => _.messages["app.alert.cancel"])
    const swAsync = Number.parseInt(target.getAttribute('data-alert-async'), 10) || false

    const stopProp = target.getAttribute('data-stop') || false

    if (stopProp) {
        ev.stopPropagation()
    }

    swal({
        title: swTitle,
        text: swText,
        type: swType,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonColor: "#30b19b",
        confirmButtonText: swConfirm,
        cancelButtonText: swCancel
    }).then(
        () => {
            if (alertCallbacks.hasOwnProperty(callback)) {
                alertCallbacks[callback]()
            }

            if (target.hasAttribute('data-alert-form')) {
                document[target.getAttribute('data-alert-form')].submit()
            } else if (target.hasAttribute('href')) {
                if (swAsync) {
                    fetchRequest(target.getAttribute('href'))
                } else {
                    window.location.href = target.getAttribute('href')
                }
            }
        }
    ).catch(swal.noop)
}

on('click', '[data-alert]', triggerAlert)


// Manuals
//
on('click', '[data-toggle]', ev => {
    ev.preventDefault()

    const el = u(ev.currentTarget).closest('.tree__item')

    el.toggleClass('is-open')
})


// Modal
//
on('click', '[data-modal]', (ev) => {
    if (!ev.target.hasAttribute('data-modal') && u(ev.target).closest('.js-nomodal').length) {
        return
    }

    ev.preventDefault()
    //ev.stopPropagation()

    const url = ev.currentTarget.getAttribute('data-url') || ev.currentTarget.getAttribute('href')
    if (url) {
        modal.openAsync(url)
    }
})


// Calendar
//
flatpickr('[data-datetime]', {
    dateFormat: 'd/m/Y',
    locale: window.flatpickr ? window.flatpickr.l10ns[window.__LOCALE.locale] : 'en',
    onReady: function (dateObj, dateStr, instance) {
        if (!instance.calendarContainer.querySelector('.flatpickr-clear')) {
            const div = document.createElement('div')
            div.className = 'flatpickr-clear'
            div.innerText = idx(window.__LOCALE, _ => _.messages['app.clear'])
            div.onclick = ev => {
                instance.clear()
                instance.close()
            }

            instance.calendarContainer.append(div)
        }
    }
})


// Collapse All
//
if (collapseItems.length > 0) {
    const collapseAllBtn = document.querySelector('.js-collapse-all')
    if (collapseAllBtn) {
        collapseAllBtn.onclick = ev => {
            ev.preventDefault()

            collapseItems.map(it => it.close())
        }
    }
}

var defaults = {
	duration: 500,
    offset: 0,
    effect: 'easeInOut'
}

function scrollTo(element, to, options = {}) {
	const opts = Object.assign({}, defaults, options)
    var start = element.scrollTop,
        change = to - element.offsetTop - start + opts.offset,
        increment = 20;

    var animateScroll = function(elapsedTime) {
        elapsedTime += increment;
        var position = easeInOut(elapsedTime, start, change, opts.duration);
        element.scrollTop = position;
        if (elapsedTime < opts.duration) {
            setTimeout(function() {
                animateScroll(elapsedTime);
            }, increment);
        }
    };

    animateScroll(0);
}

function easeInOut(currentTime, start, change, duration) {
    currentTime /= duration / 2;
    if (currentTime < 1) {
        return change / 2 * currentTime * currentTime + start;
    }
    currentTime -= 1;
    return -change / 2 * (currentTime * (currentTime - 2) - 1) + start;
}

/*function linear(currentTime, start, change, duration) {
	return (change / duration) * currentTime
}*/

// Anchor
//
on('click', '.js-anchor', ev => {
    ev.preventDefault()

    const $this = ev.currentTarget
    const selector = $this.getAttribute('href')
    const target = document.querySelector(selector)
    if (!target) {
        return
    }

    let start = null
    if ($this.hasAttribute('data-source')) {
        try {
            start = document.querySelector($this.getAttribute('data-source'))
        } catch (e) {}
    }

    if (!start) {
        start = document.body
    }

    scrollTo(start, target.offsetTop)
})


// Post Link
//
on('click', '.js-postlink', ev => {
    ev.preventDefault()

    const el = ev.currentTarget
    if (!el.hasAttribute('href')) {
        return
    }

    const form = document.createElement('form')
    form.method = 'POST'
    form.action = el.getAttribute('href')
    form.style.visibility = 'hidden'

    // forms cannot be submitted outside of body
    document.body.appendChild(form);
    form.submit();
})


// Select Input
//
const checkSelectValue = (target) => {
    u(target)[target.value === '' ? 'addClass' : 'removeClass']('is-empty')
}
on('change', 'select.input__field', ev => checkSelectValue(ev.currentTarget))


// Warranties
//
const redirectTo = (url) => {
    if (url) {
        window.location = url
    }
}

on('click', '.js-warranty-new', ev => {
    ev.preventDefault()

    swal({
        title: idx(window.__LOCALE, _ => _.messages["warranty.newAlert.title"]),
        input: 'text',
        inputPlaceholder: idx(window.__LOCALE, _ => _.messages["warranty.newAlert.placeholder"]),
        inputValidator: value => {
            return new Promise((resolve, reject) => {
                if (value) {
                    resolve()
                } else {
                    reject(idx(window.__LOCALE, _ => _.messages["warranty.newAlert.required"]))
                }
            })
        },
        showCancelButton: true,
        confirmButtonColor: "#30b19b",
        confirmButtonText: idx(window.__LOCALE, _ => _.messages["app.alert.confirm"]),
        cancelButtonText: idx(window.__LOCALE, _ => _.messages["app.alert.cancel"]),
        showLoaderOnConfirm: true,
        preConfirm: function (chasis) {
            return fetchRequest('/warranties/create.json', {
                method: 'POST',
                body: {
                    c: chasis
                }
            }).then(({ json, response }) => {
                if (!response.ok) {
                    return Promise.reject('Error')
                }

                return json
            }).catch(response => {
                return Promise.reject('Error')
            })
        }
    }).then(json => {
        if (!json.success) {
            swal({
                type: 'error',
                title: idx(window.__LOCALE, _ => _.messages["warranty.newAlert.notavailableTitle"]),
                text: json.error.msg
            }).catch(swal.noop)
        } else {
            swal({
                type: 'success',
                title: idx(window.__LOCALE, _ => _.messages["warranty.newAlert.availableTitle"]),
                text: json.data.msg,
                confirmButtonText: idx(window.__LOCALE, _ => _.messages["warranty.newAlert.availableConfirm"])
            })
            .then(redirectTo(json.data.redirect))
            .catch(swal.noop)

            setTimeout(() => redirectTo(json.data.redirect), 5000)
        }
    })
    .catch(swal.noop)
})

// WarrantyHistory
//
on('click', '.js-warranty-history', ev => {
    ev.preventDefault()

    const parent = ev.currentTarget.parentNode
    const input = parent.querySelector('input')

    const url = parent.getAttribute('data-url')
    const number = input ? input.value : null

    if (!url || !number) {
        return
    }

    modal.openAsync(`${url}?n=${number}`)
})


// Display
//
on('click', '.js-display', ev => {
    const modelId = ev.currentTarget.getAttribute('data-id')

    if (!modelId) {
        return
    }

    // Html
    const text = idx(window.__LOCALE, _ => _.messages["app.display.text"])
    const html = []
    html.push('<p class="u-mgb--20 txt-left txt--medium">')
    html.push(text ? text.replace('{example}', '<i>'+ idx(window.__LOCALE, _ => _.messages["app.display.example"]) +'</i>') : '')
    html.push('</p>')
    html.push('<div class="input input--text"><input type="text" class="input__field" placeholder="')
    html.push(idx(window.__LOCALE, _ => _.messages["app.display.price"]))
    html.push('" id="displayPrice" /></div><div class="input input--textarea"><textarea id="displayComment" placeholder="')
    html.push(idx(window.__LOCALE, _ => _.messages["app.display.textarea"]))
    html.push('" class="input__field"></textarea></div>')

    swal({
        title: idx(window.__LOCALE, _ => _.messages["app.display.title"]) || "Get a display",
        html: html.join(""),
        showCancelButton: true,
        confirmButtonColor: "#30b19b",
        confirmButtonText: idx(window.__LOCALE, _ => _.messages["app.display.alert.ok"]) || "Ok",
        cancelButtonText: idx(window.__LOCALE, _ => _.messages["app.alert.cancel"]) || "Cancel",
        preConfirm: () => {
            return new Promise(resolve => {
                resolve([
                    document.querySelector('#displayPrice').value,
                    document.querySelector('#displayComment').value
                ])
            })
        }
    }).then (result => {
        const price = result[0] || 0
        const comment = btoa(unescape(encodeURIComponent(result[1] || '')));
        const lang = Cookies.get('locale').substr(0, 2)

        window.open(`https://rieju.es/pdf.php?i=${modelId}&l=${lang}&p=${price}&c=${comment}&pr=1`, '_blank')
    }).catch(swal.noop)
})


// Subdealers Menu
//
const appendBadge = (el, content) => {
    const newNode = document.createElement('div')
    newNode.className = 'badge badge--error'
    newNode.innerText = content

    el.parentNode.insertBefore(newNode, el)
}

const hasSubdealersBtn = document.querySelector('.js-subdealersmenu')
if (hasSubdealersBtn) {
    fetchRequest('/subdealers/check-orders.json')
        .then(({ json, response }) => {
            if (!response.ok) {
                return Promise.reject('Error')
            }

            const count = idx(json, _ => _.data.count)
            if (count) {
                const caret = hasSubdealersBtn.querySelector('.nav__icon--caret')
                appendBadge(caret, count)
            }
        }).catch(response => {
            return Promise.reject('Error')
        })
}
