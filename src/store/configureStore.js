import { createStore, compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import reduxCatch from 'redux-catch'
import { addLocaleData } from 'react-intl'

import api from '../middleware/api'
import rootReducer from '../reducers'

const errorHandler = (error, getState, lastAction) => {
    console.error(error)
    console.debug('Current State', getState())
    console.debug('Action', lastAction)
}

/* Redux DevTools Browser extension */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middleware = [
    thunk,
    api,
    reduxCatch(errorHandler)
]

/* INIT STATE */
const initState = {
    intl: window.hasOwnProperty('__LOCALE') ? window.__LOCALE : {
        locale: 'en'
    }
}

if (window.hasOwnProperty('ReactIntlLocaleData')) {
    addLocaleData(window.ReactIntlLocaleData[initState.intl.locale])
}

export default function configureStore(preloadedState = initState) {
    const store = createStore(
        rootReducer,
        preloadedState,
        composeEnhancers(
            applyMiddleware(...middleware)
        )
    )

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducers', () => {
            const nextReducer = require('../reducers').default
            store.replaceReducer(nextReducer)
        })
    }

    return store
}
