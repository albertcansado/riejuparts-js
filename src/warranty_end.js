import intlWrapper from 'lib/intlWrapper'

import WarrantyContainer from './container/Backend/WarrantyContainer'

const getIdFromRequest = () => {
    const paths = window.location.pathname.substring(1).split('/')
    return paths[paths.length - 1]
}

const rootParts = document.getElementById('root-warranty')
if (rootParts) {
    intlWrapper({
        element: rootParts,
        component: WarrantyContainer,
        props: {
            id: getIdFromRequest()
        }
    })
}
