import { u } from 'umbrellajs'
import { on } from 'delegated-events'
import intlWrapper from 'lib/intlWrapper'
import ImageUploader from 'components/Form/ImageUploader'

import './scss/login.scss'

require('lib/webfontload')

on('click', '.js-showpassword', ev => {
    ev.preventDefault()

    const $div = u(ev.currentTarget)
        .closest('.input');
    const input = ev.currentTarget.previousSibling

    input.type = $div.hasClass('is-showing') ? 'password' : 'text'
    $div.toggleClass('is-showing')
})

const rootModel = document.querySelector('.js-dropzone')
if (rootModel) {
    intlWrapper({
        element: rootModel,
        component: ImageUploader,
        props: {
            labelTitle: rootModel.getAttribute('data-labelTitle'),
            name: rootModel.getAttribute('data-name'),
            image: rootModel.getAttribute('data-value')
        }
    })
}
