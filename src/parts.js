import intlWrapper from 'lib/intlWrapper'

import PartsTranslationsContainer from './container/Backend/Parts/PartsTranslationsContainer'
import PartsReplacementContainer from './container/Backend/Parts/PartsReplacementContainer'

const rootParts = document.getElementById('root-parts')
if (rootParts) {
    intlWrapper({
        element: rootParts,
        component: PartsTranslationsContainer
    })
}

const rootReplace = document.getElementById('root-parts-replace')
if (rootReplace) {
    intlWrapper({
        element: rootReplace,
        component: PartsReplacementContainer
    })
}
