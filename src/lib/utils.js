/* globals define, module, jQuery */
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define([], factory);
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory();
    } else {
        root.utils = factory();
    }
}(this, function () {

    function classReg( className ) {
        return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
    }

    var u = {};

    var classMethods = {
        has: null,
        add: null,
        remove: null
    };

    if ('classList' in document.documentElement) {
        classMethods.has = function (elem, c) {
            return elem.classList.contains(c);
        };

        classMethods.add = function (elem, c) {
            elem.classList.add(c);
        };

        classMethods.remove = function (elem, c) {
            elem.classList.remove(c);
        };
    } else {
        classMethods.has = function (elem, c) {
            return classReg(c).test(elem.className);
        };

        classMethods.add = function (elem, c) {
            if (!u.hasClass(elem, c)) {
                elem.className = elem.className + ' ' + c;
            }
        };

        classMethods.remove = function (elem, c) {
            elem.className = elem.className.replace(classReg(c), ' ');
        };
    }

    // Class
    u.hasClass = function(elem, c) {
        if (c[0] === '.') c = c.substring(1);
        return classMethods.has(elem, c);
    };
    u.addClass = function(elem, c) {
        if (u.isArrayLike(elem)) {
            u.foreach(elem, function(item) {
                u.addClass(item, c);
            });
            return;
        }
        if (u.isArray(c)) {
            u.foreach(c, function(item) {
                u.addClass(elem, item);
            });
        } else {
            classMethods.add(elem, c);
        }

        return this;
    };
    u.removeClass = function(elem, c) {
        if (u.isArrayLike(elem)) {
            u.foreach(elem, function(item) {
                u.removeClass(item, c);
            });
            return;
        }

        if (u.isArray(c)) {
            u.foreach(c, function(item) {
                u.removeClass(elem, item);
            });
        } else {
            classMethods.remove(elem, c);
        }

        return this;
    };
    u.toggleClass = function(elem, c) {
        var fn = u.hasClass(elem, c) ? u.removeClass : u.addClass;
        fn(elem, c);
    };

    // Events
    u.on = function(elem, event, listener) {
        if (u.isElement(elem) || elem === window || elem === document) {
            elem.addEventListener(event, listener.bind(event));
        } else if (u.isNodeList(elem)) {
            u.foreach(elem, function (item) {
                u.on(item, event, listener);
            });
            return;
        }
    };
    u.off = function(elem, event) {
        elem.removeEventListener(event);
    };
    u.trigger = function (elem, ev) {
        ev = (ev instanceof Event) ? ev : new Event(ev);
        elem.dispatchEvent(ev);
    };

    // is...
    u.isNull = function (foo) {
        return Boolean(foo === null);
    };
    u.isUndefined = function(foo) {
        return typeof foo === 'undefined';
    };
    u.isEmpty = function (foo) {
        if (foo == null) return true;
        if (this.isArray(foo) || this.isString(foo) || this.isArguments(foo)) return foo.length === 0;
        return this.keys(foo).length === 0;
    };
    u.isEmail = function(foo) {
        return /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$/.test(foo);
    };
    u.isElement = function (foo) {
        return !!(foo && foo.nodeType === 1);
    };
    u.isNodeList = function(foo) {
        // Work in IE??
        return !!(foo && !u.isUndefined(foo.length) && !u.isUndefined(foo.item));
    };
    u.isNumber = function (num) {
        var type = typeof num;
        if (type !== 'number' && type !== 'string') {
            return false;
        }
        var n = +num;
        return (n - n + 1) >= 0 && num !== '';
    };
    u.isObject = function (obj) {
        var type = typeof obj;
        return (type === 'function') || (type === 'object' && !! obj);
    };
    u.isJqObject = function(foo) {
        try {
            return foo instanceof jQuery;
        } catch (err) {
            return false;
        }
    };
    u.isFunction = function (foo) {
        return typeof foo === 'function';
    };
    u.isString = function(foo) {
        return typeof foo === 'string';
    };
    u.isArray = function (foo) {
        return Array.isArray(foo);
    };
    u.isArguments = function (foo) {
        return this.has(foo, 'callee');
    };
    u.isArrayLike = function (foo) {
        return this.isArray(foo) || this.isNodeList(foo) || this.isArguments(foo);
    };
    u.equalTag = function (el, tagName) {
        try {
            if (u.isArray(tagName)) {
                var result = false;
                var i = 0;
                var length = tagName.length;
                while (i < length && !result) {
                    result = u.equalTag(el, tagName[i]);
                    i++;
                }

                return result;
            }
            return el.tagName.toLowerCase() === tagName.toLowerCase();
        } catch (err) {
            return false;
        }
    };

    // Helpers
    u.has = function (obj, key) {
        if (u.isArray(obj)) {
            return obj.indexOf(key) !== -1;
        } else if (u.isObject(obj)) {
            return hasOwnProperty.call(obj, key);
        }

        return false;
    }

    u.keys = function (obj) {
        if (!this.isObject(obj)) return [];
        return Object.keys(obj);
    }

    u.prop = function (el, prop, enable) {
        enable = enable || true;
        if (prop === 'selected') {
            el[(enable ? 'setAttribute' : 'removeAttribute')](prop, prop);
        }
    }

    // Inserts and find
    u.insert = function(el, foo, isHtml) {
        isHtml = !u.isUndefined(isHtml) ? Boolean(isHtml) : true;
        if (u.isElement(foo)) {
            el.appendChild(foo);
        } else {
            el[isHtml ? 'innerHTML' : 'textContent'] = foo.toString();
        }
        return el;
    };
    u.html = function (el, foo) {
        if (u.isElement(el)) {
            el.innerHTML = foo.toString();
        }
    };
    u.value = function(el, val) {
        if (!u.isElement(el) || !u.equalTag(el, ['input', 'option', 'textarea', 'select'])) {
            new Error('element must be an input|option|textarea');
        }

        if (!u.isUndefined(val)) {
            el.value = val;
            u.trigger(el, 'change');
        }
        return el.value;
    };

    u.style = function (el, properties, values) {
        if ( !u.isElement(el) || u.isEmpty(properties) || (!u.isNumber(values) && u.isEmpty(values)) ) {
            return u;
        }

        var prop = null;
        var value = null;
        if (u.isArray(properties) && u.isArray(values)) {
            // Get and Transform property name
            prop = properties.shift();
            var hasDash = prop.indexOf('-');
            if (hasDash !== -1) {
                // see: https://github.com/jquery/jquery/blob/master/src/core.js#L289
                prop.replace(/-([a-z])/g, function (all, letter) {
                    return letter.toUpperCase();
                })
            }

            // Get and Transform property value
            value = values.shift();
        } else {
            prop = properties;
            value = values;

            properties = values = null;
        }

        if (prop && value) {
            el.style[prop] = value;
        }

        // Recursive
        return u.style(el, properties, values);
    }

    u.matchSelector = function (elem, selector, firstChar) {
        if (!selector) {
            return false;
        }

        if (u.isUndefined(firstChar)) {
            firstChar = selector.charAt(0);
        }
        var matched = false;

        if (firstChar === '.') {
            // Class
            matched = u.hasClass(elem, selector.substr(1));
        } else if (firstChar === '#') {
            // Id
            matched = (elem.id === selector.substr(1));
        } else if (firstChar === '[') {
            // Attribute
            matched = (elem.hasAttribute(selector.substr(1, selector.length - 1)));
        } else {
            // element tag
            matched = u.equalTag(elem, selector);
        }
        return matched;
    }

    u.getParents = function(elem, selector, stopWhenFind) {
        var parents = [];
        var firstChar = '';
        if (selector) {
            firstChar = selector.charAt(0);
        }

        if (u.matchSelector(elem, selector, firstChar)) {
            return [elem];
        }

        // First Parent
        elem = elem.parentNode;

        for ( ; elem && elem !== document; elem = elem.parentNode ) {

            if (selector) {
                if (u.matchSelector(elem, selector, firstChar)) {
                    parents.push(elem);
                }
            } else {
                parents.push(elem);
            }

            if (stopWhenFind && parents.length) {
                break;
            }
        }

        return parents;
    };

    u.getParent = function (elem, selector) {
        var result = u.getParents(elem, selector, true);
        return (result.length) ? result[0] : [];
    };

    u.remove = function (el) {
        if (u.isElement(el)) {
            if (el.parentNode) {
                el.parentNode.removeChild(el);
            }
        }
    };

    // For and extend
    u.foreach = function (el, callback) {
        if (u.isNodeList(el)) {
            [].slice.call(el).forEach(callback);
        } else {
            var i, length;
            for (i = 0, length = el.length; i < length; i++) {
                callback(el[i], i);
            }
        }
    };
    u.extend = function () {
        var extended = {};
        var merge = function (obj) {
            for (var prop in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                    if (u.isObject(obj[prop]) && !u.isFunction(obj[prop]) && !u.isArray(obj[prop])) {
                        extended[prop] = u.extend(extended[prop], obj[prop]);
                    }
                    else {
                        extended[prop] = obj[prop];
                    }
                }
            }
        };
        merge(arguments[0]);
        for (var i = 1, j = arguments.length; i < j; i++) {
            var obj = arguments[i];
            merge(obj);
        }
        return extended;
    };

    // Scroll
    u.scrollTop = function(obj) {
        obj = obj || window;
        return (obj.pageYOffset !== undefined) ? obj.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
    };
    u.scrollTo = function(element, target, time) {
        var scrollLoop = function (element, target, time) {
            var startOffset = element.scrollTop,
                endOffset = target.offsetTop;

            var diffOffset = endOffset - startOffset;

            // time when scroll starts
            var startTime = new Date().getTime(),

            // set an interval to update scrollTop attribute every 25 ms
            timer = setInterval(function() {

                // calculate the step, i.e the degree of completion of the smooth scroll
                var step = Math.min(1, (new Date().getTime() - startTime) / time);

                // calculate the scroll distance and update the scrollTop
                element.scrollTop = (startOffset + (step * diffOffset));

                // end interval if the scroll is completed
                if (step === 1) clearInterval(timer);
            }, 25);
        };

        if (u.isArray(element)) {
            u.foreach(element, function (el) {
                scrollLoop(el, target, time);
            });
        } else if (u.isElement(element)) {
            scrollLoop(element, target, time);
        }
    };

    u.recursiveProp = function (obj, path, value, delimiter) {
        var delm = delimiter || '.';
        if (u.isString(path)) {
            path = path.split(delm);
        }

        if(path.length > 1) {
            var key = path.shift();
            if (u.isNull(obj[key]) || !u.isObject(obj[key])) {
                obj[key] = {};
            }
            return u.recursiveProp(obj[key], path, value, delm);
        } else {
            if (!u.isUndefined(value)) {
                obj[path[0]] = value;
            }
            return obj[path[0]];
        }
    };

    return u;
}));
