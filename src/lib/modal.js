/* globals define, module, require */
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['./utils'], factory);
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(require('utils.js'));
    } else {
      root.Modal = factory();
  }
}(this, function(utils) {

    const _defaults = {
        classMap: {
            open: 'is-active',
            loading: 'is-loading'
        },
        selector: {
            overlay: '.modal__background',
            content: '.modal__content',
            close: '.modal__close'
        }
    }

    /**
     * A simple forEach() implementation for Arrays, Objects and NodeLists
     * @private
     * @param {Array|Object|NodeList} collection Collection of items to iterate
     * @param {Function} callback Callback function for each iteration
     * @param {Array|Object|NodeList} scope Object/NodeList/Array that forEach is iterating over (aka `this`)
     */
    var forEach = function (collection, callback, scope) {
        if (Object.prototype.toString.call(collection) === '[object Object]') {
            for (var prop in collection) {
                if (Object.prototype.hasOwnProperty.call(collection, prop)) {
                    callback.call(scope, collection[prop], prop, collection);
                }
            }
        } else {
            for (var i = 0, len = collection.length; i < len; i++) {
                callback.call(scope, collection[i], i, collection);
            }
        }
    };

    /**
     * Merge defaults with user options
     * @private
     * @param {Object} defaults Default settings
     * @param {Object} options User options
     * @returns {Object} Merged values of defaults and options
     */
    var extend = function ( defaults, options ) {
        var extended = {};
        forEach(defaults, function (value, prop) {
            extended[prop] = defaults[prop];
        });
        forEach(options, function (value, prop) {
            extended[prop] = options[prop];
        });
        return extended;
    };

    var index = function(obj, is, value) {
        if (typeof is === 'string')
            return index(obj, is.split('.'), value);
        else if (is.length === 1 && value !== undefined)
            return (Array.isArray(obj[is[0]])) ? obj[is[0]].push(value) : obj[is[0]] = value;
        else if (is.length === 0)
            return obj;
        else
            return index(obj[is[0]], is.slice(1), value);
    };

    var _trigger = function (plugin, method, args) {
        var fn = plugin._config(method);
        if (typeof fn === 'function') {
            if (typeof args === 'undefined') {
                args = [];
            }
            args.push(plugin);
            fn.apply(null, args);
        }
    };

    var _html = '<div class="modal__background"></div><div class="modal__content"></div><button class="modal__close"></button>'

    const _attachHtml = () => {
        const el = document.createElement('div')
        el.className = 'modal'
        el.innerHTML = _html
        document.body.appendChild(el)

        return el
    }

    var _init = function _init(plugin) {
        plugin.el = _attachHtml()

        plugin._close = plugin.el.querySelector(plugin._config('selector.close'))
        plugin._overlay = plugin.el.querySelector(plugin._config('selector.overlay'))
        plugin._content = plugin.el.querySelector(plugin._config('selector.content'))
    }

    var _eventHandler = function _eventHandler(plugin) {
        plugin._close.onclick = (ev) => {
            ev.preventDefault()

            plugin.close()
        }

        plugin._overlay.onclick = (ev) => {
            ev.preventDefault()

            plugin.close()
        }
    }

    const Plugin = function (el, options) {
        this.options = extend(_defaults, options || {} );

        _init(this)
        _eventHandler(this)

        this.closeOnEsc = this.closeOnEsc.bind(this)
    };

    Plugin.prototype._config = function (key, value) {
        return index(this.options, key, value);
    }

    Plugin.prototype.closeOnEsc = function (ev) {
        ev = ev || window.event
        if (ev.keyCode === 27) {
            this.close()
        }
    }

    Plugin.prototype._enableCaptureEsc = function (enable) {
        enable = enable || false
        if (enable) {
            document.addEventListener('keydown', this.closeOnEsc, true)
        } else {
            document.removeEventListener('keydown', this.closeOnEsc, true)
        }

        return this
    }

    Plugin.prototype.close = function () {
        utils.removeClass(this.el, this._config('classMap.open'))
        this
            .clearContent()
            ._enableCaptureEsc(false)

        return this
    }

    Plugin.prototype.open = function (content) {
        this.setContent(content)
        utils.addClass(this.el, this._config('classMap.open'))

        this._enableCaptureEsc(true)

        return this
    }

    Plugin.prototype.enableLoading = function (enable = true) {
        utils[enable ? 'addClass' : 'removeClass'](this.el, this._config('classMap.loading'))

        return this
    }

    Plugin.prototype.openAsync = function (url) {
        this
            .open()
            .enableLoading(true);

        fetch(url, { headers: { 'X-Requested-With': 'XMLHttpRequest' }, credentials: 'include' })
            .then(response => response.text())
            .then(body => {
                this
                    .enableLoading(false)
                    .setContent(body)
            })

        return this
    }

    Plugin.prototype.setContent = function (content) {
        if (!utils.isEmpty(content)) {
            this._content.innerHTML = content
        }

        return this
    }

    Plugin.prototype.clearContent = function () {
        this._content.innerHTML = '';
        return this
    }

    return Plugin;
}));
