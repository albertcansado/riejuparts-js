/* globals define, module, require */
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['./utils'], factory);
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(require('utils.js'));
    } else {
        root.Dropdown = factory(root.utils);
    }
}(this, function(utils) {

    var _defaults = {
        className: {
            open: 'dropdown--is-active'
        },
        selector: {
            trigger: '.dropdown__trigger',
            content: '.dropdown__content'
        }
    };

    var index = function(obj, is, value) {
        if (utils.isString(is))
            return index(obj, is.split('.'), value);
        else if (is.length === 1 && !utils.isUndefined(value))
            return obj[is[0]] = value;
        else if (is.length === 0)
            return obj;
        else
            return index(obj[is[0]], is.slice(1), value);
    };

    var _trigger = function (plugin, method, args) {
        var fn = plugin._config(method);
        if (utils.isFunction(fn)) {
            if (utils.isUndefined(args)) {
                args = [];
            }
            args.push(plugin);
            fn.apply(null, args);
        }
    };

    var _init = function _init(plugin) {
        plugin._isOpen = false;

        plugin._trigger = plugin.el.querySelector(plugin._config('selector.trigger'));
    };

    var _eventHandler = function _eventHandler(plugin) {
        utils.on(plugin._trigger, 'click', function(ev) {
            ev.preventDefault();

            plugin.toggle();
        });

        utils.on(window, 'click', function (ev) {
            if (plugin._isOpen && ( ev.target !== plugin._trigger && !plugin._trigger.contains(ev.target) )) {
                plugin.close()
            }
        });
    };

    var Plugin = function(el, options) {
        this.el = el;
        this.options = utils.extend(_defaults, options || {});

        _init(this);
        _eventHandler(this);
    };

    Plugin.prototype._config = function(key, value) {
        return index(this.options, key, value);
    };

    Plugin.prototype.open = function(target) {
        this._isOpen = true;
        utils.addClass(this.el, this._config('className.open'))
    };

    Plugin.prototype.close = function(target) {
        this._isOpen = false;
        utils.removeClass(this.el, this._config('className.open'))
    };

    Plugin.prototype.toggle = function () {
        this[this._isOpen ? 'close' : 'open']()
    }

    var ParameterException = function (value) {
        this.value = value;
        this.message = "first parameter must be an Element, a NodeList or a String selector";
        this.toString = function () {
            return this.message + ". Value: " + this.value;
        }
    }

    return function(sel, args) {
        var p = []; // Save reference
        var elem = [];

        if (utils.isElement(sel)) {
            return new Plugin(sel, args);
        } else if (utils.isNodeList(sel)) {
            elem = sel;
        } else if (utils.isString(sel)) {
            elem = document.querySelectorAll(sel);
        } else {
            throw new ParameterException(sel);
        }

        utils.foreach(elem, function(el) {
            p.push(new Plugin(el, args));
        });

        return (p.length === 1) ? p[0] : p;
    };
}));
