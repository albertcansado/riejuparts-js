/* globals define, module, require */
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['umbrellajs'], factory);
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(require('umbrellajs'));
    } else {
      root.Nav = factory(root._);
  }
}(this, function (u) {

    /**
     * A simple forEach() implementation for Arrays, Objects and NodeLists
     * @private
     * @param {Array|Object|NodeList} collection Collection of items to iterate
     * @param {Function} callback Callback function for each iteration
     * @param {Array|Object|NodeList} scope Object/NodeList/Array that forEach is iterating over (aka `this`)
     */
    var forEach = function (collection, callback, scope) {
        if (Object.prototype.toString.call(collection) === '[object Object]') {
            for (var prop in collection) {
                if (Object.prototype.hasOwnProperty.call(collection, prop)) {
                    callback.call(scope, collection[prop], prop, collection);
                }
            }
        } else {
            for (var i = 0, len = collection.length; i < len; i++) {
                callback.call(scope, collection[i], i, collection);
            }
        }
    };

    /**
     * Merge defaults with user options
     * @private
     * @param {Object} defaults Default settings
     * @param {Object} options User options
     * @returns {Object} Merged values of defaults and options
     */
    var extend = function ( defaults, options ) {
        var extended = {};
        forEach(defaults, function (value, prop) {
            extended[prop] = defaults[prop];
        });
        forEach(options, function (value, prop) {
            extended[prop] = options[prop];
        });
        return extended;
    };

    var index = function(obj, is, value) {
        if (typeof is === 'string')
            return index(obj, is.split('.'), value);
        else if (is.length === 1 && value !== undefined)
            return (Array.isArray(obj[is[0]])) ? obj[is[0]].push(value) : obj[is[0]] = value;
        else if (is.length === 0)
            return obj;
        else
            return index(obj[is[0]], is.slice(1), value);
    };

    var _trigger = function (plugin, method, args) {
        var fn = plugin._config(method);
        if (typeof fn === 'function') {
            if (typeof args === 'undefined') {
                args = [];
            }
            args.push(plugin);
            fn.apply(null, args);
        }
    };

    var _init = function _init(plugin) {
        plugin.$el = u.u(plugin.el)
        plugin._children = plugin.$el.find('.nav__item--has-children > .nav__link')
    };

    var _eventHandler = function _eventHandler(plugin) {
        plugin._children.on('click', ev => {

            const $target = u.u(ev.target)
            const navItem = $target.closest('.nav__item')
            const classOpen = plugin._config('className.open')

            if ($target.closest('.nav__icon--caret').length || $target.is('span')) {
                ev.preventDefault()

                const currentOpenEl = plugin._config('open')

                if (!navItem.is(currentOpenEl)) {
                    plugin.close(currentOpenEl)
                }

                if (!navItem.hasClass(classOpen)) {
                    plugin.open(navItem)
                } else {
                    plugin.close(navItem)
                }
            }
        })
    };

    var _defaults = {
        open: null,
        className: {
            open: 'is-open'
        },
        childrenMaxHeight: 0
    };

    var Plugin = function (el, options) {
        this.el = el;
        this.options = extend(_defaults, options || {} );

        _init(this);
        _eventHandler(this);
    };

    Plugin.prototype._config = function (key, value) {
        return index(this.options, key, value);
    };

    Plugin.prototype.open = function (el) {
        // Set max-height
        const child = el.find('.nav__children').first()
        const maxHeight = this._config('childrenMaxHeight')
        if (child && maxHeight > 0) {
            child.style['maxHeight'] = maxHeight + 'px'
        }

        el.addClass(this._config('className.open'))
        this._config('open', el)

        return this
    }

    Plugin.prototype.close = function (el) {
        const child = el.find('.nav__children').first()
        if (child) {
            child.style['maxHeight'] = 0
        }

        el.removeClass(this._config('className.open'))
        this._config('open', null)

        return this
    }

    var ParameterException = function (value) {
        this.value = value;
        this.message = "first parameter must be an Element, a NodeList or a String selector";
        this.toString = function () {
            return this.message + ". Value: " + this.value;
        };
    };

    return function (sel, args) {
        var p = []; // Save references
        var elem = [];

        if (!!(sel && sel.nodeType === 1)) {
            return new Plugin(sel, args);
        } else if (Array.isArray(sel)) {
            elem = sel;
        } else if (typeof sel === 'string') {
            elem = document.querySelectorAll(sel);
        } else {
            throw new ParameterException(sel);
        }

        forEach(elem, function (el) {
            p.push(new Plugin(el, args));
        });

        return (p.length === 1) ? p[0] : p;
    };
}));
