/* globals define, module, require */
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['./utils'], factory);
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(require('utils.js'));
    } else {
        root.Tab = factory(root.utils);
    }
}(this, function(utils) {

    /*
    Element to slide gets the following CSS:
        max-height: 0;
        opacity: 0;
        overflow: hidden;
        transition: max-height 0.4s ease 0s;
    */

    /**
     * Call once after timeout
     * @param  {Number}   seconds  Number of seconds to wait
     * @param  {Function} callback Callback function
     */
    var once = function once (seconds, callback) {
        var counter = 0;
        var time = window.setInterval( function () {
            counter++;
            if ( counter >= seconds ) {
                callback();
                window.clearInterval( time );
            }
        }, 400 );
    }

    /**
     * Like jQuery's slideDown function - uses CSS3 transitions
     * @param  {Node} elem Element to show and hide
     */
    var slideDown = function slideDown(elem, callback) {
        elem.style.transition = 'height .3s linear, padding .1s'
    	elem.style.height = elem.scrollHeight + 'px'
    	// We're using a timer to set opacity = 0 because setting max-height = 0 doesn't (completely) hide the element.
    	elem.style.opacity = '1'
        elem.style.padding = null
        elem.style.maxHeight = null

        once(1, function () {
    		elem.style.height = null
            elem.style.transition = null
            if (callback) {
                callback()
            }
    	});
    }

    /**
     * Slide element up (like jQuery's slideUp)
     * @param  {Node} elem Element
     * @return {[type]}      [description]
     */
    var slideUp = function slideUp(elem, callback) {
        elem.style.transition = 'max-height 2s linear, padding .01s linear .29s'
    	elem.style.maxHeight = '0'
        elem.style.padding = '0'
    	once(2, function () {
    		// elem.style.opacity = '0'
            elem.style.transition = null
            if (callback) {
                callback()
            }
    	});
    }

    var _defaults = {
        class: {
            open: 'is-open'
        },
        selector: {
            panel: '.panel',
            header: '.panel__header',
            content: '.panel__content'
        }
    };

    var index = function(obj, is, value) {
        if (utils.isString(is))
            return index(obj, is.split('.'), value);
        else if (is.length === 1 && !utils.isUndefined(value))
            return obj[is[0]] = value;
        else if (is.length === 0)
            return obj;
        else
            return index(obj[is[0]], is.slice(1), value);
    };

    var _trigger = function (plugin, method, args) {
        var fn = plugin._config(method);
        if (utils.isFunction(fn)) {
            if (utils.isUndefined(args)) {
                args = [];
            }
            args.push(plugin);
            fn.apply(null, args);
        }
    };

    var _init = function _init(plugin) {
        plugin._header = plugin.el.querySelector(plugin._config('selector.header'))
        plugin._content = plugin.el.querySelector(plugin._config('selector.content'))

        plugin._isOpen = utils.hasClass(plugin.el, plugin._config('class.open'))
        plugin._moving = false
    };

    var _eventHandler = function _eventHandler(plugin) {
        utils.on(plugin._header, 'click', function(ev) {
            ev.preventDefault();

            // Ensure is tab element
            if (!utils.hasClass(ev.target, plugin._config('selector.header')) || plugin._moving) {
                return;
            }

            plugin.toggle();
        });
    };

    var Plugin = function(el, options) {
        this.el = el;
        this.options = utils.extend(_defaults, options || {});

        _init(this);
        _eventHandler(this);
    };

    Plugin.prototype._config = function(key, value) {
        return index(this.options, key, value);
    };

    Plugin.prototype._afterMove = function () {
        this._moving = false
    }

    Plugin.prototype.close = function() {
        if (this._moving) {
            return
        }

        if (this._isOpen) {
            this._moving = true
            this._isOpen = false
            slideUp(this._content, this._afterMove.bind(this))
            utils.removeClass(this.el, this._config('class.open'))
        }
    };

    Plugin.prototype.open = function(target) {
        if (this._moving) {
            return
        }

        if (!this._isOpen) {
            this._moving = true
            this._isOpen = true
            slideDown(this._content, this._afterMove.bind(this))
            utils.addClass(this.el, this._config('class.open'))
        }
    };

    Plugin.prototype.toggle = function(target) {
        this[this._isOpen ? 'close' : 'open']()
    };

    var ParameterException = function (value) {
        this.value = value;
        this.message = "first parameter must be an Element, a NodeList or a String selector";
        this.toString = function () {
            return this.message + ". Value: " + this.value;
        }
    }

    return function(sel, args) {
        var p = []; // Save reference
        var elem = [];

        if (utils.isElement(sel)) {
            return new Plugin(sel, args);
        } else if (utils.isNodeList(sel)) {
            elem = sel;
        } else if (utils.isString(sel)) {
            elem = document.querySelectorAll(sel);
        } else {
            throw new ParameterException(sel);
        }

        utils.foreach(elem, function(el) {
            p.push(new Plugin(el, args));
        });

        return (p.length === 1) ? p[0] : p;
    };
}));
