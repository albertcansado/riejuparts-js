import Choices from 'choices.js'

import { fetchRequest } from '../middleware/api'

const defaults = {
    label: 'name',
    value: 'id'
}

const GroupSelect = (selector, options) => {
    const config = Object.assign({}, defaults, options)
    new Choices(selector, config)
        .ajax(callback => {
            fetchRequest('/d/groups.json')
                .then(({ json, response}) => {
                    callback(json.data, config.value, config.label)
                })
        })
}

export default GroupSelect
