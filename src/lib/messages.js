import { defineMessages } from 'react-intl'
import { forEach } from 'lodash'


const translateMessages = (messages, fn) => {
    let labels = {}
    forEach(messages, (value, key) => {
        labels[key] = fn(value)
    })

    return labels
}

// Public Methods with all messages
//

// Breadcrumbs Messages (Breadcrumbs.jsx)
export const getBreadcrumbsMessages = (fn) => {
    return translateMessages(
        defineMessages({
            breadcrumbsCatalog: {
                id: 'breadcrumbs.breadcrumbsCatalog',
                defaultMessage: "Catalog"
            }
        }),
        fn
    )
}

// CartItemAdd Messages (CartItemAdd.jsx)
export const getCartItemAddMessages = (fn) => {
    return translateMessages(
        defineMessages({
            cartIASyncPlaceholder: {
                id: 'cartItemAdd.cartIASyncPlaceholder',
                defaultMessage: "Introduce la referencia de una pieza"
            },
            cartIASyncNoResults: {
                id: 'cartItemAdd.cartIASyncNoResults',
                defaultMessage: "Not has results"
            },
            cartIASyncSearchText: {
                id: 'cartItemAdd.cartIASyncSearchText',
                defaultMessage: "Type to search"
            },
            cartIASyncLoading: {
                id: 'cartItemAdd.cartIASyncLoading',
                defaultMessage: "Loading..."
            }
        }),
        fn
    )
}

// Availability Messages (Availability.jsx)
export const getAvailabilityMessages = (fn) => {
    return translateMessages(
        defineMessages({
            availability: {
                id: 'part.availability',
                defaultMessage: "Availability"
            },
            partAvaiIm: {
                id: 'part.availability.immediate',
                defaultMessage: "Immediate availability"
            },
            partAvaiNotGuar: {
                id: 'part.availability.notguaranteed',
                defaultMessage: "Availability not guaranteed"
            },
            partAvaiRest: {
                id: 'part.availability.resticted',
                defaultMessage: "Restricted availability"
            }
        }),
        fn
    )
}

// CartExtra Messages (CartExtra.jsx, CartExtraAdd.jsx)
export const getCartExtraMessages = (fn) => {
    return translateMessages(
        defineMessages({
            cartExtraDeleteTitle: {
                id: 'cartextra.delete.title',
                defaultMessage: "Are you sure?",
            },
            /*cartExtraDeleteText: {
                id: 'cartextra.delete.text',
                defaultMessage: " "
            },*/
            cartExtraDeleteConfirm: {
                id: 'cartextra.delete.confirm',
                defaultMessage: "Yes"
            },
            cartextraCreateNoresults: {
                id: 'cartextra.create.noresults',
                defaultMessage: "Not has results"
            },
            cartextraCreateAdd: {
                id: 'cartextra.create.add',
                defaultMessage: "Add"
            },
            cartextraCreatePlaceholder: {
                id: 'cartextra.create.placeholder',
                defaultMessage: "Select/add one work"
            },
            cartextraCreateSearchText: {
                id: 'cartextra.create.searchtext',
                defaultMessage: "Type to search"
            },
            cartextraCreateLoading: {
                id: 'cartextra.create.loading',
                defaultMessage: "Loading..."
            }
        }),
        fn
    )
}

// CartList Messages (CartList.jsx)
export const getCartListMessages = (fn) => {
    return translateMessages(
        defineMessages({
            cartlistDeleteTitle: {
                id: 'cartlist.delete.title',
                defaultMessage: "Are you sure?"
            },
            /*cartlistDeleteText: {
                id: 'cartlist.delete.text',
                defaultMessage: " "
            },*/
            cartListDeleteConfirm: {
                id: 'cartlist.delete.confirm',
                defaultMessage: "Yes"
            }
        }),
        fn
    )
}

// Search Messages (Search.jsx)
export const getSearchMessages = (fn) => {
    return translateMessages(
        defineMessages({
            searchPlaceholder: {
                id: 'search.placeholder',
                defaultMessage: "enter a number or name",
            }
        }),
        fn
    )
}

// Cart Messages (Search.jsx)
export const getCartMessages = (fn) => {
    return translateMessages(
        defineMessages({
            cartSendTitle: {
                id: 'cart.send.title',
                defaultMessage: "Are you sure?",
            },
            cartSendText: {
                id: 'cart.send.text',
                defaultMessage: "Do you want to send this quotation?"
            },
            cartSendConfirm: {
                id: 'cart.send.confirm',
                defaultMessage: "Yes"
            },
            cartSendCancel: {
                id: 'warranty.newAlert.cancel',
                defaultMessage: "warranty.newAlert.cancel"
            },
            cartAliasPlaceholder: {
                id: 'cart.alias.placeholder',
                defaultMessage: "Enter an alias name"
            },
            cartSendSuccess: {
                id: 'cart.send.success',
                defaultMessage: 'Se esta enviando su peticion...'
            },
            defaultErrorSend: {
                id: 'cart.send.error',
                defaultMessage: 'Error on sending the quotation. Please try again'
            },
            cartAutoclose: {
                id: 'cart.autoclose',
                defaultMessage: 'This message will disappear in [seconds] seconds'
            }
        }),
        fn
    )
}

// Versions Messages (Search.jsx)
export const getVersionsMessages = (fn) => {
    return translateMessages(
        defineMessages({
            versionLastModels: {
                id: 'versions.lastmodels',
                defaultMessage: "Last Models",
            }
        }),
        fn
    )
}

// BarTop Messages (Bartop/Bartop.jsx)
export const getBarTopMessages = (fn) => {
    return translateMessages(
        defineMessages({
            bartopCatalog: {
                id: 'bartop.catalog',
                defaultMessage: "Catalog",
            }
        }),
        fn
    )
}

// Warranty (container/Warranty/WarrantyContainer.jsx)
export const getWarrantyMessages = fn => {
    return translateMessages(
        defineMessages({
            warrantyTitlePlaceholder: {
                id: 'warranty.header.title.placeholder',
                defaultMessage: "Add a title (Optional)"
            },
            warrantySaving: {
                id: 'warranty.header.saving.saving',
                defaultMessage: "Saving"
            },
            warrantySaved: {
                id: 'warranty.header.saving.saved',
                defaultMessage: "Saved"
            },
            warrantyPreWarrantyTitle: {
                id: 'warranty.prewarranty.title',
                defaultMessage: "Select the reason for the prewarranty"
            },
            warrantyTransportInfo: {
                id: 'warranty.prewarranty.transport.info',
                defaultMessage: "En caso de daño durante el transporte, indica el problema en Observaciones"
            },
            warrantyTransportTitle: {
                id: 'warranty.prewarranty.transport.title',
                defaultMessage: "Daños de transporte"
            },
            warrantyKm0Info: {
                id: 'warranty.prewarranty.km0.info',
                defaultMessage: "En caso de averia tecnica durante la inspeccion de pre-entrega, indica el problema en Observaciones"
            },
            warrantyKm0Title: {
                id: 'warranty.prewarranty.km0.title',
                defaultMessage: "Pre-entrega Km 0"
            },
            sendAlertTitle: {
                id: 'warranty.send.title',
                defaultMessage: 'Cerrar y enviar la garantia'
            },
            sendAlertText: {
                id: 'warranty.send.text',
                defaultMessage: 'Al darle a "Si", enviaras la garantia y no podras modificarla. ¿Has finalizado la garantia y deseas enviarla?'
            },
            sendAlertConfirm: {
                id: 'warranty.send.confirm',
                defaultMessage: 'Yes'
            },
            csvTitle: {
                id: 'warranty.csv.title',
                defaultMessage: 'Add or update lines using a CSV file'
            }
        }),
        fn
    )
}

// Warranty Add Part (components/Warranty/AddPart.jsx)
export const getWarrantyAddPartMessages = fn => {
    return translateMessages(
        defineMessages({
            warrantyPartPlaceholder: {
                id: 'warranty.addpart.part.placeholder',
                defaultMessage: "Enter part references"
            },
            warrantyPartNoResults: {
                id: 'warranty.addpart.part.noresults',
                defaultMessage: "Not has results"
            },
            warrantyPartSearchText: {
                id: 'warranty.addpart.part.searchText',
                defaultMessage: "Type to search"
            },
            warrantyPartLoading: {
                id: 'warranty.addpart.part.loading',
                defaultMessage: "Loading..."
            },
            warrantyCauseEmpty: {
                id: 'warranty.addpart.cause.empty',
                defaultMessage: "Select a cause"
            }
        }),
        fn
    )
}

// Warranty Info (components/Warranty/Header.jsx)
export const getWarrantyInfoMessages = fn => {
    return translateMessages(
        defineMessages({
            warrantyMechanicName: {
                id: 'warranty.mechanic.name',
                defaultMessage: "Name"
            },
            warrantyMechanicPhone: {
                id: 'warranty.mechanic.phone',
                defaultMessage: "Phone"
            },
            warrantyCommentsPlaceholder: {
                id: 'warranty.comments.placeholder',
                defaultMessage: "Add a brief explanation"
            }
        }),
        fn
    )
}

// Warranty History (components/Warranty/HistoryList.jsx)
export const getWarrantyHistory = fn => {
    return translateMessages(
        defineMessages({
            warrantyHistoryNotYet: {
                id: 'warranty.history.notYet',
                defaultMessage: "Not yet"
            },
            warrantyHistoryPreWarranty: {
                id: 'warranty.history.prewarranty',
                defaultMessage: "PreWarranty"
            },
            warrantyCommentsPlaceholder: {
                id: 'warranty.comments.placeholder',
                defaultMessage: "Add a brief explanation"
            }
        }),
        fn
    )
}

// Warranty Parts (components/Warranty/WarrantyParts.jsx)
export const getWarrantyPartsMessages = fn => {
    return translateMessages(
        defineMessages({
            warrantyPartsInfoParts: {
                id: 'warranty.parts.info.strong',
                defaultMessage: "piezas que han causado la averia y se deben substituir"
            },
            warrantyPartsInfoCatalog: {
                id: 'warranty.addpart.catalog',
                defaultMessage: "Use catalog"
            },
            warrantyCommentsPlaceholder: {
                id: 'warranty.comments.placeholder',
                defaultMessage: "Add a brief explanation"
            },
            warrantyCauseEmpty: {
                id: 'warranty.addpart.cause.empty',
                defaultMessage: "Select a cause"
            },
            warrantyPartsLineYes: {
                id: 'warranty.parts.yes',
                defaultMessage: "Yes"
            },
            warrantyPartsLineNo: {
                id: 'warranty.parts.no',
                defaultMessage: "No"
            },
            warrantyDeleteLineTitle: {
                id: 'warranty.parts.line.delete.title',
                defaultMessage: "Are you sure?"
            },
            warrantyDeleteLineConfirm: {
                id: 'warranty.parts.line.delete.confirm',
                defaultMessage: "Yes"
            },
        }),
        fn
    )
}
