import { defineMessages } from 'react-intl'
import { forEach, merge } from 'lodash'


const translateMessages = (messages, fn) => {
    let labels = {}
    forEach(messages, (value, key) => {
        labels[key] = fn(value)
    })

    return labels
}

// Public Methods with all messages
//

// Select Category Messages (Form/SelectCategory.jsx)
export const getSelectAsyncMessages = (fn) => {
    return translateMessages(
        defineMessages({
            selectAsyncNoResults: {
                id: 'selectAsync.noResults',
                defaultMessage: "Not has results"
            },
            selectAsyncPlaceholder: {
                id: 'selectAsync.placeholder',
                defaultMessage: "Select an option"
            },
            selectAsyncLoading: {
                id: 'selectAsync.loading',
                defaultMessage: "Loading..."
            }
        }),
        fn
    )
}

// List Part
export const getPartsListMessages = (fn) => {
    return translateMessages(
        defineMessages({
            deleteLineTitle: {
                id: 'partslist.alert.deleteLineTitle',
                defaultMessage: "Are you sure?"
            },
            deleteLineText: {
                id: 'partslist.alert.deleteLineText',
                defaultMessage: "Do you want to unattach this part from card"
            },
            deleteLineCancel: {
                id: 'partslist.alert.deleteLineCancel',
                defaultMessage: "No"
            },
            deleteLineConfirm: {
                id: 'partslist.alert.deleteLineConfirm',
                defaultMessage: "Yes"
            }
        }),
        fn
    )
}

// Search Part
export const getLineFormMessages = (fn) => {
    return translateMessages(
        defineMessages({
            labelPart: {
                id: 'lineform.label.part',
                defaultMessage: "Part"
            },
            labelNumber: {
                id: 'lineform.label.number',
                defaultMessage: "Number"
            },
            labelQty: {
                id: 'lineform.label.qty',
                defaultMessage: "Quantity"
            },
            labelInfo: {
                id: 'lineform.label.info',
                defaultMessage: "Information"
            },
            labelOptional: {
                id: 'app.optional',
                defaultMessage: "(Optional)"
            },
            noResultsText: {
                id: 'lineform.search.noResults',
                defaultMessage: "Not has results"
            },
            searchPlaceholder: {
                id: 'lineform.search.searchPlaceholder',
                defaultMessage: "Enter at least 3 character of the part sku"
            },
            searchPromptText: {
                id: 'lineform.search.searchPromptText',
                defaultMessage: "Choose one part"
            },
            loadingPlaceholder: {
                id: 'lineform.search.loadingPlaceholder',
                defaultMessage: "Loading..."
            }
        }),
        fn
    )
}

// Search Part
export const getEngineEditMessages = (fn) => {
    return translateMessages(
        defineMessages({
            enginePlaceholder: {
                id: 'engineedit.engine.placeholder',
                defaultMessage: "Select an engine"
            },
            textPlaceholder: {
                id: 'engineedit.text.placeholder',
                defaultMessage: "Enter an information (Optional)"
            }
        }),
        fn
    )
}

// Versions Container (components/Backend/VersionsContainer.jsx)
export const getVersionsMessages = (fn) => {
    return translateMessages(
        defineMessages({
            versionsAddTitle: {
                id: 'versions.add.title',
                defaultMessage: "Enter a version name"
            },
            versionsDeleteTitle: {
                id: 'versions.delete.title',
                defaultMessage: "Are you sure?"
            },
            versionsDeleteText: {
                id: 'versions.delete.text',
                defaultMessage: "Do you want to delete this version?"
            }
        }),
        fn
    )
}

// (components/Backend/Versions/VersionsPartsList.jsx)
export const getVersionsPartsMessages = (fn) => {
    return translateMessages(
        defineMessages({
            verPartsDeleteTitle: {
                id: 'versions.partsList.deleteTitle',
                defaultMessage: "Are you sure?"
            },
            verPartsDeleteConfirm: {
                id: 'versions.partsList.deleteConfirm',
                defaultMessage: "Yes"
            }
        }),
        fn
    )
}

// (components/Backend/Versions/AddForm.jxs)
export const getVersionsAddFormMessages = (fn) => {
    return merge(
        getLineFormMessages(fn),
        translateMessages(
            defineMessages({
                stepcardLabel: {
                    id: 'addform.card.label',
                    defaultMessage: "Card"
                },
                stepcardPhrase: {
                    id: 'addForm.card.phrase',
                    defaultMessage: "Selecciona la lamina donde quieres añadir la nueva pieza"
                }
            }),
            fn
        )
    )
}

// (components/Backend/Versions/RemoveForm.jxs)
export const getVersionsRemoveFormMessages = (fn) => {
    return merge(
        getLineFormMessages(fn),
        translateMessages(
            defineMessages({
                stepcardLabel: {
                    id: 'removeForm.card.label',
                    defaultMessage: "Card"
                },
                stepcardPhrase: {
                    id: 'removeForm.card.phrase',
                    defaultMessage: "Select the card where the part is to be removed"
                },
                stepCardPhraseParts: {
                    id: 'removeForm.card.phraseparts',
                    defaultMessage: "Select the part to remove"
                }
            }),
            fn
        )
    )
}

// (components/Backend/Versions/ReplaceForm.jxs)
export const getVersionsReplaceFormMessages = (fn) => {
    return merge(
        getLineFormMessages(fn),
        translateMessages(
            defineMessages({
                stepcardLabel: {
                    id: 'replaceForm.card.label',
                    defaultMessage: "Card"
                },
                stepcardPhrase: {
                    id: 'replaceForm.card.phrase',
                    defaultMessage: "Select the card where the card part to be replaced"
                },
                stepCardPhraseParts: {
                    id: 'replaceForm.card.phraseparts',
                    defaultMessage: "Select the card part to replace"
                }
            }),
            fn
        )
    )
}

// Cards
// (components/Backend/CardsList.jsx)
export const getCardsMessages = (fn) => {
    return translateMessages(
        defineMessages({
            cardDeleteTitle: {
                id: 'card.delete.title',
                defaultMessage: "Are you sure?"
            },
            cardDeleteText: {
                id: 'card.delete.text',
                defaultMessage: "Are you sure do you want to delete this card?"
            },
            cardDeleteConfirm: {
                id: 'card.delete.confirm',
                defaultMessage: "Yes"
            },
            cardDeleteCancel: {
                id: 'card.delete.cancel',
                defaultMessage: "No"
            }
        }),
        fn
    )
}
