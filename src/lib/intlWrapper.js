import React from 'react'
import { render } from 'react-dom'
import { isUndefined } from 'lodash'
import { IntlProvider, addLocaleData } from 'react-intl'

const getIdFromRequest = () => {
    const id = window.location.pathname.match(/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/i)
    return id === null ? null : id[0]
}

const intlWrapper = ({ element, component, props }) => {
    const intlProps = window.hasOwnProperty('__LOCALE')
        ? window.__LOCALE
        : {
            locale: 'en',
            messages: {}
        }

    if (window.hasOwnProperty('ReactIntlLocaleData')) {
        addLocaleData(window.ReactIntlLocaleData[intlProps.locale])
    }

    // Find and inject id on request
    if (isUndefined(props)) {
        props = {}
    }
    props['id'] = getIdFromRequest()

    render(
        <IntlProvider {...intlProps}>
            {React.createElement(component, props)}
        </IntlProvider>,
        element
    )
}

export default intlWrapper
