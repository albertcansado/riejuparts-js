/* globals define, module, require */
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['./utils'], factory);
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(require('utils.js'));
    } else {
        root.Uploader = factory(root.utils);
    }
}(this, function(utils) {

    var _defaults = {
        className: {},

        selector: {
            textField: '.upload-box__text',
            fileField: '.upload-box__field'
        },

        maxSize: 15728640
    };

    var index = function(obj, is, value) {
        if (utils.isString(is))
            return index(obj, is.split('.'), value);
        else if (is.length === 1 && !utils.isUndefined(value))
            return obj[is[0]] = value;
        else if (is.length === 0)
            return obj;
        else
            return index(obj[is[0]], is.slice(1), value);
    };

    var _trigger = function (plugin, method, args) {
        var fn = plugin._config(method);
        if (utils.isFunction(fn)) {
            if (utils.isUndefined(args)) {
                args = [];
            }
            args.push(plugin);
            fn.apply(null, args);
        }
    };

    var _init = function _init(plugin) {
        plugin.inputFile = plugin.el.querySelector(plugin._config('selector.fileField'))
        plugin.inputText = plugin.el.querySelector(plugin._config('selector.textField'))
    };

    var _eventHandler = function _eventHandler(plugin) {
        if (plugin.inputFile) {
            plugin.inputFile.onchange = plugin.onChange.bind(plugin)
        }
    }

    var Plugin = function (el, options) {
        this.el = el;
        this.options = utils.extend(_defaults, options || {});

        _init(this);
        _eventHandler(this);
    }

    Plugin.prototype._config = function (key, value) {
        return index(this.options, key, value);
    }

    Plugin.prototype.validSize = function () {
        var isValid = true;
        if (!!window.FileReader) {
            isValid = Boolean(this.inputFile.files[0].size < this._config('maxSize'))
        }

        return isValid;
    }

    Plugin.prototype.onChange = function (ev) {
        if (this.validSize()) {
            this.inputText.value = this.inputFile.value;
        }
    }

    var ParameterException = function (value) {
        this.value = value;
        this.message = "first parameter must be an Element, a NodeList or a String selector";
        this.toString = function () {
            return this.message + ". Value: " + this.value;
        }
    }

    return function(sel, args) {
        var p = []; // Save reference
        var elem = [];

        if (utils.isElement(sel)) {
            return new Plugin(sel, args);
        } else if (utils.isNodeList(sel)) {
            elem = sel;
        } else if (utils.isString(sel)) {
            elem = document.querySelectorAll(sel);
        } else {
            throw new ParameterException(sel);
        }

        utils.foreach(elem, function(el) {
            p.push(new Plugin(el, args));
        });

        return (p.length === 1) ? p[0] : p;
    }
}));
