/* globals define, module, require */
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['./utils'], factory);
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(require('utils.js'));
    } else {
        root.Tab = factory(root.utils);
    }
}(this, function(utils) {

    var _defaults = {
        class: {
            active: 'tabs__item--is-active',
            open: 'tabs__panel--is-open',
            effect: 'in'
        },
        selector: {
            tab: '.tabs__tab',
            panel: '.tabs__panel',
            content: '.tabs__content'
        }
    };

    var index = function(obj, is, value) {
        if (utils.isString(is))
            return index(obj, is.split('.'), value);
        else if (is.length === 1 && !utils.isUndefined(value))
            return obj[is[0]] = value;
        else if (is.length === 0)
            return obj;
        else
            return index(obj[is[0]], is.slice(1), value);
    };

    var _trigger = function (plugin, method, args) {
        var fn = plugin._config(method);
        if (utils.isFunction(fn)) {
            if (utils.isUndefined(args)) {
                args = [];
            }
            args.push(plugin);
            fn.apply(null, args);
        }
    };

    var _init = function _init(plugin) {
        plugin._tabs = plugin.el.querySelectorAll(plugin._config('selector.tab'));


        plugin._content = _getContentWrapper(plugin);
        plugin._panels = [];

        plugin.active = plugin.el.querySelectorAll('.' + plugin._config('class.active') + ' > a');
        if (!plugin.active.length) {
            plugin.show(plugin._tabs[0]);
        }

        plugin.activeSelector = plugin._getSelector(plugin.active[0]);
    };

    var _eventHandler = function _eventHandler(plugin) {
        utils.on(plugin.el, 'click', function(ev) {
            ev.preventDefault();

            // Ensure is tab element
            if (!utils.hasClass(ev.target, plugin._config('selector.tab'))) {
                return;
            }

            // click tab is current tab -> exit
            if (plugin._getSelector(ev.target) === plugin.activeSelector) {
                return;
            }
            plugin.hide();
            plugin.show(ev.target);
        });
    };

    // Find content wrapper, first look for child, otherwise look from document root
    var _getContentWrapper = function _getContentWrapper(plugin) {
        var c = plugin.el.querySelector(plugin._config('selector.content'));
        return (!utils.isNull(c)) ? c : document.querySelector(plugin._config('selector.content'));
    };

    var Plugin = function(el, options) {
        this.el = el;
        this.options = utils.extend(_defaults, options || {});

        _init(this);
        _eventHandler(this);
    };

    Plugin.prototype._config = function(key, value) {
        return index(this.options, key, value);
    };

    Plugin.prototype._getSelector = function(el) {
        if (utils.isElement(el)) {
            return el.getAttribute('href');
        }
        return undefined;
    };

    Plugin.prototype._getPanel = function(name) {
        if (utils.isUndefined(this._panels[name])) {
            this._panels[name] = this._content.querySelector(name);
        }

        return this._panels[name];
    };

    Plugin.prototype.hide = function() {
        if (this.active) {
            var _this = this;
            utils.foreach(this.active, function(el) {
                utils.removeClass(utils.getParent(el), _this._config('class.active'));
            });
            utils.removeClass(
                this._getPanel(this._getSelector(this.active[0])), [this._config('class.open'), this._config('class.effect')]
            );
            this.active = null;
        }
    };

    Plugin.prototype.show = function(target) {
        var id = this._getSelector(target);
        if (id) {
            var _this = this;
            this.active = this.el.querySelectorAll('[href="' + id + '"]');
            this.activeSelector = id;

            utils.foreach(this.active, function(el) {
                utils.addClass(utils.getParent(el), _this._config('class.active'));
            });
            utils.addClass(
                this._getPanel(id), [this._config('class.open'), this._config('class.effect')]
            );
        }
    };

    var ParameterException = function (value) {
        this.value = value;
        this.message = "first parameter must be an Element, a NodeList or a String selector";
        this.toString = function () {
            return this.message + ". Value: " + this.value;
        }
    }

    return function(sel, args) {
        var p = []; // Save reference
        var elem = [];

        if (utils.isElement(sel)) {
            return new Plugin(sel, args);
        } else if (utils.isNodeList(sel)) {
            elem = sel;
        } else if (utils.isString(sel)) {
            elem = document.querySelectorAll(sel);
        } else {
            throw new ParameterException(sel);
        }

        utils.foreach(elem, function(el) {
            p.push(new Plugin(el, args));
        });

        return (p.length === 1) ? p[0] : p;
    };
}));
