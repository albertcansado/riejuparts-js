import React from 'react'
import { render } from 'react-dom'

import RootEngine from './container/Backend/RootEngine'
import configureStore from './store/editorStore'

const store = configureStore()

const rootEngine = document.getElementById('root-engine')
if (rootEngine) {
    render(
        <RootEngine store={store} />,
        rootEngine
    )
}
