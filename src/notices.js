import intlWrapper from 'lib/intlWrapper'

import NoticesContainer from 'container/Backend/NoticesContainer'

const noticeDOM = document.querySelector('#notices-root')
if (noticeDOM) {
    intlWrapper({
        element: noticeDOM,
        component: NoticesContainer,
        props: {
            fetchUrl: '/d/notices/{id}.json',
            saveUrl: '/d/notices'
        }
    })
}
