import intlWrapper from 'lib/intlWrapper'

import ProductsContainer from 'container/Backend/ProductsContainer'

const rootDOM = document.querySelector('#root-products')
if (rootDOM) {
    intlWrapper({
        element: rootDOM,
        component: ProductsContainer,
        props: {
            fetchUrl: '/d/products/{id}.json',
            saveUrl: '/d/products'
        }
    })
}
