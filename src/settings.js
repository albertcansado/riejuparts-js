import intlWrapper from 'lib/intlWrapper'
import ImageUploader from 'components/Form/ImageUploader'

import './scss/settings.scss'

const rootModel = document.querySelector('.js-dropzone')
if (rootModel) {
    intlWrapper({
        element: rootModel,
        component: ImageUploader,
        props: {
            labelTitle: rootModel.getAttribute('data-labelTitle'),
            name: rootModel.getAttribute('data-name'),
            image: rootModel.getAttribute('data-value')
        }
    })
}
