import React, { Component, PropTypes } from 'react'
import { Provider } from 'react-intl-redux'

import CartBtn from '../components/Cart/CartBtn'

export default class RootCartBtn extends Component {
    render() {
        const { store } = this.props
        return (
            <Provider store={store}>
                <CartBtn />
            </Provider>
        )
    }
}

RootCartBtn.propTypes = {
    store: PropTypes.object.isRequired
}
