import React, { PureComponent, PropTypes } from 'react'
import { Provider } from 'react-intl-redux'
import Cookies from 'js-cookie'

import WarrantyContainer from './Warranty/WarrantyContainer'
import Modal from './ModalReduxContainer'

const getIdFromRequest = () => {
    const paths = window.location.pathname.substring(1).split('/')
    return paths[paths.length - 1]
}

export default class Root extends PureComponent {
    getChildContext = () => {
        return {
            locale: Cookies.get('locale')
        }
    }

    render() {
        const { store } = this.props
        return (
            <Provider store={store}>
                <div className="u-h100">
                    <WarrantyContainer id={getIdFromRequest()} />
                    <Modal />
                </div>
            </Provider>
        )
    }
}

Root.childContextTypes = {
    locale: PropTypes.string.isRequired
}

Root.propTypes = {
    store: PropTypes.object.isRequired
}
