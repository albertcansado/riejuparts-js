import React, { PureComponent, PropTypes } from 'react'
import { connect } from 'react-redux'
// import cx from 'classnames'

import { openModal } from '../actions/modal'
import { addToCart, updateCartLine } from '../actions/carts'
import { fetchProductIfNeeded } from '../actions/products'
import { isInitProductFinish } from '../reducers/helpers'
import { getProducts, getCartParts } from '../reducers/entities'

import ProductsList from '../components/Products/ProductsList'
import Loading from '../components/Loading'

class ProductsContainer extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this._addToCart = this._addToCart.bind(this)
        this._updateCart = this._updateCart.bind(this)
        this._handleOpenModal = this._handleOpenModal.bind(this)
    }

    componentDidMount = () => {
        const { dispatch } = this.props

        dispatch(fetchProductIfNeeded())
    }

    _addToCart = (part, qty) => {
        const { dispatch } = this.props

        return dispatch(addToCart(part, qty))
    }

    _updateCart = (line, nextQty) => {
        const { dispatch } = this.props

        return dispatch(
            updateCartLine(line, nextQty)
        )
    }

    _handleOpenModal = (options) => {
        const { dispatch } = this.props

        return dispatch(
            openModal(options)
        )
    }

    render = () => {
        const { isLoading, products, cartProducts } = this.props
        return (
            <Loading isLoading={isLoading} className="loading--abs loading--60">
                <ProductsList
                    products={products}
                    cartProducts={cartProducts}
                    addToCart={this._addToCart}
                    updateCart={this._updateCart}
                    openModal={this._handleOpenModal}
                />
            </Loading>
        )
    }
}

ProductsContainer.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    products: PropTypes.array.isRequired,
    cartProducts: PropTypes.object.isRequired
}
ProductsContainer.defaultProps = {}

const mapStateToProps = (state, ownProps) => {
    return {
        isLoading: !isInitProductFinish(state),
        products: getProducts(state),
        cartProducts: getCartParts(state)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductsContainer)
