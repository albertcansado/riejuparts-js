import React, { Component } from 'react'
import { connect } from 'react-redux'
import { intlShape } from 'react-intl'
import { map as _map, groupBy, forEachRight } from 'lodash'

// import { fetchModelsIfNeeded } from '../actions/models'
import { openModal } from '../actions/modal'
import { isModelsFetched, getModels } from '../reducers/entities'
import { getVersionsMessages } from '../lib/messages'

import ModelCard from '../components/ModelCard'
import Spinner from '../components/Spinner'
import LazyLoad from '../components/LazyLoad'

const getLastKey = (obj) => {
    const ks = Object.keys(obj)

    return ks[ks.length - 1]
}

class VersionsContainer extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {}

        this._content = document.querySelector('.content')

        // Cache Translate Messages
        this._messages = getVersionsMessages(context.intl.formatMessage)
    }

    /*componentDidMount = () => {
        const { dispatch, request } = this.props
        dispatch(fetchModelsIfNeeded(request.category))
    }*/

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.isLoading !== this.props.isLoading
    }

    _renderLoading = () => {
        return <div className="loading loading--abs loading--60"><Spinner /></div>
    }

    _render = () => {
        let { items, request } = this.props
        items = groupBy(items, 'year')
        let indents = []

        const last = getLastKey(items)
        forEachRight(items, (it, k) => {
            indents.push(
                <div className="list__block" key={k}>
                    <h2 className="content__title">{last === k ? this._messages.versionLastModels : k}</h2>
                    <div className="row">
                        {_map(it, item => {
                            if (!item.versions.length) {
                                return null
                            }

                            return (
                                <div className="col-xs-12 col-sm-6 col-md-4 col-xl-3" key={item.id}>
                                    <LazyLoad container={this._content} height={378}>
                                        <ModelCard item={item} url={`/catalog/${request.category}`} handleOpenModal={this.props.openModal} />
                                    </LazyLoad>
                                </div>
                            )
                        })}
                    </div>
                </div>
            )
        })
        return (
            <div className="list list--models">
                {indents.length ? indents : <h2 className="txt-center">No hay modelos disponibles</h2>}
            </div>
        )
    }

    render = () => {
        const { isLoading } = this.props
        return (
            isLoading ? this._renderLoading() : this._render()
        )
    }
}

VersionsContainer.contextTypes = {
    intl: intlShape.isRequired
}

VersionsContainer.propTypes = {}
VersionsContainer.defaultProps = {}

const mapStateToProps = (state, ownProps) => {
    return {
        isLoading: !isModelsFetched(state, ownProps.request.category),
        items: getModels(state, ownProps.request.category)
    }
}

const mapDispatchToProps = dispatch => {
    return {
        openModal: (...args) => dispatch(openModal(...args)),
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps

)(VersionsContainer)
