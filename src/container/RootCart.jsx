import React, { Component, PropTypes } from 'react'
import { Provider } from 'react-intl-redux'

import CartContainer from './CartContainer'

export default class RootCart extends Component {
    render() {
        const { store } = this.props
        return (
            <Provider store={store}>
                <CartContainer />
            </Provider>
        )
    }
}

RootCart.propTypes = {
    store: PropTypes.object.isRequired
}
