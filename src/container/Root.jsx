import React, { Component, PropTypes } from 'react'
import { Provider } from 'react-intl-redux'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

import App from './App'
import ModelsContainer from './ModelsContainer'
import VersionsContainer from './VersionsContainer'
import CardContainer from './CardContainer'

export default class Root extends Component {
    render() {
        const { store } = this.props
        const history = syncHistoryWithStore(browserHistory, store)
        return (
            <Provider store={store}>
                <Router history={history}>
                    <Route path="/catalog" component={App}>
                        <IndexRoute component={ModelsContainer} />
                        <Route path=":slug">
                            <IndexRoute component={VersionsContainer} />
                            <Route path=":version/:card" component={CardContainer} />
                        </Route>
                    </Route>
                </Router>
            </Provider>
        )
    }
}

Root.propTypes = {
    store: PropTypes.object.isRequired
}
