import React, { PureComponent, PropTypes } from 'react'
import { connect } from 'react-redux'

import { closeModal } from '../actions/modal'
import { isOpen, getComponent, getProps, getModalProps } from '../reducers/modal'

import Modal from '../components/Modal'

class ModalReduxContainer extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.handleClose = this.handleClose.bind(this)
    }

    handleClose = () => {
        const { dispatch } = this.props
        dispatch(closeModal())
    }

    render = () => {
        const { isOpen, component, componentProps, modalProps, enableKeyboardInput } = this.props

        return (
            <Modal
                isOpen={isOpen}
                component={component}
                componentProps={componentProps}
                handleClose={this.handleClose}
                modalProps={modalProps}
                enableKeyboardInput={enableKeyboardInput}
            />
        )
    }
}

ModalReduxContainer.propTypes = {
    isOpen: PropTypes.bool,
    enableKeyboardInput: PropTypes.bool
}
ModalReduxContainer.defaultProps = {
    isOpen: false,
    enableKeyboardInput: true
}

const mapStateToProps = (state, ownProps) => {
    return {
        isOpen: isOpen(state),
        component: getComponent(state),
        componentProps: getProps(state),
        modalProps: getModalProps(state)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps

)(ModalReduxContainer)
