import React, { PureComponent, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { FormattedMessage, intlShape } from 'react-intl'
import { isObject, forEach as _forEach, isEqual } from 'lodash'
import idx from 'idx'
import cx from 'classnames'
import swal from 'sweetalert2'

import { getWarrantyMessages } from '../../lib/messages'
import { fetchWarrantyInit, updateWarranty, createLine, updateLine, deleteLine, uploadAttachments, deleteAttachment, createCsvLines } from '../../actions/warranty/entities'
import * as ModalActions from '../../actions/modal'
import { getWarranty, isCompleted } from '../../reducers/warranty'

import Icon from '../../components/Icon'
import Input from '../../components/Form/Input'
import Spinner from '../../components/Spinner'

import CatalogHijacking from '../../components/Warranty/CatalogHijacking'
import CsvHijacking from '../../components/Warranty/CsvHijacking'
import Header from '../../components/Warranty/Header'
import HistoryBtn from '../../components/Warranty/HistoryBtn'
import HistoryContainer from './HistoryContainer'
import Saving from '../../components/Warranty/Saving'
import WarrantyAttachmentsContainer from './WarrantyAttachmentsContainer'
import WarrantyParts from '../../components/Warranty/WarrantyParts'
import WarrantyPre from '../../components/Warranty/WarrantyPre'

const fieldIsModified = (warranty, field, value) => {
    return warranty.hasOwnProperty(field) && !isEqual(warranty[field], value)
}

class WarrantyContainer extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            isLoading: true,
            isSaving: false
        }

        // Cache Translate Messages
        this._messages = getWarrantyMessages(context.intl.formatMessage)

        // Trick
        this._allowSubmit = false

        // Methods
        this._changeTitle = this._changeTitle.bind(this)
        this.onChangeValue = this.onChangeValue.bind(this)
        this._sendCSV = this._sendCSV.bind(this)
        this._showCSV = this._showCSV.bind(this)
    }

    componentDidMount = () => {
        const { dispatch, id } = this.props
        dispatch(fetchWarrantyInit(id))
        .then(() => {
            this.setState({
                isLoading: false
            })
        })
    }

    _changeTitle = (value) => {
        this.onChangeValue('title', value)
    }

    onChangeValue = (field, value) => {
        const { dispatch, warranty } = this.props

        if (isObject(field)) {
            // Loop
            let modified = false
            _forEach(field, (value, key) => {
                if (fieldIsModified(warranty, key, value)) {
                    modified = true
                }
            })

            if (!modified) {
                return
            }
        } else if (!fieldIsModified(warranty, field, value)) {
            return
        }

        this.setState({
            isSaving: true
        })

        dispatch(
            updateWarranty(warranty, isObject(field)
                ? field
                : {
                    [field]: value
                }
            )
        ).then(() => {
            this.setState({
                isSaving: false
            })
        })
    }

    _sendWarranty = ev => {
        const { warrantyCompleted } = this.props

        if (warrantyCompleted && this._allowSubmit) {
            return
        }

        ev.preventDefault()

        if (!warrantyCompleted) {
            return
        }

        const target = ev.target

        swal({
            title: this._messages.sendAlertTitle,
            text: this._messages.sendAlertText,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: this._messages.sendAlertConfirm
        }).then(() => {
            this._allowSubmit = true
            target.submit()
        }).catch(swal.noop)
    }

    uploadAttachments = files => {
        const { dispatch, warranty } = this.props

        return dispatch(
            uploadAttachments(warranty, files)
        )
    }

    deleteAttachment = file => {
        const { dispatch, warranty } = this.props

        return dispatch(
            deleteAttachment(warranty, file)
        )
    }

    _wrapperAction = (action, data) => {
        const { warranty, actions } = this.props

        if (actions.hasOwnProperty(action)) {
            this.setState({
                isSaving: true
            })

            actions[action](warranty, data)
            .then(() => {
                this.setState({
                    isSaving: false
                })
            })
        }
    }

    _showCatalog = () => {
        const { actions, id } = this.props

        actions.openModal({
            component: CatalogHijacking,
            props: {
                id,
                handleAdd: data => this._wrapperAction('createLine', data),
                handleUpdate: null
            }
        })
    }

    _sendCSV = data => {
        this._wrapperAction('createCsvLines', data)
    }

    _showCSV = () => {
        const { actions } = this.props

        actions.openModal({
            component: CsvHijacking,
            props: {
                handleSubmit: this._sendCSV,
                title: this._messages.csvTitle
            }
        })
    }

    _showHistoryModal = () => {
        const { actions, warranty } = this.props
        const number = idx(warranty, _ => _.frame.number)

        if (number) {
            actions.openModal({
                component: HistoryContainer,
                props: {
                    number
                }
            })
        }
    }

    _renderLoading = () => {
        return <div className="loading loading--abs loading--60"><Spinner /></div>
    }

    _renderBar = (warranty) => {
        const { warrantyCompleted } = this.props
        return (
            <header className="w-container__header box__header u-flex flex--middle flex--between">
                <div className="u-flex flex--middle">
                    <h3 className="u-mgr--5">
                        <span>{idx(warranty, _ => _.number)}</span> -
                    </h3>
                    <Input
                        inputProps={{
                            placeholder: this._messages.warrantyTitlePlaceholder
                        }}
                        theme={{
                            input: "input--inline"
                        }}
                        value={warranty.title}
                        onChange={this._changeTitle}
                    />
                </div>
                <div className="w-container__header-options u-flex flex--end flex--middle">
                    <Saving isSaving={this.state.isSaving} messages={this._messages} />
                    <HistoryBtn count={warranty.history_count} handleClick={this._showHistoryModal} />
                    <form action={`/warranties/send/${warranty.id}`} method="POST" className="warranty__send" onSubmit={this._sendWarranty}>
                        <input type="hidden" name="_method" value="put" />
                        <input type="hidden" name="id" value={`${warranty.id}`} />
                        <button type="submit" className={cx("btn btn--success", {
                            'is-disabled': !warrantyCompleted
                        })}>
                            <Icon icon="plane" className="icon--16" />
                            <FormattedMessage id="warranty.header.send" defaultMessage="Send" />
                        </button>
                    </form>
                </div>
            </header>
        )
    }

    _renderWarranty = () => {
        const { warranty } = this.props
        return (
            <div className="w-container box box--clear">
                {this._renderBar(warranty)}
                <section className="w-container__content">
                    <Header warranty={warranty} onChangeValue={this.onChangeValue} />
                    <div className="row">
                        <div className={cx("col-xs-12 col-xl-6", {'hidden-xs-up': warranty.type})}>
                            <WarrantyPre
                                type={warranty.type}
                                transport={warranty.transport}
                                pre_km0={warranty.pre_km0}
                                onSelect={this.onChangeValue}
                                messages={this._messages}
                            />
                        </div>
                        <div className={cx("col-xs-12", {'col-xl-6': !warranty.type})}>
                            <WarrantyAttachmentsContainer
                                accept="application/pdf,image/jpg,image/png,image/jpeg"
                                handleUpload={this.uploadAttachments}
                                handleDelete={this.deleteAttachment}
                                multiple
                                values={warranty.attachments}
                                limit={6}
                            />
                        </div>
                    </div>
                    <WarrantyParts
                        id={warranty.id}
                        onCreateLine={data => this._wrapperAction('createLine', data)}
                        onUpdateLine={(line, data) => this._wrapperAction('updateLine', {line, data})}
                        onDeleteLine={id => this._wrapperAction('deleteLine', id)}
                        handleCatalog={this._showCatalog}
                        handleCSV={this._showCSV}
                    />
                </section>
            </div>
        )
    }

    render = () => {
        return this.state.isLoading ? this._renderLoading() : this._renderWarranty()
    }
}

WarrantyContainer.contextTypes = {
    intl: intlShape.isRequired
}

WarrantyContainer.propTypes = {
    id: PropTypes.string.isRequired
}
// WarrantyContainer.defaultProps = {}

const mapStateToProps = (state, ownProps) => {
    return {
        warranty: getWarranty(state, ownProps.id),
        warrantyCompleted: isCompleted(state, ownProps.id)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators({ createLine, updateLine, deleteLine, createCsvLines, ...ModalActions }, dispatch),
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WarrantyContainer)
