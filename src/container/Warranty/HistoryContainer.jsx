import React, { PureComponent, PropTypes } from 'react'
import { connect } from 'react-redux'
import { intlShape } from 'react-intl'
import { map as _map } from 'lodash'

import { getWarrantyHistory } from '../../lib/messages'

import { fetchHistoriesIfNeeded } from '../../actions/warranty/histories'
import { getHistories } from '../../reducers/warranty/'

import HistoryList from '../../components/Warranty/HistoryList'

class HistoryContainer extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            loading: true
        }

        // Cache Translate Messages
        this._messages = getWarrantyHistory(context.intl.formatMessage)
    }

    componentWillMount = () => {
        const { dispatch, number } = this.props

        dispatch(fetchHistoriesIfNeeded(number))
        .then(() => {
            this.setState({
                loading: false
            })
        })
    }

    render = () => {
        const { histories, handleClose } = this.props
        const { loading } = this.state

        if (loading) {
            return null
        }

        return <HistoryList
            handleClose={handleClose}
            histories={histories}
            messages={{
                notYet: this._messages.warrantyHistoryNotYet,
                prewarranty: this._messages.warrantyHistoryPreWarranty
            }} />
    }
}

HistoryContainer.contextTypes = {
    intl: intlShape.isRequired
}

HistoryContainer.propTypes = {
    number: PropTypes.string.isRequired
}
HistoryContainer.defaultProps = {}

const mapStateToProps = (state, ownProps) => {
    return {
        histories: getHistories(state)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HistoryContainer)
