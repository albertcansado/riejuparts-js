import { connect } from 'react-redux'

import { getAttachments } from '../../reducers/warranty'

import WarrantyAttachmentsList from '../../components/Warranty/WarrantyAttachmentsList'

const mapStateToProps = (state, ownProps) => {
    return {
        attachments: getAttachments(state, ownProps.values)
  }
}

const mapDispatchToProps = dispatch => {
    return {}
}

const WarrantyAttachmentsContainers = connect(
    mapStateToProps,
    mapDispatchToProps
)(WarrantyAttachmentsList)

export default WarrantyAttachmentsContainers
