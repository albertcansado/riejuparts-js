import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { map as _map } from 'lodash'

// import { fetchCatergoriesIfNeeded } from '../actions/models'
// import { isCategoriesFetched } from '../reducers/helpers'
import { getCategories } from '../reducers/entities'

import CategoryCard from '../components/CategoryCard'
import Spinner from '../components/Spinner'
import LazyLoad from '../components/LazyLoad'

class ModelsContainer extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {}

        this._content = document.querySelector('.content')
    }

    _renderLoading = () => {
        return <div className="loading loading--abs loading--60"><Spinner /></div>
    }

    _render = () => {
        const { items } = this.props
        return (
                <div className="list row">
                    {_map(items, item => {
                        return (
                            <div className="col-xs-12 col-sm-6 col-md-4 col-xl-3" key={item.id}>
                                <LazyLoad container={this._content} height={317}>
                                    <CategoryCard item={item} url="catalog" />
                                </LazyLoad>
                            </div>
                        )
                    })}
                </div>
        )
    }

    render = () => {
        const { isLoading } = this.props
        return (
            isLoading ? this._renderLoading() : this._render()
        )
    }
}

ModelsContainer.propTypes = {
    request:  PropTypes.object.isRequired
}
ModelsContainer.defaultProps = {}

const mapStateToProps = (state) => {
    return {
        isLoading: false,
        items: getCategories(state)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        // dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps

)(ModelsContainer)
