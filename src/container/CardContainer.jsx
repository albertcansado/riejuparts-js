import React, { Component, PropTypes } from 'react'
import { findDOMNode } from 'react-dom'
import { connect } from 'react-redux'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import { FormattedMessage } from 'react-intl'
import { Link as RouterLink } from 'react-router'
import Drift from 'drift-zoom/dist/Drift.js'
import Fuse from 'fuse.js'
import 'url-search-params-polyfill'
import cx from 'classnames'

import { API_ROOT, ASSETS_ROOT } from '../middleware/api'
import { fetchCardIfNeeded } from '../actions/models'
import { getCardEntity, getCartParts, getCategoryEntity } from '../reducers/entities';

import Image from '../components/Image'
import Search from '../components/Search'
import PartsList from '../components/Cards/PartsList'
import Loading from '../components/Loading'
import Link from '../components/Link'
import Icon from '../components/Icon'

const setElementsHeight = (component) => {
    const laminaHeight = component._lamina.offsetHeight
    const laminaContentHeight = laminaHeight - component._lamina.querySelector('.lamina__header').offsetHeight
    const laminaListHeight = laminaContentHeight - (component._search.getBoundingClientRect().bottom - component._laminaContent.getBoundingClientRect().top)

    if (component._list) {
        component._list.style.height = laminaListHeight + 'px'
    }

    if (component._laminaContent) {
        component._laminaContent.style.height = laminaContentHeight + 'px'
    }

    if (component._imageContent) {
        component._imageContent.style.height = component._mediaQuery.matches
            ? laminaContentHeight + 'px'
            : 'auto'
    }
}

const getNumberFromRequest = () => {
    const search = new URLSearchParams (window.location.search)

    return search.get('n')
}

const filterInitItems = (items, number) => {
    if (number === null) {
        return items
    }

    number = Number.parseInt(number, 10)
    return items.filter(item => item.number === number)
}

class CardContainer extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {
            isLoadingItems: !props.card.isFetched,
            items: props.card.items,
            useZoom: false,
            initZoom: true,
            searchValue: getNumberFromRequest()
        }

        this._fuse = null
        this._drift = null

        this._lamina = null
        this._list = null
        this._search = null
        this._laminaContent = null
        this._imageContent = null

        /* Public Method */
        this._filterItems = this._filterItems.bind(this)
        this._calculateHeight = this._calculateHeight.bind(this)
        this._enableCalculation = this._enableCalculation.bind(this)
        this.toggleZoom = this.toggleZoom.bind(this)
    }

    componentDidMount = () => {
        const { dispatch, request, card } = this.props
        dispatch(fetchCardIfNeeded(request.version, request.card, card.id))

        this._lamina = findDOMNode(this.refs.lamina)
        this._list = findDOMNode(this.refs.list)
        this._imageContent = findDOMNode(this.refs.img)
        this._search = this._lamina.querySelector('.lamina__search')
        this._laminaContent = this._lamina.querySelector('.lamina__content')

        // Check Media query
        this._mediaQuery = window.matchMedia("(min-width: 1024px)")
        this._enableCalculation(this._mediaQuery)
        this._mediaQuery.addListener(this._enableCalculation)

        setElementsHeight(this)
        window.addEventListener('resize', this._calculateHeight, true)
    }

    componentWillReceiveProps = (nextProps) => {
        const { dispatch, request, card } = this.props
        if (nextProps.card.id !== card.id) {
            // Load new data!
            dispatch(fetchCardIfNeeded(request.version, nextProps.card.alias, nextProps.card.id))
            this.setState({
                isLoadingItems: true
            })
        } else if (nextProps.card.isFetched !== card.isFetched) {
            // Update State
            const items = nextProps.card.items === undefined ? [] : nextProps.card.items
            this.setState({
                isLoadingItems: !nextProps.card.isFetched,
                initZoom: true,
                useZoom: false,
                items: filterInitItems(items, this.state.searchValue)
            })

            this._fuse = this._initFuse(nextProps.card.items)
        }
    }

    componentDidUpdate = () => {
        const { isLoadingItems, initZoom } = this.state
        if (this.props.card.image && !isLoadingItems && initZoom) {
            this._setDrift()
        }
    }

    componentWillUnmount() {
        this._mediaQuery.removeListener(this._enableCalculations)
        window.removeEventListener('resize', this._calculateHeight, true)
    }

    _enableCalculation = (media) => {
        this._shouldCalculateHeight = media.matches
    }

    _calculateHeight = () => {
        setElementsHeight(this)
    }

    _initFuse = (items) => {
        return new Fuse(items, {
            threshold: 0.2,
            location: 0,
            distance: 100,
            maxPatternLength: 32,
            minMatchCharLength: 1,
            keys: ["items.name"]
        })
    }

    _setDrift = () => {
        if (this._drift) {
            // this._drift.disable()
            this._drift.destroy()
        }

        const img = this.refs.img.querySelector('img')
        if (!img) {
            return
        }

        this._drift = new Drift(img, {
            containInline: true,
            inlinePane: true,
            zoomFactor: 2
        })

        if (!this.state.useZoom) {
            this._drift.disable()
        }

        this.setState({
            initZoom: false
        })
    }

    _filterItems = (text) => {
        if (!text) {
            // Reset
            this.setState({
                items: this.props.card.items
            })
        } else if (!isNaN(Number(text))) {
            // Number only
            this.setState({
                items: this.props.card.items.filter(item => item.number.toString().indexOf(text) !== -1)
            })
        } else {
            // Part name
            this.setState({
                items: this._fuse.search(text).sort((a, b) => {
                    return a.number - b.number
                })
            })
        }
    }

    toggleZoom = () => {
        const { useZoom } = this.state
        this._drift[useZoom ? 'disable' : 'enable']()

        this.setState({
            useZoom: !useZoom
        })
    }

    render = () => {
        const { card, cartItems, dispatch, request, category } = this.props
        const { useZoom, searchValue, isLoadingItems } = this.state

        const zoomClass = cx('btn--small', {
            'btn--error': useZoom,
            'btn--success': !useZoom,
            'is-disabled': !card.image
        })

        return (
            <div className="lamina" ref="lamina">
                <header className="lamina__header">
                    <div className="row">
                        <div className="col-xs-8 col-sm-7 col-xl-9">
                            <div className="u-flex flex--middle u-h100">
                                <RouterLink to={`/catalog/${request.category}`} className="btn btn--small">
                                    <Icon icon="left-arrow" className="hidden-md-up" />
                                    <span className="hidden-sm-down">
                                        <FormattedMessage
                                            id="card.returnTo"
                                            defaultMessage="Return to {category}"
                                            values={{
                                                category: category.name
                                            }}
                                        />
                                    </span>
                                </RouterLink>
                                <h4 className="lamina__name u-truncate">
                                    {card.name}
                                </h4>
                            </div>
                        </div>
                        <div className="col-xs-4 col-sm-5 col-xl-3">
                            <div className="btn-group row-xs--end">
                                <Link
                                    btn
                                    className="btn--notice btn--small"
                                    href={`${API_ROOT}/cards/view/${request.version}/${request.card}.pdf`}
                                    target="_blank"
                                >
                                    <Icon icon="download" className="hidden-sm-up" />
                                    <span className="hidden-xs-down">
                                        <FormattedMessage id="cardC.header.pdf" defaultMessage="PDF" />
                                    </span>
                                </Link>
                                <Link btn onClick={this.toggleZoom} className={zoomClass}>
                                    <Icon icon="search" className="hidden-sm-up" />
                                    <span className="hidden-xs-down">
                                        {useZoom
                                            ? <FormattedMessage id="cardC.header.disableZoom" defaultMessage="Disable Zoom" />
                                            : <FormattedMessage id="cardC.header.enableZoom" defaultMessage="Enable Zoom" />
                                        }
                                    </span>
                                </Link>
                            </div>
                        </div>
                    </div>
                </header>
                <article className="lamina__content">
                    <div className="row">
                        <div className="col-xs-12 col-lg-4">
                            <div className="lamina__inner u-flex">
                                <div className="lamina__search">
                                    <Search onChange={this._filterItems} reset={isLoadingItems} defaultValue={searchValue} />
                                </div>
                                <div className="lamina__list" ref="list">
                                    <Loading isLoading={isLoadingItems} className="loading--60">
                                        <PartsList items={this.state.items} cartItems={cartItems} dispatch={dispatch} />
                                    </Loading>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-lg-8 alpha-lg-up">
                            <div className="lamina__image" ref="img">
                                <ReactCSSTransitionGroup
                                    transitionName="fade"
                                    transitionEnterTimeout={500}
                                    transitionLeaveTimeout={500}
                                >
                                    {card.image
                                        ? <Image src={`/img/${card.image}`} key={card.image} alt={card.name} className="lamina__image-img u-centered" data-zoom={`${ASSETS_ROOT}/files/${card.image}`} />
                                        : null
                                    }
                                </ReactCSSTransitionGroup>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        )
    }
}

CardContainer.propTypes = {
    request:  PropTypes.object.isRequired,
    cartItems: PropTypes.object.isRequired
}

CardContainer.defaultProps = {}

const mapStateToProps = (state, ownProps) => {
    return {
        category: getCategoryEntity(state, ownProps.request.category),
        card: getCardEntity(state, ownProps.request.card),
        cartItems: parent._warrantyMode
            ? parent._warranty.items
            : getCartParts(state)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps

)(CardContainer)
