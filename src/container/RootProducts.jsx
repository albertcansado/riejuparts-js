import React, { PureComponent, PropTypes } from 'react'
import { Provider } from 'react-intl-redux'

import ProductsContainer from './ProductsContainer'
import Modal from './ModalReduxContainer'

export default class RootProducts extends PureComponent {
    render() {
        const { store } = this.props
        return (
            <Provider store={store}>
                <div className="u-h100">
                    <ProductsContainer />
                    <Modal />
                </div>
            </Provider>
        )
    }
}

RootProducts.propTypes = {
    store: PropTypes.object.isRequired
}
