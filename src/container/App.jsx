import React, { Component } from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import { connect } from 'react-redux'
import { isEqual } from 'lodash'

import { fetchDataOnInit, fetchModelsIfNeeded, fetchVersionsIfNeeded } from '../actions/models'
import { getLoading } from '../reducers/helpers'

import Modal from './ModalReduxContainer'
import BarTop from '../components/BarTop/BarTop'
import Spinner from '../components/Spinner'

const Wrapper = ({ children }) => {
    return <div className="content">{children}</div>
}

const fetchDataFromRequest = (request, dispatch) => {
    if (request.version !== null) {
        dispatch(
            fetchVersionsIfNeeded(request.category, request.version)
        )
    } else if (request.category !== null) {
        dispatch(
            fetchModelsIfNeeded(request.category)
        )
    }
}

class App extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {
            isLoading: props.isFetching
        }
    }

    componentWillMount = () => {
        const { dispatch, request } = this.props
        dispatch(fetchDataOnInit(request))
    }

    componentWillReceiveProps = (nextProps) => {
        if (!isEqual(nextProps.request, this.props.request)) {
            // this.setState({ isLoading: true })
            fetchDataFromRequest(nextProps.request, this.props.dispatch)
        } else if (nextProps.isFetching !== this.state.isLoading) {
            this.setState({
                isLoading: nextProps.isFetching
            })
        }
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        return !isEqual(nextProps.request, this.props.request)
            || nextProps.isFetching !== this.props.isFetching
    }

    _renderLoading = () => {
        return <div className="loading loading--abs loading--60"><Spinner /></div>
    }

    _renderContent = () => {
        const { request } = this.props
        return  React.Children.map(this.props.children,
            (child) => React.cloneElement(child, {
                request
            })
        )
    }

    render = () => {
        const { request } = this.props
        const { isLoading } = this.state

        return (
            <div style={{height: '100%'}}>
                <BarTop request={request} show={!isLoading} url={`/catalog/${request.category}/${request.version}`} />
                <ReactCSSTransitionGroup
                    transitionName="fade-scale"
                    transitionEnterTimeout={600}
                    transitionLeaveTimeout={300}
                    component={Wrapper}
                >
                    {isLoading ? this._renderLoading() : this._renderContent()}
                </ReactCSSTransitionGroup>
                <Modal />
            </div>
        )
    }
}

/*
 * Options:
 *  - /catalog || /
 *  - /catalog/[catagoryAlias]
 *  - /catalog/[catagoryAlias]/[versionId]/[cardId]
 */
const parseUrl = (state, url) => {
    const parts = url.substring(1).split('/')

    return {
        category: parts[1] || null,
        version: parts[2] || null,
        card: parts[3] || null
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        isFetching: getLoading(state),
        request: parseUrl(state, ownProps.location.pathname)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps

)(App)
