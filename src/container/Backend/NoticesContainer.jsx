import React, { PropTypes } from 'react'
// import { FormattedMessage } from 'react-intl'
import Switch from 'react-ios-switch'

import BaseBackendContainer from './BaseBackendContainer'
import Header from 'components/Backend/Notices/Header'
import Input from 'components/Form/Input'
import Label from 'components/Form/Label'
import Uploader from 'components/Form/Uploader'
import SelectGroups from 'components/Form/SelectGroups'

const initState = {
    entity: {
        important: false,
        show_subdealer: true,
        title: '',
        attachment: '',
        groups: ''
    }
}

class NoticesContainer extends BaseBackendContainer {
    constructor(props, context) {
        super(props, context)

        this.state = Object.assign({}, this.initState, initState)

        this._multipleFields = ['groups']
    }

    _enableSave = nextEntity => !!nextEntity.title

    _setImportant = value => this._updateEntity('important', value)

    _setShowSubdealer = value => this._updateEntity('show_subdealer', value)

    _renderEntity = () => {
        const { entity, isDraft, isNew, isSaving } = this.state
        return (
            <section className="notice box box--clear">
                <Header
                    title={entity.title}
                    isNew={isNew}
                    isDraft={isDraft}
                    isSaving={isSaving}
                    handleSave={this._handleSave}
                    savingMessages={{
                        saving: 'Guardando',
                        special: 'Borrador',
                        notSaved: 'No guardado',
                        saved: 'Guardado'
                    }}
                />
                <div className="box__content">
                    <div className="row">
                        <div className="col-xs-12 col-md-6">
                            <div className="input input--switch input--horizontal">
                                <Label title="Importante" />
                                <Switch
                                    checked={entity.important}
                                    onChange={this._setImportant}
                                />
                            </div>
                            <Input
                                label="Titulo"
                                type="text"
                                name="title"
                                value={entity.title}
                                onChange={this._setProperty}
                            />
                            <SelectGroups
                                label="Grupos"
                                selectAll
                                onSelect={this._setSelectMultiple}
                                value={entity.groups}
                            />
                        </div>
                        <div className="col-xs-12 col-md-6">
                            <div className="input input--switch input--horizontal">
                                <Label title="Mostar al subagente" />
                                <Switch
                                    checked={entity.show_subdealer}
                                    onChange={this._setShowSubdealer}
                                />
                            </div>
                            <Uploader
                                label="Adjunto"
                                name="attachment"
                                accept="application/pdf"
                                onChange={this._setProperty}
                                attachment={entity.attachment}
                            />
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

NoticesContainer.propTypes = {
    id: PropTypes.string,
    fetchUrl: PropTypes.string,
    saveUrl: PropTypes.string.isRequired
}
NoticesContainer.defaultProps = {
    id: null,
    fetchUrl: null
}

export default NoticesContainer
