import React, { PropTypes } from 'react'
// import { FormattedMessage } from 'react-intl'
import Switch from 'react-ios-switch'

import BaseBackendContainer from './BaseBackendContainer'
import Header from 'components/Backend/Notices/Header'
import Icon from 'components/Icon'
import Input from 'components/Form/Input'
import Label from 'components/Form/Label'
import ModelsTree from 'components/Form/ModelsTree'
import Select from 'components/Form/Select'
import SelectAsync from 'components/Form/SelectAsync'
import SelectGroups from 'components/Form/SelectGroups'
import Uploader from 'components/Form/Uploader'

const initState = {
    entity: {
        title: '',
        active: false,
        discontinued: false,
        attachment: {},
        locale: '',
        categories: '',
        groups: '',
        type: '',
        models: ''
    },
    dirty: ['active']
}

class ManualsContainer extends BaseBackendContainer {
    constructor(props, context) {
        super(props, context)

        this.state = Object.assign({}, this.initState, initState)

        this._multipleFields = ['categories', 'groups', 'models']
    }

    _enableSave = nextEntity => !!nextEntity.title

    _setActive = value => this._updateEntity('active', value)

    _setDiscontinued = value => this._updateEntity('discontinued', value)

    _setModels = values => this._updateEntity('models', values.join(','))

    _renderTypesOption = option => {
        return (
            <div className="select-icon u-flex">
                <Icon icon={option.icon} className="select-icon__icon icon--20" />
                <span className="select-icon__label txt-bold">{option.label}</span>
            </div>
        )
    }

    _renderEntity = () => {
        const { entity, isDraft, isNew, isSaving } = this.state
        return (
            <section className="manual box box--clear">
                <Header
                    title={entity.title}
                    isNew={isNew}
                    isDraft={isDraft}
                    isSaving={isSaving}
                    handleSave={this._handleSave}
                    savingMessages={{
                        saving: 'Guardando',
                        special: 'Borrador',
                        notSaved: 'No guardado',
                        saved: 'Guardado'
                    }}
                />
                <div className="box__content">
                    <div className="row">
                        <div className="col-xs-12 col-md-6">
                            <div className="input input--switch input--horizontal">
                                <Label title="Activo" />
                                <Switch
                                    checked={entity.active}
                                    onChange={this._setActive}
                                />
                            </div>
                            <Input
                                label="Titulo"
                                type="text"
                                name="title"
                                value={entity.title}
                                onChange={this._setProperty}
                            />

                            <div className="input row">
                                <div className="col-xs-12 col-md-6">
                                    <Select
                                        label="Idioma"
                                        name="locale"
                                        options={window.config.langs.available}
                                        value={entity.locale}
                                        onChange={this._setSelectProperty}
                                        valueKey="locale"
                                    />
                                </div>
                                <div className="col-xs-12 col-md-6">
                                    <div className="input input--select">
                                        <Label title="Tipo" />
                                        <SelectAsync
                                            url="/d/technical-zone/types.json"
                                            renderOptions={this._renderTypesOption}
                                            name="type"
                                            value={entity.type}
                                            onSelect={this._setSelectProperty}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="input input--select">
                                <Label title="Categorias" />
                                <SelectAsync
                                    url="/d/technical-zone/categories.json"
                                    multiple
                                    name="categories"
                                    value={entity.categories}
                                    searchProps={{
                                        label: 'name',
                                        value: 'id'
                                    }}
                                    joinValues
                                    onSelect={this._setSelectMultiple}
                                />
                            </div>
                            <SelectGroups
                                label="Grupos"
                                selectAll
                                onSelect={this._setSelectMultiple}
                                value={entity.groups}
                            />

                            <Uploader
                                label="Archivo"
                                accept="application/pdf, video/*, image/png, image/jpg, image/gif, image/jpeg, application/octet-stream, application/zip, application/x-zip, application/x-zip-compressed"
                                onChange={this._setProperty}
                                name='attachment'
                                attachment={entity.attachment}
                            />
                        </div>
                        <div className="col-xs-12 col-md-6">
                            <div className="input input--switch input--horizontal">
                                <Label title="Descatalogado" />
                                <Switch
                                    checked={entity.discontinued}
                                    onChange={this._setDiscontinued}
                                />
                            </div>
                            <div className="input input--select">
                                <Label title="Modelos" addon="(Opcional)" />
                                <p className="txt--small txt-italic">Asigna a que modelos corresponde el recurso, lo que hará que aparezaca en la ficha del modelo dentro del catálogo.</p>
                                <ModelsTree
                                    url="/d/models/tree.json"
                                    onUpdate={this._setModels}
                                    values={entity.models}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

ManualsContainer.propTypes = {
    id: PropTypes.string,
    fetchUrl: PropTypes.string,
    saveUrl: PropTypes.string.isRequired
}
ManualsContainer.defaultProps = {
    id: null,
    fetchUrl: null
}

export default ManualsContainer
