import React, { PureComponent, PropTypes} from 'react'
import { connect } from 'react-redux'
import { FormattedMessage } from 'react-intl'

import { attachEngine, unattachEngine } from '../../actions/backend/entities'
import { getEngines } from '../../reducers/backend/entities'

import Engine from '../../components/Backend/Engine'
import EngineEdit from '../../components/Backend/EngineEdit'
import Information from '../../components/Information'

class EnginesContainer extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.handleDelete = this.handleDelete.bind(this)
        this.handleAdd = this.handleAdd.bind(this)
    }

    handleDelete = (engine) => {
        const { model, dispatch } = this.props

        dispatch(
            unattachEngine(model, engine)
        )
    }

    handleAdd = (data) => {
        const { model, dispatch } = this.props

        dispatch(
            attachEngine(model, data)
        )
    }

    render = () => {
        const { engines } = this.props

        const count = engines.length

        return (
            <div>
                <h2 className="u-mgb--15">
                    <FormattedMessage id="models.engines.title" defaultMessage="Available Engines" />
                </h2>
                <Information>
                    <p>A continuación aparecen todos los motores de que dispone el modelo. Puedes añadir tantos motores como quieras.
                    Añade información cuando haya más de un motor para que el cliente entienda que diferencia hay</p>
                </Information>
                <div className="table u-mgt--15">
                    <header className="table__head">
                        <span className="table__col table__col--1 table__col--45">
                            <FormattedMessage id="models.engines.engine" defaultMessage="Engine" />
                        </span>
                        <span className="table__col table__col--2 table__col--45">
                            <FormattedMessage id="models.engines.information" defaultMessage="Information" />
                        </span>
                        <span className="table__col table__col--options"></span>
                    </header>
                    <section className="table__body">
                        {engines.map(engine => <Engine engine={engine} key={engine.id} handleDelete={count > 1 ? this.handleDelete : null} />)}
                        <EngineEdit key="ENGINE_ADD" handleSubmit={this.handleAdd} />
                    </section>
                </div>
            </div>
        )
    }
}

EnginesContainer.propTypes = {
    model: PropTypes.object.isRequired,
    engines: PropTypes.array.isRequired
}

const mapStateToProps = (state, ownProps) => {
    return {
        engines: getEngines(state, ownProps.model.engines)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EnginesContainer)
