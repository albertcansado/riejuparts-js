import React, { PropTypes } from 'react'
import BaseBackendContainer from './BaseBackendContainer'
import Switch from 'react-ios-switch'
import idx from 'idx'
import Cookies from 'js-cookie'

import Header from 'components/Backend/Notices/Header'
import Label from 'components/Form/Label'
import Translations from 'components/Form/Translations'
import SelectAsync from 'components/Form/SelectAsync'
import SelectGroups from 'components/Form/SelectGroups'
import ImageUploader from 'components/Form/ImageUploader'
import ProductsPartsList from 'components/Backend/Products/ProductsPartsList'

const initState = {
    entity: {
        name: {},
        active: false,
        featured: false,
        image: null,
        category_id: '',
        parts: []
    },
    dirty: ['active']
}

class ProductsContainer extends BaseBackendContainer {
    constructor(props, context) {
        super(props, context)

        this.state = Object.assign({}, this.initState, initState)

        this._locale = Cookies.get('locale')
    }

    _enableSave = nextEntity => !!idx(nextEntity, _ => _.name[this._locale])

    _beforeAppendToBody = (entity, key) => {
        if (key === 'parts') {
            return entity.parts.map(part => part.id).join(',')
        } else if (key === 'name') {
            return JSON.stringify(entity.name)
        }

        return entity[key]
    }

    _setActive = value => this._updateEntity('active', value)

    _setFeatured = value => this._updateEntity('featured', value)

    _appendPart = part => this._updateEntity('parts', [...this.state.entity.parts, part])

    _deletePart = part => this._updateEntity('parts', this.state.entity.parts.filter(item => item.id !== part.id))

    _renderEntity = () => {
        const { entity, isDraft, isNew, isSaving } = this.state
        const headerTitle = idx(entity, _ => _.name[this._locale])
        return (
            <section className="products box box--clear">
                <Header
                    title={headerTitle}
                    isNew={isNew}
                    isDraft={isDraft}
                    isSaving={isSaving}
                    handleSave={this._handleSave}
                    savingMessages={{
                        saving: 'Guardando',
                        special: 'Borrador',
                        notSaved: 'No guardado',
                        saved: 'Guardado'
                    }}
                />
                <div className="box__content">
                    <div className="row">
                        <div className="col-xs-12 col-md-6">
                            <div className="input-group">
                                <div className="input input--switch input--horizontal">
                                    <Label title="Activo" />
                                    <Switch
                                        checked={entity.active}
                                        onChange={this._setActive}
                                    />
                                </div>
                                <div className="input input--switch input--horizontal">
                                    <Label title="Destacado" />
                                    <Switch
                                        checked={entity.featured}
                                        onChange={this._setFeatured}
                                    />
                                </div>
                            </div>
                            <Translations
                                labelTitle="Name"
                                name="name"
                                defaultValues={entity.name}
                                onChange={this._setProperty}
                            />

                            <div className="input input--select">
                                <Label title="Categorias" />
                                <SelectAsync
                                    url="/d/products/categories.json"
                                    name="category_id"
                                    value={entity.category_id}
                                    searchProps={{
                                        label: 'name',
                                        value: 'id'
                                    }}
                                    onSelect={this._setSelectProperty}
                                />
                            </div>

                            <SelectGroups
                                label="Grupos"
                                selectAll
                                onSelect={this._setSelectMultiple}
                                value={entity.groups}
                            />
                        </div>
                        <div className="col-xs-12 col-md-6">
                            <ImageUploader
                                labelTitle="Imagen"
                                name="image"
                                image={entity.image}
                                onChange={this._setProperty}
                            />
                        </div>
                    </div>
                    <ProductsPartsList parts={entity.parts} handleAdd={this._appendPart} handleDelete={this._deletePart} />
                </div>
            </section>
        )
    }
}

ProductsContainer.propTypes = {
    id: PropTypes.string,
    fetchUrl: PropTypes.string,
    saveUrl: PropTypes.string.isRequired
}
ProductsContainer.defaultProps = {
    id: null,
    fetchUrl: null
}

export default ProductsContainer
