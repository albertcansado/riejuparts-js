import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import { debounce } from 'lodash'

import { fetchRequest } from '../../../middleware/api'

import Filter from '../../../components/Backend/Translations/Filter'
import Part from '../../../components/Backend/Translations/Part'
import LazyLoad from '../../../components/LazyLoad'

const generateUrl = ({ paging, filters, isFetching }) => {
    let params = {
        page: paging.page + 1,
    }

    // We only use filters when list isFetching (On init, or filtering)
    if (isFetching) {
        if (filters.sku) {
            params['s'] = filters.sku
        }
    }

    const args = Object.keys(params)
        .map(key => key + '=' + encodeURIComponent(params[key]))
        .join('&')

    return `/d/parts/translations.json?${args}`
}

class PartsTranslationsContainer extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {
            langs: [],
            items: [],
            isLoaded: false,
            isFetching: true,
            isFetchingNext: false,
            paging: {
                page: 0,
                count: 0,
                perPage: 20,
                pageCount: 0
            }
        }
    }

    componentDidMount = () => {
        Promise.all([
            this.fetchUserLangs(),
            this.fetchItems()
        ]).then(() => {
            this.setState({
                isLoaded: true
            })
        })
    }

    shouldComponentUpdate = (_, nextState) => {
        if (
            nextState.isFetching !== this.state.isFetching ||
            nextState.paging.page !== this.state.paging.page
        ) {
            return true
        }

        return false
    }

    fetchItems = debounce(() => {
        const { paging, isFetching, isFetchingNext } = this.state

        if (paging.page > paging.pageCount || isFetchingNext) {
            return
        }

        this.setState({
            isFetchingNext: true
        })

        const url = generateUrl({ paging, filters: this.refs.filter.getData(), isFetching })
        fetchRequest(url)
            .then(({ json, response }) => {
                if (!response.ok || !json.success) {
                    this.setState({
                        isFetching: false,
                        isFetchingNext: false
                    })

                    // TODO Show alert

                    return Promise.resolve()
                }

                this.setState({
                    items: [
                        ...this.state.items,
                        ...json.data,
                    ],
                    paging: {
                        ...this.state.paging,
                        ...json.meta
                    },
                    isFetching: false,
                    isFetchingNext: false
                })
            })
    }, 100)

    fetchUserLangs = () => {
        return fetchRequest('/d/parts/translations/langs.json')
            .then(({ json, response }) => {
                if (!response.ok || !json.success) {
                    this.setState({
                        isFetching: false,
                        isFetchingNext: false
                    })

                    // TODO Show alert

                    return Promise.resolve()
                }

                this.setState({
                    langs: json.data
                })
            })
    }

    fetchUpdateTranslation = (id, data) => {
        return fetchRequest(`/d/parts/translations/${id}.json`,
            {
                method: 'POST',
                body: data
            }
        )
    }

    handleFilter = () => {
        // "Reset"
        this.setState({
            items: [],
            isFetching: true,
            paging: {
                page: 0,
                count: 0
            }
        })

        this.fetchItems()
    }

    /* Filter Selector */
    _renderSelectModels = (option) => {
        return (
            <div className="u-flex flex--middle">
                <div className="txt-bold">{option.name}</div>
                <div className="u-mgl--10">{option.year}</div>
            </div>
        )
    }

    /* Loading */
    _renderLoading = () => {
        const { paging, isFetching } = this.state

        if (isFetching) {
            return (<div className="m-translations__loadmore txt-center txt--middle txt-bold">Loading...</div>)
        }

        if (paging.page < paging.pageCount) {
            return (
                <LazyLoad height={1} onVisible={() => this.fetchItems()} key={`lazy-${paging.page}`}>
                    <div className="m-translations__loadmore txt-center txt--middle txt-bold">Loading...</div>
                </LazyLoad>
            )
        }

        return null
    }

    /* List Item Renderer */
    renderItem = (item, key) => {
        const { langs } = this.state

        return <Part part={item} langs={langs} handleUpdate={::this.fetchUpdateTranslation} key={key} />
    }

    _renderItems = () => {
        const { items, paging, isLoaded, isFetching } = this.state

        if (!isLoaded || isFetching) {
            return null
        }

        if (paging.count === 0) {
            return (
                <p className="txt-center txt-bold">There are not parts with these criterias</p>
            )
        }

        return items.map(::this.renderItem)
    }

    _renderHeader = () => {
        const { paging, isFetching, isLoaded } = this.state

        if (isFetching || !isLoaded) {
            return null
        }

        return (
            <header className="m-translations__meta txt-right">
                <FormattedMessage
                    id="parts.records"
                    defaultMessage="{count} of {total} records"
                    values={{
                        count: paging.perPage > paging.count ? paging.count.toString() : (paging.perPage * paging.page).toString(),
                        total: paging.count.toString()
                    }}
                />
            </header>
        )
    }

    render = () => {
        const { langs } = this.state

        return (
            <div className="m-translations">
                <header className="m-translations__filter box">
                    <Filter ref="filter" onSubmit={this.handleFilter} langs={langs} />
                </header>
                <section className="m-translations__content box">
                    <div className="m-translations__content-inner">
                        {this._renderHeader()}
                        <div className="m-translations__list u-mgt--15">
                            {this._renderItems()}
                            {this._renderLoading()}
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default PartsTranslationsContainer
