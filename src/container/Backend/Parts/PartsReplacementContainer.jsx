import React, { PureComponent } from 'react'
// import { FormattedMessage } from 'react-intl'
import cx from 'classnames'
import swal from 'sweetalert2'
import idx from 'idx'

import SearchPart from '../../../components/Form/SearchPart'
import SelectAsync from '../../../components/Form/SelectAsync'
import Label from '../../../components/Form/Label'
import Information from '../../../components/Information'
import Link from '../../../components/Link'

import { fetchRequest } from '../../../middleware/api'

const defaultState = {
    isLoading: false,
    replaceTo: null,
    replaceWith: null,
    filter: {
        category: null
    }
}

class PartsReplacementContainer extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = defaultState

        this.reset = this.reset.bind(this)
        this.ejecute = this.ejecute.bind(this)
        this.selectTo = this.selectTo.bind(this)
        this.selectWith = this.selectWith.bind(this)
    }

    reset = () => {
        this.setState(defaultState)
    }

    ejecute = ev => {
        const { replaceTo, replaceWith, filter, isLoading } = this.state

        if (!replaceTo || !replaceWith || isLoading) {
            return
        }

        this.setState({
            isLoading: true
        })

        fetchRequest('/d/parts/replacements.json', {
            method: 'POST',
            body: {
                to: replaceTo.id,
                'with': replaceWith.id,
                filter: {
                    category: idx(filter, _ => _.category.value)
                }
            }
        }).then(({ json, response }) => {
            if (!response.ok || !json.success) {
                swal({
                    title: 'Error',
                    text: json.hasOwnProperty('error') ? json.error.msg : response.statusText,
                    type: "error",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Ok'
                }).then(() => {
                    this.setState({
                        isLoading: false
                    })
                }).catch(swal.noop)
            } else {
                swal({
                    title: json.data.msg,
                    type: "success",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Ok'
                }).then(() => {
                    this.reset()
                }).catch(swal.noop)
            }
        }).catch(error => {
            this.reset()
            swal({
                title: 'Error',
                text: "General Error",
                type: "error",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Ok'
            })
        })
    }

    selectTo = option => {
        this.setState({
            replaceTo: option
        })
    }

    selectWith = option => {
        this.setState({
            replaceWith: option
        })
    }

    selectCategory = option => {
        this.setState({
            filter: {
                ...this.state.filter,
                category: option
            }
        })
    }

    _renderOption = (option) => {
        return (
            <div className="u-flex flex--middle">
                <div className="u-left">
                    <div className="txt-bold">{option.sku}</div>
                    <div>{option.name}</div>
                </div>
            </div>
        )
    }

    render = () => {
        const { replaceTo, replaceWith, filter, isLoading } = this.state
        return (
            <div className="box box--clear">
                <header className="box__header u-flex flex--middle flex--between">
                    <h3 className="box__header-title">Sustituciones</h3>
                    <Link
                        btn
                        type="success"
                        className={cx({
                            'is-disabled': !replaceTo || !replaceWith,
                            'btn--loading': isLoading
                        })}
                        onClick={this.ejecute}>
                        Procesar
                    </Link>
                </header>
                <section className="box__content">
                    <Information className="u-mgb--15">
                        En el primer desplegable busca la referencia que quieres sustituir. En el segundo desplegable introduce la referencia que quieres que la sustituya.
                        <br />
                        <strong>RECUERDA:</strong> Sustitución significa que la segunda pieza aparecerá debajo la primera en las láminas. <strong>nunca</strong> se borrará la primera
                    </Information>
                    <div className="row">
                        <div className="col-xs-12 col-md-4">
                            <SearchPart
                                label="Sustituir la pieza"
                                fetchUrl="/d/parts/lookup.json?u=1&id=1&s={input}"
                                onChange={this.selectTo}
                                value={replaceTo}
                            />
                        </div>
                        <div className="col-xs-12 col-md-4">
                            <SearchPart
                                label="Por"
                                fetchUrl="/d/parts/lookup.json?s={input}"
                                onChange={this.selectWith}
                                value={replaceWith}
                            />
                        </div>
                        <div className="col-xs-12 col-md-4">
                            <div className="input input--select">
                                <Label title="Aplicar en: (Opcional)" className="input__label" />
                                <SelectAsync
                                    url="/d/categories.json"
                                    clearable
                                    onSelect={this.selectCategory}
                                    value={filter.category}
                                />
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default PartsReplacementContainer
