import React, { PureComponent, PropTypes } from 'react'
import { connect } from 'react-redux'
import { FormattedMessage } from 'react-intl'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import { shallowCompareWithoutFunctions } from 'shallow-compare-without-functions'
import validate from 'uuid-validate'

import { fetchModelIfNeeded, updateModel } from '../../actions/backend/entities'
import { createCard } from '../../actions/backend/cards'
import { getLoading } from '../../reducers/helpers'
import { getEntity } from '../../reducers/backend/entities'

import CardsContainer from './CardsContainer'
import EnginesContainer from './EnginesContainer'
import Input from '../../components/Form/Input'
import Label from '../../components/Form/Label'
import SelectAsync from '../../components/Form/SelectAsync'
import SelectGroups from '../../components/Form/SelectGroups'
import Spinner from '../../components/Spinner'
import VersionsContainer from './VersionsContainer'

class ModelContainer extends PureComponent {
    constructor(props, context) {
        super(props, context)

        if (!validate(props.id)) {
            throw Error('Model id must be a valid uuid ' + props.id + ' given')
        }

        this.handleAddCard = this.handleAddCard.bind(this)
    }

    componentWillMount = () => {
        const { dispatch, id } = this.props
        dispatch(fetchModelIfNeeded(id))
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        return shallowCompareWithoutFunctions(this, nextProps, nextState)
    }

    handleAddCard = (name) => {
        const { dispatch, id } = this.props
        return dispatch(
            createCard({
                name,
                model: 'Models',
                foreign_key: id,
            })
        )
    }

    handleUpdateModel = (field, value) => {
        const { dispatch, model } = this.props

        if (model.hasOwnProperty(field) && model[field] !== value) {
            dispatch(
                updateModel(model, {
                    [field]: value
                })
            )
        }
    }

    _setProperty = (value, name) => this.handleUpdateModel(name, value)

    _setSelectProperty = (option, options) => this.handleUpdateModel(options.name, option[options.valueKey])

    _setSelectMultiple = (value, options) => this.handleUpdateModel(options.name, value.map(it => it[options.valueKey]).join(','))

    _renderLoading = () => {
        return <div className="loading loading--abs loading--60"><Spinner /></div>
    }

    render = () => {
        const { isLoading, model } = this.props

        if (isLoading) {
            return this._renderLoading()
        }

        return (
            <Tabs
                className="tabs u-h100"
                selectedTabPanelClassName="tabs__panel--is-open"
                selectedTabClassName="tabs__tab--is-active"
            >
                <TabList className="tabs__list tabs__list--shadow box">
                    <Tab className="tabs__tab">
                        <FormattedMessage id="models.model" defaultMessage="Model" />
                    </Tab>
                    <Tab className="tabs__tab">
                        <FormattedMessage id="models.engines" defaultMessage="Engines" />
                        <span className="tabs__num">({model.engines ? model.engines.length : 0})</span>
                    </Tab>
                    <Tab className="tabs__tab">
                        <FormattedMessage id="models.cards" defaultMessage="Cards" />
                        <span className="tabs__num">({model.cards ? model.cards.length : 0})</span>
                    </Tab>
                    <Tab className="tabs__tab">
                        <FormattedMessage id="models.verions" defaultMessage="Versions" />
                        <span className="tabs__num">({model.versions ? model.versions.length : 0})</span>
                    </Tab>
                </TabList>
                    <TabPanel className="tabs__panel box">
                        <div className="row">
                            <div className="col-xs-12 col-md-6">
                                <Input
                                    label="Name"
                                    name="name"
                                    value={model.name}
                                    onBlur={this._setProperty}
                                />

                                <Input
                                    label="Year"
                                    value={model.year}
                                    name="year"
                                    onBlur={this._setProperty}
                                />
                            </div>
                            <div className="col-xs-12 col-md-6">
                                <div className="input input--select">
                                    <Label title="Category" />
                                    <SelectAsync
                                        url="/d/categories.json"
                                        value={model.category}
                                        placeholder="Select a category"
                                        name="category"
                                        onSelect={this._setSelectProperty}
                                    />
                                </div>
                                <SelectGroups
                                    label="Grupos"
                                    selectAll
                                    onSelect={this._setSelectMultiple}
                                    value={model.groups}
                                />
                            </div>
                        </div>
                    </TabPanel>

                    <TabPanel className="tabs__panel box">
                        <EnginesContainer model={model} />
                    </TabPanel>

                    <TabPanel className="tabs__panel box">
                        <CardsContainer
                            cards={model.cards}
                            model="Models"
                            entity={model}
                        />
                    </TabPanel>

                    <TabPanel className="tabs__panel box">
                        <VersionsContainer ids={model.versions} model_id={model.id} />
                    </TabPanel>
            </Tabs>
        )
    }
}

ModelContainer.propTypes = {
    id: PropTypes.string.isRequired,
    isLoading: PropTypes.bool.isRequired,
    model: PropTypes.object.isRequired
}

const mapStateToProps = (state, ownProps) => {
    return {
        isLoading: getLoading(state),
        model: getEntity(state, ownProps.id)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ModelContainer)
