import React, { PureComponent, PropTypes } from 'react'
import { intlShape } from 'react-intl'
import { map as _map } from 'lodash'

import { getWarrantyHistory } from '../../lib/messages'
import { fetchRequest } from '../../middleware/api'

import HistoryList from '../../components/Warranty/HistoryList'

class HistoryContainer extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            loading: true,
            histories: []
        }

        // Cache Translate Messages
        this._messages = getWarrantyHistory(context.intl.formatMessage)

        this.fetchHistories = this.fetchHistories.bind(this)

        if (this.state.loading) {
            this.fetchHistories(props.number)
        }
    }

    fetchHistories = () => {
        const { number } = this.props

        fetchRequest(`/warranties/history.json?n=${number}`)
        .then(({ json, response }) => {
            if (!response.ok || !json.success) {
                return Promise.reject(json.error.msg)
            }

            this.setState({
                loading: false,
                histories: json.data.histories
            })
        })
    }

    render = () => {
        const { handleClose } = this.props
        const { histories, loading } = this.state

        if (loading) {
            return null
        }

        return <HistoryList
            handleClose={handleClose}
            histories={histories}
            messages={{
                notYet: this._messages.warrantyHistoryNotYet,
                prewarranty: this._messages.warrantyHistoryPreWarranty
            }} />
    }
}

HistoryContainer.contextTypes = {
    intl: intlShape.isRequired
}

HistoryContainer.propTypes = {
    number: PropTypes.string.isRequired,
    handleClose: PropTypes.func
}
HistoryContainer.defaultProps = {}

export default HistoryContainer
