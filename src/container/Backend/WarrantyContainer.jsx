import React, { PureComponent, PropTypes } from 'react'
// import { FormattedMessage } from 'react-intl'
import { keyBy as _keyBy, maxBy as _maxBy } from 'lodash'
import idx from 'idx'
import swal from 'sweetalert2'

import { fetchRequest } from '../../middleware/api'

import Header from '../../components/Backend/Warranty/Header'
import WarrantyComments from '../../components/Backend/Warranty/WarrantyComments'
import WarrantyInformation from '../../components/Backend/Warranty/WarrantyInformation'
import WarrantyParts from '../../components/Backend/Warranty/WarrantyParts'
import WarrantyGenerations from '../../components/Backend/Warranty/WarrantyGenerations'
import WarrantyAttachments from '../../components/Backend/Warranty/WarrantyAttachments'
import Modal from '../../components/Modal'
import HistoryContainer from './HistoryContainer'

import Spinner from '../../components/Spinner'
// import Information from '../../components/Information'

class WarrantyContainer extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            isLoading: true,
            errorOnLoad: false,
            isSaving: false,
            status: null,
            warranty: {},
            causes: {},
            statuses: [],
            actions: {
                pgr: {
                    allowed: true,
                    value: true
                },
                pga: {
                    allowed: true,
                    value: true
                },
                pgt: {
                    allowed: true,
                    value: true
                }
            },
            modalProps: {
                isOpen: false
            }
        }

        // Public Methods
        this._async = this._async.bind(this)
        this._updateAction = this._updateAction.bind(this)
        this._updateWarranty = this._updateWarranty.bind(this)
        this._addLine = this._addLine.bind(this)
        this._deleteLine = this._deleteLine.bind(this)

        this._closeModal = this._closeModal.bind(this)
    }

    _loadWarranty = () => {
        return fetchRequest(`/d/warranties/${this.props.id}.json`)
        .then(({ json, response }) => {
            if (!response.ok || !json.success) {
                return Promise.reject(json.error.msg)
            }

            this.setState({
                status: json.data.warranty.status,
                warranty: json.data.warranty,
                statuses: json.data.statuses,
                actions: json.data.actions
            })
        })
    }

    _loadCauses = () => {
        return fetchRequest(`/d/warranties/causes.json`)
        .then(({ json, response }) => {
            if (!response.ok || !json.success) {
                return Promise.reject(json.error.msg)
            }

            this.setState({
                causes: _keyBy(json.data, 'id')
            })
        })
    }

    _async = ({ body, method, url }) => {
        return fetchRequest(url, { method, body })
            .then(({ json, response}) => {
                if (!response.ok || !json.success) {
                    return Promise.reject(
                        idx(json, _ => _.error.msg) || response.statusText
                    )
                }

                return json.data
            })
    }

    componentWillMount = () => {
        Promise.all([
            this._loadWarranty(),
            this._loadCauses()
        ]).then (() => {
            this.setState({
                isLoading: false
            })
        })/*.catch (text => {
            alert(text)
            this.setState({
                isLoading: false,
                errorOnLoad: true
            })
        })*/
    }

    _closeModal = () => {
        this.setState({
            modalProps: {
                isOpen: false
            }
        })
    }

    _checkPartProcessing = line => {
        const { actions } = this.state

        const repair = idx(line, _ => _.part.repair)
        if (repair && !actions.pgr.allowed) {
            line.part['repair'] = false
        } else if (!repair && !actions.pga.allowed) {
            line.part['repair'] = true
        }

        return line
    }

    _updateAction = (key, value) => {
        this.setState({
            actions: {
                ...this.state.actions,
                [key]: {
                    ...this.state.actions[key],
                    value
                }
            }
        })
    }

    _updateWarranty = (key, value) => {
        let async = true
        switch (key) {
            case 'extra_time':
                value = Math.max(0, Number.parseFloat(value))
                break;
            case 'status':
                value = Number.parseInt(value, 10)
                /*if (value === '') {
                    return
                }*/
                async = false
                break
            default:
        }

        const oldValue = this.state.warranty[key]
        if (!this.state.warranty.hasOwnProperty(key) || oldValue === value) {
            return
        }

        this.setState({
            isSaving: async,
            warranty: {
                ...this.state.warranty,
                [key]: value
            }
        })

        if (!async) {
            return
        }

        this._async({
            url: `/d/warranties/${this.props.id}`,
            method: 'POST',
            body: {
                [key]: value
            }
        }).then(() => {
            this.setState({
                isSaving: false
            })
        }).catch(err => {
            this.setState({
                isSaving: false,
                warranty: {
                    ...this.state.warranty,
                    [key]: oldValue
                }
            })
        })
    }

    _addLine = line => {
        const nextLine = this.state.warranty.lines.length

        line = this._checkPartProcessing(line)

        this.setState({
            isSaving: true,
            warranty: {
                ...this.state.warranty,
                lines: [
                    ...this.state.warranty.lines,
                    line
                ]
            }
        })

        this._async({
            url: `/d/warranties/${this.props.id}/lines.json`,
            method: 'PUT',
            body: {
                qty: line.qty,
                cause_id: line.cause_id,
                part_id: line.part.id
            }
        }).then(data => {
            const lines = this.state.warranty.lines
            lines[nextLine]['internal_id'] = data.id

            this.setState({
                isSaving: false,
                warranty: {
                    ...this.state.warranty,
                    lines: lines
                }
            })
        }).catch(err => {
            this.setState({
                isSaving: false,
                warranty: {
                    ...this.state.warranty,
                    lines: this.state.warranty.lines.filter((lines, key) => key !== nextLine)
                }
            })
        })
    }

    _deleteLine = line => {
        swal({
            title: 'Are you sure',//this._messages.lineDeleteTitle,
            // text: this._messages.cartListDeleteText,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: 'Si'//this._messages.lineDeleteConfirm,
        }).then (() => {
            const backendId = line.hasOwnProperty('internal_id') ? line.internal_id : line.id
            const linesCopy = this.state.warranty.lines

            this.setState({
                isSaving: true,
                warranty: {
                    ...this.state.warranty,
                    lines: this.state.warranty.lines.filter(l => l.id !== line.id)
                }
            })

            this._async({
                url: `/d/warranties/${this.props.id}/lines/${backendId}.json`,
                method: 'DELETE'
            }).then(() => {
                this.setState({
                    isSaving: false
                })
            }).catch(err => {
                this.setState({
                    isSaving: false,
                    warranty: {
                        ...this.state.warranty,
                        lines: linesCopy
                    }
                })
            })
        }).catch(swal.noop)
    }

    _renderDealersNotMatch = () => {
        const { warranty } = this.state

        if (warranty.dealer_id === warranty.frame.dealer_id) {
            return null;
        }

        return (
            <div className="information information--warranty txt--small txt-bold flex--center u-mgt--15">
                <p>La garantia la ha tramitado un distribuidor distinto al que vendio la moto</p>
            </div>
        )
    }

    _showHistoryModal = () => {
        this.setState({
            modalProps: {
                isOpen: true,
                component: HistoryContainer,
                componentProps: {
                    number: idx(this.state.warranty, _ => _.frame.number)
                }
            }
        })
    }

    _renderLoading = () => {
        return <div className="loading loading--abs loading--60"><Spinner /></div>
    }

    _renderWarranty = () => {
        const { warranty, causes, statuses, actions, isSaving, status } = this.state
        const maxLineTime = Math.max(idx(_maxBy(warranty.lines, 'part.hours'), _ => _.part.hours), 0)

        return (
            <section className="warranty box box--clear">
                <Header
                    statuses={statuses}
                    status={status}
                    warranty={warranty}
                    isSaving={isSaving}
                    savingMessages={{saving: 'Guardando', saved: 'Guardado'}}
                    actions={actions}
                    displayHistories={this._showHistoryModal}
                />
                <div className="box__content">
                    <section className="warranty-block warranty-block--info">
                        <div className="row">
                            <div className="col-xs-12 col-md-7">
                                <WarrantyInformation
                                    warranty={warranty}
                                    extraTime={warranty.extra_time}
                                    handleTime={val => this._updateWarranty('extra_time', val)}
                                    statuses={statuses}
                                    handleStatus={val => this._updateWarranty('status', val)}
                                />
                                {this._renderDealersNotMatch()}
                            </div>
                            <div className="col-xs-12 col-md-5">
                                <h4 className="warranty-block__subtitle">Generar</h4>
                                <WarrantyGenerations
                                    actions={actions}
                                    onChange={this._updateAction}
                                    totalTime={maxLineTime + warranty.extra_time}
                                    lines={warranty.lines}
                                    status={warranty.status}
                                />
                            </div>
                        </div>
                    </section>
                    <WarrantyComments
                        dealer={warranty.comment_dealer}
                        factory={warranty.comment_factory}
                        onChange={val => this._updateWarranty('comment_factory', val)}
                    />
                    <WarrantyAttachments
                        attachments={warranty.attachments}
                    />
                    <WarrantyParts
                        lines={warranty.lines}
                        causes={causes}
                        maxTime={maxLineTime}
                        onAddLine={this._addLine}
                        dealerId={warranty.dealer.id}
                        onDeleteLine={this._deleteLine}
                    />
                </div>
            </section>
        )
    }

    _renderModal = () => {
        const { modalProps } = this.state

        if (!modalProps.isOpen) {
            return null
        }

        return React.createElement(
            Modal,
            {
                ...modalProps,
                handleClose: this._closeModal
            }
        )
    }

    render = () => {
        const { isLoading, errorOnLoad } = this.state

        if (errorOnLoad) {
            return null
        }

        if (isLoading) {
            return this._renderLoading()
        }

        return (
            <div>
                {this._renderWarranty()}
                {this._renderModal()}
            </div>
        )
    }
}

WarrantyContainer.propTypes = {
    id: PropTypes.string.isRequired
}
WarrantyContainer.defaultProps = {}

export default WarrantyContainer
