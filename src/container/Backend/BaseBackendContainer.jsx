import React, { PureComponent, PropTypes } from 'react'
import { isEqual } from 'lodash'
import idx from 'idx'
import swal from 'sweetalert2'

import { fetchRequest } from '../../middleware/api'

import Spinner from 'components/Spinner'

export default class BaseBackendContainer extends PureComponent {
    static propTypes = {
        id: PropTypes.string,
        fetchUrl: PropTypes.string,
        saveUrl: PropTypes.string
    }

    static defaultProps = {
        id: null
    }

    constructor(props, context) {
        super(props, context)

        this.initState = {
            isNew: props.id === null,
            isDraft: false,
            isSaving: false,
            isLoading: props.id !== null,
            entity: {},
            dirty: []
        }

        this._multipleFields = []
    }

    componentDidMount = () => {
        if (!this.state.isNew) {
            this._fetchEntity();
        }
    }

    _parseEntityOnFetch = (data, fields) => {
        const joinedFields = {}
        fields.forEach(field => {
            if (data.hasOwnProperty(field)) {
                joinedFields[field] = data[field].join(',')
            }
        })

        return Object.assign({}, data, joinedFields)
    }

    _fetchEntity = () => {
        const { id, fetchUrl } = this.props
        const url = fetchUrl.replace('{id}', id)

        fetchRequest(url)
            .then(({ json, response}) => {
                if (!response.ok || !json.success) {
                    return Promise.reject(
                        idx(json, _ => _.error.msg) || response.statusText
                    )
                }

                this.setState({
                    isLoading: false,
                    entity: this._parseEntityOnFetch(json.data, this._multipleFields)
                })

                return Promise.resolve()
            }).catch (e => {
                console.log(e)
                swal('Error', 'Error cargando los datos', 'error')
            })
    }

    _beforeAppendToBody = (entity, key) => entity[key]

    _handleSave = () => {
        const { isNew, isSaving, isDraft, entity, dirty } = this.state

        if (isSaving || !isDraft) {
            return
        }

        this.setState({
            isSaving: true
        })

        const data = new FormData()
        let url = this.props.saveUrl
        if (!isNew) {
            url += '/' + entity.id
            data.append('_method', 'PUT')
        }
        url += '.json'

        // Body
        dirty.forEach(key => data.append(key, this._beforeAppendToBody(entity, key)))

        fetchRequest(url, {
            method: 'POST',
            body: data
        }).then(({json, response}) => {
            if (!response.ok || !json.success) {
                return Promise.reject(
                    idx(json, _ => _.error.msg) || response.statusText
                )
            }

            let nextState = {
                isSaving: false,
                isDraft: false,
                dirty: []
            }
            if (isNew) {
                nextState['isNew'] = false
                nextState['entity'] = {
                    ...this.state.entity,
                    id: json.data.id
                }
            }

            this.setState(nextState)
        }).catch (response => {
            this.setState({
                isSaving: false
            })

            swal('Error', 'Error on save', 'error')
        })
    }

    _setDirty = key => {
        if (this.state.dirty.indexOf(key) !== -1) {
            return this.state.dirty
        }

        return [
            ...this.state.dirty,
            key
        ]
    }

    _enableSave = nextEntity => {
        return true
    }

    _updateEntity = (key, value) => {
        if (isEqual(this.state.entity[key], value)) {
            return
        }

        const nextEntity = {
            ...this.state.entity,
            [key]: value
        }

        this.setState({
            isDraft: this._enableSave(nextEntity, this.state.entity),
            entity: nextEntity,
            dirty: this._setDirty(key)
        })
    }

    _setProperty = (value, name) => this._updateEntity(name, value)

    _setSelectProperty = (option, options) => this._updateEntity(options.name, option[options.valueKey])

    _setSelectMultiple = (value, options) => this._updateEntity(options.name, value.map(it => it[options.valueKey]).join(','))

    /** Render **/
    _renderLoading = () => {
        return <div className="loading loading--abs loading--60"><Spinner /></div>
    }

    _renderEntity = () => null

    render = () => {
        const { isLoading } = this.state
        return isLoading ? this._renderLoading() : this._renderEntity()
    }
}
