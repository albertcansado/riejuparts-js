import { connect } from 'react-redux'

import { createCard, enableDisableCard, deleteCard } from '../../actions/backend/cards'
import { fetchUpdateEntityCardOrders } from '../../actions/backend/entities'

import { getCards } from '../../reducers/backend/entities'

import CardsList from '../../components/Backend/CardsList'

const mapStateToProps = (state, ownProps) => {
    return {
        cards: getCards(state, ownProps.cards)
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        handleAddCard: name => {
            return dispatch(createCard({
                name,
                model: ownProps.model,
                foreign_key: ownProps.entity.id
            }))
        },
        handleEnableDisableCard: card => dispatch(enableDisableCard(card)),
        handleDeleteCard: card => dispatch(deleteCard(card)),
        handleSaveOrder: (entity, cards) => dispatch(fetchUpdateEntityCardOrders(entity, cards))
    }
}

const CardsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CardsList)

export default CardsContainer
