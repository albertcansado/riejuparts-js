import React, { PureComponent, PropTypes } from 'react'
import { FormattedMessage, intlShape } from 'react-intl'
import { connect } from 'react-redux'
import swal from 'sweetalert2'

import { createVersion, enableDisableVersion, removeVersion } from '../../actions/backend/versions'
import { getVersions } from '../../reducers/backend/entities'
import { getVersionsMessages } from '../../lib/backend_messages'

import Version from '../../components/Backend/Version'
import VersionEdit from '../../components/Backend/VersionEdit'
import Link from '../../components/Link'

class VersionsContainer extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            action: 'list',
            loadingAdd: false,
            index: 0
        }

        // Cache Translate Messages
        this._messages = getVersionsMessages(context.intl.formatMessage)

        this.goToList = this.goToList.bind(this)
        this.handleAddVersion = this.handleAddVersion.bind(this)
        this.handleEdit = this.handleEdit.bind(this)
    }

    goToList = () => {
        this.setState({
            index: 0,
            action: 'list'
        })
    }

    handleAddVersion = () => {
        swal({
            title: this._messages.versionsAddTitle,
            input: 'text',
            showCancelButton: true
        }).then(name => {
            if (!name) {
                return
            }

            this.setState({
                loadingAdd: true
            })

            const nextIndex = this.props.versions.length
            this.props.dispatch(
                createVersion({
                    name,
                    model_id: this.props.model_id
                })
            )
                .then(response => {
                    if (response.hasOwnProperty('error')) {
                        // Show error
                        return
                    }

                    this.setState({
                        loadingAdd: false,
                        action: 'edit',
                        index: nextIndex
                    })
                })
        }, (dismiss) => {})
    }

    handleEdit = (index) => {
        this.setState({
            action: 'edit',
            index
        })
    }

    handleEnable = (version) => {
        const { dispatch } = this.props

        dispatch(
            enableDisableVersion(version)
        )
    }

    handleDelete = (version) => {
        const { dispatch } = this.props

        swal({
            title: this._messages.versionsDeleteTitle,
            text: this._messages.versionsDeleteText,
            type: 'warning',
            'showCancelButton': true
        }).then (() => {
            dispatch(
                removeVersion(version)
            )
        }).catch(swal.noop)
    }

    _renderList = () => {
        const { versions } = this.props
        return (
            <div>
                <header className="mCards-list__header u-mgb--15 u-flex">
                    <h2 className="mCards-list__title">
                        <FormattedMessage id="versions.header.title" defaultMessage="Available Versions" />
                    </h2>
                    <Link btn type="notice" onClick={this.handleAddVersion}>
                        <FormattedMessage id="versions.header.add" defaultMessage="Add New Version" />
                    </Link>
                </header>
                <div className="u-mgt--25">
                    <div className="row">
                        {versions.map((version, key) => {
                            return (
                                <div className="col-xs-12 col-md-3" key={version.id}>
                                    <Version
                                        version={version}
                                        onSelect={() => this.handleEdit(key)}
                                        handleEnable={this.handleEnable}
                                        handleDelete={this.handleDelete}
                                    />
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }

    _renderEdit = () => {
        const { index } = this.state
        const { versions, dispatch } = this.props

        if (!versions[index]) {
            throw Error('versions not has index ' + index)
        }

        return <VersionEdit version={versions[index]} goBack={this.goToList} dispatch={dispatch} />
    }

    render = () => {
        const { action } = this.state

        return action === 'list' ? this._renderList() : this._renderEdit()
    }
}

VersionsContainer.contextTypes = {
    intl: intlShape.isRequired
}

VersionsContainer.propTypes = {
    ids: PropTypes.array.isRequired,
    versions: PropTypes.array.isRequired,
    model_id: PropTypes.string.isRequired
}

const mapStateToProps = (state, ownProps) => {
    return {
        versions: getVersions(state, ownProps.ids)
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(VersionsContainer)
