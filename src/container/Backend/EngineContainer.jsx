import React, { PureComponent, PropTypes } from 'react'
import { connect } from 'react-redux'
import { FormattedMessage } from 'react-intl'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import validate from 'uuid-validate'

import { fetchEngineIfNeeded, updateEngine } from '../../actions/backend/entities'
import { getLoading } from '../../reducers/helpers'
import { getEntity } from '../../reducers/backend/entities'

import CardsContainer from './CardsContainer'
import Input from '../../components/Form/Input'
import Spinner from '../../components/Spinner'

class EngineContainer extends PureComponent {
    constructor(props, context) {
        super(props, context)

        if (!validate(props.id)) {
            throw Error('Engine id must be a valid uuid ' + props.id + ' given')
        }
    }

    componentWillMount = () => {
        const { dispatch, id } = this.props
        dispatch(fetchEngineIfNeeded(id))
    }

    handleUpdateModel = (value, field) => {
        const { dispatch, engine } = this.props

        if (engine.hasOwnProperty(field) && engine[field] !== value) {
            dispatch(
                updateEngine(engine, {
                    [field]: value
                })
            )
        }
    }

    _renderLoading = () => {
        return <div className="loading loading--abs loading--60"><Spinner /></div>
    }

    render = () => {
        const { isLoading, engine } = this.props

        if (isLoading) {
            return this._renderLoading()
        }

        return (
            <Tabs
                className="tabs u-h100"
                selectedTabPanelClassName="tabs__panel--is-open"
                selectedTabClassName="tabs__tab--is-active"
            >
                <TabList className="tabs__list tabs__list--shadow box">
                    <Tab className="tabs__tab">
                        <FormattedMessage id="engines.engine" defaultMessage="Engine" />
                    </Tab>
                    <Tab className="tabs__tab">
                        <FormattedMessage id="engines.cards" defaultMessage="Cards" />
                        <span className="tabs__num">({engine.cards.length})</span>
                    </Tab>
                </TabList>
                    <TabPanel className="tabs__panel box">
                        <div className="row">
                            <div className="col-xs-12 col-md-6">
                                <Input
                                    label='Name'
                                    value={engine.name}
                                    onBlur={val => this.handleUpdateModel(val, 'name')}
                                    />
                                <Input
                                    label='Description'
                                    value={engine.description}
                                    onBlur={val => this.handleUpdateModel(val, 'description')}
                                    />
                            </div>
                        </div>
                    </TabPanel>

                    <TabPanel className="tabs__panel box">
                        <CardsContainer
                            cards={engine.cards}
                            model="Engines"
                            entity={engine}
                        />
                    </TabPanel>
            </Tabs>
        )
    }
}

EngineContainer.propTypes = {
    id: PropTypes.string.isRequired,
    isLoading: PropTypes.bool.isRequired,
    engine: PropTypes.object.isRequired
}

const mapStateToProps = (state, ownProps) => {
    return {
        isLoading: getLoading(state),
        engine: getEntity(state, ownProps.id)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps

)(EngineContainer)
