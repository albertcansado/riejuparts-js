import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { FormattedMessage, intlShape } from 'react-intl'
import { chain as _chain } from 'lodash'
import swal from 'sweetalert2'

import { fetchVersionPartsIfNeeded, addModification, editModification, deleteModification } from '../../actions/backend/versions'
import { openModal } from '../../actions/modal'
import { isVersionFetching, getModifications } from '../../reducers/backend/entities'
import { getVersionsPartsMessages } from '../../lib/backend_messages'

import Loading from '../../components/Loading'
import AddForm from '../../components/Backend/Versions/AddForm'
import ReplaceForm from '../../components/Backend/Versions/ReplaceForm'
import RemoveForm from '../../components/Backend/Versions/RemoveForm'
import AddDisplay from '../../components/Backend/Versions/AddDisplay'
import RemoveDisplay from '../../components/Backend/Versions/RemoveDisplay'
import ReplaceDisplay from '../../components/Backend/Versions/ReplaceDisplay'
import Link from '../../components/Link'
import Information from '../../components/Information'

const renderWrapper = (modification, handleRemoveLine, handleEdit) => {
    switch (modification.action) {
        case 1:
            return <AddDisplay mod={modification} onDelete={handleRemoveLine} onEdit={handleEdit} key={modification.id} />
        case 2:
            return <RemoveDisplay mod={modification} onDelete={handleRemoveLine} key={modification.id} />
        case 3:
            return <ReplaceDisplay mod={modification} onDelete={handleRemoveLine} onEdit={handleEdit} key={modification.id} />
        default:
            return null
    }
}

class VersionsPartsList extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {}

        this.handleRemoveLine = this.handleRemoveLine.bind(this)

        // Cache Translate Messages
        this._messages = getVersionsPartsMessages(context.intl.formatMessage)
    }

    componentWillMount = () => {
        const { dispatch, version_id } = this.props
        dispatch(
            fetchVersionPartsIfNeeded(version_id)
        )
    }

    handleAdd = () => {
        const { model_id, version_id, dispatch } = this.props
        dispatch(openModal({
            component: AddForm,
            props: {
                handleSubmit: (data) => dispatch(
                    addModification(version_id, data)
                ),
                model_id
            }
        }))
    }

    handleEdit = (modification) => {
        const { model_id, version_id, dispatch } = this.props

        let component = null
        switch (modification.action) {
            case 1:
                component = AddForm
                break
            case 3:
                component = ReplaceForm
                break
            default:
                return null
        }

        dispatch(
            openModal({
                component,
                props: {
                    handleSubmit: (data) => dispatch(
                        editModification(modification, version_id, data)
                    ),
                    modification,
                    model_id
                }
            })
        )
    }

    handleRemove = () => {
        const { model_id, version_id, dispatch } = this.props
        dispatch(
            openModal({
                component: RemoveForm,
                props: {
                    handleSubmit: (data) => dispatch(
                        addModification(version_id, data)
                    ),
                    model_id
                }
            })
        )
    }

    handleReplace = () => {
        const { model_id, version_id, dispatch } = this.props
        dispatch(
            openModal({
                component: ReplaceForm,
                props: {
                    handleSubmit: (data) => dispatch(
                        addModification(version_id, data)
                    ),
                    model_id
                }
            })
        )
    }

    handleRemoveLine = (modification) => {
        const { version_id, dispatch } = this.props

        swal({
            title: this._messages.verPartsDeleteTitle,
            // text: this._messages.verPartsDeleteText,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: this._messages.verPartsDeleteConfirm
        }).then(() => {
            dispatch(
                deleteModification(version_id, modification)
            )
        }).catch(swal.noop)
    }

    _renderEmpty = () => {
        return (
            <div className="row">
                <div className="col-xs-12">
                    <div className="m-vpartlist__empty txt-bold txt--large txt-center">
                        <FormattedMessage id="modifications.noResults" defaultMessage="There are not results" />
                    </div>
                </div>
            </div>
        )
    }

    render = () => {
        const { isFetching, modifications } = this.props

        // Sort By Action
        const mods = _chain(modifications)
            .orderBy([
                item => {
                    if (item.action === 1) {
                        return item.target.name
                    } else {
                        return item.target.card.name
                    }
                },
                item => {
                    if (item.action === 2) {
                        return item.target.number
                    } else {
                        return item.extra.number
                    }
                }
            ])
            .value()

        return (
            <div className="m-vpartlist">
                <header>
                    <Information className="u-mgt--10">
                        <p>
                            Listado con las piezas propias de la versión. Puedes <strong>añadir</strong> nuevas piezas a esta versión.
                             <strong>Substituir</strong> piezas del modelo por nuevas piezas. O <strong>eliminar</strong> piezas del modelo.
                            <br />
                            Cualquier cambio <strong>solo afectara a esta versión</strong>.
                        </p>
                    </Information>
                    <div className="m-vpartlist__actions u-flex row-xs--end row-xs--middle u-mgt--25 u-mgb--15">
                        <div className="btn-group txt-bold">
                            <Link btn onClick={this.handleAdd}>
                                <FormattedMessage id="modifications.header.add" defaultMessage="Add" />
                            </Link>
                            <Link btn onClick={this.handleReplace}>
                                <FormattedMessage id="modifications.header.replace" defaultMessage="Replace" />
                            </Link>
                            <Link btn onClick={this.handleRemove}>
                                <FormattedMessage id="modifications.header.delete" defaultMessage="Delete" />
                            </Link>
                        </div>
                    </div>
                </header>
                <div className="table table--modifications">
                    <header className="table__head">
                        <div className="table__col table__col--1 table__col--20">
                            <FormattedMessage id="modifications.head.card" defaultMessage="Card" />
                        </div>
                        <div className="table__col table__col--1 table__col--10">
                            <FormattedMessage id="modifications.head.action" defaultMessage="Action" />
                        </div>
                        <div className="table__col table__col--1">
                            <FormattedMessage id="modifications.head.info" defaultMessage="Information" />
                        </div>
                        <div className="table__col table__col--options"></div>
                    </header>
                    <Loading isLoading={isFetching}>
                        <section className="table__body">
                            {mods.length
                                ? mods.map(modification => renderWrapper(modification, this.handleRemoveLine, this.handleEdit))
                                : this._renderEmpty()
                            }
                        </section>
                    </Loading>
                </div>
            </div>
        )
    }
}

VersionsPartsList.contextTypes = {
    intl: intlShape.isRequired
}

VersionsPartsList.propTypes = {
    version_id: PropTypes.string.isRequired,
    model_id: PropTypes.string.isRequired
}

VersionsPartsList.defaultProps = {}

const mapStateToProps = (state, ownProps) => {
    return {
        isFetching: isVersionFetching(state, ownProps.version_id),
        modifications: getModifications(state, ownProps.version_id, true)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(VersionsPartsList)
