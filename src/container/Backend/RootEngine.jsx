import React, { PureComponent, PropTypes } from 'react'
import { Provider } from 'react-intl-redux'
import Cookies from 'js-cookie'

import EngineContainer from './EngineContainer'
import Modal from '../ModalReduxContainer'

const getIdFromRequest = () => {
    const paths = window.location.pathname.substring(1).split('/')
    return paths[paths.length - 1]
}

export default class RootEngine extends PureComponent {
    getChildContext = () => {
        return {
            locale: Cookies.get('locale')
        }
    }

    render() {
        const { store } = this.props
        return (
            <Provider store={store}>
                <div className="u-h100">
                    <EngineContainer id={getIdFromRequest()} />
                    <Modal />
                </div>
            </Provider>
        )
    }
}

RootEngine.childContextTypes = {
    locale: PropTypes.string.isRequired
}

RootEngine.propTypes = {
    store: PropTypes.object.isRequired
}
