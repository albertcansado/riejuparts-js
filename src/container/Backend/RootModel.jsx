import React, { PureComponent, PropTypes } from 'react'
import { Provider } from 'react-intl-redux'
import Cookies from 'js-cookie'

import ModelContainer from './ModelContainer'
import Modal from '../ModalReduxContainer'

const getIdFromRequest = () => {
    const paths = window.location.pathname.substring(1).split('/')
    return paths[paths.length - 1]
}

export default class RootModel extends PureComponent {
    getChildContext = () => {
        return {
            locale: Cookies.get('locale')
        }
    }

    render = () => {
        const { store } = this.props
        return (
            <Provider store={store}>
                <div className="u-h100">
                    <ModelContainer id={getIdFromRequest()} />
                    <Modal />
                </div>
            </Provider>
        )
    }
}

RootModel.childContextTypes = {
    locale: PropTypes.string.isRequired
}

RootModel.propTypes = {
    store: PropTypes.object.isRequired
}
