import React, { PropTypes } from 'react'
// import { FormattedMessage } from 'react-intl'
import idx from 'idx'
import Switch from 'react-ios-switch'
import swal from 'sweetalert2'
import cx from 'classnames'

import { fetchRequest } from '../../middleware/api'

import BaseBackendContainer from './BaseBackendContainer'
import Header from 'components/Backend/Notices/Header'
import Input from 'components/Form/Input'
import Label from 'components/Form/Label'
import Select from 'components/Form/Select'
import SelectAsync from 'components/Form/SelectAsync'
import SelectGroups from 'components/Form/SelectGroups'
import Uploader from 'components/Form/Uploader'

const initState = {
    entity: {
        title: '',
        active: false,
        extra: '',
        attachment: {},
        categories: '',
        groups: '',
        type: null
    },
    dirty: ['active'],
    isLoadingExtra: false,
    extraOptions: null
}

class CommercialContainer extends BaseBackendContainer {
    constructor(props, context) {
        super(props, context)

        this.state = Object.assign({}, this.initState, initState)

        this._multipleFields = ['categories', 'groups']
    }

    componentDidMount = () => {
        if (!this.state.isNew) {
            this._fetchEntity()
            this._fetchExtra()
        }
    }

    _fetchExtra = () => {
        this.setState({
            isLoadingExtra: true
        })

        fetchRequest(`/d/commercial/webmodels.json`)
            .then(({ json, response}) => {
                if (!response.ok || !json.success) {
                    return Promise.reject(
                        idx(json, _ => _.error.msg) || response.statusText
                    )
                }

                this.setState({
                    isLoadingExtra: false,
                    extraOptions: json.data
                })

                return Promise.resolve()
            }).catch (e => {
                console.log(e)
                swal('Error', 'Error cargando los datos', 'error')
            })
    }

    _enableSave = nextEntity => !!nextEntity.title

    _setDisplayType = () => {
        if (this.state.extraOptions === null) {
            this._fetchExtra()
        }

        this._updateEntity('type', 0);
    }

    _setActive = value => this._updateEntity('active', value)

    _renderCustomFields = () => {
        const { entity, extraOptions, isLoadingExtra } = this.state

        if (entity.type === 0) {
            return (
                <div className="input input--select">
                    <Label title="Modelo Web" />
                    <Select
                        name="extra"
                        value={entity.extra}
                        isLoading={isLoadingExtra}
                        onChange={this._setSelectProperty}
                        options={extraOptions}
                    />
                </div>
            )
        } else if (entity.type === 1) {
            return <Uploader
                label="Archivo"
                accept="application/pdf"
                onChange={this._setProperty}
                name='attachment'
                attachment={entity.attachment} />
        } else {
            return null
        }
    }

    _renderEntity = () => {
        const { entity, isDraft, isNew, isSaving } = this.state
        return (
            <section className="manual box box--clear">
                <Header
                    title={entity.title}
                    isNew={isNew}
                    isDraft={isDraft}
                    isSaving={isSaving}
                    handleSave={this._handleSave}
                    savingMessages={{
                        saving: 'Guardando',
                        special: 'Borrador',
                        notSaved: 'No guardado',
                        saved: 'Guardado'
                    }}
                />
                <div className="box__content">
                    <div className="row">
                        <div className="col-xs-12 col-md-6">
                            <div className="input input--switch input--horizontal">
                                <Label title="Activo" />
                                <Switch
                                    checked={entity.active}
                                    onChange={this._setActive}
                                />
                            </div>
                            <Input
                                label="Titulo"
                                type="text"
                                name="title"
                                value={entity.title}
                                onChange={this._setProperty}
                            />

                            <div className="input input--select">
                                <Label title="Categorias" />
                                <SelectAsync
                                    multiple
                                    url="/d/commercial/categories.json"
                                    name="categories"
                                    value={entity.categories}
                                    searchProps={{
                                        label: 'name',
                                        value: 'id'
                                    }}
                                    joinValues
                                    onSelect={this._setSelectMultiple}
                                />
                            </div>

                            <SelectGroups
                                label="Grupos"
                                selectAll
                                onSelect={this._setSelectMultiple}
                                value={entity.groups}
                            />
                        </div>
                        <div className="col-xs-12 col-md-6">
                            <div className="input">
                                <Label title="Tipo" />
                                <div className="comm-type__btns btn-group">
                                    <div
                                        className={cx("btn btn--block", {'btn--success': entity.type === 0})}
                                        onClick={entity.type === 'display' ? null : this._setDisplayType}
                                    >
                                        <strong>Display</strong>
                                        <span className="txt--small">Product display</span>
                                    </div>
                                    <div
                                        className={cx("btn btn--block", {'btn--success': entity.type === 1})}
                                        onClick={entity.type === 'file' ? null : () => this._updateEntity('type', 1)}
                                    >
                                        <strong>Archivo</strong>
                                        <span className="txt--small">PDF con información comercial</span>
                                    </div>
                                </div>
                            </div>
                            {this._renderCustomFields()}
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

CommercialContainer.propTypes = {
    id: PropTypes.string,
    fetchUrl: PropTypes.string,
    saveUrl: PropTypes.string.isRequired
}
CommercialContainer.defaultProps = {
    id: null,
    fetchUrl: null
}

export default CommercialContainer
