/* eslint-disable react/style-prop-object */
import React, { Component } from 'react'
import { findDOMNode } from 'react-dom'
import { connect } from 'react-redux'
import cx from 'classnames'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import { FormattedNumber, FormattedMessage, intlShape } from 'react-intl'
import { map as _map, reduce as _reduce } from 'lodash'
import { u } from 'umbrellajs'
import swal from 'sweetalert2'
// import Select from 'react-select'

import { callApi, API_ROOT } from '../middleware/api'
import Schemas from '../constants/Schemas'
import { getCartMessages } from '../lib/messages'

import { fetchCartInit, toggleCart, createCart, updateCart, changeCart, sendCSV } from '../actions/carts'
import { isInitCartFinish } from '../reducers/helpers'
import { getCart, getCartTotals } from '../reducers/entities'
import { getCartCount, isCartOpen } from '../reducers/cart'

import Icon from '../components/Icon'
import Link from '../components/Link'
import CartList from '../components/Cart/CartList'
import CartExtra from '../components/Cart/CartExtra'
import SpinLoading from '../components/SpinLoading'
import InlineInput from '../components/Form/InlineInput'
import UploaderCSV from '../components/Form/UploaderCSV'

const SENDING = {
    NOTSEND: 'NOTSEND',
    SENDING: 'SENDING',
    HASSEND: 'HASSEND'
}

const isSwalElement = (target) => {
    if (!target || !target.hasAttribute('class')) {
        return false
    }

    return target.getAttribute('class').search(/swal/) >= 0
}

class CartContainer extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {
            showCsv: false,
            send: SENDING.NOTSEND,
            spinnerProps: {
                animate: true
            },
            sendMessage: null,
            loadingCreate: false
        }

        this._el = null
        this._body = null

        this._closeCart = this._closeCart.bind(this)
        this._handleNew = this._handleNew.bind(this)
        this._sendCSV = this._sendCSV.bind(this)
        this.handleDocumentClick = this.handleDocumentClick.bind(this)
        this.resetState = this.resetState.bind(this)

        // Cache Translate Messages
        this._messages = getCartMessages(context.intl.formatMessage)
    }

    componentDidMount = () => {
        document.addEventListener('click', this.handleDocumentClick, true)

        const { dispatch } = this.props
        dispatch(fetchCartInit())

        this._body = u('body')
    }

    componentWillReceiveProps = (nextProps) => {
        const { loadingCreate } = this.state
        const { cart } = this.props

        if (loadingCreate && nextProps.cart.id !== cart.id) {
            this.setState({
                loadingCreate: false
            })
        }

        if (!nextProps.isOpen && this.state.showCsv) {
            this._closeCSV()
        }
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        return nextProps.isOpen || (this.props.isLoading !== nextProps.isLoading)
    }

    componentDidUpdate = () => {
        const { cart, dispatch } = this.props
        const { send } = this.state

        if (!this._el) {
            const root = findDOMNode(this)
            this._el = root.querySelector('.cart__modal')
        }

        if (send !== SENDING.SENDING) {
            return
        }

        callApi(`/carts/send/${cart.id}.json`, Schemas.NOPARSE, {
            method: 'POST'
        }).then(json => {
            this.setState({
                send: SENDING.HASSEND,
                spinnerProps: {
                    animate: false,
                    finish: true,
                    type: 'success',
                    showAutoclose: true
                },
                sendMessage: json.message
            })

            setTimeout(() => {
                this.resetState()

                dispatch(
                    changeCart(json.next_order)
                )
            }, 3000)
        }).catch(json => {
            this.setState({
                send: SENDING.HASSEND,
                spinnerProps: {
                    animate: false,
                    finish: true,
                    type: 'error',
                    showAutoclose: true
                },
                sendMessage: json.hasOwnProperty('error') ? json.error.message : this._messages.defaultErrorSend
            })

            setTimeout(this.resetState.bind(this), 3000)
        })
    }

    componentWillUnmount = () => {
        document.removeEventListener('click', this.handleDocumentClick, true)
    }

    resetState = () => {
        this.setState({
            send: SENDING.NOTSEND,
            spinnerProps: {
                animate: true
            },
            sendMessage: null,
        })
    }

    handleDocumentClick = (ev) => {
        if( !this._body.hasClass('stop-scrolling') &&
            this.props.isOpen &&
            !isSwalElement(ev.target) && // Not click inside swal modal
            ev.target !== this._el &&
            !this._el.contains(ev.target)
        ) {
            this._closeCart()
        }
    }

    _handleNew = () => {
        const { dispatch } = this.props

        dispatch(createCart())
        this.setState({
            loadingCreate: true
        })
    }

    _closeCart = () => {
        const { dispatch } = this.props

        this._body.toggleClass('is-cart-open')

        dispatch(toggleCart())
    }

    handleChangeName = (name) => {
        const { cart, dispatch } = this.props

        dispatch(updateCart(cart.id, {
            title: name
        }))
    }

    _sendCart = () => {
        swal({
            title: this._messages.cartSendTitle,
            text: this._messages.cartSendText,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: this._messages.cartSendConfirm,
            cancelButtonText: this._messages.cartSendCancel
        }).then(() => {
            this.setState({
                send: SENDING.SENDING
            })
        }).catch(swal.noop)
    }

    _sendCSV = results => {
        const { cart, dispatch } = this.props

        return dispatch(
            sendCSV(cart, results)
        )
    }

    _onShowCSV = () => {
        this.setState({
            showCsv: true
        })
    }

    _closeCSV = () => {
        this.setState({
            showCsv: false
        })
    }

    _renderHeader = () => {
        const { cart } = this.props
        const { send, loadingCreate, showCsv } = this.state

        const isSending = send !== SENDING.NOTSEND

        return (
            <header className="cart__header row">
                <div className="col-xs-12 col-md-8 col-xl-9">
                    <div className="u-flex flex--middle row-xs-column">
                        <h2 className="cart__number txt-black txt-upper">#{cart.number}</h2>
                        <div className="cart-name txt--medium">
                            <InlineInput value={cart.title} placeholder={this._messages.cartAliasPlaceholder} onClose={this.handleChangeName} />
                        </div>
                    </div>
                </div>
                <div className="col-xs-12 col-md-4 col-xl-3">
                    <nav className="cart__actions btn-group">
                        <Link
                            btn
                            type="success"
                            className={cx("btn--block", {
                                'is-disabled': isSending || showCsv,
                                'is-loading': loadingCreate
                            })}
                            onClick={!isSending && !showCsv ? this._handleNew : null}
                        >
                            <Icon icon="file-add" />
                            <span className="btn__txt">
                                <FormattedMessage id="cart.actions.new_quotation" defaultMessage="New Quotation" />
                            </span>
                        </Link>
                    </nav>
                </div>
            </header>
        )
    }

    _renderFooter = () => {
        const { cart, totals } = this.props
        const { send, loadingCreate, showCsv } = this.state

        if (!cart.hasOwnProperty('orders_lines')) {
            return null
        }

        const isSending = send !== SENDING.NOTSEND

        return (
            <footer className="cart__footer">
                <nav className="cart__actions cart__actions--footer row">
                    <div className="col-xs-12 col-md-4 col-lg-6">
                        <div className="cart__total">
                            <span className="cart__total-label txt-black">
                                <FormattedMessage id="cart.actions.total" defaultMessage="TOTAL" />
                            </span>
                            <span className="cart__total-value">
                                <FormattedNumber
                                    value={totals.lines + totals.extras}
                                    minimumFractionDigits={2}
                                    maximumFractionDigits={2}
                                    currency="EUR"
                                    currencyDisplay="symbol"
                                    style="currency" />
                            </span>
                        </div>
                    </div>
                    <div className="col-xs-6 col-md-4 col-lg-3">
                        <Link
                            btn
                            type="notice"
                            href={`${API_ROOT}/orders/view/${cart.id}.pdf`}
                            target='_blank'
                            className={cx("btn--block", {
                                'is-disabled': !cart.orders_lines.length || isSending || loadingCreate || showCsv
                            })}
                        >
                            <Icon icon="file" />
                            <span className="btn__txt">
                                <FormattedMessage id="cart.actions.pdf" defaultMessage="PDF / Print" />
                            </span>
                        </Link>
                    </div>
                    <div className="col-xs-6 col-md-4 col-lg-3">
                        <Link
                            btn
                            type="success"
                            className={cx("btn--block", {
                                'is-disabled': !cart.orders_lines.length || loadingCreate || showCsv,
                                'btn--loading': isSending
                            })}
                            onClick={cart.orders_lines.length && !isSending && !showCsv ? this._sendCart : null}
                        >
                            <Icon icon="plane" />
                            <span className="btn__txt">
                                <FormattedMessage id="cart.actions.send" defaultMessage="Send Quotation" />
                            </span>
                        </Link>
                    </div>
                </nav>
            </footer>
        )
    }

    _renderContent = () => {
        const { cart, totals, dispatch } = this.props
        const { showCsv } = this.state
        if (!cart.hasOwnProperty('orders_lines')) {
            return null
        }

        return (
            <div className="cart__main" key="CART_MAIN">
                <section className="cart__container">
                    <header className="u-flex flex--middle flex--between">
                        <h2 className="cart__title">
                            <FormattedMessage id="cart.title.parts" defaultMessage="Parts" /> <span className="txt-normal" style={{fontSize: '80%'}}>({cart.orders_lines.length})</span>
                        </h2>
                        <Link btn type="reset" className="btn--small is-error" onClick={this._onShowCSV}>
                            <Icon icon="file" className="icon--14" />
                            <span className="btn__text">
                                <FormattedMessage id="warranty.addpart.csv" defaultMessage="Upload a CSV" />
                            </span>
                        </Link>
                    </header>
                    {showCsv
                        ? <UploaderCSV onCancel={this._closeCSV} onSubmit={this._sendCSV} className="u-mgt--10" exampleUrl={`${API_ROOT}/files/example_csv.csv`} />
                        : <CartList items={cart.orders_lines} total={totals.lines} dispatch={dispatch} />
                    }
                </section>
                <section className="cart__container">
                    <h2 className="cart__title">
                        <FormattedMessage id="cart.title.extra" defaultMessage="Extra" /> <span className="txt-normal" style={{fontSize: '80%'}}>({cart.orders_extras.length})</span>
                    </h2>
                    <CartExtra ids={cart.orders_extras} total={totals.extras} dispatch={dispatch} />
                </section>
            </div>
        )
    }

    _renderCart = () => {
        const { loadingCreate } = this.state
        return (
            <div className="cart__wrapper">
                <Link btn type="reset" onClick={this._closeCart} className="cart__close">
                    <Icon icon="close" className="icon--22 icon--dark" />
                </Link>
                {this._renderHeader()}
                <ReactCSSTransitionGroup
                    transitionName="fade"
                    transitionEnterTimeout={400}
                    transitionLeaveTimeout={400}
                >
                    {!loadingCreate ? this._renderContent() : null}
                </ReactCSSTransitionGroup>
            </div>
        )
    }

    _renderSending = () => {
        const { spinnerProps, sendMessage } = this.state
        return (
            <div className="cart__wrapper u-flex flex--centerfull" style={{height: '100%'}}>
                <div className="cart__send">
                    <SpinLoading className="cart__send-loading u-centered" {...spinnerProps} />
                    <div className="cart__send-text u-mgt--15 txt-center">
                        <h2>{spinnerProps.hasOwnProperty('finish') ? sendMessage : this._messages.cartSendSuccess}</h2>
                        <p>
                            {spinnerProps.showAutoclose
                                ? this._messages.cartAutoclose.replace('[seconds]', '3')
                                : null
                            }
                        </p>
                    </div>
                </div>
            </div>
        )
    }

    render = () => {
        const { isLoading } = this.props
        const { send } = this.state

        const isSending = send !== SENDING.NOTSEND

        if (isLoading) {
            return null
        }

        return (
            <div className="cart">
                <div className="cart__modal">
                    <div className="cart__inner">
                        {!isSending ? this._renderCart() : this._renderSending()}
                        {this._renderFooter()}
                    </div>
                </div>
            </div>
        )
    }
}

CartContainer.contextTypes = {
    intl: intlShape.isRequired
}

const mapStateToProps = (state, ownProps) => {
    const isLoading = !isInitCartFinish(state)
    return {
        isOpen: isCartOpen(state),
        total: getCartCount(state),
        cart: getCart(state),
        totals: getCartTotals(state),
        isLoading
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps

)(CartContainer)
