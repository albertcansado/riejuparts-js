import React, { Component, PropTypes } from 'react'
import { findDOMNode } from 'react-dom'

/* eslint-disable jsx-a11y/img-has-alt */
class Image extends Component {
    constructor(props, context) {
        super(props, context)

        this._el = null
    }

    componentDidMount = () => {
        this._el = findDOMNode(this)
    }

    handleError = () => {
        const { handleError } = this.props

        this._el.style.display = 'none'

        if (handleError) {
            handleError()
        }
    }

    render = () => {
        const { handleError, ...newProps } = this.props // eslint-disable-line no-unused-vars

        return <img {...newProps} onError={this.handleError} />
    }
}

Image.propTypes = {
    handleError: PropTypes.func
}

export default Image
