import React, { PropTypes } from 'react'
import cx from 'classnames'

const Availability = ({ availability, disable, tooltip, showLabel, messages, theme }) => {
    if (disable) {
        return null
    }

    let bubbleType = 'success'
    let text = messages.partAvaiIm
    if (availability === 1) {
        bubbleType = 'error';
        text = messages.partAvaiNotGuar
    } else if (availability === 2) {
        bubbleType = 'warning'
        text = messages.partAvaiRest
    }

    const propsTooltip = tooltip
        ? {
            'data-tip': text,
            'data-place': "top",
            'data-effect': "solid"
        }
        : {}

    return (
        <div
            className={cx("availability", theme.container)}
            {...propsTooltip}
        >
            {showLabel
                ? <span className={cx("availability__text", theme.text)} children={messages.availability} />
                : null
            }
            <span
                className={cx('availability__bubble bubble', {
                    [`bubble--${bubbleType}`]: true
                }, theme.bubble)}
            ></span>
        </div>
    )
}

Availability.propTypes = {
    availability: PropTypes.number.isRequired,
    messages: PropTypes.object,
    disable: PropTypes.bool,
    tooltip: PropTypes.bool,
    showLabel: PropTypes.bool,
    theme: PropTypes.object
}

Availability.defaultProps = {
    messages: {},
    disable: false,
    tooltip: false,
    showLabel: false,
    theme: {}
}

export default Availability
