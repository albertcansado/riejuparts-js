import React, { Component, PropTypes } from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import { findDOMNode } from 'react-dom'
import classnames from 'classnames'


// DropdownContent
const DropdownContent = ({ className, children }, context) => {
    const classNames = classnames("dropdown__content", className)
    return (
        <div className={classNames} aria-hidden={!context.isActive ? "true" : "false"}>
            <ReactCSSTransitionGroup
                transitionName="dropanim"
                transitionEnterTimeout={300}
                transitionLeaveTimeout={300}
            >
                {context.isActive ? children : null}
            </ReactCSSTransitionGroup>
        </div>
    )
}

DropdownContent.contextTypes = {
    isActive: PropTypes.bool.isRequired
}

DropdownContent.propTypes = {
    className: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ])
}

DropdownContent.defaultProps = {
    className: ''
}


// DropdownTrigger
const DropdownTrigger = ({ className, children }, context) => {
    const classNames = classnames("dropdown__trigger", className)
    return (
        <span className={classNames} onClick={context.handleOpen}>
            {children}
        </span>
    )
}

DropdownTrigger.contextTypes = {
    handleOpen: PropTypes.func.isRequired
}

DropdownTrigger.propTypes = {
    className: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ])
}

DropdownTrigger.defaultProps = {
    className: ''
}


// Dropdown
class Dropdown extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {
            active: props.active
        }

        this._el = null

        this.handleDocumentClick = this.handleDocumentClick.bind(this)

        // Public methods
        this.show = () => this.setState({ active: true })
        this.hide = this._handleHide.bind(this)
        this.toggle = () => this.setState({ active: !this.state.active })
    }

    componentDidMount() {
        this._el = findDOMNode(this)
        document.addEventListener('click', this.handleDocumentClick, true)
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleDocumentClick, true)
    }

    handleDocumentClick(ev) {
        if( this.state.active && ev.target !== this._el && !this._el.contains(ev.target) ) {
            this.hide()
        }
    }

    _handleHide = () => {
        const { onHide } = this.props

        if (onHide !== null) {
            onHide()
        }

        this.setState({ active: false })
    }

    getChildContext() {
        return {
            isActive: this.state.active,
            handleOpen: (ev) => {
                ev.preventDefault()
                ev.stopPropagation()
                this.toggle()
            }
        }
    }

    render() {
        const type = this.props.type || "centered"
        const classNames = classnames("dropdown", {
            "dropdown--is-active": this.state.active,
            [`dropdown--${type}`]: true
        }, this.props.className)
        return (
            <div className={classNames} aria-haspopup="true">
                {this.props.children}
            </div>
        )
    }
}

Dropdown.childContextTypes = {
    isActive: PropTypes.bool.isRequired,
    handleOpen: PropTypes.func.isRequired
}

Dropdown.propTypes = {
    active: PropTypes.bool,
    className: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ]),
    type: PropTypes.string,
    onHide: PropTypes.func
}

Dropdown.defaultProps = {
    active: false,
    className: '',
    type: '',
    onHide: null
}


// Export
export { DropdownContent, DropdownTrigger }
export default Dropdown
