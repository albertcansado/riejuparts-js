import React, { PropTypes } from 'react'
import cx from 'classnames'

const Bubble = ({ type, className }) => {
    return (
        <span
            className={cx('bubble', {
                [`bubble--${type}`]: true
            }, className)}
        ></span>
    )
}

Bubble.propTypes = {
    type: PropTypes.string.isRequired,
    className: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ])
}

Bubble.defaultProps = {
    className: ''
}

export default Bubble
