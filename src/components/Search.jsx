import React, { PureComponent, PropTypes } from 'react'
import { intlShape } from 'react-intl'

import { getSearchMessages } from '../lib/messages'

import Input from './Form/Input'

class Search extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            value: props.defaultValue
        }

        // Cache Translate Messages
        this._messages = getSearchMessages(context.intl.formatMessage)
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.reset && !this.props.reset) {
            this.setState({
                value: ''
            })
        }
    }

    handleChange = (value) => {
        if (this.props.disabled) {
            return
        }

        this.props.onChange(value)

        this.setState({
            value
        })
    }

    render = () => {
        const { value } = this.state
        const { disabled } = this.props
        const clearClass = ["search__clear"]
        if (value) {
            clearClass.push('is-show')
        }

        return (
            <div className="search">
                <Input
                    onChange={this.handleChange}
                    value={value}
                    before="search"
                    inputProps={{
                        placeholder: this._messages.searchPlaceholder
                    }}
                    theme={{
                        input: disabled ? 'is-disabled' : null,
                        beforeIcon: 'icon--20'
                    }}
                />
                <span className={clearClass.join(' ')} onClick={() => this.handleChange('')}>x</span>
            </div>
        )
    }
}

Search.contextTypes = {
    intl: intlShape.isRequired
}

Search.propTypes = {
    onChange: PropTypes.func.isRequired,
    defaultValue: PropTypes.string,
    reset: PropTypes.bool,
    disabled: PropTypes.bool
}

Search.defaultProps = {
    defaultValue: '',
    disabled: false,
    reset: false
}

export default Search
