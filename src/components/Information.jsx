import React, { PropTypes } from 'react'
import cx from 'classnames'

import Icon from './Icon'

const Information = ({ className, children }) => {
    return (
        <div className={cx("information", className)}>
            <Icon icon="info" className="information__icon icon--20" />
            <div className="information__content">
                {children}
            </div>
        </div>
    )
}

Information.propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ])
}

Information.defaultProps = {
    className: ''
}

export default Information
