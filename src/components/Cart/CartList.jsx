/* eslint-disable react/style-prop-object */
import React, { Component, PropTypes } from 'react'
import { FormattedNumber, FormattedMessage, intlShape } from 'react-intl'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import { updateCartLine, deleteCartLine, addToCart } from '../../actions/carts'
import swal from 'sweetalert2'

import { getCartListMessages } from '../../lib/messages'

import CartItem from './CartItem'
import CartItemAdd from './CartItemAdd'

const renderEmpty = () => {
    return (
        <div className="table__row cart-list-empty" key="ROW_EMPTY">
            <span className="table__col table__col--full txt-center txt--medium">
                <FormattedMessage
                    id="cartlist.empty"
                    defaultMessage="Your quotation is empty. Add products from {link}"
                    values={{
                        link: <a href="/catalog" className="is-error txt-italic"><FormattedMessage id="cart.list.emptyLink" defaultMessage="Catalog" /></a>
                    }}
                />
            </span>
        </div>
    )
}

class CartList extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {}

        // Cache Translate Messages
        this._messages = getCartListMessages(context.intl.formatMessage)
    }

    _update = (line, nextQty) => {
        const { dispatch } = this.props
        dispatch(
            updateCartLine(line, nextQty)
        )
    }

    _delete = (item) => {
        const { dispatch } = this.props
        swal({
            title: this._messages.cartlistDeleteTitle,
            // text: this._messages.cartListDeleteText,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: this._messages.cartListDeleteConfirm,
        }).then (() => {
            dispatch(
                deleteCartLine(item)
            )
        }).catch(swal.noop)
    }

    _add = (item, qty) => {
        const { dispatch } = this.props
        dispatch(
            addToCart(item, qty, true)
        )
    }

    render = () => {
        const { items, total } = this.props
        return (
            <div>
                <section className="table cart-list cart-list--add u-mgt--10 u-mgb--10">
                    <div className="table__body">
                        <CartItemAdd onSubmit={this._add} />
                    </div>
                </section>
                <article className="table cart-list">
                    <header className="table__head">
                        <span className="table__col table__col--1">
                            <FormattedMessage id="cart.list.name" defaultMessage="Name" />
                        </span>
                        <span className="table__col table__col--2">
                            <FormattedMessage id="cart.list.qty" defaultMessage="Qty" />
                        </span>
                        <span className="table__col table__col--3">
                            <FormattedMessage id="cart.list.price" defaultMessage="Price" />
                        </span>
                        <span className="table__col table__col--options"></span>
                    </header>
                    <section className="table__body">
                        <ReactCSSTransitionGroup
                            transitionName="slide-fade"
                            transitionEnterTimeout={400}
                            transitionLeaveTimeout={400}
                        >
                            {items.length ? items.map(item => <CartItem id={item} handleUpdate={this._update} handleDelete={this._delete} key={item} />) : renderEmpty()}
                        </ReactCSSTransitionGroup>
                    </section>
                    <footer className="cart-list__total">
                        <span className="cart-list__total-name txt-bold">
                            <FormattedMessage id="cart.list.total" defaultMessage="TOTAL" />
                        </span>
                        <span className="cart-list__total-value">
                            <FormattedNumber
                                value={total}
                                minimumFractionDigits={2}
                                maximumFractionDigits={2}
                                currency="EUR"
                                currencyDisplay="symbol"
                                style="currency" />
                        </span>
                    </footer>
                </article>
            </div>
        )
    }
}

CartList.contextTypes = {
    intl: intlShape.isRequired
}

CartList.propTypes = {
    items: PropTypes.array.isRequired,
    total: PropTypes.number.isRequired
}

export default CartList
