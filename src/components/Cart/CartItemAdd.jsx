/* eslint-disable react/style-prop-object */
import React, { PureComponent, PropTypes } from 'react'
import { connect } from 'react-redux'
import { FormattedNumber, intlShape } from 'react-intl'
import NumberInput from '../Form/NumberInput'
import { isObject as _isObject } from 'lodash'

import { getCartItemAddMessages } from '../../lib/messages'
import { getCartParts } from '../../reducers/entities'

import Icon from '../Icon'
import Link from '../Link'
import SearchPart from '../Form/SearchPart'

const calcPrice = (item, qty = 0) => {
    if (!_isObject(item) || !item.hasOwnProperty('price')) {
        return 0
    }

    return qty * item.price
}

/*const getMax = (item) => {
    if (!_isObject(item) || !item.hasOwnProperty('stock')) {
        return 0
    }

    return item.stock
}*/

class CartItemAdd extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            item: null,
            qty: 1
        }

        this._handleClear = this._handleClear.bind(this)
        this._handleSubmit = this._handleSubmit.bind(this)
        this._handleChange = this._handleChange.bind(this)

        // Cache Translate Messages
        this._messages = getCartItemAddMessages(context.intl.formatMessage)
    }

    _handleChange = (value) => {
        this.setState({
            item: value
        })
    }

    _handleClear = () => {
        this.setState({
            item: null,
            qty: 1
        })
    }

    _handleSubmit = () => {
        const { item, qty } = this.state
        this.props.onSubmit(
            item,
            qty
        )

        this._handleClear()
    }

    _filterParts = data => {
        return data.filter(it => !this.props.partsOnCart.hasOwnProperty(it.sku))
    }

    render = () => {
        const { item, qty } = this.state
        const couldSave = _isObject(item) && item.hasOwnProperty('sku')

        return (
            <div className="table__row not-hover">
                <div className="table__col table__col--1 txt--small">
                    <SearchPart
                        fetchUrl="/parts/lookup.json?s={input}"
                        filterFn={this._filterParts}
                        showPrice
                        value={item}
                        onChange={this._handleChange}
                        messages={{
                            noResultsText: this._messages.cartIASyncNoResults,
                            placeholder: this._messages.cartIASyncPlaceholder,
                            loadingPlaceholder: this._messages.cartIASyncLoading,
                            searchPromptText: this._messages.cartIASyncSearchText
                        }}
                    />
                </div>
                <div className="table__col table__col--2">
                    <NumberInput value={qty} onChange={(qty) => this.setState({ qty })} min={1} />
                </div>
                <div className="table__col table__col--3">
                    <FormattedNumber
                        value={calcPrice(item, qty)}
                        minimumFractionDigits={2}
                        maximumFractionDigits={2}
                        currency="EUR"
                        currencyDisplay="symbol"
                        style="currency" />
                </div>
                <div className="table__col table__col--options">
                    <div className="btn-group txt-center">
                        <Link btn type="reset" onClick={couldSave ? this._handleSubmit : null} className={!couldSave ? 'is-disabled' : null}>
                            <Icon icon="checked" className={couldSave ? "icon--24 icon--success" : "icon--24"} />
                        </Link>
                        <Link btn type="reset" onClick={this._handleClear}>
                            <Icon icon="close" className="icon--18 icon--error" />
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

CartItemAdd.contextTypes = {
    intl: intlShape.isRequired
}

CartItemAdd.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    partsOnCart: PropTypes.object.isRequired
}

const mapStateToProps = (state, ownProps) => {
    return {
        partsOnCart: getCartParts(state)
    }
}

export default connect(
    mapStateToProps,
    {}

)(CartItemAdd)
