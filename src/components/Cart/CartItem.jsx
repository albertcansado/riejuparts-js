/* eslint-disable react/style-prop-object */
import React, { PureComponent, PropTypes } from 'react'
import { connect } from 'react-redux'
import { shallowCompareWithoutFunctions } from 'shallow-compare-without-functions'
import { FormattedNumber, intlShape } from 'react-intl'

import { getCartLine } from '../../reducers/entities'
import { getAvailabilityMessages } from '../../lib/messages'

import Icon from '../Icon'
import Availability from '../Availability'
import NumberInput from '../Form/NumberInput'

class CartItem extends PureComponent {
    constructor(props, context) {
        super(props, context)

        // Cache Translate Messages
        this._messages = getAvailabilityMessages(context.intl.formatMessage)
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        return shallowCompareWithoutFunctions(this, nextProps, nextState)
    }

    _update = (nextQty) => {
        const { handleUpdate, item } = this.props

        handleUpdate(item, nextQty)
    }

    render = () => {
        const { item, handleDelete } = this.props

        if (!item.hasOwnProperty('name')) {
            return null
        }

        return (
            <div className="table__row">
                <div className="table__col table__col--1">
                    <div className="u-flex flex--middle">
                        <Availability availability={item.part.availability} messages={this._messages} theme={{ container: 'u-mgr--5', text: 'hidden-xs-up' }} />
                        <div>
                            <strong className="cart-item__sku u-block">
                                {item.part.sku}
                            </strong>
                            <span className="cart-item__name u-truncate txt--small">
                                {item.name}
                            </span>
                        </div>
                    </div>
                </div>
                <div className="table__col table__col--2">
                    <NumberInput value={item.qty} onChange={this._update} />
                </div>
                <div className="table__col table__col--3">
                    <FormattedNumber
                        value={item.part.price * item.qty}
                        minimumFractionDigits={2}
                        maximumFractionDigits={2}
                        currency="EUR"
                        currencyDisplay="symbol"
                        style="currency" />
                </div>
                <div className="table__col table__col--options">
                    <span className="cart-list__btn" onClick={() => handleDelete(item)}>
                        <Icon icon="trash" className="icon--error icon--18" />
                    </span>
                </div>
            </div>
        )
    }
}

CartItem.contextTypes = {
    intl: intlShape.isRequired
}

CartItem.propTypes = {
    id: PropTypes.string.isRequired,
    handleDelete: PropTypes.func.isRequired,
    handleUpdate: PropTypes.func.isRequired
}

const mapStateToProps = (state, ownProps) => {
    return {
        item: getCartLine(state, ownProps.id)
    }
}

export default connect(
    mapStateToProps,
    {}

)(CartItem)
