import React, { PureComponent, PropTypes } from 'react'
import { intlShape } from 'react-intl'
import { Creatable } from 'react-select'

// import { fetchRequest } from '../../middleware/api'
import { getCartExtraMessages } from '../../lib/messages'

import Input from '../Form/Input'
import Icon from '../Icon'
import Link from '../Link'

class CartExtraAdd extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            name: '',
            price: 0,
            optionsItems: [],
            isLoading: true
        }

        this._handleClear = this._handleClear.bind(this)
        this._handleSubmit = this._handleSubmit.bind(this)
        this._handleChange = this._handleChange.bind(this)

        // Cache Translate Messages
        this._messages = getCartExtraMessages(context.intl.formatMessage)
    }

    componentDidMount = () => {
        /*fetchRequest('/carts/extra_options.json', { method: 'GET'})
            .then(({ json, response }) => {
                let items = [];
                if (!response.ok || !json.success) {
                    // Error
                    this.setState({
                        optionsItems: [],
                        isLoading: false
                    })

                    return Promise.resolve();
                }

                items = json.data

                this.setState({
                    optionsItems: items,
                    isLoading: false
                })
            })*/
        this.setState({
            optionsItems: [],
            isLoading: false
        })
    }

    _handleSubmit = () => {
        const { name, price } = this.state
        this.props.onSubmit(name, price)

        this._handleClear()
    }

    _handleClear = () => {
        this.setState({
            name: '',
            price: 0
        })
    }

    _handleChange = (result) => {
        const { price } = this.state

        if (!result) {
            this.setState({
                name: '',
                price: 0
            })
        } else {
            this.setState({
                name: result.label,
                price: (result.label !== result.value) ? result.value : price
            })
        }
    }

    _setPrice = (val) => {
        this.setState({
            price: (val && val !== '-') ? Number.parseFloat(val) : val
        })
    }

    render = () => {
        const { name, price, isLoading, optionsItems } = this.state
        const hasName = name !== ''

        return (
            <div className="table__row not-hover">
                <div className="table__col table__col--1">
                    <Creatable
                        value={name}
                        valueKey="label"
                        multi={false}
                        onChange={this._handleChange}
                        isLoading={isLoading}
                        options={optionsItems}
                        noResultsText={this._messages.cartextraCreateNoresults}
                        promptTextCreator={label => `${this._messages.cartextraCreateAdd} ${label}`}
                        placeholder={this._messages.cartextraCreatePlaceholder}
                        searchPromptText={this._messages.cartextraCreateSearchText}
                        loadingPlaceholder={this._messages.cartextraCreateLoading}
                    />
                    {/*<Input value={name} onChange={(val) => this.setState({ name: val })} />*/}
                </div>
                <div className="table__col table__col--2">
                    <Input
                        after="euro"
                        value={price}
                        theme={{
                            afterIcon: 'icon--18'
                        }}
                        onChange={this._setPrice}
                    />
                </div>
                <div className="table__col table__col--options">
                    <div className="btn-group txt-center">
                        <Link btn type="reset" onClick={hasName ? this._handleSubmit : null} className={!hasName ? 'is-disabled' : null}>
                            <Icon icon="checked" className="icon--24 icon--success" />
                        </Link>
                        <Link btn type="reset" onClick={this._handleClear}>
                            <Icon icon="close" className="icon--18 icon--error" />
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

CartExtraAdd.contextTypes = {
    intl: intlShape.isRequired
}

CartExtraAdd.propTypes = {
    onSubmit: PropTypes.func.isRequired
}

export default CartExtraAdd
