/* eslint-disable react/style-prop-object */
import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import { FormattedMessage, FormattedNumber, intlShape } from 'react-intl'
import swal from 'sweetalert2'

import { addExtraToCart, deleteExtra } from '../../actions/carts'
import { getCartExtraLines } from '../../reducers/entities'
import { getCartExtraMessages } from '../../lib/messages'

import Icon from '../Icon'
import CartExtraAdd from './CartExtraAdd'

const chooseId = (item) => {
    return (item.hasOwnProperty('draftId')) ? item.draftId : item.id
}

const renderExtraLine = (item, handleDelete) => {
    return (
        <div className="table__row" key={chooseId(item)}>
            <div className="table__col table__col--1">
                {item.title}
            </div>
            <div className="table__col table__col--2 txt-right">
                {item.pvp}
            </div>
            <div className="table__col table__col--options txt-center">
                <span className="is-selectable" onClick={() => handleDelete(item)}>
                    <Icon icon="trash" className="icon--error icon--18" alt="Delete" />
                </span>
            </div>
        </div>
    )
}

const renderEmpty = () => {
    return (
        <div className="table__row not-hover cart-list-empty" key="ROW_EMPTY">
            <span className="table__col table__col--full txt-center txt--medium">
                <FormattedMessage id="cartextra.empty" defaultMessage="Your quotation not has works" />
            </span>
        </div>
    )
}

class CartExtra extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {}

        this._addItem = this._addItem.bind(this)
        this._deleteItem = this._deleteItem.bind(this)

        // Cache Translate Messages
        this._messages = getCartExtraMessages(context.intl.formatMessage)
    }

    _addItem = (name, price) => {
        const { dispatch } = this.props
        dispatch(
            addExtraToCart(name, price)
        )
    }

    _deleteItem = (item) => {
        const { dispatch } = this.props
        swal({
            title: this._messages.cartExtraDeleteTitle,
            // text: this._messages.cartExtraDeleteText,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: this._messages.cartExtraDeleteConfirm,
        }).then(() => {
            dispatch(
                deleteExtra(item)
            )
        }).catch(swal.noop)
    }

    render = () => {
        const { items, total } = this.props
        return (
            <div>
                <article className="table cart-extra cart-extra--add u-mgt--10 u-mgb--10">
                    <section className="table__body">
                        <CartExtraAdd onSubmit={this._addItem} />
                    </section>
                </article>
                <article className="cart-extra table">
                    <header className="table__head">
                        <span className="table__col table__col--1">
                            <FormattedMessage id="cart.extra.concept" defaultMessage="Concept" />
                        </span>
                        <span className="table__col table__col--2">
                            <FormattedMessage id="cart.extra.price" defaultMessage="Price" />
                        </span>
                        <span className="table__col table__col--options"></span>
                    </header>
                    <section className="table__body">
                        <ReactCSSTransitionGroup
                            transitionName="slide-fade"
                            transitionEnterTimeout={400}
                            transitionLeaveTimeout={400}
                        >
                            {items.length ? items.map(it => renderExtraLine(it, this._deleteItem)) : renderEmpty()}
                        </ReactCSSTransitionGroup>
                    </section>
                    <footer className="cart-list__total">
                        <span className="cart-list__total-name txt-bold">
                            <FormattedMessage id="cart.list.total" defaultMessage="TOTAL" />
                        </span>
                        <span className="cart-list__total-value">
                            <FormattedNumber
                                value={total}
                                minimumFractionDigits={2}
                                maximumFractionDigits={2}
                                currency="EUR"
                                currencyDisplay="symbol"
                                style="currency" />
                        </span>
                    </footer>
                </article>
            </div>
        )
    }
}

CartExtra.contextTypes = {
    intl: intlShape.isRequired
}

CartExtra.propTypes = {
    ids: PropTypes.array.isRequired,
    items: PropTypes.array.isRequired,
    dispatch: PropTypes.func.isRequired,
    total: PropTypes.number.isRequired
}
CartExtra.defaultProps = {}

const mapStateToProps = (state, ownProps) => {
    return {
        items: getCartExtraLines(state, ownProps.ids)
    }
}

export default connect(
    mapStateToProps,
    {}

)(CartExtra)
