import React, { Component } from 'react'
import { connect } from 'react-redux'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import { u } from 'umbrellajs'

import { toggleCart } from '../../actions/carts'
import { isInitCartFinish } from '../../reducers/helpers'
import { getCartCount } from '../../reducers/cart'

import Icon from '../Icon'

class CartBtn extends Component {
    constructor(props, context) {
        super(props, context)

        this._handleToggleCart = this._handleToggleCart.bind(this)
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        return (this.props.total !== nextProps.total) || (this.props.isLoading !== nextProps.isLoading)
    }

    _handleToggleCart = () => {
        const { dispatch } = this.props

        u('body').toggleClass('is-cart-open')

        dispatch(toggleCart())
    }

    _renderBadge = () => {
        const { total, isLoading } = this.props

        if (isLoading) {
            return null
        }

        return <span className="cart-btn__badge badge badge--error" key={total}>{total}</span>
    }

    render = () => {
        const { isLoading } = this.props
        return (
            <div className="cart-btn is-selectable" onClick={!isLoading ? this._handleToggleCart : null}>
                <Icon icon="cart" className="cart-btn__icon icon--20" />
                <ReactCSSTransitionGroup
                    transitionName="badge"
                    transitionEnterTimeout={300}
                    transitionLeaveTimeout={300}
                >
                    {this._renderBadge()}
                </ReactCSSTransitionGroup>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const isLoading = !isInitCartFinish(state)
    return {
        total: getCartCount(state),
        isLoading
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps

)(CartBtn)
