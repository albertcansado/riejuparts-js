import React, { PropTypes } from 'react'

import Image from './Image'
import Icon from './Icon'

const LightBox = ({ src, handleClose}) => {
    return (
        <div className="lightbox u-centered">
            <div className="lightbox__close txt-right u-mgb--5 is-selectable" onClick={handleClose}>
                <Icon icon="close" className="lightbox__close-icon icon--20" />
            </div>
            <div className="lightbox__content">
                <Image
                    src={src}
                    className="lightbox__image"
                />
            </div>
        </div>
    )
}

LightBox.propTypes = {
    src: PropTypes.string.isRequired,
    handleClose: PropTypes.func.isRequired
}
LightBox.defaultProps = {}

export default LightBox
