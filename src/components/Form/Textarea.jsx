import React, { PureComponent, PropTypes } from 'react'
import { isString, isEmpty } from 'lodash'
import cx from 'classnames'

import Label from './Label'

const patchTextAreaValue = (value, defaultValue = '') => {
    return isEmpty(value)
        ? isString(defaultValue) ? defaultValue : defaultValue.toString()
        : isString(value) ? value : value.toString()
}

class Textarea extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            value: patchTextAreaValue(props.value, props.defaultValue)
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleBlur = this.handleBlur.bind(this)
        this.getValue = this.getValue.bind(this)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.state.value) {
            this.setState({
                value: patchTextAreaValue(nextProps.value, this.props.defaultValue)
            })
        }
    }

    handleChange (ev) {
        const value = ev.target.value

        this.setState({
            value
        })

        if (this.props.onChange) {
            this.props.onChange(ev.target.value)
        }
    }

    handleBlur (ev) {
        if (this.props.onBlur) {
            this.props.onBlur(this.state.value)
        }
    }

    focus = () => {
        this.refs.field.focus()
    }

    getValue = () => {
        return this.state.value
    }

    _renderInput = (options = {}) => {
        const { inputProps, theme } = this.props
        const { value } = this.state
        const inputElementProps = {
            ...inputProps,
            autoComplete: 'off',
            role: 'input',
            ref: 'field',
            onChange: this.handleChange,
            onBlur: this.handleBlur,
            value,
            className: cx('input__field', theme.field),
        }

        return React.createElement('textarea', inputElementProps)
    }

    render = () => {
        const { theme, label } = this.props
        return (
            <div className={cx('input input--textarea', theme.input)}>
                {label ? <Label title={label} className={cx('input__label', theme.label)} /> : null}
                {this._renderInput()}
            </div>
        )
    }
}

Textarea.propTypes = {
    label: PropTypes.string,
    inputProps: PropTypes.object,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    value: PropTypes.any,
    defaultValue: PropTypes.any,
    theme: PropTypes.shape({
        label: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object
        ]),
        input: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object
        ]),
        field: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object
        ])
    })
}
Textarea.defaultProps = {
    label: null,
    inputProps: {},
    onChange: null,
    onBlur: null,
    value: null,
    defaultValue: '',
    theme: {}
}

export default Textarea
