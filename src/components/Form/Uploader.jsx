import React, { PureComponent, PropTypes } from 'react'
import { FormattedMessage } from 'react-intl'
import {isEmpty} from 'lodash'
// import idx from 'idx'
import Dropzone from 'react-dropzone'
import swal from 'sweetalert2'

import Label from './Label'
import Attachment from '../Warranty/Attachment'

class Uploader extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            isUploading: false,
            showDropzone: isEmpty(props.attachment)
        }

        this._clearFile = this._clearFile.bind(this)
        this._preProcessFiles = this._preProcessFiles.bind(this)
    }

    _preProcessFiles = files => {
        const { handleUpload, multiple } = this.props

        let randomId = Math.floor(new Date().valueOf() + Math.random())
        const data = files.map(file => {
            file.id = ++randomId
            file.isUploading = !!handleUpload

            return file
        })

        return multiple ? data : data[0]
    }

    onDrop = files => {
        const { onChange, handleUpload } = this.props

        if ( (!handleUpload || this.state.isUploading) && !onChange) {
            return
        }

        if (!files.length) {
            swal('Select correct format file', null, 'error')
            return
        }

        if (handleUpload) {
            this.props.handleUpload(
                this._preProcessFiles(files),
                this.props.name
            ).then(() => {
                this.setState({
                    isUploading: false
                })
            })
        } else if (onChange) {
            onChange(this._preProcessFiles(files), this.props.name)
        }

        this.setState({
            showDropzone: false
        })
    }

    _clearFile = () => {
        this.setState({
            showDropzone: true
        })

        /*if (this.props.onChange) {
            this.props.onChange(null, this.props.name)
        }*/
    }

    _renderDropzone = () => {
        const { accept, name, multiple } = this.props
        const { isUploading } = this.state

        return (
            <Dropzone
                multiple={multiple}
                onDrop={this.onDrop}
                name={name}
                accept={accept}
                style={{}}
                disableClick={isUploading}
                disabled={isUploading}
                className="uploader__container uploader__container--dashed is-selectable u-flex flex--centerfull"
            >
                <div className="uploader__text txt-center">
                    <p className="uploader__text-drag txt-bold">
                        <FormattedMessage id="dropzone.text.drag" defaultMessage="Drag a file here" />
                    </p>
                    <p className="uploader__text-click txt-underline txt--small">
                        <FormattedMessage id="dropzone.text.select" defaultMessage="or click to select a file to upload" />
                    </p>
                </div>
            </Dropzone>
        )
    }

    _renderFile = () => {
        return <Attachment file={this.props.attachment} handleDelete={this._clearFile} trashIcon />
    }

    render = () => {
        const { label, labelProps } = this.props

        return (
            <div className="input input--simpleUpload">
                {label ? <Label title={label} {...labelProps} /> : null}
                {this.state.showDropzone ? this._renderDropzone() : this._renderFile() }
            </div>
        )
    }
}

Uploader.propTypes = {
    label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.node,
        PropTypes.bool
    ]),
    labelProps: PropTypes.object,
    handleUpload: PropTypes.func,
    onChange: PropTypes.func,
    name: PropTypes.string,
    multiple: PropTypes.bool,
    attachment: PropTypes.object,
    accept: PropTypes.string
}
Uploader.defaultProps = {
    label: false,
    labelProps: {},
    handleUpload: null,
    onChange: null,
    name: '',
    multiple: false,
    attachment: {},
    accept: '*'
}

export default Uploader
