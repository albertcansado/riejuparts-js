/* eslint-disable react/style-prop-object */
import React, { PureComponent, PropTypes } from 'react'
import { FormattedNumber } from 'react-intl'
import Select from 'react-select'
import cx from 'classnames'

import { fetchRequest } from '../../middleware/api'

import Label from './Label'
import Availability from '../Availability'

class SearchPart extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {}

        this._showPrice = this._showPrice.bind(this)
    }

    fetchParts = input => {
        const { minLength, filterFn, fetchUrl } = this.props
        if (input.length <= minLength) {
            return Promise.resolve({ options: [] });
        }

        const url = fetchUrl.replace('{input}', input)
        return fetchRequest(url, { method: 'GET' })
            .then(({ json, response }) => {
                return {
                    options: filterFn(json.data)
                }
            })
    }

    _showPrice = option => {
        if (!this.props.showPrice) {
            return null
        }

        return (
            <div className="txt-bold">
                <FormattedNumber
                    value={option.price}
                    minimumFractionDigits={2}
                    maximumFractionDigits={2}
                    currency="EUR"
                    currencyDisplay="symbol"
                    style="currency" />
            </div>
        )
    }

    _renderOption = option => {
        return (
            <div className="u-flex flex--middle">
                <Availability availability={option.availability} disable={option.disabled} theme={{ 'container': 'u-mgr--5' }} />
                <div className="u-left">
                    <div className="txt-bold">{option.sku}</div>
                    <div className="txt--small">{option.name}</div>
                    {this._showPrice(option)}
                </div>
            </div>
        )
    }

    _renderLabel = () => {
        const { label, theme } = this.props

        if (!label) {
            return null
        }

        return <Label title={label} className={cx('input__label', theme.label)} />
    }

    render = () => {
        const { messages, value, onChange, searchProps } = this.props

        /*let currentValue = value
        if (typeof value === 'object' && value.hasOwnProperty(searchProps.value)) {
            currentValue = value
        }*/

        return (
            <div className="input input--search">
                {this._renderLabel()}
                <Select.Async
                    value={value}
                    multi={false}
                    valueKey={searchProps.value}
                    labelKey={searchProps.label}
                    loadOptions={this.fetchParts}
                    optionRenderer={this._renderOption}
                    onChange={onChange}
                    placeholder={messages.placeholder}
                    noResultsText={messages.noResultsText}
                    searchPromptText={messages.searchPromptText}
                    loadingPlaceholder={messages.loadingPlaceholder}
                />
            </div>
        )
    }
}

SearchPart.propTypes = {
    fetchUrl: PropTypes.string.isRequired,
    label: PropTypes.string,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ]),
    minLength: PropTypes.number,
    onChange: PropTypes.func,
    filterFn: PropTypes.func,
    showPrice: PropTypes.bool,
    searchProps: PropTypes.shape({
        value: PropTypes.string,
        label: PropTypes.string
    }),
    theme: PropTypes.shape({
        label: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object
        ])
    }),
    messages: PropTypes.shape({
        placeholder: PropTypes.string,
        noResultsText: PropTypes.string,
        searchPromptText: PropTypes.string,
        loadingPlaceholder: PropTypes.string
    })
}

SearchPart.defaultProps = {
    value: '',
    minLength: 3,
    filterFn: foo => foo,
    showPrice: false,
    searchProps: {
        value: 'sku',
        label: 'name'
    },
    theme: {},
    messages: {}
}

export default SearchPart
