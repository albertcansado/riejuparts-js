import React, { PureComponent, PropTypes } from 'react'
import TagsInput from 'react-tagsinput'
import cx from 'classnames'

import Label from './Label'

class Tags extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {}
    }

    _renderLabel = () => {
        const { label, theme } = this.props

        if (!label) {
            return null
        }

        return <Label title={label} className={cx('input__label', theme.label)} />
    }

    _renderInput = () => {
        const { tagsProps, placeholder } = this.props

        return React.createElement(
            TagsInput,
            Object.assign({}, tagsProps, {
                tagProps: {
                    className: 'input-tag__tag',
                    classNameRemove: 'input-tag__remove close close--block'
                },
                inputProps: {
                    className: 'input-tag__field',
                    placeholder
                },
                className: 'input-tag'
            })
        )
    }

    render = () => {
        return (
            <div className="input input--tags">
                {this._renderLabel()}
                {this._renderInput()}
            </div>
        )
    }
}

Tags.propTypes = {
    label: PropTypes.string,

    tagsProps: PropTypes.object.isRequired,

    placeholder: PropTypes.string,

    theme: PropTypes.object
}

Tags.defaultProps = {
    label: null,
    placeholder: null,
    theme: {}
}

export default Tags
