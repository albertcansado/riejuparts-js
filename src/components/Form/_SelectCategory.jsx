import React, { PureComponent, PropTypes } from 'react'
import { intlShape } from 'react-intl'
import Select from 'react-select'

import { fetchRequest } from '../../middleware/api'
import { getSelectCategoryMessages } from '../../lib/backend_messages'

import Icon from '../Icon'

class SelectCategory extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            value: props.value,
            options: [],
            isFetching: true
        }

        // Cache Translate Messages
        this._messages = getSelectCategoryMessages(context.intl.formatMessage)
    }

    componentWillMount = () => {
        fetchRequest('/d/categories.json')
            .then(({ json, response }) => {
                if (!response.ok || !json.success) {
                    this.setState({
                        isFetching: false
                    })

                    // TODO Show alert

                    return Promise.resolve()
                }

                this.setState({
                    options: json.data,
                    isFetching: false
                })
            })
    }

    renderOptions = (option) => {
        return (
            <div className="select-icon u-flex">
                <Icon icon={option.icon} className="select-icon__icon icon--20" />
                <span className="select-icon__label txt-bold">{option.label}</span>
            </div>
        )
    }

    handleChange = (value) => {
        this.setState({
            value
        })

        if (this.props.onSelect) {
            this.props.onSelect(value)
        }
    }

    render = () => {
        const { options, value, isFetching } = this.state
        return (
            <Select
              value={value}
              options={options}
              clearable={false}
              isLoading={isFetching}
              onChange={this.handleChange}
              noResultsText={this._messages.selectCategoryNoResults}
              placeholder={this._messages.selectCategoryPlaceholder}
              loadingPlaceholder={this._messages.selectCategoryLoading}
            />
        )
    }
}

SelectCategory.contextTypes = {
    intl: intlShape.isRequired
}

SelectCategory.propTypes = {
    value: PropTypes.string,
    onSelect: PropTypes.func
}

SelectCategory.defaultProps = {
    value: ''
}

export default SelectCategory
