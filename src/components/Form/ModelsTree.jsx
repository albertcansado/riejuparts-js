import React, { PureComponent, PropTypes } from 'react'
// import { FormattedMessage } from 'react-intl'
import idx from 'idx'
import TreeView from 'react-treeview'

import { fetchRequest } from '../../middleware/api'

const parseDefaultValues = foo => {
    return (typeof foo !== 'string' || foo === '')
        ? []
        : foo.split(',')
}

class ModelsTree extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            data: [],
            selected: parseDefaultValues(props.values),
            collapsed: [],
            isLoading: true,
            error: null
        }

        if (this.state.isLoading) {
            this._fetchModels()
        }

        this.handleSelected = this.handleSelected.bind(this)
        this.handleCollapsed = this.handleCollapsed.bind(this)
        this.getSelected = this.getSelected.bind(this)
    }

    _fetchModels = () => {
        const { url } = this.props

        fetchRequest(url)
            .then(({ json, response}) => {
                if (!response.ok || !json.success) {
                    return Promise.reject(
                        idx(json, _ => _.error.msg) || response.statusText
                    )
                }

                this.setState({
                    isLoading: false,
                    data: json.data,
                    collapsed: json.data.map((_, i) => i.toString())
                })

                return Promise.resolve()
            }).catch (error => {
                this.setState({
                    isLoading: false,
                    error
                })
            })
    }

    _isSelected = (key) => {
        return this.state.selected.indexOf(key) !== -1
    }

    handleSelected = node => {
        const { selected } = this.state

        const nextSelected = !this._isSelected(node.id)
            ? [
                ...selected,
                node.id
            ]
            : selected.filter(it => it !== node.id)

        this.setState({
            selected: nextSelected
        })

        if (this.props.onUpdate) {
            this.props.onUpdate(nextSelected)
        }
    }

    _isCollapsed = (key) => {
        return this.state.collapsed.indexOf(key) !== -1
    }

    handleCollapsed = key => {
        const { collapsed } = this.state

        if (!this._isCollapsed(key)) {
            this.setState({
                collapsed: [
                    ...collapsed,
                    key
                ]
            })
        } else {
            this.setState({
                collapsed: collapsed.filter(it => it !== key)
            })
        }
    }

    getSelected = () => {
        return this.state.selected
    }

    _renderLoadingOrError = () => {
        if (this.state.error) {
            return <p className="txt-bold txt--medium is-error" children={this.state.error} />
        }

        return <p className="txt-bold txt--medium" children='Cargando' />
    }

    _treeNode = (node, index) => {
        const { selected } = this.state
        const key = index

        if (!node.name) {
            return null
        }

        if (!node.hasOwnProperty('children')) {
            return (
                <div className="tree-view__leaf u-flex flex--middle is-selectable" key={key} onClick={() => this.handleSelected(node)}>
                    <input type="checkbox" className="tree-view__checkbox is-selectable" checked={selected.indexOf(node.id) !== -1} readOnly />
                    {node.name} {!node.active ? <span className="is-error txt-italic u-mgl--5" children="(inactive)" /> : null}
                </div>
            )
        } else {
            return (
                <TreeView
                    key={key}
                    collapsed={this._isCollapsed(key)}
                    onClick={() => this.handleCollapsed(key)}
                    nodeLabel={<span className="tree-view__node is-selectable" children={node.name} onClick={() => this.handleCollapsed(key)} />}
                    itemClassName="tree-view__item"
                    childrenClassName="tree-view__children"
                >
                    {node.children.map((item, i) => this._treeNode(item, key + i))}
                </TreeView>
            )
        }
    }

    _renderTreeView = () => {
        const { data } = this.state

        return data.map((node, i) => this._treeNode(node, i.toString()))
    }

    render = () => {
        const { isLoading, error } = this.state

        let innerClassName = ['models-tree__inner']
        if (isLoading) {
            innerClassName.push('u-flex flex--centerfull')
        }

        return (
            <div className="models-tree">
                <div className="models-tree__content">
                    <p className="txt--small txt-right u-mgr--10">
                        Seleccionados: <strong>{this.state.selected.length}</strong>
                    </p>
                    <div className={innerClassName.join(' ')}>
                        {isLoading || error ? this._renderLoadingOrError() : this._renderTreeView()}
                    </div>
                </div>
            </div>
        )
    }
}

ModelsTree.propTypes = {
    url: PropTypes.string.isRequired,
    values: PropTypes.string,
    onUpdate: PropTypes.func
}
ModelsTree.defaultProps = {
    values: '',
    onUpdate: null
}

export default ModelsTree
