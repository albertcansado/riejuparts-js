import React, { PropTypes } from 'react'
import cx from 'classnames'
import { isString } from 'lodash'

const renderAddon = (addon) => {
    if (!addon) {
        return null
    } else if (isString(addon)) {
        return <span className="input__label-addon txt--small" children={addon} />
    }

    return addon
}

const Label = ({ title, addon, className }) => {
    return (
        <label className={cx('input__label', className)}>
            {title}
            {renderAddon(addon)}
        </label>
    )
}

Label.propTypes = {
    title: PropTypes.string.isRequired,
    addon: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.node
    ]),
    className: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ])
}

export default Label
