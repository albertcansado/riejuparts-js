import React, { PureComponent, PropTypes } from 'react'
import { map as _map, omit as _omit } from 'lodash'
import Select from 'react-select'

import { fetchRequest } from '../../middleware/api'

import Label from './Label'

const joinOptionsValues = (options, valueKey) => {
    return options
        .map(item => item[valueKey])
        .join(',')
}

class SelectAsync extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            value: props.value,
            options: [],
            isFetching: true
        }

        this.fetchOptions = this.fetchOptions.bind(this)
        this._getOptionsFromJson = this._getOptionsFromJson.bind(this)
    }

    componentDidMount = () => {
        this.fetchOptions()
    }

    componentWillReceiveProps = nextProps => {
        if (nextProps.url !== this.props.url) {
            this.setState({
                options: !nextProps.url ? [] : this.state.options,
                isFetching: !!nextProps
            })
        }
    }

    componentDidUpdate = prevProps => {
        if (prevProps.url !== this.props.url) {
            this.fetchOptions()
        }
    }

    _getOptionsFromJson = json => {
        let data = [];

        if (json.hasOwnProperty('data')) {
            data = json.data
        } else if (json.hasOwnProperty('viewVar')) {
            data = json[json.viewVar]
        }

        return Array.isArray(data)
            ? data
            : _map(data, (value, key) => {
                return {
                    label: value,
                    value: key
                }
            })
    }

    fetchOptions = () => {
        const { url, disabled } = this.props

        if (disabled) {
            this.setState({
                isFetching: false
            })

            return
        }

        fetchRequest(url)
            .then(({ json, response }) => {
                if (!response.ok || !json.success) {
                    this.setState({
                        isFetching: false
                    })

                    // TODO Show alert

                    return Promise.resolve()
                }

                this.setState({
                    options: this._getOptionsFromJson(json),
                    isFetching: false
                })
            })
    }

    handleChange = value => {
        this.setState({
            value
        })

        if (this.props.onSelect) {
            this.props.onSelect(value, {name: this.props.name, valueKey: this.props.searchProps.value, simple: this.props.simpleValue})
        }
    }

    selectAllOptions = () => {
        if (!this.props.multiple || !this.props.selectAll) {
            return
        }

        const values = (this.props.simpleValue && Array.isArray(this.state.options))
            ? joinOptionsValues(this.state.options, this.props.searchProps.value)
            : this.state.options

        this.handleChange(values)
    }

    _renderLabelGroup = () => {
        const { label, labelProps, multiple, selectAll } = this.props

        return (
            <div className="u-flex flex--middle flex--between">
                {label ? <Label title={label} {...labelProps} /> : null}
                {multiple && selectAll ? <span className="input__all is-notice" onClick={this.selectAllOptions}>Select all</span> : null}
            </div>
        )
    }

    render = () => {
        const { options, value, isFetching } = this.state
        const { multiple, renderOptions, messages, searchProps, help, ...others } = this.props

        return (
            <div className="input input--select">
                {this._renderLabelGroup()}
                <Select
                    {...(_omit(others, ['url', 'multiple', 'renderOptions', 'onSelect', 'messages', 'label', 'labelProps', 'help', 'searchProps', 'selectAll']))}
                    value={value}
                    multi={multiple}
                    valueKey={searchProps.value}
                    labelKey={searchProps.label}
                    options={options}
                    optionRenderer={renderOptions}
                    isLoading={isFetching}
                    onChange={this.handleChange}
                    noResultsText={messages.selectAsyncNoResults}
                    placeholder={messages.placeholder}
                    loadingPlaceholder={messages.loadingPlaceholder}
                    searchPromptText={messages.searchPromptText}
                />
                {help ? help : null}
            </div>
        )
    }
}

SelectAsync.propTypes = {
    url: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    clearable: PropTypes.bool,
    multiple: PropTypes.bool,
    selectAll: PropTypes.bool,
    simpleValue: PropTypes.bool,
    joinValues: PropTypes.bool,
    name: PropTypes.string,
    value: PropTypes.string,
    onSelect: PropTypes.func,
    renderOptions: PropTypes.func,
    searchProps: PropTypes.shape({
        value: PropTypes.string,
        label: PropTypes.string
    }),
    messages: PropTypes.shape({
        placeholder: PropTypes.string,
        noResultsText: PropTypes.string,
        searchPromptText: PropTypes.string,
        loadingPlaceholder: PropTypes.string
    }),
    label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.node
    ]),
    labelProps: PropTypes.object
}

SelectAsync.defaultProps = {
    disabled: false,
    clearable: false,
    multiple: false,
    selectAll: false,
    simpleValue: false,
    joinValues: false,
    name: '',
    value: '',
    onSelect: null,
    renderOptions: null,
    searchProps: {
        value: 'value',
        label: 'label'
    },
    messages: {},
    label: null,
    labelProps: {}
}

export default SelectAsync
