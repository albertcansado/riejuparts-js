import React, { PropTypes } from 'react'
import { injectIntl } from "react-intl"

import Icon from '../Icon'
import SelectAsync from './SelectAsync'

const renderIcon = option => {
    return (
        <div className="select-icon u-flex">
            <Icon icon={option.icon} className="select-icon__icon icon--20" />
            <span className="select-icon__label txt-bold">{option.label}</span>
        </div>
    )
}

const SelectIcon = props => {
    return (
        <SelectAsync
            url="/d/cards/icons.json"
            renderOptions={renderIcon}
            {...props}
        />
    )
}

const I18nSelect = props => {
    const ourProps = {
        messages: {
            placeholder: props.intl.formatMessage({
                id: props.placeholderId
            }),
            noResultsText:props.intl.formatMessage({
                id: props.noResultId
            }),
            /*searchPromptText: props.intl.formatMessage({
                id: props.searchPrompthId
            }),*/
            loadingPlaceholder: props.intl.formatMessage({
                id: props.loadingPlaceholderId
            })
        },
        ...props
  }

  return <SelectIcon {...ourProps} />
}

I18nSelect.propTypes = {
    intl: PropTypes.object.isRequired,
    placeholderId: PropTypes.string,
    noResultId: PropTypes.string,
    // searchPrompthId: PropTypes.string
    loadingPlaceholderId: PropTypes.string,
}

I18nSelect.defaultProps = {
    placeholderId: "selectIcon.placeholder",
    noResultId: "selectIcon.noResults",
    // searchPrompthId: "app.components.Select.ReactSelect.clearAllText",
    loadingPlaceholderId: "selectIcon.loading"
}

export default injectIntl(I18nSelect)
