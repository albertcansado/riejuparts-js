import React, { PropTypes } from 'react'

import SelectAsync from '../../components/Form/SelectAsync'

const helpHTML = () => {
    return (
        <p className="txt--small u-mgl--5">Selecciona los grupos de distribuidores que <strong>podrán ver</strong> el recurso</p>
    )
}

const SelectGroups = props => {
    return (
        <SelectAsync
            {...props}
            help={helpHTML()}
        />
    )
}

SelectGroups.defaultProps = {
    url: "/d/groups.json",
    name: 'groups',
    clearable: true,
    multiple: true,
    joinValues: true,
    searchProps: {
        label: 'name',
        value: 'id'
    }
}

export default SelectGroups
