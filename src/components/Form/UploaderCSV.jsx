import React, { PureComponent, PropTypes } from 'react'
import { FormattedMessage } from 'react-intl'
import Dropzone from 'react-dropzone'
import Papa from 'papaparse'
import cx from 'classnames'

import Icon from 'components/Icon'
import Link from 'components/Link'

const validateCSVFile = data => {
    // Filter unique
    const errorsIndex = data.errors
        .map(err => err.row)
        .filter((v, i, a) => a.indexOf(v) === i)

    // Check data length
    const items = data.data.filter((item, index) => {
        return item.length === 2 && item[0] !== '' && item[1] !== ''
    })

    return {
        errorsRows: errorsIndex,
        rows: items
    }
}

const couldUploadFile = state => {
    return !state.isParsing && !state.isUploading && state.rows.length
}

const isDisableBtn = state => {
    return state.isParsing || !state.rows.length
}

class UploaderCSV extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            filename: null,
            total: 0,
            rows: [],
            errors: [],
            fileParsed: false,
            isAttached: false,
            isParsing: false,
            isUploading: false
        }

        this.onHandleSubmit = this.onHandleSubmit.bind(this)
    }

    onDrop = (files) => {
        if (this.state.isAttached) {
            return
        }

        const file = files[0]

        this.setState({
            filename: file.name,
            isParsing: true,
            isAttached: true
        })

        Papa.parse(file, {
            header: false,
            skipEmptyLines: true,
            delimiter: ';',
            complete: results => {
                const {errorsRows, rows} = validateCSVFile(results)
                this.setState({
                    total: results.data.length,
                    rows: rows,
                    errors: errorsRows,
                    isParsing: false
                })
            }
        })
    }

    onHandleSubmit = () => {
        this.setState({
            isUploading: true
        })

        this.props.onSubmit(this.state.rows)
        .then(json => {
            this.setState({
                isUploading: false
            })

            this.props.onCancel()
        })
    }

    _renderParsingResults = () => {
        const { filename, total, rows, errors, isParsing, isUploading } = this.state
        return (
            <div className="upl-csv__result u-flex flex--middle flex--between">
                <div className="u-flex flex--middle flex--between u-mgr--10">
                    <Icon icon="file" className="icon--30 u-mgr--15" />
                    <div>
                        <p className="txt--small txt-italic">
                            {filename}
                        </p>
                        <p>
                            <span>
                                <strong>{isParsing ? 0 : total}</strong> <FormattedMessage id="dropzone.csv.lines" defaultMessage="Lines" />
                            </span>
                            <span className="u-mgl--10 u-mgr--10">
                                <strong className="is-success">{isParsing ? 0 : rows.length}</strong> <FormattedMessage id="dropzone.csv.correct" defaultMessage="Correct" />
                            </span>
                            <span>
                                <strong className="is-error">{isParsing ? 0 : errors.length}</strong> <FormattedMessage id="dropzone.csv.errors" defaultMessage="Errors" />
                            </span>
                        </p>
                    </div>
                </div>
                <Link
                    btn
                    type="success"
                    onClick={couldUploadFile(this.state) ? this.onHandleSubmit : null}
                    className={cx({'is-disabled': isDisableBtn(this.state), 'btn--loading': isUploading})}
                >
                    <FormattedMessage id="dropzone.csv.upload" defaultMessage="Upload file" />
                </Link>
            </div>
        )
    }

    _renderDropzone = () => {
        return (
            <Dropzone
                accept="text/plain, .csv"
                multiple={false}
                onDrop={this.onDrop}
                style={null}
                disableClick={this.state.isParsing}
                className="uploader__container is-selectable u-flex flex--centerfull"
                >
                <div className="uploader__text txt-center">
                    <p className="uploader__text-drag txt-bold">
                        <FormattedMessage id="dropzone.text.dragfile" defaultMessage="Drag a file here" />
                    </p>
                    <p className="uploader__text-click txt-underline txt--small">
                        <FormattedMessage id="dropzone.text.select" defaultMessage="or click to select a file to upload" />
                    </p>
                </div>
            </Dropzone>
        )
    }

    render = () => {
        const { isAttached, isUploading } = this.state

        return (
            <div className={cx("upl-csv", this.props.className)}>
                <p className="u-mgb--15 txt--small">
                    <FormattedMessage id="dropzone.csv.text" defaultMessage="Through a CSV file (Comma separated values) you can add parts to the order or, if they already exist, increase the quantity." />
                    <br />
                    <FormattedMessage
                        id="dropzone.csv.text2"
                        defaultMessage="The CSV file must have the following structure {struct}. You can download an example file {example}."
                        values={{
                            struct: <i className="txt-bold" children={<FormattedMessage id="dropzone.csv.struct" defaultMessage="reference;quantity" />} />,
                            example: this.props.exampleUrl ? <Link href={this.props.exampleUrl} target="_blank" className="link" children={<FormattedMessage id="dropzone.csv.here" defaultMessage="here" />} /> : null
                        }}
                    />
                </p>
                {isAttached ? this._renderParsingResults() : this._renderDropzone()}
                <div className={cx("is-error u-mgt--10 txt-center", {'is-selectable': !isUploading, 'is-disabled': isUploading})} onClick={!isUploading ? this.props.onCancel : null}>
                    <FormattedMessage id="app.cancel" defaultMessage="Cancel" />
                </div>
            </div>
        )
    }
}

UploaderCSV.propTypes = {
    onCancel: PropTypes.func.isRequired,
    onSubmit: PropTypes.func,
    exampleUrl: PropTypes.string,
    className: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ])
}
UploaderCSV.defaultProps = {
    className: '',
    exampleUrl: null
}

export default UploaderCSV
