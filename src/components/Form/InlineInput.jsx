import React, { Component, PropTypes } from 'react'
import { findDOMNode } from 'react-dom'
import cx from 'classnames'

import Input from './Input'
import Link from '../Link'
import Icon from '../Icon'

class InlineInput extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {
            isEditing: false,
            value: props.value
        }

        this._el = null

        this.handleDocumentClick = this.handleDocumentClick.bind(this)
    }

    componentDidMount() {
        this._el = findDOMNode(this)
        document.addEventListener('click', this.handleDocumentClick, true)
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.value !== this.props.value) {
            this.setState({
                value: nextProps.value
            })
        }
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        return nextProps.value !== this.props.value ||
            nextProps.placeholder !== this.props.placeholder ||
            nextState.isEditing !== this.state.isEditing ||
            nextState.value !== this.state.value
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleDocumentClick, true)
    }

    handleDocumentClick(ev) {
        if( this.state.isEditing && ev.target !== this._el && !this._el.contains(ev.target)) {
            this.close()
        }
    }

    close = () => {
        if (this.props.onClose) {
            this.props.onClose(this.state.value)
        }

        this.setState({
            isEditing: false
        })
    }

    clear = () => {
        this.refs.input.focus()
        this.setState({
            value: ''
        })
    }

    _renderInput = (value = '') => {
        const { theme, placeholder } = this.props

        return (
            <div className="in-input__input u-flex">
                <Input
                    value={value}
                    className={theme.input}
                    inputProps={{
                        placeholder,
                        autoFocus: true
                    }}
                    theme={{
                        field: 'txt--medium'
                    }}
                    ref='input'
                    onChange={(value) => this.setState({value})}
                />
                {value ? <Link btn type="reset" className="in-input__clear" onClick={this.clear} children={<Icon icon="close" className="icon--14" />} /> : null}
            </div>
        )
    }

    _renderValue = (value = '') => {
        const { placeholder, theme } = this.props
        const text = value ? value : placeholder

        return (
            <span
                className={cx("in-input__name", {
                    'in-input__name--placeholder': !value
                }, value ? theme.value : theme.placeholder)}
            >
                {text}
                <Icon icon="edit" className="icon--14" />
            </span>
        )
    }

    render = () => {
        const { isEditing, value } = this.state
        const { theme } = this.props

        return (
            <div
                className={cx("in-input", {
                    'is-selectable': !isEditing
                }, theme.root)}
                onClick={!isEditing ? () => this.setState({ isEditing: true }) : null}
            >
                {isEditing ? this._renderInput(value) : this._renderValue(value) }
            </div>
        )
    }
}

InlineInput.propTypes = {
    value: PropTypes.string,
    placeholder: PropTypes.string,
    theme: PropTypes.shape({
        root: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object
        ]),
        'placeholder': PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object
        ]),
        'value': PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object
        ]),
        'input': PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object
        ])
    })
}

InlineInput.defaultProps = {
    value: '',
    placeholder: 'Enter text',
    theme: {}
}

export default InlineInput
