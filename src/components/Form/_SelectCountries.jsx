import React, { PureComponent, PropTypes } from 'react'
import idx from 'idx'

import SelectAsync from '../../components/Form/SelectAsync'
import Label from '../../components/Form/Label'
import Image from '../../components/Image'

const renderCountryValue = ({ placeholder, children, value, onRemove }) => {
    return (
        <div className="Select-value" title={value.name}>
            <span className="Select-value-icon" onClick={() => onRemove(value)} aria-hidden="true">×</span>
			<div className="Select-value-label">
                <div className="u-flex flex--middle">
                    <Image src={value.flag} width="20" className="u-mgr--5" />
                    {children}
                </div>
			</div>
		</div>
    )
}

const renderCountryOption = option => {
    return (
        <div className="u-flex flex--middle">
            <Image src={option.flag} className="u-mgr--10" width="25" />
            <span>{option.name}</span>
        </div>
    )
}

class SelectCountries extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.selectAllOptions = this.selectAllOptions.bind(this)
    }

    selectAllOptions = () => {
        const options = idx(this.refs.select, _ => _.state.options)
        if (Array.isArray(options)) {
            const value = options
                .map(item => item.id)
                .join(',')

            this.refs.select.setState({
                value
            })
            this.props.onSelect(value)
        }
    }

    render = () => {
        const { label, value, onSelect } = this.props
        return (
            <div className="input input--select">
                <div className="u-flex flex--middle flex--between">
                    {label ? <Label title={label} /> : null}
                    <span className="input__all is-notice" onClick={this.selectAllOptions}>Select all</span>
                </div>
                <SelectAsync
                    url="/d/countries.json"
                    clearable
                    multiple
                    searchProps={{
                        label: 'name',
                        value: 'id'
                    }}
                    renderOptions={renderCountryOption}
                    valueComponent={renderCountryValue}
                    onSelect={onSelect}
                    simpleValue
                    joinValues
                    value={value}
                    ref="select"
                />
            </div>
        )
    }
}

SelectCountries.propTypes = {
    onSelect: PropTypes.func.isRequired,
    label: PropTypes.string,
    value: PropTypes.string
}
SelectCountries.defaultProps = {
    label: null,
    value: ''
}

export default SelectCountries
