import React, { PureComponent, PropTypes } from 'react'
import { findDOMNode } from 'react-dom'
import { isEmpty } from 'lodash'
import cx from 'classnames'
import idx from 'idx'

import Label from './Label'
import Icon from '../Icon'

class Translations extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            currentIndex: 0,
            values: props.defaultValues,
            isOpen: false,
            listTop: props.dropdownOnTop
        }

        this.handleDocumentClick = this.handleDocumentClick.bind(this)

        // Public Methods
        this.open = this.open.bind(this)
        this.close = this.close.bind(this)
        this.toggle = this.toggle.bind(this)
        this.setIndex = this.setIndex.bind(this)
        this.getValues = this.getValues.bind(this)
    }

    componentDidMount = () => {
        this.node = findDOMNode(this)

        document.addEventListener('click', this.handleDocumentClick, true)
    }

    componentWillUnmount = () => {
        document.removeEventListener('click', this.handleDocumentClick, true)
    }

    handleDocumentClick = ev => {
        if( ev.target !== this.node && !this.node.contains(ev.target) ) {
            this.close()
        }
    }

    /*_calculateDropdownPosition = () => {
        // const { parentElement } = this.props

        this.parent = this.node.parentNode

        const nodeBottom = this.node.getBoundingClientRect().bottom
        const parentBottom = this.node.getBoundingClientRect().bottom
        // TODO check top or add calculate max height and scroll it
        if (nodeBottom >= parentBottom) {
            this.setState({
                listTop: true
            })
        }
    }*/

    open = () => {
        this.setState({
            isOpen: true
        })
    }

    close = () => {
        this.setState({
            isOpen: false
        })
    }

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    setIndex = index => {
        const { langs } = this.props

        if (index >= 0 && index < langs.length) {
            this.setState({
                currentIndex: index
            })
        }

        this.close()
    }

    getValues = () => {
        return this.state.values
    }

    _getCurrentLocale = () => {
        const { currentIndex } = this.state
        const { langs } = this.props

        return langs[currentIndex].locale
    }

    _getCurrentText = () => {
        const { values } = this.state

        const currentLang = this._getCurrentLocale()

        if (!values.hasOwnProperty(currentLang)) {
            return ''
        }

        const currentText = values[currentLang]
        return currentText !== null ? currentText : ''
    }

    _hasLangValue = lang => {
        return !isEmpty(idx(this.state.values, _ => _[lang]))
    }

    _updateLang = ev => {
        const { values } = this.state
        const nextText = ev.target.value

        const currentLang = this._getCurrentLocale()
        const oldLangText = this._getCurrentText()

        if (oldLangText === nextText) {
            return
        }

        this.setState({
            values: {
                ...values,
                [currentLang]: nextText
            }
        })
    }

    _onBlurInput = (ev) => {
        const { onChange, defaultValues } = this.props

        if (!onChange) {
            return
        }

        const currentLang = this._getCurrentLocale()
        const oldLangText = defaultValues.hasOwnProperty(currentLang) ? defaultValues[currentLang] : ''
        const nextText = ev.target.value

        if (oldLangText === nextText) {
            return
        }

        onChange(this.getValues(), this.props.name, {current: currentLang})
    }

    _renderLang = (lang, index) => {
        return <span
                className={cx("lang-dropdown__lang is-selectable", { 'lang-dropdown__lang--has-value': this._hasLangValue(lang.locale)})}
                onClick={() => this.setIndex(index)}
                key={lang.locale}
                children={lang.label} />
    }

    render = () => {
        const { langs, labelTitle, labelProps } = this.props
        const { currentIndex, isOpen, listTop } = this.state

        const currentValue = this._getCurrentText()
        return (
            <div className="input input--translations">
                <Label title={labelTitle} {...labelProps} />
                <div className="input__group">
                    <div className="input__addon">
                        <div className="lang-dropdown">
                            <span className="lang-dropdown__trigger is-selectable" onClick={this.toggle}>
                                <span className="lang-dropdown__current txt-bold">
                                    {langs[currentIndex].label}
                                </span>
                                <Icon icon="left-arrow" className="lang-dropdown__caret icon--caret icon--15" />
                            </span>
                            <div className={cx("lang-dropdown__list", { 'is-open': isOpen, 'lang-dropdown__list--top': listTop })}>
                                {langs.map((lang, index) =>
                                    index === currentIndex
                                        ? null
                                        : this._renderLang(lang, index)
                                )}
                            </div>
                        </div>
                    </div>
                    <input type="text" className="input__field" value={currentValue} onChange={this._updateLang} onBlur={this._onBlurInput} />
                </div>
            </div>
        )
    }
}

Translations.propTypes = {
    labelTitle: PropTypes.string.isRequired,

    labelProps: PropTypes.object,

    name: PropTypes.string,

    langs: PropTypes.array,

    dropdownOnTop: PropTypes.bool,

    defaultValues: PropTypes.object,

    // parentElement: PropTypes.string,

    onChange: PropTypes.func
}

Translations.defaultProps = {
    labelProps: {},
    name: '',
    langs: idx(window, _ => _.config.langs.available),
    defaultValues: {},
    // parentElement: null,
    dropdownOnTop: false
}

export default Translations
