import React, { PureComponent, PropTypes} from 'react'
import Dropzone from 'react-dropzone'
import { FormattedMessage } from 'react-intl'
import cx from 'classnames'
// import fetch from 'isomorphic-fetch'

import Label from './Label'
import Image from '../Image'

class ImageUploader extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            file: {},
            hasFile: !!props.image,
            isUploading: false
        }

        this.onDrop = this.onDrop.bind(this)
    }

    _unattachFile = () => {
        const { file } = this.state

        if (file.hasOwnProperty('preview')) {
            window.URL.revokeObjectURL(file.preview)
        }

        return this
    }

    _setFile = (file) => {
        const { handleUpload } = this.props

        this.setState({
            file,
            hasFile: true,
            isUploading: !!handleUpload
        })
    }

    onDrop = (files) => {
        const { isUploading } = this.state

        if (isUploading) {
            return
        }

        this
            //._unattachFile()
            ._setFile(files[0])

        if (this.props.handleUpload) {
            this.props.handleUpload(files[0], this.props.name)
                .then(response => {
                    if (response.error) {
                        this._unattachFile()
                    }

                    this.setState({
                        isUploading: false
                    })
                })
        } else if (this.props.onChange) {
            this.props.onChange(files[0], this.props.name)
        }
    }

    _chooseUrlImage = () => {
        const { file } = this.state
        const { image, imagePrefix } = this.props

        if (file.hasOwnProperty('preview')) {
            return file.preview;
        }

        return imagePrefix ? `${imagePrefix}/${image}` : image
    }

    _renderImage = () => {
        const { hasFile, isUploading } = this.state

        if (!hasFile) {
            return null
        }

        const url = this._chooseUrlImage()
        return (
            <div className="uploader-img txt-center" onClick={!isUploading ? () => this.refs.dropzone.open() : null}>
                {url ? <Image src={url} className="uploader-img__image u-centered" /> : null }
                {isUploading ? null : <span className="uploader-img__text txt-black">Click to <br />upload another image</span>}
            </div>
        )
    }

    render = () => {
        const { labelTitle, name } = this.props
        const { isUploading, hasFile } = this.state

        return (
            <div className="input input--uploader">
                <Label title={labelTitle} />
                <div className={cx("uploader is-selectable", {
                    'is-selectable': !isUploading,
                    'has-file': hasFile
                })}>
                    <Dropzone
                        ref="dropzone"
                        accept="image/jpeg, image/png, image/jpg"
                        multiple={false}
                        onDrop={this.onDrop}
                        style={null}
                        name={name}
                        disableClick={isUploading}
                        className="uploader__container u-flex u-h100"
                    >
                        <div className="uploader__text txt-center">
                            <span className="uploader__text-drag txt-bold">
                                <FormattedMessage id="dropzone.text.drag" defaultMessage="Drag an image here" />
                            </span>
                            <span className="uploader__text-click txt-underline txt--small">
                                <FormattedMessage id="dropzone.text.select" defaultMessage="or click to select a file to upload" />
                            </span>
                        </div>
                    </Dropzone>
                    {this._renderImage()}
                    {isUploading ? <span className="uploader__loader" /> : null}
                </div>
            </div>
        )
    }
}

ImageUploader.propTypes = {
    labelTitle: PropTypes.string.isRequired,
    imagePrefix: PropTypes.string,
    image: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ]),
    handleUpload: PropTypes.func,
    onChange: PropTypes.func,
    name: PropTypes.string
}

ImageUploader.defaultProps = {
    name: '',
    image: '',
    onChange: null,
    imagePrefix: null
}

export default ImageUploader
