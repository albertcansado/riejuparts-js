import React, { Component, PropTypes } from 'react'
import { isElement } from 'lodash'

import Input from './Input'

const getMaxMin = (foo, type = 'min') => {
    if (isElement(foo) && foo[type]) {
        return foo[type]
    } else if (foo.props && foo.props.hasOwnProperty(type)) {
        return foo.props[type]
    }

    return Number[(type === 'min') ? 'MIN_SAFE_INTEGER' : 'MAX_SAFE_INTEGER']
}

class NumberInput extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {
            value: this._parseValue(props.value ? props.value : props.defaultValue)
        }

        this.decrease = this.decrease.bind(this)
        this.increase = this.increase.bind(this)
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.value !== this.state.value) {
            this.setState({
                value: nextProps.value
            })
        }
    }

    _parseValue = (value) => {
        const val = Math.max(
            getMaxMin(this, 'min'),
            Math.min(
                value,
                getMaxMin(this, 'max')
            )
        )
        return (isNaN(val)) ? 0 : val
    }

    _setValue = (val, sendCallback = true) => {
        const value = this._parseValue(val)

        if (this.props.onChange && sendCallback) {
            this.props.onChange(value)
        }

        this.setState({ value })
    }

    decrease = () => {
        this._setValue(this.state.value - 1)
    }

    increase = () => {
        this._setValue(this.state.value + 1)
    }

    getValue = () => {
        return this.state.value
    }

    render = () => {
        const { defaultValue, label, vertical } = this.props
        const { value } = this.state

        const inputClassName = ['input--numeric']
        if (!vertical) {
            inputClassName.push('input--horizontal')
        }

        return (
            <Input
                before="line"
                beforeAddonProps={{
                    onClick: this.decrease
                }}
                after="add"
                afterAddonProps={{
                    onClick: this.increase
                }}
                defaultValue={defaultValue}
                value={value}
                onChange={(val) => this._setValue(val)}
                label={label}
                theme={{
                    label: "is-middle",
                    input: inputClassName.join(' '),
                    beforeAddon: "is-selectable",
                    beforeIcon: "icon--14",
                    afterAddon: "is-selectable",
                    afterIcon: "icon--14"
                }}
            />
        )
    }
}

NumberInput.propTypes = {
    min: PropTypes.number,
    max: PropTypes.number,
    value: PropTypes.number,
    defaultValue: PropTypes.number,
    vertical: PropTypes.bool,
    onChange: PropTypes.func
}

NumberInput.defaultProps = {
    min: 0,
    value: 0,
    defaultValue: 0,
    vertical: false,
    onChange: null
}

export default NumberInput
