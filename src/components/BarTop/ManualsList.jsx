import React, { PropTypes } from 'react'
import { FormattedMessage } from 'react-intl'

import { API_ROOT } from '../../middleware/api'

import Link from '../Link'

const renderItem = (manual, handleClick) => {
    return (
        <Link
            href={`${API_ROOT}/technical-zone/download/${manual.id}`}
            type="reset"
            target="_black"
            className="modal-list__item u-flex flex--centerfull is-selectable"
            key={manual.id}
        >
            <span className="modal-list__name">
                {manual.title}
            </span>
        </Link>
    )
}

const ManualsList = ({ items, handleClose }) => {
    return (
        <div className="modal-card">
            <div className="modal-card__body">
                <header className="u-flex flex--middle flex--between u-mgb--10">
                    <h4>
                        <span className="txt-bold u-mgr--5">
                            {items.length}
                        </span>
                        <FormattedMessage id="bartop.manuals.select" defaultMessage="available manuals" />
                    </h4>
                </header>
                <section className="modal__content-inner">
                    <div className="modal-list modal-list--manuals row">
                        {items.map(manual => renderItem(manual))}
                    </div>
                </section>
            </div>
        </div>
    )
}

ManualsList.propTypes = {
    handleClose: PropTypes.func.isRequired,
    items: PropTypes.array.isRequired
}

export default ManualsList
