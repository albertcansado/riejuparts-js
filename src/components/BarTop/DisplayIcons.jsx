import React, { Component, PropTypes } from 'react'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router'
import { find as _find} from 'lodash'
import cx from 'classnames'

import Icon from '../Icon'
// import Engines from './Engines'
import ManualsList from './ManualsList'
import CardsList from './CardsList'
import EnginesList from './EnginesList'

const renderEngineButton = ({ engines, url, showModal, isActive }) => {
    const countEngines = engines ? engines.length : 0
    let engineCards = 0
    if (countEngines === 1) {
        engineCards = engines[0].cards.length
    } else if (countEngines > 1) {
        engineCards = 2
    }

    let elementWrapper = 'div'
    let elementProps = {
        className: cx('iconpart txt-center', {
            'is-selectable': countEngines,
            'is-disabled': !countEngines,
            'is-active': isActive
        })
    }

    // Single engine - Single card
    if (countEngines === 1 && engineCards === 1) {
        elementWrapper = Link
        elementProps = Object.assign(elementProps, {
            to: `${url}/${engines[0].cards[0].id}`,
            activeClassName: "is-active",
        })
    } else {
        // One/Many Engines and/or n Cards
        elementProps = Object.assign(elementProps, {
            onClick: () => showModal(EnginesList, engines)
        })
    }

    return React.createElement(
            elementWrapper,
            elementProps,
            <Icon icon="motor" className="iconpart__icon" />,
            <div className="iconpart__text hidden-md-down">
                {countEngines.length > 1
                    ? <FormattedMessage id="bartop.engine" defaultMessage="Engine" />
                    : <FormattedMessage id="bartop.engines" defaultMessage="Engines" />
                }
            </div>
        )
}


class DisplayIcons extends Component {
    /*constructor(props, context) {
        super(props, context)

        this.state = {}

        this.showModal = this.showModal.bind(this)
    }*/

    showModal = (component, data) => {
        const { openModal, url, currentCard } = this.props

        openModal({
            component: component,
            props: {
                items: data,
                url,
                currentCard
            }
        })
    }

    render = () => {
        const { entity, currentCard, url } = this.props

        if (!entity.hasOwnProperty('id')) {
            return null
        }

        const isVersionCard = _find(entity.cards, { id: currentCard })

        // Manuals
        const hasManuals = entity.hasOwnProperty('manuals') ? entity.manuals.length : false

        return (
            <div className="iconparts u-flex">
                <div
                    className={
                        cx('iconpart txt-center', {
                            'is-selectable': entity.cards.length,
                            'is-active': isVersionCard
                        })
                    }
                    onClick={() => this.showModal(CardsList, entity.cards)}
                >
                    <Icon icon="grid" className="iconpart__icon" />
                    <div className="iconpart__text hidden-md-down">
                        <FormattedMessage id="bartop.cards" defaultMessage="Cards" />
                    </div>
                </div>
                {renderEngineButton({
                    engines: entity.engines,
                    showModal: this.showModal,
                    isActive: !isVersionCard,
                    url
                })}
                <div
                    className={
                        cx('iconpart txt-center', {
                            'is-selectable': hasManuals,
                            'is-disabled': !hasManuals
                        })
                    }
                    onClick={() => this.showModal(ManualsList, entity.manuals)}
                >
                    <Icon icon="file" className="iconpart__icon" />
                    <div className="iconpart__text hidden-md-down">
                        <FormattedMessage id="bartop.docs" defaultMessage="Docs" />
                    </div>
                </div>
            </div>
        )
    }
}

DisplayIcons.propTypes = {
    currentCard: PropTypes.string,
    entity: PropTypes.object.isRequired,
    url: PropTypes.string.isRequired,
    // openModal: PropTypes.func.isRequired
}

DisplayIcons.defaultProps = {
    currentCard: ''
}

export default DisplayIcons
