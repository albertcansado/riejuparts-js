import React, { PureComponent, PropTypes } from 'react'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router'
import { find as _find } from 'lodash'
import cx from 'classnames'

import Icon from '../Icon'

const renderItem = (card, url, handleClick) => {
    return (
        <Link
            to={`${url}/${card.id}`}
            activeClassName="is-active"
            className="modal-list__item"
            onClick={handleClick}
            key={card.id}
        >
            <div className="modal-list__item-inner u-flex flex--middle">
                <Icon icon={card.icon ? card.icon : 'lamina'} className="modal-list__icon icon--20" />
                <span className="modal-list__name">
                    {card.name}
                </span>
            </div>
        </Link>
    )
}

class EnginesList extends PureComponent {
    constructor(props, context) {
        super(props, context)

        const count = props.items.length
        this.state = {
            selectedIndex: 0,
            chooseEngine: count > 1,
            count
        }
    }

    _handleSelectEngine = (index) => {
        const count = this.props.items.length - 1
        if (index < 0 || index > count ) {
            return
        }

        this.setState({
            selectedIndex: index,
            chooseEngine: false
        })
    }

    _getCurrentEngine = () => {
        return this.props.items[this.state.selectedIndex]
    }

    _renderEngines = () => {
        const { items, url, handleClose, currentCard } = this.props

        return (
            <div className="modal-list modal-list--engines row">
                {items.map((engine, k) => {
                    const isActive = _find(engine.cards, {id: currentCard})
                    const defaultProps = {
                        key: engine.id,
                        className: cx("modal-list__item u-flex flex--centerfull is-selectable", {
                            //'is-selectable': !isActive || engine.cards.length > 1,
                            'is-active': isActive
                        })
                    }
                    const elementWrapper = engine.cards.length > 1 ? 'div' : Link
                    const elementProps = Object.assign(
                        defaultProps,
                        engine.cards.length > 1
                            ? { onClick: () => this._handleSelectEngine(k)}
                            : {
                                to: `${url}/${engine.cards[0].id}`,
                                activeClassName: "is-active",
                                onClick: handleClose
                            }
                    )

                    return React.createElement(
                        elementWrapper,
                        elementProps,
                        <p className="modal-list__name txt-bold">{engine.name}</p>,
                        engine.text !== '' ? <span className="modal-list__info u-block txt-small" children={engine.text} /> : null
                    )
                })}
            </div>
        )
    }

    _renderGoBack = () => {
        if (this.state.count < 2) {
            return null
        }

        return (
            <div
                className="modal-list__item modal-list__item--back is-selectable"
                onClick={() => this.setState({ chooseEngine: true })}
                key="GOBACK"
            >
                <div className="modal-list__item-inner u-flex flex--middle">
                    <Icon icon="left-arrow" className="modal-list__icon icon--20" />
                    <span className="modal-list__name txt--medium txt-bold">
                        <FormattedMessage id="bartop.go_back" defaultMessage="Go Back" />
                    </span>
                </div>
            </div>
        )
    }

    _renderCards = () => {
        const { url, handleClose } = this.props

        const currentEngine = this._getCurrentEngine()
        return (
            <div className="modal-list modal-list--cards row">
                {this._renderGoBack(currentEngine)}
                {currentEngine.cards.map(card => renderItem(card, url, handleClose))}
            </div>
        )
    }

    _renderTitle = () => {
        const { chooseEngine } = this.state

        if (chooseEngine) {
            return <FormattedMessage id="bartop.engines.select" defaultMessage="Select an engine" />
        }

        const currentEngine = this._getCurrentEngine()
        return (
            <span>
                <span className="txt-bold u-mgr--5">
                    {currentEngine.cards.length}
                </span>
                <FormattedMessage id="bartop.cards.select" defaultMessage="available cards" />
            </span>
        )
    }

    render = () => {
        const { chooseEngine } = this.state
        const { handleClose } = this.props

        return (
            <div className="modal-card">
                <div className="modal-card__body">
                    <header className="u-flex flex--middle flex--between u-mgb--10">
                        <p className="txt--large">{this._renderTitle()}</p>
                        <span className="close close--dark" onClick={handleClose}></span>
                    </header>
                    <section className="modal__content-inner">
                        {chooseEngine ? this._renderEngines() : this._renderCards()}
                    </section>
                </div>
            </div>
        )
    }
}

EnginesList.propTypes = {
    handleClose: PropTypes.func.isRequired,
    items: PropTypes.array.isRequired,
    url: PropTypes.string.isRequired,
    currentCard: PropTypes.string.isRequired
}

// EnginesList.defaultProps = {}

export default EnginesList
