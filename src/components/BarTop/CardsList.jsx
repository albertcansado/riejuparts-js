import React, { PropTypes } from 'react'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router'

import Icon from '../Icon'

const renderItem = (card, url, handleClick) => {
    return (
        <Link
            to={`${url}/${card.id}`}
            activeClassName="is-active"
            className="modal-list__item"
            onClick={handleClick}
            key={card.id}
        >
            <div className="modal-list__item-inner u-flex flex--middle">
                <Icon icon={card.icon ? card.icon : 'lamina'} className="modal-list__icon icon--20" />
                <span className="modal-list__name">
                    {card.name}
                </span>
            </div>
        </Link>
    )
}

const CardsList = ({ items, url, handleClose }) => {
    return (
        <div className="modal-card">
            <div className="modal-card__body">
                <header className="u-flex flex--middle flex--between u-mgb--10">
                    <h4>
                        <span className="txt-bold u-mgr--5">
                            {items.length}
                        </span>
                        <FormattedMessage id="bartop.cards.select" defaultMessage="available cards" />
                    </h4>
                    <span className="close close--dark" onClick={handleClose}></span>
                </header>
                <section className="modal__content-inner">
                    <div className="modal-list modal-list--cards row">
                        {items.map(card => renderItem(card, url, handleClose))}
                    </div>
                </section>
            </div>
        </div>
    )
}

CardsList.propTypes = {
    handleClose: PropTypes.func.isRequired,
    items: PropTypes.array.isRequired,
    url: PropTypes.string.isRequired,
    currentCard: PropTypes.string.isRequired
}

export default CardsList
