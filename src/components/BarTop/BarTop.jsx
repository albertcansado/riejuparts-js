import React, { Component, PropTypes } from 'react'
import { intlShape } from 'react-intl'
import { bindActionCreators } from 'redux'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import { connect } from 'react-redux'
import { eq } from 'lodash'

import { openModal } from '../../actions/modal'

import { getCategoryEntity, getVersionEntity, getModelEntity } from '../../reducers/entities'
import { getBarTopMessages } from '../../lib/messages'

import DisplayIcons from './DisplayIcons'
import Breadcrumbs from './Breadcrumbs'
// import Icon from '../Icon'

const FirstChild = (props) => {
    const childrenArray = React.Children.toArray(props.children)
    return childrenArray[0] || null
}

class BarTop extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {}

        // Cache Translate Messages
        this._messages = getBarTopMessages(context.intl.formatMessage)
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !eq(nextProps.request, this.props.request)
    }

    _renderRight = () => {
        const { request, entities, url, openModal } = this.props

        if (request.version === null) {
            return null
            /*return (
                <div className="bartop-search u-flex flex--middle u-h100">
                    <input type="text" className="bartop-search__input u-h100" placeholder="Nº Bastidor" />
                    <Icon icon="search" className="bartop-search__icon icon--20" />
                </div>
            )*/
        }

        return <DisplayIcons entity={entities.version} url={url} currentCard={request.card} openModal={openModal} />
    }

    _renderBar = () => {
        const { entities, title } = this.props

        return (
            <div className="bartop" key="bartop">
                <div className="row">
                    <div className="col-xs-7">
                        <h1 className="page-title u-truncate">{title || this._messages.bartopCatalog}</h1>
                        <Breadcrumbs entities={entities} />
                    </div>
                    <div className="col-xs-5">
                        {this._renderRight()}
                    </div>
                </div>
            </div>
        )
    }

    render = () => {
        return (
            <ReactCSSTransitionGroup
                transitionName="slide"
                transitionAppear={true}
                transitionAppearTimeout={500}
                transitionEnterTimeout={300}
                transitionLeaveTimeout={300}
                component={FirstChild}
            >
                {this.props.show ? this._renderBar() : null}
            </ReactCSSTransitionGroup>
        )
    }
}

BarTop.contextTypes = {
    intl: intlShape.isRequired
}

BarTop.propTypes = {
    request: PropTypes.object.isRequired,
    show: PropTypes.bool
}

BarTop.defaultProps = {
    show: false
}

const getEntitiesFromRequest = (state, request) => {
    const version = getVersionEntity(state, request.version)

    return {
        category: getCategoryEntity(state, request.category),
        version,
        model: version.hasOwnProperty('id') ? getModelEntity(state, version.model_id) : {}
        // card: getCardEntity(state, request.card)
    }
}

const getTitle = (state, { version, category, model }) => {
    if (version.hasOwnProperty('id')) {
        return `${model.name} - ${version.name}`
    } else if (category.hasOwnProperty('id')) {
        return category.name
    }

    return null
}

const mapStateToProps = (state, { request, show }) => {
    const entities = getEntitiesFromRequest(state, request)
    return {
        title: show ? getTitle(state, entities) : null,
        entities: entities
    }
}

export default connect(
    mapStateToProps,
    (dispatch) => bindActionCreators({ openModal }, dispatch)
)(BarTop)
