import React, { Component, PropTypes } from 'react'
import { intlShape } from 'react-intl'
import { Link } from 'react-router'

import { getBreadcrumbsMessages } from '../../lib/messages'

const renderLink = ({ children, ...others}) => {
    return React.createElement(
        Link,
        others,
        children
    )
}

const separator = (num = 0) => {
    return <span className="breadcrumb__separator" children="/" key={`separator${num}`} />
}

class Breadcrumbs extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {}

        // Cache Translate Messages
        this._messages = getBreadcrumbsMessages(context.intl.formatMessage)
    }

    render = () => {
        const { entities } = this.props
        let routes = []
        let url = '/catalog'

        routes.push(
            renderLink({
                to: url,
                children: this._messages.breadcrumbsCatalog,
                key: "catalog",
                className: 'breadcrumb__link'
            })
        )

        if (entities.category.hasOwnProperty('id')) {
            url = `${url}/${entities.category.alias}`
            routes.push(separator(1))
            routes.push(
                renderLink({
                    to: url,
                    children: entities.category.name,
                    key: entities.category.id,
                    className: 'breadcrumb__link'
                })
            )
        }

        if (entities.version.hasOwnProperty('id')) {
            // /catalog/:slug/:alias
            url = `${url}/${entities.version.id}/${entities.version.card}`
            routes.push(separator(2))
            routes.push(
                renderLink({
                    to: url,
                    children: `${entities.model.name} - ${entities.version.name}`,
                    key: entities.version.id,
                    className: 'breadcrumb__link u-truncate'
                })
            )
        }

        return (
            <nav className="global__breadcrumb u-flex hidden-md-down">
                {routes}
            </nav>
        )
    }
}

Breadcrumbs.contextTypes = {
    intl: intlShape.isRequired
}

Breadcrumbs.propTypes = {
    entities: PropTypes.object.isRequired
}
Breadcrumbs.defaultProps = {}

export default Breadcrumbs
