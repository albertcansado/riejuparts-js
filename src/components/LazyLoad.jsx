import React, { Component, PropTypes } from 'react'
import { findDOMNode } from 'react-dom'
import inViewport from 'in-viewport'

const checkVisibility = (elem, options, callback) => {
    return inViewport(elem, options, callback)
}

class LazyLoad extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {
            visible: false
        }
    }

    componentDidMount = () => {
        checkVisibility(findDOMNode(this), {
            container: this.props.container
        }, () => {
            if (this.props.onVisible) {
                this.props.onVisible()
            }

            this.setState({
                visible: true
            })
        })
    }

    shouldComponentUpdate = () => !this.state.visible

    render = () => {
        return this.state.visible
            ? this.props.children
            : <div className="lazyload-placholder" style={{ height: this.props.height }} />
    }
}

LazyLoad.propTypes = {
    height: PropTypes.number,
    container: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.bool
    ]),
    onVisible: PropTypes.func
}

LazyLoad.defaultProps = {
    height: 0,
    container: false,
    children: null,
    onVisible: null
}

export default LazyLoad
