import React, { PropTypes } from 'react'
import cx from 'classnames'

import Spinner from './Spinner'

const Loading = ({ isLoading, className, children }) => {
    const _className = cx('loading', {
        'is-loading': isLoading
    }, className)

    if (isLoading) {
        return <div className={_className} children={<Spinner />} />
    } else {
        return {...children}
    }
}

Loading.propTypes = {
    isLoading: PropTypes.bool,
    className: PropTypes.string,
    children: PropTypes.node
}

Loading.defaultProps = {
    isLoading: false,
    className: ''
}

export default Loading
