import React, { PropTypes } from 'react'
import cx from 'classnames'

const Ribbon = ({ type, className, children }) => {
    return (
        <div className={cx('ribbon', {[`ribbon--${type}`]: type}, className)}>
            {children}
        </div>
    )
}

Ribbon.propTypes = {
    type: PropTypes.string,
    className: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.string
    ])
}
Ribbon.defaultProps = {
    type: null,
    className: {}
}

export default Ribbon
