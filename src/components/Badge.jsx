import React, { PropTypes } from 'react'
import cx from 'classnames'

const Badge = ({ type, className, children }) => {
    return (
        <span className={cx("badge", { [`badge--${type}`]: true}, className)}>
            {children}
        </span>
    )
}

Badge.propTypes = {
    type: PropTypes.string.isRequired,
    className: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ])
}

Badge.defaultProps = {
    className: ''
}

export default Badge
