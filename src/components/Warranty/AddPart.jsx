/* eslint-disable react/style-prop-object */
import React, { PureComponent, PropTypes } from 'react'
import { FormattedMessage, intlShape } from 'react-intl'
import { isFunction } from 'lodash'

import { getWarrantyAddPartMessages } from '../../lib/messages'

import Select from '../Form/Select'
import NumberInput from '../Form/NumberInput'
import SearchPart from '../Form/SearchPart'
import Icon from '../Icon'
import Link from '../Link'

const initState = {
    data: {
        qty: 0,
        cause: '',
        part: null,
        sendback: false
    },
    completed: false
}

class AddPart extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = initState

        // Cache Translate Messages
        this._messages = getWarrantyAddPartMessages(context.intl.formatMessage)

        /* Methods */
        this._handleSave = this._handleSave.bind(this)
        this._handleClear = this._handleClear.bind(this)
        this._setValue = this._setValue.bind(this)
    }

    _setValue = (field, value) => {

        const nextData = Object.assign({}, this.state.data, {
            [field]: value
        })
        const completed = nextData.qty > 0 && (!this.props.causeRequired || nextData.cause !== '')

        this.setState({
            data: nextData,
            completed
        })
    }

    _handleSave = () => {
        const { completed, data } = this.state
        const { onSubmit } = this.props

        if (!completed) {
            return
        }

        if (isFunction(onSubmit)) {
            onSubmit(data)
        }

        this._handleClear()
    }

    _handleClear = () => {
        this.setState(initState)
    }

    _filterParts = data => {
        const { lines } = this.props

        return data
            .filter(it =>
                lines.indexOf(it.sku) === -1
            )
            .map(it => {
                return {
                    ...it,
                    disabled: it.price === 0
                }
            })
    }

    _renderInMethods = () => {
        const { handleCatalog, handleCSV, allowCatalog } = this.props

        if (!allowCatalog) {
            return null
        }

        return (
            <div className="btn-group txt-right is-error">
                <Link btn type="reset" className="btn--small" onClick={handleCatalog}>
                    <Icon icon="layers" className="icon--14" />
                    <span className="btn__text">
                        <FormattedMessage id="warranty.addpart.catalog" defaultMessage="Use catalog" />
                    </span>
                </Link>
                <Link btn type="reset" className="btn--small" onClick={handleCSV}>
                    <Icon icon="file" className="icon--14" />
                    <span className="btn__text">
                        <FormattedMessage id="warranty.addpart.csv" defaultMessage="Upload a CSV" />
                    </span>
                </Link>
            </div>
        )
    }

    render = () => {
        const { completed, data } = this.state
        const { causes, url } = this.props

        return (
            <div className="warranty__partsadd u-mgt--20 u-mgb--20">
                <header className="warranty__partsadd-header u-flex flex--middle flex--between u-mgt--20">
                    <h5>
                        <FormattedMessage id="warranty.addpart.addline" defaultMessage="Add line" />
                    </h5>
                    {this._renderInMethods()}
                </header>
                <div className="table__row not-hover">
                    <div className="table__col table__col--1 txt--small">
                        <SearchPart
                            fetchUrl={url}
                            filterFn={this._filterParts}
                            showPrice
                            value={data.part}
                            onChange={value => this._setValue('part', value)}
                            messages={{
                                noResultsText: this._messages.warrantyPartNoResults,
                                placeholder: this._messages.warrantyPartPlaceholder,
                                loadingPlaceholder: this._messages.warrantyPartLoading,
                                searchPromptText: this._messages.warrantyPartSearchText
                            }}
                        />
                    </div>
                    <div className="table__col table__col--2">
                        <NumberInput
                            onChange={value => this._setValue('qty', value)}
                            value={data.qty}
                        />
                    </div>
                    <div className="table__col table__col--3">
                        <Select
                            options={causes}
                            labelKey="title"
                            valueKey="id"
                            empty={this._messages.warrantyCauseEmpty}
                            value={data.cause}
                            onChange={option => this._setValue('cause', option.id)}
                        />
                    </div>
                    <div className="table__col table__col--4"></div>
                    <div className="table__col table__col--options">
                        <div className="btn-group txt-center">
                            <Link btn type="reset" className={!completed ? 'is-disabled' : null} onClick={this._handleSave}>
                                <Icon icon="checked" className="icon--24 icon--success" />
                            </Link>
                            <Link btn type="reset" onClick={this._handleClear}>
                                <Icon icon="close" className="icon--18 icon--error" />
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

AddPart.contextTypes = {
    intl: intlShape.isRequired
}

AddPart.propTypes = {
    url: PropTypes.string.isRequired,
    causes: PropTypes.array.isRequired,
    lines: PropTypes.array.isRequired,
    onSubmit: PropTypes.func.isRequired,
    handleCatalog: PropTypes.func,
    handleCSV: PropTypes.func,
    allowCatalog: PropTypes.bool,
    causeRequired: PropTypes.bool,
    minLength: PropTypes.number
}
AddPart.defaultProps = {
    allowCatalog: false,
    causeRequired: false,
    minLength: 3
}

export default AddPart
