import React, { PropTypes } from 'react'

const Saving = ({ isSaving, messages }) => {
    if (isSaving) {
        return (
            <div className="warranty__saving">
                <span className="warranty__saving-spinner"></span>
                <span className="warranty__saving-text">{messages.warrantySaving}</span>
            </div>
        )
    }

    return (
        <div className="warranty__saving">
            <span className="warranty__saving-text txt-bold">{messages.warrantySaved}</span>
        </div>
    )
}

Saving.propTypes = {
    isSaving: PropTypes.bool,
    messages: PropTypes.object.isRequired
}
Saving.defaultProps = {
    isSaving: false
}

export default Saving
