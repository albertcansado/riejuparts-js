import React, { PureComponent, PropTypes } from 'react'
import { connect } from 'react-redux'
import { findDOMNode } from 'react-dom'
import { FormattedMessage } from 'react-intl'

import { getLinesParts } from '../../reducers/warranty/'

class CatalogHijacking extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this._body = null
        this._iframe = null

        this._setIframeHeight = this._setIframeHeight.bind(this)

        window._warrantyMode = true
        window._warranty = {
            actions: {
                add: props.handleAdd
            },
            items: props.linesBySku
        }
    }

    componentDidMount = () => {
        this._setIframeHeight()
        window.addEventListener('resize', this._setIframeHeight, true)
    }

    componentWillUnmount = () => {
        window.removeEventListener('resize', this._setIframeHeight, true)
        delete window._warrantyMode
        delete window._actions
    }

    _setIframeHeight = () => {
        if (this._body === null) {
            this._body = findDOMNode(this.refs.body)
        }
        if (this._iframe === null) {
            this._iframe = findDOMNode(this.refs.iframe)
        }

        this._iframe.height = this._body.offsetHeight + 'px'
    }

    render = () => {
        const { handleClose, linesBySku } = this.props
        const itemsCount = Object.keys(linesBySku).length

        return (
            <div className="modal-card modal--full">
                <header className="modal-card__head">
                    <h3 className="modal-card__title">
                        <FormattedMessage id="warranty.catalogHijacking.title" defaultMessage="Select parts from catalog" />
                    </h3>
                    <span className="close close--dark" onClick={handleClose}></span>
                </header>
                <section className="modal-card__body u-p--0" ref="body">
                    <iframe src="/catalog?full=1" width="100%" height="100%" ref="iframe"></iframe>
                </section>
                <footer className="modal-card__foot bg--white">
                    <h4>{itemsCount} {itemsCount === 1
                        ? <FormattedMessage id="warranty.catalogHijacking.product" defaultMessage="product" />
                        : <FormattedMessage id="warranty.catalogHijacking.products" defaultMessage="products" />
                    }</h4>
                </footer>
            </div>
        )
    }
}

CatalogHijacking.propTypes = {}
CatalogHijacking.defaultProps = {}

const mapStateToProps = (state, ownProps) => {
    return {
        linesBySku: getLinesParts(state, ownProps.id)
    }
}

export default connect(
    mapStateToProps,
    {}
)(CatalogHijacking)
