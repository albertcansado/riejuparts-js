import React from 'react'
import cx from 'classnames'

const WarrantyPre = ({ type, transport, pre_km0, onSelect, messages }) => {
    // Garantia skip
    if (type) {
        return null
    }

    return (
        <section className="warranty__section warranty__section--pre">
            <h4 className="warranty__section-title">{messages.warrantyPreWarrantyTitle}</h4>
            <div className="row u-mgt--15">
                <div className="col-xs-12 col-sm-6">
                    <div className="prewarranty u-flex u-h100 flex--between">
                        <div className="warranty__section-info">
                            {messages.warrantyTransportInfo}
                        </div>
                        <div
                            className={cx("prewarranty__btn btn btn--block u-mgt--15", {
                                'btn--notice is-current': transport
                            })}
                            onClick={transport ? null : ev => onSelect({ transport: 1, pre_km0: 0 })}
                        >
                            {messages.warrantyTransportTitle}
                        </div>
                    </div>
                </div>
                <div className="col-xs-12 col-sm-6">
                    <div className="prewarranty u-flex u-h100 flex--between">
                        <div className="warranty__section-info">
                            {messages.warrantyKm0Info}
                        </div>
                        <div
                            className={cx("prewarranty__btn btn btn--block u-mgt--15", {
                                'btn--notice is-current': pre_km0
                            })}
                            onClick={pre_km0 ? null : ev => onSelect({ transport: 0, pre_km0: 1 })}
                        >
                            {messages.warrantyKm0Title}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}


export default WarrantyPre
