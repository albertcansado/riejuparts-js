import React, { PureComponent, PropTypes } from 'react'
import { FormattedMessage } from 'react-intl'
import Dropzone from 'react-dropzone'
import cx from 'classnames'

import Attachment from './Attachment'

const checkLimit = (files, limit) => {
    return limit === -1 || files.length < limit
}

class WarrantyAttachmentsList extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            isUploading: false,
            allowUploads: checkLimit(props.attachments, props.limit)
        }
    }

    componentWillReceiveProps = nextProps => {
        if (this.props.attachments.length !== nextProps.attachments.length) {
            this.setState({
                allowUploads: checkLimit(nextProps.attachments, nextProps.limit)
            })
        }
    }

    /**
     * Agaramos tantos files como limite - attachment.length tenemos
     */
    _preProcessFiles = files => {
        const { limit, attachments } = this.props

        if (limit > 0) {
            files = files.slice(0, limit - attachments.length)
        }

        let randomId = Math.floor(new Date().valueOf() + Math.random())
        return files.map(file => {
            file.id = ++randomId
            file.isUploading = true

            return file
        })
    }

    onDrop = files => {
        const { allowUploads } = this.state

        if (!allowUploads) {
            return
        }

        this.setState({
            isUploading: true
        })

        this.props.handleUpload(
            this._preProcessFiles(files)
        ).then(() => {
            this.setState({
                isUploading: false
            })
        })
    }

    _renderFiles = () => {
        const { attachments } = this.props

        return attachments.map((file, key) => {
            return (
                <div className="col-xs-6 col-lg-4 col-xl-2" key={key}>
                    <Attachment file={file} handleDelete={this.props.handleDelete} />
                </div>
            )
        })
    }

    _renderDropzone = () => {
        const { name, accept } = this.props
        const { isUploading } = this.state

        return (
            <div className="col-xs" key="FILE_ADD">
                <Dropzone
                    ref="dropzone"
                    accept={accept}
                    multiple={true}
                    onDrop={this.onDrop}
                    style={null}
                    name={name}
                    disableClick={isUploading}
                    disabled={isUploading}
                    className="uploader__container u-flex u-h100"
                >
                    <div className={cx("file flex--centerfull file--add txt-bold", {'is-selectable': !isUploading})}>
                        <FormattedMessage id="warranty.attachments.upload" defaultMessage="Upload an image" />
                    </div>
                </Dropzone>
            </div>
        )
    }

    render = () => {
        const { allowUploads } = this.state

        return (
            <section className="warranty__section warranty__section--attachments">
                <h4 className="warranty__section-title">
                    <FormattedMessage id="warranty.attachments.title" defaultMessage="Add attachments" /> <span className="u-optional txt-normal"><FormattedMessage id="app.optional" defaultMessage="(Optional)" /></span>
                </h4>
                <div className="warranty__section-info u-mgt--15">
                    <p>
                        <FormattedMessage id="warranty.attachments.text" defaultMessage="Puedes adjuntar hasta {num} imagenes del problema a la garantia" values={{num: 6}} />
                    </p>
                </div>
                <div className="row u-mgt--10 files">
                    {this._renderFiles()}
                    {allowUploads ? this._renderDropzone() : null}
                </div>
            </section>
        )
    }
}

WarrantyAttachmentsList.propTypes = {
    attachments: PropTypes.array.isRequired,
    handleUpload: PropTypes.func.isRequired,
    handleDelete: PropTypes.func,
    limit: PropTypes.number,
    accept: PropTypes.string
}
WarrantyAttachmentsList.defaultProps = {
    handleDelete: null,
    limit: -1,
    accept: 'image/jpg, image/png, image/jpeg'
}

export default WarrantyAttachmentsList
