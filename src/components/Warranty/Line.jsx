import React from 'react'
import PropTypes from 'prop-types'
import idx from 'idx'

import Link from '../Link'
import Icon from '../Icon'
import Select from '../Form/Select'

const Line = ({ line, causes, handleDelete, handleUpdate, messages }) => {
    return (
        <article className="table__row">
            <div className="table__col table__col--1">
                <p><strong>{idx(line, _ => _.part.sku)}</strong></p>
                <p className="txt--small">{idx(line, _ => _.part.name)}</p>
            </div>
            <div className="table__col table__col--2 txt-center">
                {idx(line, _ => _.qty)}
            </div>
            <div className="table__col table__col--3">
                <Select
                    options={causes}
                    labelKey="title"
                    valueKey="id"
                    empty={messages.warrantyCauseEmpty}
                    value={idx(line, _ => _.cause.id)}
                    onChange={option => handleUpdate(line, {cause: option.id})}
                />
            </div>
            <div className="table__col table__col--4 txt-center txt-bold">
                { !!idx(line, _ => _.part.ship)
                    ? <span className="is-success">{messages.warrantyPartsLineYes}</span>
                    : <span className="is-error">{messages.warrantyPartsLineNo}</span>
                }
            </div>
            <div className="table__col table__col--options">
                <Link btn type="reset" className="w-part__btn" onClick={() => handleDelete(line)}>
                    <Icon icon="trash" className="icon--error icon--18" />
                </Link>
            </div>
        </article>
    )
}

Line.propTypes = {
    line: PropTypes.object.isRequired,
    handleDelete: PropTypes.func.isRequired
}

export default Line
