import React, { PureComponent, PropTypes } from 'react'
import { FormattedMessage, FormattedDate, intlShape } from 'react-intl'
import idx from 'idx'

import { getWarrantyInfoMessages } from '../../lib/messages'

import Input from '../../components/Form/Input'
import Textarea from '../../components/Form/Textarea'

class Header extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {}

        // Cache Translate Messages
        this._messages = getWarrantyInfoMessages(context.intl.formatMessage)
    }

    render = () => {
        const { warranty, onChangeValue } = this.props

        let kmInputProps = {
            placeholder: "Km"
        }

        // Pregarantia
        if (!warranty.type) {
            kmInputProps['readOnly'] = 'readOnly'
        }

        const soldDate = idx(warranty, _ => _.frame.sold)

        return (
            <header className="warranty__header">
                <div className="row">
                    <div className="col-xs-12 col-xl-7">
                        <div className="row">
                            <div className="col-xs-12 col-md-7">
                                <h4 className="warranty__header-title">
                                    <FormattedMessage id="warranty.info.vehicle" defaultMessage="Vehicle" />
                                </h4>

                                <div className="warranty__model">
                                    <span className="warranty__model-label u-block txt-bold">
                                        <FormattedMessage id="warranty.info.name" defaultMessage="Name" />:
                                    </span>
                                    <span className="warranty__model-text">{idx(warranty, _ => _.frame.model.name)}</span>
                                </div>

                                <div className="row u-mgt--15">
                                    <div className="col-xs-12 col-md-6">
                                        <div className="warranty__model">
                                            <span className="warranty__model-label u-block txt-bold">
                                                <FormattedMessage id="warranty.info.frame" defaultMessage="Vehicle frame" />:
                                            </span>
                                            <span className="warranty__model-text">{idx(warranty, _ => _.frame.number)}</span>
                                        </div>
                                        <div className="warranty__model">
                                            <span className="warranty__model-label u-block txt-bold">
                                                <FormattedMessage id="warranty.info.engine" defaultMessage="Engine" />:
                                            </span>
                                            <span className="warranty__model-text">
                                                {idx(warranty, _ => _.frame.engine)}
                                            </span>
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <div className="warranty__model">
                                            <span className="warranty__model-label u-block txt-bold">
                                                <FormattedMessage id="warranty.info.soldDate" defaultMessage="Sold date" />:
                                            </span>
                                            <span className="warranty__model-text">
                                                {soldDate
                                                    ? <FormattedDate value={soldDate} />
                                                    : 'Not yet'
                                                }
                                            </span>
                                        </div>
                                        <Input
                                            type="text"
                                            inputProps={kmInputProps}
                                            ignoreZero
                                            onBlur={value => onChangeValue('km', value)}
                                            value={!warranty.type ? "0" : warranty.km}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-12 col-md-5">
                                <h4 className="warranty__header-title">
                                    <FormattedMessage id="warranty.info.mechanic" defaultMessage="Mechanic" /> <span className="u-optional txt-normal"><FormattedMessage id="app.opcional" defaultMessage="(Optional)" /></span>
                                </h4>
                                <Input
                                    type="text"
                                    inputProps={{
                                        placeholder: this._messages.warrantyMechanicName
                                    }}
                                    onBlur={value => onChangeValue('mechanic', value)}
                                    value={warranty.mechanic}
                                />
                                <Input
                                    type="text"
                                    inputProps={{
                                        placeholder: this._messages.warrantyMechanicPhone
                                    }}
                                    onBlur={value => onChangeValue('phone', value)}
                                    value={warranty.phone}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-12 col-xl-5">
                        <div className="warranty__comments">
                            <h4 className="warranty__header-title">
                                <FormattedMessage id="warranty.info.comments" defaultMessage="Comments" />
                            </h4>
                            <Textarea
                                inputProps={{
                                    placeholder: this._messages.warrantyCommentsPlaceholder
                                }}
                                value={warranty.comment_dealer}
                                onBlur={value => onChangeValue('comment_dealer', value)}
                            />
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}

Header.contextTypes = {
    intl: intlShape.isRequired
}

Header.propTypes = {
    warranty: PropTypes.object.isRequired,
    onChangeValue: PropTypes.func.isRequired
}
Header.defaultProps = {}

export default Header
