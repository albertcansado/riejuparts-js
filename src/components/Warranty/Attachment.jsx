import React, { PropTypes } from 'react'
import FileSize from 'file-size'

import Icon from '../Icon'

const renderTrashOrUploading = (file, handleDelete, trashIcon) => {
    if (file.hasOwnProperty('isUploading') && file.isUploading) {
        return <span className="file__btn file__btn--loader" />
    }

    if (!handleDelete) {
        return null
    }

    return (
        <div className="file__btn file__btn--trash is-selectable" onClick={() => handleDelete(file)}>
            <Icon icon={trashIcon ? "trash" : "close"} className={trashIcon ? "icon--20 is-error" : "icon--12 is-error"} />
        </div>
    )
}

const getFilename = file => {
    const name = ('name' in file)
        ? file.name
        : file.hasOwnProperty('attachment') && file.attachment !== null ? file.attachment : ''

    return name.slice(name.lastIndexOf('/') + 1)
}

const getFileIcon = type => {
    if (~type.indexOf('pdf')) {
        return 'file'
    } else {
        return 'picture'
    }
}

const Attachment = ({ file, handleDelete, withLink, trashIcon }) => {
    const name = getFilename(file)

    let rootProps = {
        className: 'file'
    }

    if (withLink) {
        rootProps['href'] = window.config.ASSETSURL + '/files/' + file.attachment
        rootProps['target'] = "_blank"
    }

    const RootElement = withLink ? 'a' : 'div'

    return (
        <RootElement {...rootProps}>
            <div className="file__inner u-flex flex--middle u-owhidden">
                <Icon icon={getFileIcon(file.type)} className="file__icon icon--24" />
                <div className="txt--small u-owhidden">
                    <p className="u-truncate txt-bold" title={name}>{name}</p>
                    {Number.isNaN(file.size) ? null : <p className="uploader__file-size txt--small" children={FileSize(Number.parseInt(file.size, 10)).human('si')} />}
                </div>
            </div>
            {renderTrashOrUploading(file, handleDelete, trashIcon)}
        </RootElement>
    )
}

Attachment.propTypes = {
    file: PropTypes.object.isRequired,
    handleDelete: PropTypes.func,
    withLink: PropTypes.bool,
    trashIcon: PropTypes.bool
}

Attachment.defaultProps = {
    withLink: false,
    trashIcon: false
}

export default Attachment
