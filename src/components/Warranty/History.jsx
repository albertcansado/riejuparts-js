import React from 'react'
import { FormattedDate } from 'react-intl'
import idx from 'idx'

const History = ({ history, messages }) => {
    return (
        <article className="history">
            <header className="history__header u-flex flex--between">
                <div>
                    <span className="history__header-date txt-bold">
                        {history.sended
                            ? <FormattedDate value={history.sended} />
                            : messages.notYet
                        }
                    </span>
                    <span className="history__header-dealer u-mgl--5">
                        {idx(history, _ => _.dealer.name)}
                    </span>
                </div>
                <span className="history__header-km">
                    {history.type === 1
                        ? messages.preWarranty
                        : history.km + 'Km'
                    }
                </span>
            </header>
            <section className="history__lines">
                {history.lines.map(line => {
                    return (
                        <article className="history__line" key={line.id}>
                            <div className="row">
                                <div className="col-xs-12 col-md-8">
                                    <div className="txt-bold">
                                        {idx(line, _ => _.part.sku)}
                                    </div>
                                    <div className="txt--small">
                                        {idx(line, _ => _.part.name)}
                                    </div>
                                </div>
                                <div className="col-xs-12 col-md-4 txt-right">
                                    {idx(line, _ => _.cause.title)}
                                </div>
                            </div>
                        </article>
                    )
                })}
            </section>
        </article>
    )
}

export default History
