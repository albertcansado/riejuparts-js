import React, { PureComponent, PropTypes } from 'react'
import { connect } from 'react-redux'
import { FormattedMessage, intlShape } from 'react-intl'
import swal from 'sweetalert2'

import { getWarrantyPartsMessages } from '../../lib/messages'

import { getCauses, getLines } from '../../reducers/warranty/'

import AddPart from './AddPart'
import Line from './Line'

class WarrantyParts extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {}

        // Cache Translate Messages
        this._messages = getWarrantyPartsMessages(context.intl.formatMessage)

        this._deleteLine = this._deleteLine.bind(this)
    }

    _renderEmptyRow = () => {
        return (
            <article className="table__row not-hover">
                <div className="table__col txt-center txt-bold">
                    <FormattedMessage id="warranty.parts.noresults" defaultMessage="No has parts yet" />
                </div>
            </article>
        )
    }

    _deleteLine = (id) => {
        const { onDeleteLine } = this.props
        swal({
            title: this._messages.warrantyDeleteLineTitle,
            // text: this._messages.cartListDeleteText,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: this._messages.warrantyDeleteLineConfirm
        }).then (() => {
            return onDeleteLine(id)
        }).catch(swal.noop)
    }

    render = () => {
        const { causes, lines, onCreateLine, onUpdateLine, handleCatalog, handleCSV } = this.props
        return (
            <section className="warranty__section warranty__section--parts">
                <h4 className="warranty__section-title">
                    <FormattedMessage id="warranty.parts.partsSection.title" defaultMessage="Parts involved in warranty" />
                </h4>
                <div className="warranty__section-info u-mgt--15">
                    <FormattedMessage
                        id="warranty.parts.partsSection.info"
                        defaultMessage="Añade las {strong} (minimo 1). Puedes añadir las piezas usando el buscador (mediante la referencia) o pulsar en {catalog} y añadirlas directamente desde las laminas"
                        values={{
                            strong: <strong>{this._messages.warrantyPartsInfoParts}</strong>,
                            catalog: <i>{this._messages.warrantyPartsInfoCatalog}</i>
                        }}
                    />
                </div>
                <section className="table table--wparts u-mgt--15">
                    <AddPart
                        url="/parts/lookup.json?s={input}&l=2"
                        causes={causes}
                        onSubmit={onCreateLine}
                        allowCatalog
                        handleCatalog={handleCatalog}
                        handleCSV={handleCSV}
                        lines={lines.map(line => line.part.sku)}
                        causeRequired={!lines.length}
                    />
                    <header className="warranty__partsadd-header u-flex flex--middle flex--between u-mgt--20 u-mgb--15">
                        <h5>
                            <FormattedMessage id="warranty.parts.list.title" defaultMessage="Parts" />
                        </h5>
                    </header>
                    <header className="table__head">
                        <div className="table__col table__col--1">
                            <FormattedMessage id="warranty.parts.list.reference" defaultMessage="Ref." />
                        </div>
                        <div className="table__col table__col--2 txt-center">
                            <FormattedMessage id="warranty.parts.list.units" defaultMessage="Units" />
                        </div>
                        <div className="table__col table__col--3">
                            <FormattedMessage id="warranty.parts.list.cause" defaultMessage="Cause" />
                        </div>
                        <div className="table__col table__col--4 txt-center">
                            <FormattedMessage id="warranty.parts.list.giveback" defaultMessage="Give back" />
                        </div>
                        <div className="table__col table__col--options"></div>
                    </header>
                    <div className="table__body">
                        {lines.length
                            ? lines.map(line => <Line
                                    line={line}
                                    key={line.id}
                                    causes={causes}
                                    messages={this._messages}
                                    handleDelete={this._deleteLine}
                                    handleUpdate={onUpdateLine} />
                            )
                            : this._renderEmptyRow()
                        }
                    </div>
                </section>
            </section>
        )
    }
}

WarrantyParts.contextTypes = {
    intl: intlShape.isRequired
}

WarrantyParts.propTypes = {
    id: PropTypes.string.isRequired,
    onCreateLine: PropTypes.func.isRequired,
    onUpdateLine: PropTypes.func.isRequired,
    onDeleteLine: PropTypes.func.isRequired,
    handleCatalog: PropTypes.func.isRequired
}
WarrantyParts.defaultProps = {}

const mapStateToProps = (state, ownProps) => {
    return {
        causes: getCauses(state),
        lines: getLines(state, ownProps.id)
    }
}

export default connect(
    mapStateToProps,
    {}
)(WarrantyParts)
