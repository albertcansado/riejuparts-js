import React, { PropTypes } from 'react'
import cx from 'classnames'

import Link from '../Link'
import Icon from '../Icon'

const HistoryBtn = ({ count, handleClick }) => {
    return (
        <Link btn type="reset" className={cx("warranty__history", {"is-disabled": !count})} onClick={count > 0 ? handleClick : null}>
            <span className="warranty__history-badge badge badge--error">{count}</span>
            <Icon icon="history" className="icon--22" />
        </Link>
    )
}

HistoryBtn.propTypes = {
    count: PropTypes.number,
    handleClick: PropTypes.func
}

HistoryBtn.defaultProps = {
    count: 0,
    handleClick: null
}

export default HistoryBtn
