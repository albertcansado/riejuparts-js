import React, { PropTypes } from 'react'

import { API_ROOT } from '../../middleware/api'

import UploaderCSV from '../Form/UploaderCSV'

const CsvHijacking = ({ handleClose, handleSubmit, title }) => {
    return (
        <div className="modal-card">
            <header className="modal-card__head">
                <h4 className="modal-card__title">
                    {title}
                </h4>
                <span className="close close--dark" onClick={handleClose}></span>
            </header>
            <section className="modal-card__body">
                <UploaderCSV onCancel={handleClose} onSubmit={handleSubmit} exampleUrl={`${API_ROOT}/files/example_csv.csv`} />
            </section>
            <footer className="modal-card__foot"></footer>
        </div>
    )
}

CsvHijacking.propTypes = {
    title: PropTypes.string,
    handleClose: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired
}

export default CsvHijacking
