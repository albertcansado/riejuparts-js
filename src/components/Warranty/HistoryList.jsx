import React, { PureComponent, PropTypes } from 'react'
import { FormattedMessage } from 'react-intl'
import { map as _map, size as _size } from 'lodash'

import History from './History'

class HistoryList extends PureComponent {
    _noRecords = () => {
        return <h3
            className="txt-center u-mgt--10 u-mgb--10"
            children={<FormattedMessage id="warranty.history.noresults" defaultMessage="The vehicle frame no has records yet" />} />
    }

    render = () => {
        const { histories, handleClose, messages } = this.props

        return (
            <div className="modal-card">
                <header className="modal-card__head">
                    <h2 className="modal-card__title">
                        <FormattedMessage id="warranty.history.history" defaultMessage="History" />
                    </h2>
                    <span className="close close--dark" onClick={handleClose}></span>
                </header>
                <section className="modal-card__body">
                    {_size(histories)
                        ? _map(histories, history => <History history={history} key={history.id} messages={messages} />)
                        : this._noRecords()
                    }
                </section>
                <footer className="modal-card__foot"></footer>
            </div>
        )
    }
}

HistoryList.propTypes = {
    histories: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]).isRequired,
    handleClose: PropTypes.func.isRequired,
    messages: PropTypes.object.isRequired
}
HistoryList.defaultProps = {}

export default HistoryList
