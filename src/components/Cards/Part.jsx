/* eslint-disable react/style-prop-object */
import React, { Component, PropTypes } from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import { shallowCompareWithoutFunctions } from 'shallow-compare-without-functions'
import { FormattedNumber, FormattedMessage, intlShape } from 'react-intl'
import cx from 'classnames'

import { getAvailabilityMessages } from '../../lib/messages'

import Icon from '../Icon'
import Link from '../Link'
import Availability from '../Availability'
import NumberInput from '../Form/NumberInput'

class Part extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {
            isOpen: false,
            isInsideCart: props.cartItem.qty > 0,
            qty: props.cartItem.qty
        }

        this._handleOpen = this._handleOpen.bind(this)
        this._handleChange = this._handleChange.bind(this)

        // Cache Translate Messages
        this._messages = getAvailabilityMessages(context.intl.formatMessage)
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        return shallowCompareWithoutFunctions(this, nextProps, nextState)
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.cartItem.qty !== this.state.qty) {
            this.setState({
                isInsideCart: nextProps.cartItem.qty > 0,
                qty: nextProps.cartItem.qty
            })
        }
    }

    _handleChange = () => {
        const { handleAddToCart, handleUpdateCart, item, cartItem } = this.props
        const { qty } = this.state
        const updatedQty = this.refs.qty.getValue()

        if (!qty) {
            // Add
            if (!updatedQty) {
                return
            }

            handleAddToCart(item, updatedQty)
        } else {
            // Update
            if (updatedQty === qty) {
                return
            }

            handleUpdateCart(
                Object.assign({}, cartItem, {
                    name: item.name,
                    part: item.part,
                    part_id: item.part.id
                }),
                updatedQty
            )
        }

        this.setState({
            isInsideCart: updatedQty !== 0,
            isOpen: false,
            qty: updatedQty
        })
    }

    _handleOpen = (open) => {
        this.setState({
            isOpen: open
        })
    }

    _canTriggerBack = () => {
        const { isInsideCart } = this.state
        const { item } = this.props

        return !(item.disabled || (parent._warrantyMode && isInsideCart))
    }

    _renderPartContent = () => {
        const { item } = this.props

        if (item.disabled) {
            return null
        }

        const { part } = item

        return (
            <section className="part__content u-flex">
                <div className="part__qty">
                    <FormattedMessage id="part.units" defaultMessage="Units" /> {item.qty}
                </div>
                <Availability
                    availability={part.availability}
                    disable={item.disabled}
                    messages={this._messages}
                    theme={{
                        container: 'part__availability',
                        text: 'hidden-lg-down'
                    }}
                    tooltip
                    showLabel
                />
                <div className="part__price txt-bold">
                    {/* eslint-disable-next-line react/style-prop-object */}
                    <FormattedNumber
                        value={part.price}
                        minimumFractionDigits={2}
                        maximumFractionDigits={2}
                        currency="EUR"
                        currencyDisplay="symbol"
                        style="currency" />
                </div>
            </section>
        )
    }

    _renderFront = () => {
        const { item } = this.props
        const { part } = item
        const _frontClass = cx('part__front', {
            'is-selectable': this._canTriggerBack()
        })
        return (
            <div className={_frontClass} onClick={this._canTriggerBack() ? () => this._handleOpen(true) : null} key="front">
                <header className="part__header">
                    <span className="part__name txt-bold">{item.name}</span>
                    <span className="part__sku">{part.sku !== -1 ? part.sku : null}</span>
                </header>
                {this._renderPartContent()}
                {!item.disabled && item.info ? <footer className="part__footer">{item.info}</footer> : null}
            </div>
        )
    }

    _renderBack = () => {
        const { isInsideCart, qty } = this.state

        // If we call from warranty not showing it
        if (parent._warrantyMode && isInsideCart) {
            return null
        }

        return (
            <div className="part__back" key="back">
                <div className="part__back-content u-flex">
                    <div className="part__close is-selectable" onClick={() => this._handleOpen(false)}>
                        <Icon icon="left-arrow" className="icon--18" />
                        <span className="part__close-text">
                            <FormattedMessage id="part.back" defaultMessage="Back" />
                        </span>
                    </div>
                    <section className="part__content u-flex">
                        <NumberInput
                            ref="qty"
                            defaultValue={this.props.cartQty}
                            value={qty}
                            min={1} />
                        <Link btn type={isInsideCart ? "warning" : "success"} className="part__back-addtocart" onClick={this._handleChange}>
                            <Icon icon={isInsideCart ? "spin" : "cart"} className="icon--20" />
                        </Link>
                    </section>
                </div>
            </div>
        )
    }

    render = () => {
        const { item } = this.props

        const _partClass = cx('part', {
            'is-disabled': item.disabled,
            'is-inside': this.state.isInsideCart
        })
        const _innerClass = cx('part__inner', {
            // 'is-moving': this.state.isOpen
        })

        return (
            <article className={_partClass}>
                <ReactCSSTransitionGroup
                    transitionName="moving"
                    transitionEnterTimeout={500}
                    transitionLeaveTimeout={500}
                    className="part__inner"
                >
                    {item.disabled || !this.state.isOpen
                        ? this._renderFront()
                        : this._renderBack()
                    }
                </ReactCSSTransitionGroup>
            </article>
        )
    }
}

Part.contextTypes = {
    intl: intlShape.isRequired
}

Part.propTypes = {
    item: PropTypes.object.isRequired,
    cartItem: PropTypes.object.isRequired,
    handleAddToCart: PropTypes.func.isRequired,
    handleUpdateCart: PropTypes.func.isRequired
}

Part.defaultProps = {}

export default Part
