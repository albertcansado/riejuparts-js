import React, { Component, PropTypes } from 'react'
import { FormattedMessage } from 'react-intl'
import ReactTooltip from 'react-tooltip'
import idx from 'idx'

import { addToCart, updateCartLine } from '../../actions/carts'

import Part from './Part'

class PartsList extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {}

        this._addToCart = this._addToCart.bind(this)
        this._updateCart = this._updateCart.bind(this)
    }

    _addToCart = (line, qty) => {
        if (idx(parent, _ => _._warranty.actions.add)) {
            // Warranty
            parent._warranty.actions.add({
                part: {
                    ...line.part,
                    name: line.name
                },
                cause: null,
                qty: qty
            })
        } else {
            const { dispatch } = this.props
            dispatch(
                addToCart(line, qty)
            )
        }
    }

    _updateCart = (cartLineId, qty, nextQty) => {
        if (!parent._warrantyMode) {
            const { dispatch } = this.props
            dispatch(
                updateCartLine(cartLineId, qty, nextQty)
            )
        }
    }

    _renderLine = (line) => {
        const { cartItems } = this.props
        const { items } = line
        return (
            <div className="cardline" key={line.number}>
                <div className="cardline__number">
                    {line.number}
                </div>
                <div className="cardline__items">
                    {items.map((item, index) => {
                        // const isInsideCart = cartItems.hasOwnProperty(item.part.sku)
                        const cart = Object.assign({}, {
                            id: null,
                            qty: 0
                        }, cartItems[item.part.sku])
                        return <Part item={item} handleAddToCart={this._addToCart} handleUpdateCart={this._updateCart} cartItem={cart} key={`line-${index}`} />
                    })}
                </div>
                <ReactTooltip />
            </div>
        )
    }

    render = () => {
        const { items } = this.props
        return (
            <div className="parts">
                {!items.length
                    ? <div className="no-results txt-bold txt-center" key="NO_RESULTS"><FormattedMessage id="part.no_results" defaultMessage="There are not results" /></div>
                    : items.map(line => {
                        return this._renderLine(line)
                    })
                }
            </div>
        )
    }
}

PartsList.propTypes = {
    items: PropTypes.array,
    cartItems: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
}

PartsList.defaultProps = {
    items: []
}

export default PartsList
