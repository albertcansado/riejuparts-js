import React, { PropTypes } from 'react'
import classnames from 'classnames'

const Link = ({ children, className, btn, type, ...others }) => {
    const _className = classnames({
        "btn": btn,
        [`btn--${type}`]: (btn && type !== '')
    }, className)

    const elementType = others.hasOwnProperty('href') ? 'a' : 'span'
    return React.createElement(
        elementType,
        {className: _className, ...others},
        children
    )
}

Link.propTypes = {
    btn: PropTypes.bool,
    type: PropTypes.string,
    children: PropTypes.node,
    className: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ])
}

Link.defaultProps = {
    btn: false,
    type: '',
    className: ''
}

export default Link
