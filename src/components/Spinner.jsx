import React, { PropTypes } from 'react'
import classNames from 'classnames'

const Spinner = ({ className }) => {
    const spinnerClass = classNames('spinner', className)
    return (
        <div className={spinnerClass}>
            <div className="spinner__bounce spinner__bounce--1" />
            <div className="spinner__bounce spinner__bounce--2" />
        </div>
    )
}

Spinner.propTypes = {
    className: PropTypes.string
}

Spinner.defaultProps = {
    className: ''
}

export default Spinner
