import React, { PropTypes } from 'react'
import cx from 'classnames'

const Saving = ({ isSaving, isNotSaved, isSpecial, styles, messages }) => {
    let text = ''
    let className = null
    let spinner = null

    if (isSaving) {
        text = messages.saving
        className = styles.saving
        spinner = <span className="saving__spinner"></span>
    } else if (isSpecial) {
        text = messages.special
        className = styles.speacial
    } else if (isNotSaved) {
        text = messages.notSaved
        className = styles.notSaved
    } else {
        text = messages.saved
        className = styles.saved
    }

    return (
        <div className={cx("saving", styles.root, className)}>
            {spinner}
            <span className={cx("saving__text", styles.text)}>
                {text}
            </span>
        </div>
    )
}

Saving.propTypes = {
    isSaving: PropTypes.bool,
    isSpecial: PropTypes.bool,
    messages: PropTypes.object.isRequired,
    styles: PropTypes.object
}
Saving.defaultProps = {
    isSaving: false,
    isSpecial: false,
    styles: {}
}

export default Saving
