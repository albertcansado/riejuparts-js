/* eslint-disable react/style-prop-object */
import React, { PureComponent, PropTypes } from 'react'
import { FormattedMessage, FormattedNumber } from 'react-intl'
import cx from 'classnames'
import idx from 'idx'

import Image from '../Image'
import Link from '../Link'
import Ribbon from '../Ribbon'
import ProductOptions from './ProductOptions'
import ProductOption from './ProductOption'

class Product extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            isSaving: false
        }

        this.showOptions = this.showOptions.bind(this)
        this._handleAdd = this._handleAdd.bind(this)
    }

    showOptions = () => {
        this.props.openModal({
            component: ProductOptions,
            props: {
                parts: this.props.product.parts,
                cartProducts: this.props.cartProducts,
                handleSubmit: this.props.handleAddToCart
            }
        })
    }

    _handleAdd = (part, qty) => {
        this.setState({
            isSaving: true
        })

        this.props.handleAddToCart(part, qty)
            .then(() => {
                this.setState({
                    isSaving: false
                })
            })
    }

    _renderSimpleOption = (part) => {
        const { isSaving } = this.state

        const partInsideCart = idx(this.props.cartProducts, _ => _[part.sku])
        return <ProductOption
                className="card__footer-inner u-flex u-w100"
                part={part}
                handleSubmit={this._handleAdd}
                insideCart={partInsideCart}
                isLoading={isSaving} />
    }

    _renderMultipleOptions = () => {
        return (
            <Link className="card__footer-inner u-flex flex--centerfull u-w100 is-selectable" onClick={this.showOptions}>
                <strong>
                    <FormattedMessage id="product.select_options" defaultMessage="Click to select an option" />
                </strong>
            </Link>
        )
    }

    _renderSubtitle = (isSimple = false) => {
        if (!isSimple) {
            return '...'
        }

        const { product } = this.props

        return (
            <div className="u-flex flex--between">
                <span>{product.parts[0].sku}</span>
                <strong>
                    <FormattedNumber
                        value={product.parts[0].price}
                        minimumFractionDigits={2}
                        maximumFractionDigits={2}
                        currency="EUR"
                        currencyDisplay="symbol"
                        style="currency" />
                </strong>
            </div>
        )
    }

    render = () => {
        const { product } = this.props
        const isSimple = product.parts.length === 1

        return (
            <div className="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <article className={cx('card product', {'product--simple': isSimple})}>
                    <header className="card__picture u-p--10">
                        <Image src={product.image} className="card__image" />
                        {product.featured ? <Ribbon type="notice" className="card__ribbon"><FormattedMessage id="product.featured" defaultMessage="Featured" /></Ribbon> : null}
                    </header>
                    <div className="card__content u-p--10">
                        <h4 className="card__title">{product.name}</h4>
                        {this._renderSubtitle(isSimple)}
                    </div>
                    <footer className="card__footer u-p--10">
                        {product.parts.length === 1
                            ? this._renderSimpleOption(product.parts[0])
                            : this._renderMultipleOptions()
                        }
                    </footer>
                </article>
            </div>
        )
    }
}

Product.propTypes = {
    product: PropTypes.object.isRequired,
    handleAddToCart: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired
}
Product.defaultProps = {}

export default Product
