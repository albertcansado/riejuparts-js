import React, { PureComponent, PropTypes } from 'react'
import cx from 'classnames'

import Link from '../Link'
import Icon from '../Icon'
import NumberInput from '../Form/NumberInput'

class ProductOption extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this._handleClick = this._handleClick.bind(this)
    }

    _handleClick = ev => {
        const { part, handleSubmit, beforeSubmit } = this.props

        if (beforeSubmit) {
            beforeSubmit()
        }

        handleSubmit(part, this.refs.input.getValue())
    }

    render = () => {
        const { insideCart, className, isLoading } = this.props
        return (
            <div className={className}>
                <NumberInput ref="input" defaultValue={1} value={insideCart ? insideCart.qty : 1} />
                <Link btn
                    type={insideCart ? 'warning' : 'success'}
                    className={cx("product__addtocart u-f1 u-mgl--10", {'is-loading': isLoading})}
                    onClick={this._handleClick}
                >
                    <Icon icon={insideCart ? 'spin' : 'cart'} className="icon--20" />
                </Link>
            </div>
        )
    }
}

ProductOption.propTypes = {
    part: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    insideCart: PropTypes.object,
    className: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.string
    ]),
    isLoading: PropTypes.bool,
    beforeSubmit: PropTypes.func
}
ProductOption.defaultProps = {
    insideCart: null,
    className: '',
    isLoading: false,
    beforeSubmit: null
}

export default ProductOption
