import React, { PureComponent, PropTypes } from 'react'
import Product from './Product'

class ProductsList extends PureComponent {
    render = () => {
        const { products } = this.props
        return (
            <div className="row">
                {products.map(prod =>
                    <Product product={prod} key={prod.id} handleAddToCart={this.props.addToCart} handleUpdateCart={this.props.updateCart} cartProducts={this.props.cartProducts} openModal={this.props.openModal} />
                )}
            </div>
        )
    }
}

ProductsList.propTypes = {
    products: PropTypes.array.isRequired,
    addToCart: PropTypes.func.isRequired,
    updateCart: PropTypes.func.isRequired,
    cartProducts: PropTypes.object.isRequired,
    openModal: PropTypes.func.isRequired
}
ProductsList.defaultProps = {}

export default ProductsList
