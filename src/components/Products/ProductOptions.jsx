/* eslint-disable react/style-prop-object */
import React, { PropTypes } from 'react'
import { FormattedMessage, FormattedNumber } from 'react-intl'
import idx from 'idx'

import ProductOption from './ProductOption'

const ProductOptions = ({ parts, handleClose, cartProducts, handleSubmit }) => {
    return (
        <div className="modal-card">
            <header className="modal-card__head">
                <h3 className="modal-card__title">
                    <FormattedMessage id="app.selectoption" defaultMessage="Select an option" />
                </h3>
                <span className="close close--dark" onClick={handleClose}></span>
            </header>
            <section className="modal-card__body">
                <div>
                    {parts.map((part, index) => {
                        const partInsideCart = idx(cartProducts, _ => _[part.sku])

                        return (
                            <div className="product__option row flex--middle" key={part.id}>
                                <div className="col-xs-12 col-md-7">
                                    <h5>{part.name}</h5>
                                    <div className="u-flex flex--between">
                                        <span>{part.sku}</span>
                                        <strong>
                                            <FormattedNumber
                                                value={part.price}
                                                minimumFractionDigits={2}
                                                maximumFractionDigits={2}
                                                currency="EUR"
                                                currencyDisplay="symbol"
                                                style="currency" />
                                        </strong>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-md-5">
                                    <ProductOption className="u-flex" part={part} handleSubmit={handleSubmit} insideCart={partInsideCart} beforeSubmit={handleClose} />
                                </div>
                            </div>
                        )
                    })}
                </div>
            </section>
        </div>
    )
}

ProductOptions.propTypes = {
    parts: PropTypes.array.isRequired,
    handleClose: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    cartProducts: PropTypes.object
}
ProductOptions.defaultProps = {
    cartProducts: {}
}

export default ProductOptions
