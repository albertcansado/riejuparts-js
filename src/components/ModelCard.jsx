import React, { PureComponent, PropTypes } from 'react'
import { Link } from 'react-router'

import Image from './Image'
import LightBox from './LightBox'
import Icon from './Icon'

const navBtn = ({type = 'prev', handleClick }) => {
    return <span className={`prevnext prevnext--${type} is-selectable`} children={<Icon icon="left-arrow" className="icon--16" />} onClick={handleClick} />
}

const initCarouselState = props => {
    return {
        numVersions: props.item.versions.length,
        carouselIndex: 0,
        useCarousel: props.item.versions.length > 3
    }
}

const getFooterItemStyle = (state, index) => {
    if (!state.useCarousel) {
        return {}
    }

    let order = 1
    if (index > state.carouselIndex) {
        // La diferencia entre donde estoy y el indice
        order = 1 + (index - state.carouselIndex)
    } else if (index < state.carouselIndex) {
        // Cuantos quedan mas donde estoy
        order = 1 + (state.numVersions - state.carouselIndex) + index
    }

    return {
        order,
        'border-left': order === 1 ? 'none' : null
    }
}

class ModelCard extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            index: 0,
            ...initCarouselState(props)
        }

        this._changeVersion = this._changeVersion.bind(this)
        this.showImage = this.showImage.bind(this)
    }

    componentWillReceiveProps = props => {
        if (props.item.versions.length !== this.props.versions.length) {
            this.setState(initCarouselState(props))
        }
    }

    _changeVersion = index => this.setState({ index })

    // 1 mas o menos. Loop infinito
    _moveNav = increment => {
        const nextIndex = this.state.carouselIndex + increment
        this.setState({
            carouselIndex: nextIndex >= this.state.numVersions ? 0 : nextIndex < 0 ? this.state.numVersions - 1 : nextIndex
        })
    }

    _prevNav = ev => {
        ev.stopPropagation()

        this._moveNav(-1)
    }

    _nextNav = ev => {
        ev.stopPropagation()

        this._moveNav(1)
    }

    showImage = ev => {
        const { item, handleOpenModal } = this.props

        ev.stopPropagation()

        handleOpenModal({
            component: LightBox,
            props: {
                src: item.versions[this.state.index].image
            }
        })
    }

    _renderFooter = () => {
        const { item, url } = this.props
        const { index, useCarousel } = this.state
        const version = item.versions[index]

        if (!item.versions.length) {
            return null
        }

        return (
            <footer className="card__footer">
                {useCarousel ? navBtn({type: 'prev', handleClick: this._prevNav}) : null}
                <nav className="card__footer-nav">
                    {item.versions.map((ver, i) => {
                        return (
                            <Link
                                to={`${url}/${ver.id}/${version.card}`}
                                className="card__footer-item card__link"
                                onMouseEnter={() => (item.versions.length > 1) ? this._changeVersion(i) : null}
                                key={ver.id}
                                style={getFooterItemStyle(this.state, i)}>
                                {ver.thumb2
                                    ? <Image src={ver.thumb} className="card__image" alt={ver.name} />
                                    : <span className="card__image card__image--empty" children={null} />
                                }
                            </Link>
                        )
                    })}
                </nav>
                {useCarousel ? navBtn({type: 'next', handleClick: this._nextNav}) : null}
            </footer>
        )
    }

    render = () => {
        const { item, url } = this.props
        const version = item.versions[this.state.index]

        return (
            <article className="card card--model">
                <header className="card__caption">
                    <Link to={`${url}/${version.id}/${version.card}`} className="card__link">
                        {version.thumb
                            ? <Image src={version.thumb} className="card__image" alt={version.name} />
                            : <span className="card__image card__image--empty" children={null} />
                        }
                    </Link>
                    <div className="card__link-icon is-selectable" onClick={this.showImage}>
                        <Icon icon="search" className="icon--24 is-error" />
                    </div>
                </header>
                <div className="card__info">
                    <h2 className="card__title">{item.name}</h2>
                    <span className="card__subtitle">{version.name}</span>
                </div>
                {this._renderFooter()}
            </article>
        )
    }
}

ModelCard.propTypes = {
    item: PropTypes.object.isRequired,
    url: PropTypes.string.isRequired
}

ModelCard.defaultProps = {}

export default ModelCard
