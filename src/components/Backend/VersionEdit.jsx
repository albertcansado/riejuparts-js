import React, { Component, PropTypes } from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import { FormattedMessage } from 'react-intl'
import { shallowCompareWithoutFunctions } from 'shallow-compare-without-functions'

import { updateVersion, uploadVersionImage } from '../../actions/backend/versions'

import Bubble from '../Bubble'
import Input from '../Form/Input'
import Tags from '../Form/Tags'
import ImageUploader from '../Form/ImageUploader'
import VersionsPartsList from '../../container/Backend/VersionsPartsList'

class VersionEdit extends Component {
    shouldComponentUpdate = (nextProps, nextState) => {
        return shallowCompareWithoutFunctions(this, nextProps, nextState)
    }

    handleUpdateVersion = (value, field) => {
        const { version, dispatch } = this.props

        if (version.hasOwnProperty(field) && version[field] !== value) {
            dispatch(
                updateVersion(version, {
                    [field]: value
                })
            )
        }
    }

    uploadCover = (file) => {
        const { version, dispatch } = this.props

        return dispatch((
            uploadVersionImage(version, file)
        ))
    }

    render = () => {
        const { version, goBack } = this.props

        return (
            <div className="m-version">
                <header className="m-version__header u-flex u-mgb--15">
                    <div className="m-version__header-left u-flex">
                        <h2 className="m-version__title">
                            <span>
                                <FormattedMessage id="versions.header.title" defaultMessage="Version" />:
                            </span>
                            <span className="m-version__title-name txt-normal">
                                {version.name}
                            </span>
                        </h2>
                        <Bubble type={version.active ? 'success' : 'error'} />
                    </div>
                    <span className="btn" onClick={goBack}>
                        <FormattedMessage id="versions.header.goback" defaultMessage="Return to versions list" />
                    </span>
                </header>
                <Tabs
                    className="tabs"
                    selectedTabPanelClassName="tabs__panel--is-open"
                    selectedTabClassName="tabs__tab--is-active"
                >
                    <TabList className="tabs__list">
                        <Tab className="tabs__tab">
                            <FormattedMessage id="versions.card.card" defaultMessage="Version" />
                        </Tab>
                        <Tab className="tabs__tab">
                            <FormattedMessage id="versions.card.parts" defaultMessage="Parts" />
                        </Tab>
                    </TabList>
                        <TabPanel className="tabs__panel">
                            {/*<header className="m-version__header u-flex">
                                <h2>
                                    <FormattedMessage id="versions.header.title" defaultMessage="Version Info" />
                                    <Bubble type={version.active ? 'success' : 'error'} />
                                </h2>
                                <span className="btn" onClick={goBack}>
                                    <FormattedMessage id="versions.header.goback" defaultMessage="Return to versions list" />
                                </span>
                            </header>*/}
                            <div className="row u-mgt--15">
                                <div className="col-xs-12 col-md-6">
                                    <Input
                                        name="name"
                                        value={version.name}
                                        label="Name"
                                        onBlur={val => this.handleUpdateVersion(val, 'name')}
                                    />
                                    <Tags
                                        label="References"
                                        placeholder="Add a reference"
                                        tagsProps={{
                                            onChange: tags => this.handleUpdateVersion(tags, 'references'),
                                            value: version.references,
                                            onlyUnique: true
                                        }}
                                    />
                                </div>
                                <div className="col-xs-12 col-md-6">
                                    <ImageUploader
                                        labelTitle="Cover"
                                        image={version.image}
                                        handleUpload={this.uploadCover}
                                    />
                                </div>
                            </div>
                        </TabPanel>

                        <TabPanel className="tabs__panel">
                            <VersionsPartsList version_id={version.id} model_id={version.model_id} />
                        </TabPanel>
                </Tabs>
            </div>
        )
    }
}

VersionEdit.propTypes = {
    version: PropTypes.object.isRequired,
    goBack: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired
}

export default VersionEdit
