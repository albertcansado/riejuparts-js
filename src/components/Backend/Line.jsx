import React, { PureComponent, PropTypes} from 'react'
import { connect } from 'react-redux'

import { getPart } from '../../reducers/backend/entities'

import Link from '../Link'
import Icon from '../Icon'

class Line extends PureComponent {

    handleDelete = (ev) => {
        const { line, handleDelete } = this.props

        ev.stopPropagation()

        handleDelete(line)
    }

    _renderObsolete = () => {
        const { part } = this.props

        if (!part.deleted) {
            return null
        }

        return <i className="is-error u-mgl--5" children="(Obsoleta)" />
    }

    render = () => {
        const { line, part, handleEdit } = this.props
        const { locale } = this.context

        return (
            <article className="m-line u-flex is-selectable" onClick={() => handleEdit(line, part)}>
                <div className="m-line__content">
                    <div className="m-line__part">
                        <p className="txt-bold">
                            {part.sku}
                        </p>
                        <p className="txt--small">
                            <span>
                                {part.name}
                            </span>
                            {this._renderObsolete()}
                        </p>
                    </div>
                    <p className="m-line__part-info txt--small">{line.info.hasOwnProperty(locale) ? line.info[locale] : ''}</p>
                </div>
                <div className="txt-right">
                    <Link btn type="reset" onClick={this.handleDelete}>
                        <Icon icon="trash" className="icon--error icon--20" />
                    </Link>
                </div>
            </article>
        )
    }
}

Line.contextTypes = {
    locale: PropTypes.string.isRequired
}

Line.propTypes = {
    line: PropTypes.object.isRequired,
    part: PropTypes.object.isRequired,
    handleEdit: PropTypes.func.isRequired,
    handleDelete: PropTypes.func.isRequired
}

const mapStateToProps = (state, ownProps) => {
    return {
        part: getPart(state, ownProps.line.part)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps

)(Line)
