import React, { Component, PropTypes } from 'react'
import { FormattedMessage, intlShape } from 'react-intl'
import { isEmpty } from 'lodash'

import { getLineFormMessages } from '../../lib/backend_messages'

import Input from '../Form/Input'
import SearchPart from '../Form/SearchPart'
import NumberInput from '../Form/NumberInput'
import Link from '../Link'
import Translations from '../Form/Translations'

/*const getTranslationsProps = (line) => {
    if (!line.hasOwnProperty('_translations') || Array.isArray(line._translations)) {
        return {}
    }

    return line._translations
}*/

class LineForm extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {
            part: props.part || {},
            number: props.line.hasOwnProperty('number') ? props.line.number : 1,
            qty: props.line.hasOwnProperty('qty') ? props.line.qty : 1,
            info: props.line.info,
            isNew: !props.line.hasOwnProperty('id')
        }

        // Cache Translate Messages
        this._messages = getLineFormMessages(context.intl.formatMessage)
    }

    handleSubmit = () => {
        const { handleClose, handleSubmit, line } = this.props

        handleSubmit({
            ...this.state,
            info: this.refs.translations.getValues()
        }, line)
        handleClose()
    }

    couldSave = () => {
        const { part } = this.state

        return !isEmpty(part)
    }

    render = () => {
        const { handleClose } = this.props
        const { isNew } = this.state

        return (
            <div className="modal-card">
                <header className="modal-card__head">
                    <h2 className="modal-card__title">
                        {isNew
                            ? <FormattedMessage id="lineform.addline" defaultMessage="Add new card line" />
                            : <FormattedMessage id="lineform.editline" defaultMessage="Edit card line" />
                        }
                    </h2>
                    <span className="close close--dark" onClick={handleClose}></span>
                </header>
                <section className="modal-card__body">
                    <div className="lineform">
                        <div className="row">
                            <div className="col-xs-12">
                                <SearchPart
                                    label={this._messages.labelPart}
                                    fetchUrl="/d/parts/lookup.json?s={input}"
                                    onChange={val => this.setState({ part: val })}
                                    value={this.state.part}
                                    messages={{
                                        placeholder: this._messages.searchPlaceholder,
                                        noResultsText: this._messages.noResultsText,
                                        searchPromptText: this._messages.searchPromptText,
                                        loadingPlaceholder: this._messages.loadingPlaceholder
                                    }}
                                />
                            </div>
                            <div className="col-xs-6">
                                <Input
                                    label={this._messages.labelNumber}
                                    onChange={val => {
                                        const num = Number.parseInt(val, 10)
                                        this.setState({ number: Number.isNaN(num) ? 1 : num })
                                    }}
                                    value={this.state.number}
                                />
                            </div>
                            <div className="col-xs-6">
                                <NumberInput
                                    label={this._messages.labelQty}
                                    onChange={val => this.setState({ qty: val })}
                                    min={1}
                                    value={this.state.qty}
                                    vertical
                                />
                            </div>
                        </div>
                        <Translations
                            labelTitle={this._messages.labelInfo}
                            labelProps={{
                                addon: this._messages.labelOptional
                            }}
                            field="info"
                            defaultValues={this.state.info}
                            ref="translations"
                            dropdownOnTop
                        />
                    </div>
                </section>
                <footer className="modal-card__foot">
                    <Link btn type="success" className={!this.couldSave() ? 'is-disabled' : null} onClick={this.couldSave() ? this.handleSubmit : null}>
                        {isNew
                            ? <FormattedMessage id="lineform.save" defaultMessage="Save" />
                            : <FormattedMessage id="lineform.update" defaultMessage="Update" />
                        }
                    </Link>
                </footer>
            </div>
        )
    }
}

LineForm.contextTypes = {
    intl: intlShape.isRequired
}

LineForm.propTypes = {
    handleClose: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    isNew: PropTypes.bool,
    line: PropTypes.object,
    part: PropTypes.object
}

LineForm.defaultProps = {
    isNew: false,
    line: {},
    part: {}
}

export default LineForm
