import React, { PropTypes } from 'react'
import { isEqual as _isEqual } from 'lodash'
import { FormattedMessage } from 'react-intl'
import cx from 'classnames'
// import swal from 'sweetalert2'

import Image from '../Image'
import Bubble from '../Bubble'
// import Icon from '../Icon'

const renderCover = (card) => {
    return (
        <figure className={cx("m-card__cover image", {'image--empty': !card.image})}>
            {card.image
                ? <Image src={card.image} className="card__image" />
                : <span className="image__placeholder txt-uppercase"><FormattedMessage id="card.noimage" defaultMessage="No image" /></span>
            }
        </figure>
    )
}

const Version = ({ version, onSelect, handleEnable, handleDelete }) => {
    return (
        <article className="card">
            <header className="card__header">
                <h5 className="card__header-title">{version.name}</h5>
                <div className="card__header-icon">
                    <Bubble type={version.active ? 'success' : 'error'} />
                </div>
            </header>
            <div className="card__content">
                {renderCover(version)}
            </div>
            <footer className="card__footer is-selectable">
                <span className="card__footer-item is-link" onClick={onSelect}>
                    <FormattedMessage id="card.edit" defaultMessage="Edit" />
                </span>
                <span className={cx("card__footer-item", {'is-error': version.active, 'is-success': !version.active})} onClick={() => handleEnable(version) }>
                    {!version.active
                        ? <FormattedMessage id="card.enable" defaultMessage="Enable" />
                        : <FormattedMessage id="card.disable" defaultMessage="Disable" />
                    }
                </span>
                <span className="card__footer-item is-error txt-bold" onClick={() => handleDelete(version) }>
                    <FormattedMessage id="card.delete" defaultMessage="Delete" />
                </span>
            </footer>
        </article>
    )
}

Version.propTypes = {
    version: PropTypes.object.isRequired,
    onSelect: PropTypes.func.isRequired,
    handleEnable: PropTypes.func.isRequired,
    handleDelete: PropTypes.func.isRequired
}

export default Version
