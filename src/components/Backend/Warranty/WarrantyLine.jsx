import React from 'react'
import idx from 'idx'
import { FormattedNumber } from 'react-intl'
// import cx from 'classnames'

import Icon from '../../Icon'
import Link from '../../Link'

const WarrantyLine = ({ line, allowDelete, handleDelete, causes }) => {
    return (
        <article className="warranty__line table__row">
            <div className="table__col table__col--1">
                <span className="u-block txt-bold">{idx(line, _ => _.part.sku)}</span>
                <span className="txt--small">
                    {line.part.hasOwnProperty('backend')
                        ? line.part.backend
                        : line.part.name
                    }
                </span>
            </div>
            <div className="table__col table__col--2">
                {line.qty}
            </div>
            <div className="table__col table__col--3">
                {line.cause_id ? causes[line.cause_id].title : null}
            </div>
            <div className="table__col table__col--4">
                <span className="u-block u-mgb--5">{line.part.repair ? 'Reposición' : 'Abono'}</span>
                <span className={line.part.ship ? 'is-success' : 'is-error'}>{line.part.ship ? 'Envio' : 'No Envio'}</span>
            </div>
            <div className="table__col table__col--5">
                {idx(line, _ => _.part.hours)}h
            </div>
            <div className="table__col table__col--6 txt-right">
                <FormattedNumber
                    value={line.total}
                    minimumFractionDigits={2}
                    maximumFractionDigits={2}
                    currency="EUR"
                    currencyDisplay="symbol"
                    style="currency" />
            </div>
            <div className="table__col table__col--options">
                <Link btn type="reset" className={!allowDelete ? 'is-disabled' : null} onClick={allowDelete ? () => handleDelete(line) : null}>
                    <Icon icon="trash" className="icon--18 is-error" />
                </Link>
            </div>
        </article>
    )
}

export default WarrantyLine
