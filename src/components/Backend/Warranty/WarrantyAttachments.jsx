import React, { PureComponent, PropTypes } from 'react'
// import { FormattedMessage } from 'react-intl'

import Attachment from '../../Warranty/Attachment'

class WarrantyAttachments extends PureComponent {
    _renderAttachment = attachment => {
        return (
            <div className="col-xs-6 col-lg-4 col-xl-2" key={attachment.id}>
                <Attachment file={attachment} withLink />
            </div>
        )
    }

    render = () => {
        const { attachments } = this.props
        return (
            <section className="warranty-block warranty-block--attachments">
                <h3 className="warranty-block__title">Archivos adjuntos</h3>
                <div className="row">
                    {attachments.length > 0
                        ? attachments.map(this._renderAttachment)
                        : <span className="col-xs-12" children="No hay archivos adjuntos" />
                    }
                </div>
            </section>
        )
    }
}

WarrantyAttachments.propTypes = {
    attachments: PropTypes.array.isRequired
}
WarrantyAttachments.defaultProps = {}

export default WarrantyAttachments
