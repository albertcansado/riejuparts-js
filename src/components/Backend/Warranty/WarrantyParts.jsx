import React, { PureComponent, PropTypes } from 'react'
import { maxBy as _maxBy, toArray } from 'lodash'
import { FormattedNumber } from 'react-intl'

import WarrantyLine from './WarrantyLine'
import AddPart from '../../Warranty/AddPart'

class WarrantyParts extends PureComponent {

    _dataToLine = data => {
        const { onAddLine } = this.props

        onAddLine({
            id: new Date().getTime(),
            cause_id: data.cause,
            part: data.part,
            qty: data.qty,
            total: data.qty * data.part.price
        })
    }

    render = () => {
        const { lines, causes, maxTime, dealerId, onDeleteLine } = this.props

        const total = lines.reduce((total, line) => { return total += line.total }, 0)
        const allowDelete = lines.length > 1
        const arrayCauses = toArray(causes)
        const skuLines = lines.map(line => line.part.sku)

        return (
            <section className="warranty-block warranty-block--parts">
                <h3 className="warranty-block__title">Piezas</h3>
                <div className="u-flex flex--middle row-xs--end txt--medium">
                    <div>
                        <span className="txt-bold">Tiempo:</span>
                        <span className="u-mgl--5">
                            {maxTime}h
                        </span>
                    </div>
                    <div className="u-mgl--15">
                        <span className="txt-bold">Total:</span>
                        <span className="u-mgl--5">
                            <FormattedNumber
                                value={total}
                                minimumFractionDigits={2}
                                maximumFractionDigits={2}
                                currency="EUR"
                                currencyDisplay="symbol"
                                style="currency" />
                        </span>
                    </div>
                </div>
                <AddPart
                    url={`/parts/lookup.json?s={input}&l=3&d=${dealerId}`}
                    lines={skuLines}
                    causes={arrayCauses}
                    onSubmit={this._dataToLine}
                />
                <div className="table table--warranty-parts">
                    <header className="table__head">
                        <span className="table__col table__col--1">
                            Referencia
                        </span>
                        <span className="table__col table__col--2">
                            Qty
                        </span>
                        <span className="table__col table__col--3">
                            Causa
                        </span>
                        <span className="table__col table__col--4">
                            A/E
                        </span>
                        <span className="table__col table__col--5">
                            Tiempo
                        </span>
                        <span className="table__col table__col--6 txt-right">
                            Total
                        </span>
                        <span className="table__col table__col--options"></span>
                    </header>
                    <section className="table__body">
                        {lines.map(line => <WarrantyLine line={line} handleDelete={onDeleteLine} allowDelete={allowDelete} causes={causes} key={line.id} />)}
                    </section>
                </div>
            </section>
        )
    }
}

WarrantyParts.propTypes = {
    lines: PropTypes.array.isRequired,
    causes: PropTypes.object.isRequired,
    maxTime: PropTypes.number.isRequired,
    dealerId: PropTypes.string.isRequired,
    onAddLine: PropTypes.func.isRequired
}
WarrantyParts.defaultProps = {}

export default WarrantyParts
