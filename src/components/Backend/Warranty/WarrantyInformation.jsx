import React from 'react'
import { FormattedDate } from 'react-intl'
import idx from 'idx'

import Input from '../../Form/Input'
import Select from '../../Form/Select'

const displayWarrantyType = (type, transport) => {
    if (type) {
        return 'Garantia'
    } else {
        const cause = transport ? 'Transporte' : 'Pre-Km0'
        return `Pre-garantia (${cause})`
    }
}

const WarrantyInformation = ({ warranty, handleTime, extraTime, statuses, handleStatus }) => {
    const soldDate = idx(warranty, _ => _.frame.sold)
    return (
        <div className="row">
            <div className="col-xs-12 col-md-4">
                <h4 className="warranty-block__subtitle">Vehiculo</h4>
                <div>
                    <span className="u-block txt-bold">Chasis:</span>
                    <span>{idx(warranty, _ => _.frame.number)}</span>
                </div>
                <div className="u-mgt--5">
                    <span className="u-block txt-bold">Motor:</span>
                    <span>{idx(warranty, _ => _.frame.engine)}</span>
                </div>
                <div className="u-mgt--5">
                    <span className="u-block txt-bold">Fecha de Venta:</span>
                    <span>
                        {soldDate
                            ? <FormattedDate value={soldDate} />
                            : 'Not yet'
                        }
                    </span>
                </div>
                <div className="u-mgt--5">
                    <span className="u-block txt-bold">Km:</span>
                    <span>{warranty.km}</span>
                </div>
            </div>
            <div className="col-xs-12 col-md-8">
                <h4 className="warranty-block__subtitle">Informacion</h4>
                <div className="row">
                    <div className="col-xs-12 col-md-6">
                        <div>
                            <span className="u-block txt-bold">Tipo</span>
                            <span>{displayWarrantyType(warranty.type, warranty.transport)}</span>
                        </div>
                        <div className="u-mgt--5">
                            <span className="u-block txt-bold">Dealer</span>
                            <span>{idx(warranty, _ => _.dealer.name)}</span>
                        </div>
                        <div className="u-mgt--5">
                            <span className="u-block txt-bold">Pais</span>
                            <span>{idx(warranty, _ => _.dealer.country.name)}</span>
                        </div>
                        <div className="u-mgt--5">
                            <span className="u-block txt-bold">Mecanico</span>
                            <span>{warranty.mechanic ? warranty.mechanic : '-'}{warranty.phone ? ` (${warranty.phone})` : null}</span>
                        </div>
                    </div>
                    <div className="col-xs-12 col-md-6">
                        <div>
                            <span className="u-block txt-bold">Tiempo Extra <span className="txt-normal txt-italic">(Opcional)</span></span>
                            <Input type="text" value={extraTime} onBlur={handleTime} theme={{input: 'u-mgt--5'}} />
                        </div>
                        <div className="u-mgt--5">
                            <span className="u-block txt-bold">Status:</span>
                            <Select
                                empty={false}
                                options={statuses}
                                onChange={option => handleStatus(option.value)}
                                value={warranty.status.toString()}
                                theme={{input: 'u-mgt--5'}}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default WarrantyInformation
