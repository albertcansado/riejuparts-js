import React from 'react'

import Textarea from '../../Form/Textarea'

const WarrantyComments = ({ dealer, factory, onChange }) => {
    return (
        <section className="warranty-block warranty-block--comments">
            <h3 className="warranty-block__title">Observaciones</h3>
            <div className="row">
                <div className="col-xs-12 col-md-5">
                    <article className="warranty__comment warranty__comment--dealer">
                        <h4 className="warranty-block__subtitle">Dealer</h4>
                        {dealer
                            ? <p className="warranty__comment-text">{dealer}</p>
                            : <p className="warranty__comment-empty txt-italic">Dealer not put any comments</p>
                        }
                    </article>
                </div>
                <div className="col-xs-12 col-md-7">
                    <article className="warranty__comment warranty__comment--factory">
                        <h4 className="warranty-block__subtitle">Your Comment</h4>
                        <Textarea
                            inputProps={{
                                placeholder: "Añade una breve explicacion si es necesario"
                            }}
                            value={factory}
                            onBlur={onChange}
                        />
                    </article>
                </div>
            </div>
        </section>
    )
}

export default WarrantyComments
