import React from 'react'
import Switch from 'react-ios-switch'
import { FormattedNumber } from 'react-intl'
import cx from 'classnames'

const ACCEPTED = 10

const WarrantyGenerations = ({ actions, onChange, totalTime, lines, status }) => {
    const totalAbono = lines
        .filter(line => !line.part.repair)
        .reduce((total, line) => { return total += line.total }, 0)

    const totalReponer = lines
        .filter(line => line.part.repair)
        .reduce((total, line) => { return total += line.total }, 0)

    const canChoose = status === ACCEPTED

    return (
        <div>
            <div className={cx("u-flex flex--middle", {'is-disabled': !actions.pgr.allowed})}>
                <Switch
                    checked={!actions.pgr.allowed ? false : actions.pgr.value}
                    onChange={checked => onChange('pgr', checked)}
                    disabled={!canChoose || !actions.pgr.allowed}
                />
                <div className="u-flex flex--middle flex--between u-f1 u-mgl--10">
                    <div>
                        <strong>PGR[XXXXXXX]XXXX</strong> (Reposición)
                    </div>
                    <div className="u-mgl--5 txt-bold">
                        <FormattedNumber
                            value={totalReponer}
                            minimumFractionDigits={2}
                            maximumFractionDigits={2}
                            currency="EUR"
                            currencyDisplay="symbol"
                            style="currency" />
                    </div>
                </div>
            </div>
            <div className={cx("u-flex flex--middle u-mgt--10", {'is-disabled': !actions.pga.allowed})}>
                <Switch
                    checked={!actions.pga.allowed ? false : actions.pga.value}
                    onChange={checked => onChange('pga', checked)}
                    disabled={!canChoose || !actions.pga.allowed}
                />
                <div className="u-flex flex--middle flex--between u-f1 u-mgl--10">
                    <div>
                        <strong>PGA[XXXXXXX]XXXX</strong> (Abono)
                    </div>
                    <div className="u-mgl--5 txt-bold">
                        <FormattedNumber
                            value={totalAbono * -1}
                            minimumFractionDigits={2}
                            maximumFractionDigits={2}
                            currency="EUR"
                            currencyDisplay="symbol"
                            style="currency" />
                    </div>
                </div>
            </div>
            <div className={cx("u-flex flex--middle u-mgt--10", {'is-disabled': !actions.pgt.allowed})}>
                <Switch
                    checked={!actions.pgt.allowed ? false : actions.pgt.value}
                    onChange={checked => onChange('pgt', checked)}
                    disabled={!canChoose || !actions.pgt.allowed}
                />
                <div className="u-flex flex--middle flex--between u-f1 u-mgl--10">
                    <div>
                        <strong>PGT[XXXXXXX]XXXX</strong> (Tiempo)
                    </div>
                    <div className="u-mgl--5 txt-bold">
                        <FormattedNumber
                            value={totalTime * 28}
                            minimumFractionDigits={2}
                            maximumFractionDigits={2}
                            currency="EUR"
                            currencyDisplay="symbol"
                            style="currency" />
                    </div>
                </div>
            </div>
            {!canChoose
                ? <span className="u-block txt--small txt-center is-error u-mgt--5">Solo se pueden seleccionar cuando el estado de la garantia es <strong>Acceptada</strong></span>
                : null
            }
        </div>
    )
}

WarrantyGenerations.propTypes = {}
WarrantyGenerations.defaultProps = {}

export default WarrantyGenerations
