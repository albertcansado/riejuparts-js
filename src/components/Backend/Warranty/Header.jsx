import React from 'react'
import { FormattedMessage, FormattedDate } from 'react-intl'
import idx from 'idx'
import { map as _map, find as _find } from 'lodash'

import Badge from '../../Badge'
import Link from '../../Link'
import Saving from '../../Warranty/Saving'
import HistoryBtn from '../../Warranty/HistoryBtn'

const displayStatus = (status, statuses) => {
    const s = _find(statuses, { value: status })
    return <Badge type={s.type} className="badge--medium" children={s.label} />
}

const submitContainer = (warranty, initStatus, isSaving, actions) => {
    if (warranty.status === initStatus || isSaving) {
        return <Link btn type="success" className="warranty__submit is-disabled">Guardar Estado</Link>
    }

    return (
        <form action={`/d/warranties/process/${warranty.id}`} method="POST">
            <input type="hidden" name="status" value={warranty.status} />
            {_map(actions, (action, key) => <input type="hidden" name={key} value={action.value} key={key} />)}
            <input type="submit" className="btn btn--success warranty__submit" value="Guardar Estado" />
        </form>
    )
}

const Header = ({ warranty, status, statuses, actions, statusMsg, isSaving, savingMessages, displayHistories }) => {
    return (
        <header className="warranty__header box__header u-flex flex--middle flex--between">
            <h4>
                <span>{idx(warranty, _ => _.reference)}</span> - <span className="txt-normal txt--medium">{idx(warranty, _ => _.frame.model.name)}</span>
            </h4>
            <div className="warranty__header-options u-flex flex--end flex--middle">
                <Saving isSaving={isSaving} messages={savingMessages} />
                <div className="u-mgl--15">
                    <span className="txt-bold">
                        Sent:
                    </span>
                    <span className="u-mgl--5">
                        <FormattedDate value={warranty.sended} />
                    </span>
                </div>
                <div className="warranty__status u-mgl--15">
                    <span className="warranty__status-label txt-bold">
                        Actual Status:
                    </span>
                    <span className="warranty__status-value u-mgl--5">
                        {displayStatus(status, statuses)}
                    </span>
                </div>
                <div className="warranty__history-btn u-mgl--15">
                    <HistoryBtn count={warranty.history_count} handleClick={displayHistories} />
                </div>
                <div className="warranty__header-btns u-mgl--15 u-pl--15">
                    {submitContainer(warranty, status, isSaving, actions)}
                </div>
            </div>
        </header>
    )
}

export default Header
