import React, { PropTypes } from 'react'

import Link from '../Link'
import Icon from '../Icon'

const Engine = ({ engine, handleDelete }) => {
    const isDisabled = !handleDelete

    return (
        <div className="table__row">
            <div className="table__col table__col--1 table__col--45">
                {engine.description}
            </div>
            <div className="table__col table__col--1 table__col--45">
                {engine.text}
            </div>
            <div className="table__col table__col--options">
                <Link
                    btn
                    type="reset"
                    className={isDisabled ? 'is-disabled' : null}
                    onClick={isDisabled ? null : () => handleDelete(engine)}
                >
                    <Icon icon="trash" className="icon--error icon--20" />
                </Link>
            </div>
        </div>
    )
}

Engine.propTypes = {
    engine: PropTypes.object.isRequired,
    handleDelete: PropTypes.func
}

Engine.defaultProps = {
    handleDelete: null
}

export default Engine
