import React, { PureComponent, PropTypes } from 'react'
import { intlShape } from 'react-intl'

import { getEngineEditMessages } from '../../lib/backend_messages'

import Link from '../Link'
import Icon from '../Icon'
import Input from '../Form/Input'
import SelectAsync from '../Form/SelectAsync'

class EngineEdit extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            engine: props.engine || '',
            text: props.engine.text || ''
        }

        this.reset = this.reset.bind(this)

        // Cache Translate Messages
        this._messages = getEngineEditMessages(context.intl.formatMessage)
    }

    reset = () => {
        this.setState({
            engine: '',
            text: ''
        })
    }

    onSubmit = () => {
        const { engine, text } = this.state
        const { handleSubmit } = this.props

        handleSubmit({
            engine_id: engine.value,
            description: engine.label,
            text
        })

        this.reset();
    }

    render = () => {
        const { engine, text } = this.state

        const hasEngine = engine.hasOwnProperty('value')

        return (
            <div className="table__row">
                <div className="table__col table__col--1 table__col--45">
                    <SelectAsync
                        url="/d/engines/list.json"
                        value={engine.value}
                        placeholder={this._messages.enginePlaceholder}
                        onSelect={(option) => this.setState({ engine: option })}
                    />
                </div>
                <div className="table__col table__col--1 table__col--45">
                    <Input
                        label={null}
                        inputProps={{
                            placeholder: this._messages.textPlaceholder
                        }}
                        value={text}
                        onChange={(val) => this.setState({ text: val })}
                    />
                </div>
                <div className="table__col table__col--options">
                    <div className="btn-group">
                        <Link
                            btn
                            type="reset"
                            className={!hasEngine ? 'is-disabled' : null}
                            onClick={hasEngine ? this.onSubmit : null}
                        >
                            <Icon icon="checked" className="icon--24 icon--success" />
                        </Link>
                        <Link btn type="reset">
                            <Icon icon="close" className="icon--18 icon--error" />
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

EngineEdit.contextTypes = {
    intl: intlShape.isRequired
}

EngineEdit.propTypes = {
    engine: PropTypes.object,
    handleSubmit: PropTypes.func.isRequired
}

EngineEdit.defaultProps = {
    engine: {}
}

export default EngineEdit
