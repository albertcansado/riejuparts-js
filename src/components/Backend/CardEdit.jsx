import React, { PureComponent, PropTypes } from 'react'
import { connect } from 'react-redux'
import { FormattedMessage } from 'react-intl'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'

import { updateCard, uploadCardImage } from '../../actions/backend/cards'
import { getCard } from '../../reducers/backend/entities'

import PartsList from './PartsList'
// import Input from '../Form/Input'
import Label from '../Form/Label'
import SelectIcon from '../Form/SelectIcon'
import ImageUploader from '../Form/ImageUploader'
import Translations from '../Form/Translations'
import Bubble from '../Bubble'

class CardEdit extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {}

        this.handleUpdateCard = this.handleUpdateCard.bind(this)
        this._setSelectProperty = this._setSelectProperty.bind(this)
    }

    handleUpdateCard = (value, field) => {
        const { card, dispatch } = this.props

        if (card.hasOwnProperty(field) && card[field] !== value) {
            dispatch(
                updateCard(card, {
                    [field]: value
                })
            )
        }
    }

    _setSelectProperty = (option, options) => this.handleUpdateCard(option[options.valueKey], options.name)

    uploadCover = (file) => {
        const { card, dispatch } = this.props
        return dispatch(
            uploadCardImage(card, file)
        )
    }

    render = () => {
        const { card, goBack } = this.props

        return (
            <div className="m-cardEdit">
                <header className="m-cardEdit__header u-flex u-mgb--15">
                    <div className="m-cardEdit__header-left u-flex">
                        <h2 className="m-cardEdit__title">
                            <span>
                                <FormattedMessage id="cardEdit.header.title" defaultMessage="Card" />:
                            </span>
                            <span className="m-cardEdit__title-name txt-normal">
                                {card.name[this.context.locale]}
                            </span>
                        </h2>
                        <Bubble type={card.active ? 'success' : 'error'} />
                    </div>
                    <span className="btn" onClick={goBack}>
                        <FormattedMessage id="cardEdit.listCards" defaultMessage="Return to card list" />
                    </span>
                </header>
                <Tabs
                    className="tabs"
                    selectedTabPanelClassName="tabs__panel--is-open"
                    selectedTabClassName="tabs__tab--is-active"
                >
                    <TabList className="tabs__list">
                        <Tab className="tabs__tab">
                            <FormattedMessage id="engines.card.card" defaultMessage="Card" />
                        </Tab>
                        <Tab className="tabs__tab">
                            <FormattedMessage id="engines.card.parts" defaultMessage="Parts" />
                        </Tab>
                    </TabList>
                        <TabPanel className="tabs__panel">
                            <div className="row u-mgt--15">
                                <div className="col-xs-12 col-md-6">
                                    <Translations
                                        labelTitle="Name"
                                        name="name"
                                        defaultValues={card.name}
                                        onChange={this.handleUpdateCard}
                                    />
                                    <div className="input">
                                        <Label title="Icon" />
                                        <SelectIcon
                                            value={card.icon}
                                            name="icon"
                                            onSelect={this._setSelectProperty}
                                        />
                                    </div>
                                </div>
                                <div className="col-xs-12 col-md-6">
                                    <ImageUploader
                                        labelTitle="Cover"
                                        image={card.image}
                                        handleUpload={this.uploadCover}
                                    />
                                </div>
                            </div>
                        </TabPanel>

                        <TabPanel className="tabs__panel">
                            <PartsList cardId={card.id} />
                        </TabPanel>
                </Tabs>
            </div>
        )
    }
}

CardEdit.contextTypes = {
    locale: PropTypes.string.isRequired
}

CardEdit.propTypes = {
    id: PropTypes.string,
    goBack: PropTypes.func.isRequired,
    card: PropTypes.object.isRequired,
}

CardEdit.defaultProps = {
    id: null
}

const mapStateToProps = (state, ownProps) => {
    return {
        card: getCard(state, ownProps.id)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps

)(CardEdit)
