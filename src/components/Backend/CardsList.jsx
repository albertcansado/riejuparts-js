import React, { PureComponent, PropTypes } from 'react'
import update from 'react/lib/update'
import { FormattedMessage, intlShape } from 'react-intl'
import swal from 'sweetalert2'
import { DragDropContext } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import { isEqual } from 'lodash'

import { getCardsMessages } from '../../lib/backend_messages'

import Card from '../../components/Backend/Card'
import CardEdit from '../../components/Backend/CardEdit'

// const availableActions = ['list', 'create', 'update']
import Link from '../../components/Link'
// import Icon from '../../components/Icon'

@DragDropContext(HTML5Backend)
export default class CardsList extends PureComponent {
    static contextTypes = {
        intl: intlShape.isRequired,
        locale: PropTypes.string.isRequired
    }

    static propTypes = {
        entity: PropTypes.object.isRequired,
        cards: PropTypes.array.isRequired,
        handleAddCard: PropTypes.func.isRequired,
        handleSaveOrder: PropTypes.func
    }

    constructor(props, context) {
        super(props, context)

        this.state = {
            loadingAdd: false,
            action: 'list',
            cards: props.cards,
            orderIsLoading: false,
            id: null
        }

        // Cache Translate Messages
        this._messages = getCardsMessages(context.intl.formatMessage)

        // Public Methods
        this.changeToList = this.changeToList.bind(this)
        this.changeToAdd = this.changeToAdd.bind(this)
        this.changeToEdit = this.changeToEdit.bind(this)
        this.moveCard = this.moveCard.bind(this)
    }

    componentWillReceiveProps = nextProps => {
        if (!isEqual(nextProps.cards, this.props.cards)) {
            this.setState({
                cards: nextProps.cards
            })
        }
    }

    changeToList = () => {
        this.setState({
            action: 'list',
            id: null
        })
    }

    saveOrder = () => {
        const { entity, handleSaveOrder } = this.props

        this.setState({
            orderIsLoading: true
        })

        handleSaveOrder(entity, this.state.cards.map(card => card.id))
        .then(() => {
            this.setState({
                orderIsLoading: false
            })
        })
    }

    changeToAdd = () => {
        const { handleAddCard } = this.props

        swal({
            title: 'Enter the card name',
            input: 'text',
            showCancelButton: true
        }).then(name => {
            if (!name) {
                return
            }

            this.setState({
                loadingAdd: true
            })

            handleAddCard({
                [this.context.locale]: name
            })
                .then(response => {
                    if (response.hasOwnProperty('error')) {
                        // Show error
                        return
                    }

                    this.setState({
                        loadingAdd: false,
                        action: 'edit',
                        id: response.payload.result
                    })
                })
        }, (dismiss) => {})
    }

    changeToEdit = id => this.setState({ action: 'edit', id })

    moveCard = (dragIndex, hoverIndex) => {
        const { cards } = this.state;
        const dragCard = cards[dragIndex];

        this.setState(update(this.state, {
            cards: {
                $splice: [
                    [dragIndex, 1],
                    [hoverIndex, 0, dragCard],
                ],
            },
        }));
    }

    _renderList = () => {
        const { loadingAdd, cards, orderIsLoading } = this.state
        const { handleEnableDisableCard, handleDeleteCard } = this.props

        return (
            <div className="mCards-list">
                <header className="mCards-list__header u-flex">
                    <h2 className="mCards-list__title">
                        <FormattedMessage id="cardEditor.list.title" defaultMessage="Available Cards" />
                    </h2>
                    <div className="btn-group">
                        <Link
                            btn
                            className={orderIsLoading ? 'btn--loading' : null}
                            onClick={!orderIsLoading ? this.saveOrder : null}
                        >
                            Save Order
                        </Link>
                        <Link btn type="notice" onClick={!loadingAdd ? this.changeToAdd : null} className={loadingAdd ? 'btn--loading' : null}>
                            <FormattedMessage id="cardEditor.addNewCard" defaultMessage="Add New Card" />
                        </Link>
                    </div>
                </header>
                <div className="mCards-list__list u-mgt--25 row">
                    {cards.map((card, i) => {
                        return (
                            <div className="col-xs-12 col-md-3" key={card.id}>
                                <Card
                                    card={card}
                                    onSelect={this.changeToEdit}
                                    handleEnable={handleEnableDisableCard}
                                    handleDelete={handleDeleteCard}
                                    messages={this._messages}
                                    index={i}
                                    moveCard={this.moveCard}
                                />
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }

    render = () => {
        const { action } = this.state

        switch (action) {
            case 'add':
                return <CardEdit goBack={this.changeToList} />
            case 'edit':
                const { id } = this.state
                return <CardEdit id={id} goBack={this.changeToList} />
            case 'list':
            default:
                return this._renderList()
        }
    }
}
