import React, { PureComponent, PropTypes } from 'react'
import { FormattedMessage } from 'react-intl'

import SearchPart from '../../Form/SearchPart'
import Icon from '../../Icon'

class ProductsPartsList extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            part: ''
        }
    }

    _setPart = part => this.setState({part})

    _renderAddBlock = () => {
        return (
            <div className="table__row" key="ADD">
                <div className="table__col">
                    <SearchPart
                        fetchUrl={'/parts/lookup.json?s={input}'}
                        onChange={this.props.handleAdd}
                        value={this.state.part}
                        messages={{
                            noResultsText: "No hay ninguna pieza que contenga esa referencia",
                            placeholder: "Introduce la referencia de una pieza",
                            loadingPlaceholder: "Cargando",
                            searchPromptText: "Empieza a escribir la referencia"
                        }}
                    />
                </div>
            </div>
        )
    }

    _renderContent = () => {
        if (!this.props.parts.length) {
            return (
                <div className="table__row">
                    <div className="table__col txt-center txt-bold">No hay piezas</div>
                </div>
            )
        }

        return this.props.parts.map(part => {
            return (
                <div className="table__row" key={part.id}>
                    <div className="table__col">
                        <strong className="u-block">{part.sku}</strong>
                        <span className="txt--small">{part.name}</span>
                    </div>
                    <div className="table__col txt-right">
                        <span className="is-selectable" onClick={() => this.props.handleDelete(part)}>
                            <Icon icon="close" className="icon--14 is-error" />
                        </span>
                    </div>
                </div>
            )
        })
    }

    render = () => {
        return (
            <section className="u-mgt--20">
                <div className="table">
                    <div className="table__head">
                        <div className="table__col">
                            Piezas
                        </div>
                    </div>
                    <div className="table__body">
                        {this._renderAddBlock()}
                        {this._renderContent()}
                    </div>
                </div>
            </section>
        )
    }
}

ProductsPartsList.propTypes = {}
ProductsPartsList.defaultProps = {}

export default ProductsPartsList
