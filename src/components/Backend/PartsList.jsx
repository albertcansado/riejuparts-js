import React, { PureComponent, PropTypes} from 'react'
import { connect } from 'react-redux'
import { FormattedMessage, intlShape } from 'react-intl'
import { chain as _chain, map as _map } from 'lodash'
import swal from 'sweetalert2'

import { fetchCardLinesIfNeeded, addCardLine, updateCardLine, deleteCardLine } from '../../actions/backend/cards'
import { openModal } from '../../actions/modal'
import { isCardFetching, getCardLines } from '../../reducers/backend/entities'
import { getPartsListMessages } from '../../lib/backend_messages'

import LineForm from './LineForm'
import Line from './Line'
import Loading from '../Loading'
import Link from '../Link'

class PartsList extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.handleSubmitNewLine = this.handleSubmitNewLine.bind(this)
        this.handleSubmitUpdateLine = this.handleSubmitUpdateLine.bind(this)

        // Cache Translate Messages
        this._messages = getPartsListMessages(context.intl.formatMessage)
    }

    componentWillMount = () => {
        const { dispatch, cardId } = this.props
        dispatch(fetchCardLinesIfNeeded(cardId))
    }

    // Add
    addNewLine = () => {
        const { dispatch } = this.props
        dispatch(openModal({
            component: LineForm,
            props: {
                handleSubmit: this.handleSubmitNewLine,
                isNew: true
            }
        }))
    }

    handleSubmitNewLine = (data) => {
        const { dispatch, cardId } = this.props
        dispatch(addCardLine(cardId, data))
    }

    // Edit
    handleEditLine = (line, part) => {
        const { dispatch } = this.props
        dispatch(openModal({
            component: LineForm,
            props: {
                handleSubmit: this.handleSubmitUpdateLine,
                isNew: false,
                line,
                part
            }
        }))
    }

    handleSubmitUpdateLine = (data, line) => {
        const { cardId, dispatch } = this.props
        dispatch(updateCardLine(cardId, line, data))
    }

    // Delete
    handleDeleteLine = (line) => {
        const { cardId, dispatch } = this.props

        swal({
            title: this._messages.deleteLineTitle,
            text: this._messages.deleteLineText,
            type: "warning",
            showCancelButton: true,
            cancelButtonText: this._messages.deleteLineCancel,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: this._messages.deleteLineConfirm
        }).then(() => {
            dispatch(
                deleteCardLine(line, cardId)
            )
        }).catch(swal.noop)
    }

    _renderEmpty = () => {
        return (
            <div className="row">
                <div className="col-xs-12">
                    <div className="m-cards-list__empty txt-bold txt--large txt-center">
                        <FormattedMessage id="partEditor.noResults" defaultMessage="There are not results" />
                    </div>
                </div>
            </div>
        )
    }

    _renderList = (cardLines) => {
        const linesGrouped = _chain(cardLines)
            .groupBy('number')
            .value()

        return (
            <div className="row">
                {_map(linesGrouped, (lines, number) => {
                    return (
                        <div className="m-cards-list__item col-xs-12 col-md-6" key={number}>
                            <article className="m-cards-part">
                                <div className="row">
                                    <div className="col-xs-3 col-md-2">
                                        <span className="m-cards-part__number u-flex txt-bold">
                                            {number}
                                        </span>
                                    </div>
                                    <div className="col-xs-9 col-md-10 u-alpha">
                                        <div className="m-cards-part__list u-flex">
                                            {lines.map(line => <Line line={line} key={line.id} handleEdit={this.handleEditLine} handleDelete={this.handleDeleteLine} />)}
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    )
                })}
                {Object.keys(linesGrouped).length % 2
                    ? <div className="m-cards-list__item col-xs-12 col-md-6" children={<article className="m-cards-part" />} key='empty' />
                    : null
                }
            </div>
        )
    }

    render = () => {
        const { isFetchingLines, cardLines } = this.props

        return (
            <div className="m-part-editor u-mgt--15">
                <header className="m-part-editor__header u-flex">
                    <h2>Card Lines</h2>
                    <Link
                        btn
                        type="notice"
                        className={isFetchingLines ? 'is-disabled' : null}
                        onClick={isFetchingLines ? null : this.addNewLine}
                    >
                        <FormattedMessage id="partEditor.addLine" defaultMessage="Add New Line" />
                    </Link>
                </header>
                <section className="m-part-editor__content">
                    <Loading isLoading={isFetchingLines} className="loading--60 u-centered">
                        <div className="m-cards-list u-mgt--10">
                            {cardLines.length ? this._renderList(cardLines) : this._renderEmpty()}
                        </div>
                    </Loading>
                </section>
            </div>
        )
    }
}

PartsList.contextTypes = {
    intl: intlShape.isRequired
}

PartsList.propTypes = {
    cardId: PropTypes.string.isRequired,
    cardLines: PropTypes.array.isRequired
}

const mapStateToProps = (state, ownProps) => {
    return {
        isFetchingLines: isCardFetching(state, ownProps.cardId),
        cardLines: getCardLines(state, ownProps.cardId)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps

)(PartsList)
