import React, { PureComponent, PropTypes } from 'react'
import { connect } from 'react-redux'
import { FormattedMessage, intlShape } from 'react-intl'
import cx from 'classnames'

import { getVersionsAddFormMessages } from '../../../lib/backend_messages'
import { getEntityCards } from '../../../reducers/backend/entities'

import StepCard from './StepCard'
import StepPart from './StepPart'
import Link from '../../Link'

const defaultState = {
    target_id: '',
    part: {},
    action: 1,
    qty: 1,
    number: '',
    info: {}
}

const modificationToState = (mod) => {
    if (!mod) {
        return defaultState
    }

    return Object.assign({}, defaultState, {
        ...mod.extra,
        target_id: mod.target_id,
        part: mod.part
    })
}

class AddForm extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            step: props.modification ? 1 : 0,
            steps: 2,
            data: modificationToState(props.modification)
        }

        // Cache Translate Messages
        this._messages = getVersionsAddFormMessages(context.intl.formatMessage)
    }

    stepTwo = (option) => {
        this.setState({
            step: 1,
            data: {
                ...this.state.data,
                target_id: option.value
            }
        })
    }

    isFinished = () => {
        const { data } = this.state

        return data.target_id
            && data.part.hasOwnProperty('id')
            && data.number
            && data.qty > 0
    }

    setPartValues = (value, label) => {
        if (label === 'part' && value === null) {
            value = {}
        }

        this.setState({
            data: {
                ...this.state.data,
                [label]: value
            }
        })
    }

    goPrevious = () => {
        const { step } = this.state
        if (step <= 0) {
            return
        }

        this.setState({
            step: step - 1
        })
    }

    goNext = () => {
        const { step, steps } = this.state
        if (step >= steps) {
            return
        }

        this.setState({
            step: step + 1
        })
    }

    handleSubmit = () => {
        const { handleSubmit, handleClose } = this.props
        const { data } = this.state

        if (handleSubmit) {
            handleSubmit({
                target_id: data.target_id,
                part: data.part,
                action: data.action,
                extra: {
                    qty: data.qty,
                    number: data.number,
                    info: data.info
                }
            })
        }

        handleClose()
    }

    render = () => {
        const { step, steps, data } = this.state
        const { cards, handleClose } = this.props

        const optionsCards = cards.map(card => {
            return {
                label: card.name[this.context.locale],
                value: card.id
            }
        })

        const isDisabledNext = (step > 0 && step < steps) || !data.target_id
        const isDisabledPrevious = step === 0

        return (
            <div className="modal-card">
                <header className="modal-card__head">
                    <h2 className="modal-card__title">
                        <FormattedMessage id="addform.title" defaultMessage="Add new part to the version" />
                    </h2>
                    <span className="close close--dark" onClick={handleClose}></span>
                </header>
                <section className="modal-card__body">
                    <div className="action-form">
                        {step === 0
                                ? <StepCard
                                        options={optionsCards}
                                        onChange={this.stepTwo}
                                        value={data.target_id}
                                        messages={this._messages}
                                    />
                                :  null
                        }

                        {step === 1
                            ? <StepPart
                                    value={data}
                                    onChange={this.setPartValues}
                                    messages={this._messages}
                                    goBack={() => this.setState({ step: 0 })}
                                />
                            : null
                        }
                    </div>
                </section>
                <footer className="modal-card__foot">
                    <div className="row u-w100">
                        <div className="col-xs-12 col-md-6">
                            <div className="btn-group btn-group--block">
                                <Link
                                    btn
                                    type="notice"
                                    className={cx({
                                        'is-disabled': isDisabledPrevious
                                    })}
                                    onClick={!isDisabledPrevious ? this.goPrevious : null}
                                >
                                    <FormattedMessage id="addform.previous" defaultMessage="Previous" />
                                </Link>
                                <Link
                                    btn
                                    type="notice"
                                    className={cx({
                                        'is-disabled': isDisabledNext
                                    })}
                                    onClick={!isDisabledNext ? this.goNext : null}
                                >
                                    <FormattedMessage id="addform.next" defaultMessage="Next" />
                                </Link>
                            </div>
                        </div>
                        <div className="col-xs-12 col-md-6">
                            <Link btn type="success" className={!this.isFinished() ? 'is-disabled' : null} onClick={this.isFinished() ? this.handleSubmit : null}>
                                <FormattedMessage id="addform.save" defaultMessage="Save" />
                            </Link>
                        </div>
                    </div>
                </footer>
            </div>
        )
    }
}

AddForm.contextTypes = {
    intl: intlShape.isRequired,
    locale: PropTypes.string.isRequired
}

AddForm.propTypes = {
    cards: PropTypes.array.isRequired,
    model_id: PropTypes.string.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    handleClose: PropTypes.func.isRequired,
    modification: PropTypes.object
}

AddForm.defaultProps = {
    modification: null
}

const mapStateToProps = (state, ownProps) => {
    return {
        cards: getEntityCards(state, ownProps.model_id)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddForm)
