import React, { PureComponent, PropTypes } from 'react'
import { connect } from 'react-redux'
import { FormattedMessage, intlShape } from 'react-intl'
import cx from 'classnames'

import { getVersionsReplaceFormMessages } from '../../../lib/backend_messages'
import { getEntityCards } from '../../../reducers/backend/entities'

import StepCard from './StepCard'
import StepPart from './StepPart'
import Link from '../../Link'

const defaultState = {
    card_id: '',
    line: {},
    part: {},
    action: 3,
    qty: 1,
    number: '',
    info: {}
}

const modificationToState = (mod) => {
    if (!mod) {
        return defaultState
    }

    return Object.assign({}, defaultState, {
        ...mod.extra,
        card_id: mod.target.card_id,
        line: {
            id: mod.target_id
        },
        part: mod.part
    })
}

class ReplaceForm extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            step: 0,
            steps: 2,
            data: modificationToState(props.modification)
        }

        // Cache Translate Messages
        this._messages = getVersionsReplaceFormMessages(context.intl.formatMessage)
    }

    stepTwo = (line) => {
        this.setState({
            step: 1,
            data: {
                ...this.state.data,
                line: line,
                qty: line.qty,
                number: line.number
            }
        })
    }

    isFinished = () => {
        const { data } = this.state

        return data.line.hasOwnProperty('id')
            && data.part.hasOwnProperty('id')
            && data.number
            && data.qty > 0
    }

    setPartValues = (value, label) => {
        this.setState({
            data: {
                ...this.state.data,
                [label]: value
            }
        })
    }

    goPrevious = () => {
        const { step } = this.state
        if (step <= 0) {
            return
        }

        this.setState({
            step: step - 1
        })
    }

    goNext = () => {
        const { step, steps } = this.state
        if (step >= steps) {
            return
        }

        this.setState({
            step: step + 1
        })
    }

    handleSubmit = () => {
        const { handleSubmit, handleClose } = this.props
        const { data } = this.state

        if (handleSubmit) {
            handleSubmit({
                target_id: data.line.id,
                part: data.part,
                action: data.action,
                extra: {
                    qty: data.qty,
                    number: data.number,
                    info: data.info
                }
            })
        }

        handleClose()
    }

    render = () => {
        const { step, steps, data } = this.state
        const { cards, handleClose } = this.props

        const optionsCards = cards.map(card => {
            return {
                label: card.name[this.context.locale],
                value: card.id
            }
        })

        let currentCard = {}
        if (step === 0 && data.card_id) {
            currentCard = cards.filter(card => card.id === data.card_id)[0]
        }

        const isDisabledNext = (step > 0 && step < steps) || !data.line.hasOwnProperty('id')
        const isDisabledPrevious = step === 0

        return (
            <div className="modal-card">
                <header className="modal-card__head">
                    <h2 className="modal-card__title">
                        <FormattedMessage id="replaceform.title" defaultMessage="Replace a part with other part" />
                    </h2>
                    <span className="close close--dark" onClick={handleClose}></span>
                </header>
                <section className="modal-card__body">
                    <div className="action-form">
                        {step === 0
                                ? <StepCard
                                        options={optionsCards}
                                        listParts={currentCard.cards_parts}
                                        onChange={(option) => this.setPartValues(option.value, 'card_id')}
                                        onSelectLine={(line) => this.stepTwo(line)}
                                        value={data.card_id}
                                        lineValue={data.line.id}
                                        messages={this._messages}
                                    />
                                : null
                        }

                        {step === 1
                            ? <StepPart
                                    value={data}
                                    onChange={this.setPartValues}
                                    messages={this._messages}
                                    goBack={() => this.setState({ step: 0 })}
                                />
                            : null
                        }
                    </div>
                </section>
                <footer className="modal-card__foot">
                    <div className="row u-w100">
                        <div className="col-xs-12 col-md-6">
                            <div className="btn-group btn-group--block">
                                <Link
                                    btn
                                    type="notice"
                                    className={cx({
                                        'is-disabled': isDisabledPrevious
                                    })}
                                    onClick={!isDisabledPrevious ? this.goPrevious : null}
                                >
                                    <FormattedMessage id="replaceform.previous" defaultMessage="Previous" />
                                </Link>
                                <Link
                                    btn
                                    type="notice"
                                    className={cx({
                                        'is-disabled': isDisabledNext
                                    })}
                                    onClick={!isDisabledNext ? this.goNext : null}
                                >
                                    <FormattedMessage id="replaceform.next" defaultMessage="Next" />
                                </Link>
                            </div>
                        </div>
                        <div className="col-xs-12 col-md-6">
                            <Link btn type="success" className={!this.isFinished() ? 'is-disabled' : null} onClick={this.isFinished() ? this.handleSubmit : null}>
                                <FormattedMessage id="replaceform.save" defaultMessage="Save" />
                            </Link>
                        </div>
                    </div>
                </footer>
            </div>
        )
    }
}

ReplaceForm.contextTypes = {
    intl: intlShape.isRequired,
    locale: PropTypes.string.isRequired
}

ReplaceForm.propTypes = {
    cards: PropTypes.array.isRequired,
    model_id: PropTypes.string.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    handleClose: PropTypes.func.isRequired,
    modification: PropTypes.object
}

ReplaceForm.defaultProps = {
    modification: null
}

const mapStateToProps = (state, ownProps) => {
    return {
        cards: getEntityCards(state, ownProps.model_id, true)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ReplaceForm)
