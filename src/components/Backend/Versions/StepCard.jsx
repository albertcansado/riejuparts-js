import React, { PropTypes } from 'react'
import Select from 'react-select'
import cx from 'classnames'

import Label from '../../Form/Label'

const renderLine = (line, actual, onClick) => {
    return (
        <div
            className={cx("step-card__line u-flex", {
                'is-selectable': actual !== line.id,
                'is-current': actual === line.id
            })}
            onClick={() => onClick(line)}
            key={line.id}
        >
            <div className="step-card__line-number txt-bold">
                {line.number}
            </div>
            <div className="step-card__line-content">
                <span className="step-card__line-sku txt-bold">
                    {line.part.sku}
                </span>
                <span className="step-card__line-name txt--small">
                    {line.part.name}
                </span>
            </div>
        </div>
    )
}

const renderCardLines = ({ lineValue, listParts, onSelectLine }, messages) => {
    if (!listParts.length) {
        return null
    }

    return (
        <div className="u-mgt--25">
            <p className="txt-bold u-mgb--10">
                {messages.stepCardPhraseParts}
            </p>
            <div className="step-card__list">
                {listParts.map(line => renderLine(line, lineValue, onSelectLine))}
            </div>
        </div>
    )
}

const StepCard = ({ options, value, onChange, messages, ...lineProps }) => {
    return (
        <div className="step-card">
            <p className="txt-bold u-mgb--10">
                {messages.stepcardPhrase}
            </p>
            <div className="input input--select">
                <Label title={messages.stepcardLabel} />
                <Select
                    options={options}
                    value={value}
                    clearable={false}
                    searchable={false}
                    onChange={onChange}
                />
            </div>
            {renderCardLines(lineProps, messages)}
        </div>
    )
}

StepCard.propTypes = {
    options: PropTypes.array.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    messages: PropTypes.object.isRequired,

    listParts: PropTypes.array,
    onSelectLine: PropTypes.func,
    lineValue: PropTypes.string
}

StepCard.defaultProps = {
    value: '',

    listParts: [],
    onSelectLine: null,
    lineValue: ''
}

export default StepCard
