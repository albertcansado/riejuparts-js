import React, { PureComponent, PropTypes } from 'react'
import { connect } from 'react-redux'
import { FormattedMessage, intlShape } from 'react-intl'

import { getVersionsRemoveFormMessages } from '../../../lib/backend_messages'
import { getEntityCards } from '../../../reducers/backend/entities'

import StepCard from './StepCard'
import Link from '../../Link'

class RemoveForm extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            card_id: '',
            line: {}
        }

        // Cache Translate Messages
        this._messages = getVersionsRemoveFormMessages(context.intl.formatMessage)
    }

    handleSubmit = () => {
        const { handleSubmit, handleClose } = this.props
        const { line } = this.state

        if (handleSubmit) {
            handleSubmit({
                target_id: line.id,
                action: 2
            })
        }

        handleClose()
    }

    changeValue = (value, field) => {
        this.setState({
            ...this.state,
            [field]: value
        })
    }

    isFinished = () => {
        const { line } = this.state

        return line.hasOwnProperty('id')
    }

    render = () => {
        const { handleClose, cards } = this.props
        const { card_id, line } = this.state

        const optionsCards = cards.map(card => {
            return {
                label: card.name[this.context.locale],
                value: card.id
            }
        })

        let currentCard = {}
        if (card_id) {
            currentCard = cards.filter(card => card.id === card_id)[0]
        }

        return (
            <div className="modal-card">
                <header className="modal-card__head">
                    <h2 className="modal-card__title">
                        <FormattedMessage id="removeform.title" defaultMessage="Delete a card part from the version" />
                    </h2>
                    <span className="close close--dark" onClick={handleClose}></span>
                </header>
                <section className="modal-card__body">
                    <div className="action-form">
                        <StepCard
                            options={optionsCards}
                            listParts={currentCard.cards_parts}
                            onChange={(option) => this.changeValue(option.value, 'card_id')}
                            onSelectLine={(line) => this.changeValue(line, 'line')}
                            lineValue={line.id}
                            value={card_id}
                            messages={this._messages}
                        />
                    </div>
                </section>
                <footer className="modal-card__foot">
                    <Link btn type="success" className={!this.isFinished() ? 'is-disabled' : null} onClick={this.isFinished() ? this.handleSubmit : null}>
                        <FormattedMessage id="removeform.save" defaultMessage="Save" />
                    </Link>
                </footer>
            </div>
        )
    }
}

RemoveForm.contextTypes = {
    intl: intlShape.isRequired,
    locale: PropTypes.string.isRequired
}

RemoveForm.propTypes = {
    model_id: PropTypes.string.isRequired,
    cards: PropTypes.array.isRequired,
    handleClose: PropTypes.func.isRequired
}

const mapStateToProps = (state, ownProps) => {
    return {
        cards: getEntityCards(state, ownProps.model_id, true)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RemoveForm)
