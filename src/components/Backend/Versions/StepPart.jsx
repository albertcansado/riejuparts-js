import React, { PropTypes } from 'react'

import SearchPart from '../../Form/SearchPart'
import Input from '../../Form/Input'
import NumberInput from '../../Form/NumberInput'
import Translations from '../../Form/Translations'

const StepPart = ({ value, messages, onChange, goBack }) => {
    return (
        <div className="lineform">
            <div className="row">
                <div className="col-xs-12">
                    <SearchPart
                        label={messages.labelPart}
                        fetchUrl="/d/parts/lookup.json?s={input}"
                        onChange={val => onChange(val, 'part')}
                        value={value.part}
                        messages={{
                            placeholder: messages.searchPlaceholder,
                            noResultsText: messages.noResultsText,
                            searchPromptText: messages.searchPromptText,
                            loadingPlaceholder: messages.loadingPlaceholder
                        }}
                    />
                </div>
                <div className="col-xs-6">
                    <Input
                        label={messages.labelNumber}
                        onChange={val => {
                            const num = Number.parseInt(val, 10)
                            onChange(Number.isNaN(num) ? 1 : num, 'number')
                        }}
                        value={value.number}
                    />
                </div>
                <div className="col-xs-6">
                    <NumberInput
                        label={messages.labelQty}
                        onChange={val => onChange(val, 'qty')}
                        min={1}
                        value={value.qty}
                        vertical
                    />
                </div>
            </div>
            <Translations
                labelTitle={messages.labelInfo}
                labelProps={{
                    addon: messages.labelOptional
                }}
                field="info"
                defaultValues={value.info}
                onChange={values => onChange(values, 'info')}
            />
        </div>
    )
}

StepPart.propTypes = {
    //value: PropTypes.obj.isRequired,
    onChange: PropTypes.func.isRequired,
    // messages: PropTypes.obj.isRequired,
    goBack: PropTypes.func.isRequired
}

export default StepPart
