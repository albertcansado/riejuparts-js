import React, { PureComponent, PropTypes } from 'react'
import { connect } from 'react-redux'
import { FormattedMessage } from 'react-intl'
import Select from 'react-select'

import { getEntityCards } from '../../../reducers/backend/entities'

import Link from '../../Link'
import Label from '../../Form/Label'

class ActionForm extends PureComponent {
    constructor(props, context) {
        super(props, context)

        this.state = {
            action: null
        }

        this._renderSelector = this._renderSelector.bind(this)
    }

    _renderSelector = () => {
        return (
            <div className="input input--select">
                <Label title="Select an action" />
                <Select
                    onChange={option => this.setState({ action: option.value })}
                    options={[
                        {
                            label: 'Add',
                            value: 'Add'
                        },
                        {
                            label: 'Delete',
                            value: 'Delete'
                        },
                        {
                            label: 'Replace',
                            value: 'Replace'
                        }
                    ]}
                />
            </div>
        )
    }

    _renderFormAdd = () => {
        const { cards } = this.props
        const optionsCards = cards.map(card => {
            return {
                label: card.name,
                value: card.id
            }
        })

        return (
            <div className="action-form-add">
                <h3>
                    <FormattedMessage id="actionform.add.title" defaultMessage="Add a part to model" />
                </h3>
                <div className="action-form__inner">
                    <div className="input input--select">
                        <Label title="Select a card" />
                        <Select
                            onChange={option => console.log(option)}
                            options={optionsCards}
                        />
                    </div>
                </div>
            </div>
        )
    }

    render = () => {
        const { action } = this.state
        const { handleClose } = this.props

        return (
            <div className="modal-card">
                <header className="modal-card__head">
                    <h2 className="modal-card__title">
                        <FormattedMessage id="actionform.title" defaultMessage="Add a modification" />
                    </h2>
                    <span className="close close--dark" onClick={handleClose}></span>
                </header>
                <section className="modal-card__body">
                    <div className="action-form">
                        {!action
                            ? this._renderSelector()
                            : this[`_renderForm${action}`]()
                        }
                    </div>
                </section>
                <footer className="modal-card__foot">
                    <Link btn type="success" onClick={this.handleSubmit}>
                        <FormattedMessage id="lineform.save" defaultMessage="Save" />
                    </Link>
                </footer>
            </div>
        )
    }
}

ActionForm.propTypes = {}
ActionForm.defaultProps = {}

const mapStateToProps = (state, ownProps) => {
    return {
        cards: getEntityCards(state, ownProps.model_id)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ActionForm)
