import React, { PropTypes } from 'react'
import { FormattedMessage } from 'react-intl'
import idx from 'idx'

import Link from '../../Link'
import Icon from '../../Icon'

const showObsolete = part => {
    if (!part.deleted) {
        return null
    }

    return (
        <div className="u-mgl--10 is-error txt-italic">
            (<FormattedMessage id="app.obsolete" defaultMessage="Obsolete" />)
        </div>
    )
}

const RemoveDisplay = ({ mod, onDelete }, context) => {
    const { target } = mod

    return (
        <div className="table__row">
            <div className="table__col table__col--1 table__col--20">
                {idx(target, _ => _.card.name[context.locale])}
            </div>
            <div className="table__col table__col--2 table__col--10">
                <span className="txt-bold is-error">
                    <FormattedMessage id="versions.part.delete" defaultMessage="Delete" />
                </span>
            </div>
            <div className="table__col table__col--3">
                <div className="u-flex">
                    <div>
                        <span className="txt-bold">{target.part.sku}</span> <span className="txt--small">{target.part.name}</span>
                    </div>
                    <div className="u-mgl--10">
                        <span className="txt--small u-mgr--5">
                            <FormattedMessage id="versions.parts.number" defaultMessage="Number" />:
                        </span>
                        <span className="txt-bold">
                            {target.number}
                        </span>
                    </div>
                </div>
                {showObsolete(target.part)}
            </div>
            <div className="table__col table__col--options table__col--10">
                <Link btn type="reset" onClick={() => onDelete(mod)}>
                    <Icon icon="trash" className="icon--error icon--20" />
                </Link>
            </div>
        </div>
    )
}

RemoveDisplay.contextTypes = {
    locale: PropTypes.string.isRequired
}

RemoveDisplay.propTypes = {
    mod: PropTypes.object.isRequired,
    onDelete: PropTypes.func.isRequired
}

export default RemoveDisplay
