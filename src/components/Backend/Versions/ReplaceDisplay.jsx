import React, { PropTypes } from 'react'
import { FormattedMessage } from 'react-intl'
import idx from 'idx'

import Link from '../../Link'
import Icon from '../../Icon'

const showObsolete = part => {
    if (!part.deleted) {
        return null
    }

    return (
        <div className="u-mgl--10 is-error txt-italic">
            (<FormattedMessage id="app.obsolete" defaultMessage="Obsolete" />)
        </div>
    )
}

const ReplaceDisplay = ({ mod, onDelete, onEdit }, context) => {
    const { target, part, extra } = mod

    return (
        <div className="table__row is-selectable" onClick={() => onEdit(mod)}>
            <div className="table__col table__col--1 table__col--20">
                {idx(target, _ => _.card.name[context.locale])}
            </div>
            <div className="table__col table__col--1 table__col--10">
                <span className="txt-bold is-notice">
                    <FormattedMessage id="versions.parts.replace" defaultMessage="Replace" />
                </span>
            </div>
            <div className="table__col table__col--1">

                <div className="txt-through txt--small u-flex">
                    <div>
                        <span className="txt-bold">{target.part.sku}</span> <span>{target.part.name}</span>
                    </div>
                    <div className="u-flex u-mgl--10">
                        <div>
                            <span className="u-mgr--5">
                                <FormattedMessage id="versions.parts.number" defaultMessage="Number" />:
                            </span>
                            <span className="txt-bold">
                                {target.number}
                            </span>
                        </div>
                    </div>
                    {showObsolete(target.part)}
                </div>

                <div className="u-flex">
                    <div>
                        <span className="txt-bold">{part.sku}</span> <span className="txt--small">{part.name}</span>
                    </div>
                    <div className="u-flex u-mgl--10">
                        <div>
                            <span className="txt--small u-mgr--5">
                                <FormattedMessage id="versions.parts.number" defaultMessage="Number" />:
                            </span>
                            <span className="txt-bold">
                                {extra.number}
                            </span>
                        </div>
                        <div className="u-mgl--10">
                            <span className="txt--small u-mgr--5">
                                <FormattedMessage id="versions.parts.qty" defaultMessage="Qty" />:
                            </span>
                            <span className="txt-bold">
                                {extra.qty}
                            </span>
                        </div>
                    </div>
                    {showObsolete(part)}
                </div>
            </div>
            <div className="table__col table__col--options table__col--10">
                <Link btn type="reset" onClick={ev => { ev.preventDefault(); onDelete(mod) }}>
                    <Icon icon="trash" className="icon--error icon--20" />
                </Link>
            </div>
        </div>
    )
}

ReplaceDisplay.contextTypes = {
    locale: PropTypes.string.isRequired
}

ReplaceDisplay.propTypes = {
    mod: PropTypes.object.isRequired,
    onDelete: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired
}

export default ReplaceDisplay
