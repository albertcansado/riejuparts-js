import React, { PropTypes } from 'react'
import cx from 'classnames'

import Link from '../../Link'
import Saving from 'components/Saving'

const Header = ({ title, isNew, isDraft, isSaving, handleSave, savingMessages }) => {
    return (
        <header className="box__header u-flex flex--middle flex--between">
            <h3>{title || '-'}</h3>
            <div className="u-flex flex--middle">
                <Saving
                    isSaving={isSaving}
                    isNotSaved={isDraft}
                    isSpecial={isNew}
                    messages={savingMessages}
                    styles={{
                        root: 'txt-bold u-mgr--15',
                        saving: 'is-notice',
                        saved: 'is-success',
                        notSaved: 'is-error'
                    }}
                />
                <Link
                    btn
                    type="success"
                    className={cx("warranty__submit", {
                        'is-disabled': !isDraft,
                        'btn--loading': isSaving
                    })}
                    onClick={isDraft && !isSaving ? handleSave : null}
                >Guardar</Link>
            </div>
        </header>
    )
}

Header.propTypes = {
    title: PropTypes.string,
    isNew: PropTypes.bool,
    isDraft: PropTypes.bool,
    isSaving: PropTypes.bool,
    handleSave: PropTypes.func.isRequired,
    savingMessages: PropTypes.object
}
Header.defaultProps = {
    title: null,
    isNew: false,
    isDraft: false,
    isSaving: false,
    savingMessages: {}
}

export default Header
