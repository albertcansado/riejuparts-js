import React, { PropTypes } from 'react'
import { isEqual as _isEqual } from 'lodash'
import { FormattedMessage } from 'react-intl'
import { DragSource, DropTarget } from 'react-dnd'
import cx from 'classnames'
import swal from 'sweetalert2'

import ItemTypes from '../../constants/backend/DnDTypes'

import Bubble from '../Bubble'
import Image from '../Image'


/* dnd Source */
const cardSource = {
    beginDrag(props) {
        return {
            id: props.card.id,
            index: props.index,
        }
    },
}

const cardTarget = {
    hover(props, monitor, component) {
        const dragIndex = monitor.getItem().index;
        const hoverIndex = props.index;

        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return;
        }

        // Time to actually perform the action
        props.moveCard(dragIndex, hoverIndex);

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        monitor.getItem().index = hoverIndex;
    }
}

/* Card methods */
const renderCover = card => {
    return (
        <figure className={cx("m-card__cover image", {'image--empty': !card.image})}>
            {card.image
                ? <Image src={card.image} className="card__image u-centered" />
                : <span className="image__placeholder txt-uppercase"><FormattedMessage id="card.noimage" defaultMessage="No image" /></span>
            }
        </figure>
    )
}

const onDelete = ({ card, messages, handleDelete }) => {
    swal({
        title: messages.cardDeleteTitle,
        text: messages.cardDeleteText,
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: messages.cardDeleteCancel,
        confirmButtonText: messages.cardDeleteConfirm
    }).then(() => {
        handleDelete(card)
    }).catch(swal.noop)
}

let Card = ({ card, onSelect, handleDelete, messages, handleEnable, isDragging, connectDragSource, connectDropTarget }, context) => {
    return connectDragSource(connectDropTarget(
        <article className={cx("card is-draggable", {'is-dragging': isDragging})}>
            <header className="card__header">
                <h5 className="card__header-title">{card.name[context.locale]}</h5>
                <div className="card__header-icon">
                    <Bubble type={card.active ? 'success' : 'error'} />
                </div>
            </header>
            <div className="card__content">
                {renderCover(card)}
            </div>
            <footer className="card__footer is-selectable">
                <span className="card__footer-item is-link" onClick={() => onSelect(card.id)}>
                    <FormattedMessage id="card.edit" defaultMessage="Edit" />
                </span>
                <span className={cx("card__footer-item", {'is-error': card.active, 'is-success': !card.active})} onClick={() => handleEnable(card)}>
                    {!card.active
                        ? <FormattedMessage id="card.enable" defaultMessage="Enable" />
                        : <FormattedMessage id="card.disable" defaultMessage="Disable" />
                    }
                </span>
                <span className="card__footer-item is-error txt-bold" onClick={() => onDelete({ card, messages, handleDelete })}>
                    <FormattedMessage id="card.delete" defaultMessage="Delete" />
                </span>
            </footer>
        </article>
    ))
}

Card.contextTypes = {
    locale: PropTypes.string.isRequired
}

Card.propTypes = {
    card: PropTypes.object.isRequired,
    onSelect: PropTypes.func.isRequired,
    handleDelete: PropTypes.func.isRequired,
    handleEnable: PropTypes.func.isRequired,
    messages: PropTypes.object.isRequired
}

Card = DropTarget(ItemTypes.CARD, cardTarget, connect => ({
    connectDropTarget: connect.dropTarget()
}))(Card)

Card = DragSource(ItemTypes.CARD, cardSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
}))(Card)

export default Card
