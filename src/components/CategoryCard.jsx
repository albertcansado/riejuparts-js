import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'

import Image from '../components/Image'

class CategoryCard extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {}
    }

    render = () => {
        const { item } = this.props
        return (
            <article className="card card--category">
                <Link to={item.url} className="card__link">
                    <header className="card__caption">
                        {item.image
                            ? <Image src={item.image} className="card__image" alt={item.name} />
                            : null
                        }
                    </header>
                    <div className="card__info">
                        <h2 className="card__title txt-center">{item.name}</h2>
                    </div>
                </Link>
            </article>
        )
    }
}

CategoryCard.propTypes = {
    item: PropTypes.object.isRequired
}

CategoryCard.defaultProps = {}

export default CategoryCard
