import intlWrapper from 'lib/intlWrapper'

import ManualsContainer from 'container/Backend/ManualsContainer'

const manualsDOM = document.querySelector('#manuals-root')
if (manualsDOM) {
    intlWrapper({
        element: manualsDOM,
        component: ManualsContainer,
        props: {
            fetchUrl: '/d/technical-zone/{id}.json',
            saveUrl: '/d/technical-zone'
        }
    })
}
